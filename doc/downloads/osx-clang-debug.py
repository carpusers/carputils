#!/usr/bin/env python
if __name__ == '__main__':
  import os, sys

  PETSC_DIR  = os.environ.get("PETSC_DIR")
  sys.path.insert(0, PETSC_DIR+'/config')
  import configure

  configure_options = ['--with-petsc-arch=osx-clang-debug2',
                       '--CC=clang',
                       '--CXX=clang++',
                       '--with-debugging=yes',
                       '--download-mpich=yes',
                       '--download-ml=yes',
                       '--download-fblaslapack=yes',
                       '--download-suitesparse=yes',
                       '--download-superlu_dist=yes',
                       '--download-superlu=yes',
                       '--download-hypre=yes',
                       '--download-mumps=yes',
                       '--download-scalapack=yes',
                       '--download-blacs=yes',
                       '--download-hdf5=yes',
                       '--download-metis=yes',
                       '--download-parmetis=yes',
                       '--download-pastix=yes',
                       '--download-sowing=yes',
                       '--download-boost=yes',
                       '--download-ptscotch=yes',
                       '--download-sundials=yes',
                       '--with-shared-libraries=yes']
  configure.petsc_configure(configure_options)

#reconfigure
#./$PETSC_ARCH/lib/petsc/conf/reconfigure-$PETSC_ARCH.py


