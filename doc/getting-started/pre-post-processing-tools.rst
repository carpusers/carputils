.. _pre-post-processing-tools:

******************************
Pre- and Post-Processing Tools
******************************

In addition to using CARPentry directly for pre- and post-processing
a number of specialized standalone tools is also available for specific purposes. 
The most widely used ones are summarized below.


.. include:: mesher.rst
.. include:: meshtool.rst


.. _igbutils:

igbutils
========

For all tools, help is output with the *-h* option.


.. _igbhead:

igbhead
-------

Operations to perform on the headers of IGB files.
Most commonly, it it used to print the header information
but can also be usd to modify it.
This utility can also be used to change the storage type of the file,
or put an IGB header on to ASCII and binary data file.

.. _igbapd:

igbapd
------

A simple utility to compute action potential durations.


.. _igbops:

igbops
------

This utility can perform basic math operations on one or two IGB files.
There are preset functions as well as a parser for arbitrary functions.
For example, one can difference two IGB files, find maxima over time, or
apply a box filter.


.. _igbextract:

igbextract
----------

This is a tool to extract a hyperslab of data from an IGB file.
The format of the output can be chosen between ASCII, binary or IGB.


.. _igbdft:

igbdft
------

This is a tool for simple frequency domain techniques.
It contains a limited number of operations which include Butterworth
filtering, DFTs, and computing phase.


FEM tools
=========


Tools recognize the *-h* to display detailed help.


.. _GlElementCenters:

GlElemCenters
-------------

Compute the element centers of a nodally defined mesh


.. _GlFilament:

GlFilament
----------

Compute phase singularities and filaments.


.. _GlGradient:

GlGradient
----------

Compute gradients of grid data and conduction velocities if activation times are used.


.. _GlInterpolate:

GlInterpolate
-------------

Interpolate data restitution-protocol-definition, from nodes to element centers or vice versa.


.. _GlRuleFibers:

GlRuleFibers
------------

Compute the fibres for a biventricular mesh.


.. _GlTransfer:

GlTransfer
----------

Transfer data between spatially overlapping grids.
