.. _model-component-classes:

***********************
Model Component Classes
***********************

The :mod:`carputils.model` subpackage provides an array of Python classes for
the set of CARP simulations in an object-oriented manner. Instead of manually
generating long lists of command line options such as::

    opts = ['-num_imp_regions',        1,
            '-imp_region[0].im',       'TT2',
            '-imp_region[0].num_IDs',  2,
            '-imp_region[0].ID[0]',    1,
            '-imp_region[0].ID[1]',    2,
            '-imp_region[0].im_param', 'GNa=14']

you can represent the model component with a class instance::

    from carputils.model.ionic import TT2IonicModel
    imp = TT2IonicModel([1, 2], GNa=14)

and generate the option list with a single function call::

    from carputils.model import optionlist
    opts = optionlist([imp])

Any number of model components of different types can be generated in a single
:func:`~carputils.model.optionlist` call, providing flexibility of use in run script logic::

    from carputils import model

    imp = [model.ionic.TT2IonicModel([1, 2]),
           model.ionic.TT2IonicModel([3, 4], GNa=14)]
    cond = [model.ConductivityRegion([1, 2], g_it=1.5)]

    opts = model.optionlist(imp + cond)

Classes check that the parameters passed to them are valid, ensuring that any
mistakes due to typographical errors etc. are caught before execution of the
CARP executable.

Detail on the usage of the various model class types is given below.

Conductivity Regions
====================

Conductivity regions, defined by the class
:class:`carputils.model.ConductivityRegion`, or its alias
:class:`carputils.model.GRegion`, take a list of material IDs, an optional name
for the region, and the intra- and extracellular conductivity for the tissue.
See the :class:`class documentation<carputils.model.ConductivityRegion>` for
the specific parameter names. See also the classmethods for easy definition of
passive and isotropic conductivity regions.

Example::

    from carputils import model
    # Tags 1 and 2, default conductivities
    cr1 = model.ConductivityRegion([1, 2], 'myo')
    # Tag 3, increased intracellular longitudinal conductivity
    cr2 = model.ConductivityRegion([3], 'fast', g_il=0.2)
    # Tag 4, isotropic conductivity
    cr3 = model.ConductivityRegion.isotropic([4], 'iso', cond=0.1)

Eikonal Regions
===============

Eikonal regions are defined by the class :class:`carputils.model.EikonalRegion`
or its alias :class:`carputils.model.EkRegion`. Unlike other classes, it
accepts only a single ID, which is actually the index of an existing
conductivity region. Like conductivity regions, it also has
:meth:`~carputils.model.EikonalRegion.isotropic` and
:meth:`~carputils.model.EikonalRegion.passive` class method alternate
constructors.

Examples::

    from carputils import model
    # Conductivity region 0, default conduction velocity
    ek1 = model.EikonalRegion(0, 'myo')
    # Conductivity region 2, isotropic conduction velocity
    ek2 = model.EikonalRegion.isotropic(2, 'iso', vel=0.3)

Electrical Stimuli
==================

The class :class:`carputils.model.Stimulus` defines a model stimulus. The user
is expected to provide the relevant keyword arguments when using the class 
directly, for example to define a stimulus with `'somefile'` as the vertex
file::

    from carputils import model
    stim = model.Stimulus('pacing', vtk_file='somefile')

The keyword arguments (like `vtk_file` above) correspond with the fields of the
CARP command line structure (`-stimulus[0].vtk_file` in this case).

Additional class methods exist for conveniently setting up a 'forced foot'
stimulus, whereby the activation times from an Eikonal simulation are used to
drive a reaction-Eikonal simulation of electrophysiology. See the
:class:`class documentation<carputils.model.Stimulus>` for more information.

Ionic Models
============

Like the conductivity regions, ionic models also take `IDs` and `name`
arguments, however each different model takes its own set of optional
parameters. The different available models, their allowed arguments and their
default parameters can be found in the documentation of the
:mod:`carputils.model.ionic` module.

Example::

    from carputils import model
    imp = model.ionic.TT2IonicModel([1, 2], 'myo', GNa=14)

Active Tension Models
=====================

The active tension models are a special case in that they are not models in
their own right, but are actually plugins to the ionic models. Active tension
models, which only take the model parameters as command line arguments, are
assigned to an existing ionic model using its `set_stress` method::

    from carputils import model
    # Create an ionic model
    imp = model.ionic.TT2IonicModel([1])
    # Create an active tension model
    act = model.activetension.TanhStress(Tpeak=150)
    # Assign the active tension model to the ionic model
    imp.set_stress(act)

Available active tension models and their allowed parameters can be found in
:mod:`carputils.model.activetension`.

Mechanics Material Models
=========================

Like ionic models, mechanics material models take `IDs`, `name` and model
parameters as their arguments. Available models and their allowed parameters
are detailed in :mod:`carputils.model.mechanics`.

Example::

    from carputils import model
    mat = model.mechanics.NeoHookeanMaterial([1, 2], c=50)
