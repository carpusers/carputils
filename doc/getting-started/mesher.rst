.. _mesher:


Mesh generation (mesher)
========================

.. sectionauthor:: |GPlank| and |ANeic|


Introduction
------------
``mesher`` is a program for generating **simple** regular FE meshes. It can also produce element tag regions
and fiber definitions during element generation.


Usage
-----
The most important parameters of ``mesher`` are

- size : Set the size of the mesh in each axis direction.
- resolution : Set the resolution of the mesh in each axis direction.
- bath : Set the bath size in each axis direction. Negative sizes denote a bath on both sides.
- fibers : Set fiber rotation.
- mesh : Set output mesh name.

The unit of the size and resolution parameters is centimeters, while the unit of the resolution
is set in micrometers. 

Fiber orientation is assigned based on following assumptions:

- The *negative* z axis is the transmural direction.
- The *positive* y axis is the apico-basal direction.
- The *positive* x axis is the circumferential direction.


.. figure:: /images/Mesher_wedge_layout.png
    :name: fig-mesher-wedge
    :scale: 65 %
    :align: center

    Convention for the wedge orientation.


Example
-------
The following example creates a myocardial slab of dimensions 0.05 x 0.6 x 0.3 cm immersed in a bath of 0.02 cm on each side:

.. code-block:: bash

    $ bin/mesher \\
    -size[0] 0.05 \\
    -size[1] 0.6 \\
    -size[2] 0.3 \\
    -bath[0] -0.02 \\
    -bath[1] -0.02 \\
    -bath[2] -0.02 \\
    -resolution[0] 100.0 \\
    -resolution[1] 100.0 \\
    -resolution[2] 100.0 \\
    -mesh block

By default, the geometry's center is located at (0,0,0). As specified, the bounding box of the
intracellular domain is

.. code-block:: bash

    Bbox:
    x: (      -250.00 ,       250.00 ), delta 500.00
    y: (     -3000.00 ,      3000.00 ), delta 6000.00
    z: (     -1500.00 ,      1500.00 ), delta 3000.00

while the bounding box of the overall mesh is

.. code-block:: bash

    Bbox:
    x: (      -450.00 ,       450.00 ), delta 900.00
    y: (     -3200.00 ,      3200.00 ), delta 6400.00
    z: (     -1700.00 ,      1700.00 ), delta 3400.00

Rule-based fibers can be added with the parameters in the ``fibers`` parameter structure. In the previous
example, fiber rotation can be added with:

.. code-block:: bash

    -fibers.rotEndo 60 \\
    -fibers.rotEpi -60 \\
    -fibers.sheetEndo 90 \\
    -fibers.sheetEpi  90 \\

The resulting geometry is:

.. figure:: /images/Mesher_block.png
    :scale: 65 %
    :name: fig-mesher-block

    A slab mesh produced by mesher.


Tag regions
-----------
By default, element tag region 0 is assigned to the bath and region 1 is assigned to the myocardium.
Additional regions can be assigned via ``regdef`` parameters. A ``regdef`` parameter defines a geometric
shape. Elements inside this shape are assigned a given element tag.

The supported region definition shapes are:

.. code-block:: bash

    $ mesher +Help regdef[0].type
    
    regdef[0].type:
    	how to define region
    
    	type:	Int
    	default:(Int)(0)
    	menu: {
    		(Int)(2)	cylinder
    		(Int)(1)	sphere
    		(Int)(0)	block
    	}

The following example shows a minimal definition of a block region:

.. code-block:: bash

    -numRegions      1 \\
    -regdeg[0].type  0 \\
    -regdef[0].tag   TAG \\
    -regdef[0].p0[0] P0X \\
    -regdef[0].p0[1] P0Y \\
    -regdef[0].p0[2] P0Z \\
    -regdef[0].p1[0] P1X \\
    -regdef[0].p1[1] P1Y \\
    -regdef[0].p1[2] P1Z
