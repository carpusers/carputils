.. _running-a-simulation:

##########################################
Running A Simulation In **carputils mode**
##########################################

.. include:: carputils-overview.rst
.. include:: carputils-first-run.rst
.. include:: writing-examples.rst
.. include:: carputils-mesh-class.rst
.. include:: carputils-model-component-classes.rst


######################################
Running A Simulation In **plain mode**
######################################

.. include:: carpentry-plain-mode.rst