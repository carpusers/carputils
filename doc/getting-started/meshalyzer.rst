.. _meshalyzer:

Meshalyzer
==========

**Meshalyzer** is a program for displaying unstructured grids, visualizing data on the
grid, as well as examining the structure of the grid.
It is an ever changing and ongoing project that has been in development since 2001.


Installation
------------

**Meshalyzer** uses the Fast Light Tool Kit ("FLTK") to provide modern GUI functionality.
It supports 3D graphics via OpenGL :sup:`TM` and its build-in GLUT emulation.
Before getting the software from a GitHub repository, a version **1.3.x** of FLTK
needs to be provided on your system by either installing it through the package
manager or compiling it from the sources:

.. code-block:: bash

    cd <installation-directory>
    wget http://fltk.org/pub/fltk/1.3.4/fltk-1.3.4-1-source.tar.gz
    tar -xzvf fltk-1.3.4-1-source.tar.gz
    cd fltk-1.3.4-1
    ./configure --enable-xft --enable-shared
    make
    sudo make install
    cd ..

.. note::

    FLTK has some additional dependencies in order to be successfully compiled.
    A list of typically missing packages is given here. Be aware that the package
    names could slightly deviate across common operating systems.

    + libpng-dev
    + libjpeg-turbo-dev
    + freeglut3-dev
    + libXft-devel
    + [ libpthread-stubs0-dev ]

Hence, get **Meshalyzer** from the freely available GitHub repository and finish
your installation.

.. code-block:: bash

    cd <installation-directory>
    git clone https://github.com/cardiosolv/meshalyzer
    cd meshalyzer
    make

Input Files
-----------

**Meshalyzer** assumes that there is a model, defined by a set of vertices,
which is to be displayed.
Beyond that, everything is optional.
The vertices may be connected to form structures, specifically
cables, triangular elements, quadrilaterals, tetrahedra, hexahedra, prisms, 
or simple line segments.
Scalar data may be associated with every point on the grid and 
may be used to colour
any of the connected structures.
An auxiliary set of points may also be defined on which to display vector data,

Furthermore, the points may be divided into different 
regions which may have their
attributes independently manipulated.
Regions are delimited by tagged volume elements (see \S\ref{sec:voleles}),
or may be specified by
a range of vertices (see~\S\ref{sec:regfile}),
As well, all the structures formed by those vertices are part of the
region.
Vertices may belong to more than one region,
and there is at least one region in a grid.
If a region is not specified, all structures belong to the default region.
If there is a element direction file (*.lon* file), and no 
volume elements are tagged, elements with no direction will be region 0, 
and other elements will be region 1.

Surfaces may be formed from collections of two-dimensional elements.
Surfaces are defined by surface element files (see \S\ref{sec:surffile}).
Also, surfaces are defined if there are any 2D elements in an *elem*
file (see \S\ref{sec:elem}). 
In the latter case, 
the 2D elements are placed in different surfaces based on their region number.
Surfaces are independent of regions, and the relationship between the points
and model structures is described in the :numref:`fig-UML`.

.. figure:: /images/UML.png
   :align: center
   :width: 75%

   Relationship between vertices (Points) and other structures.}


All files (except :ref:`IGB files <igb-file>`) are ASCII files.
They may be *gzipped*  in which case ``.gz`` should be appended to the extensions given below.
Note that files are scanned line by line 
and that extra information on each line is ignored by **meshalyzer**.

Vertices
^^^^^^^^

The file defining vertices in the model is the only mandatory file.
It must have the extension ``.pts`` following the carpentry format for :ref:`nodal files <node-file>`.
The base name of this file, i.e., without the extension, is
used as the base name for cables, connections, triangles,
tetrahedra files, and shells files.   
