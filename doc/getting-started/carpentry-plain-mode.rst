.. _carpentry:

************
Using |carp|
************

In this section the basic usage of the main executable |carp| is explained.
More detailed background referring to the individual physics :ref:`electrophysiology <carpentry-ep>`,
:ref:`elasticity <carpentry_mech>` and :ref:`hemodynamics <carpentry_fluid>` are given below in the respective sections.


Getting Help
============

As in any real *in vivo* or *ex vivo* experiment there is a large number of parameters
that have to be set and tracked.
In *in silico* experiments tend to be even higher
since not only the measurement setup must be configured, but also the organ itself.
The *general purpose* design of carpentry renders feasible the configuration of a wide range of experimental conditions.
Consequently, the number of supported parameters is large, depending on the specific flavor
on the order of a few hundreds of parameters that can be used for experimental descriptions.
With such a larger number of parameters it is therefore key to be able to retrieve easily more detailed explanations
of meaning and intended use of all parameters.

A list of all available command line parameters is obtained by

.. code-block:: bash

   carpentry +Help

   carpentry [+Default | +F file | +Help topic | +Doc] [+Save file]
   Default allocation: Global

   Parameters:
            -iout_idx_file RFile
            -eout_idx_file RFile
            -output_level Int
            -num_emission_dbc Int
            -num_illum_dbc Int
            -CV_coupling Short
            -num_cavities Int
            -num_mechanic_nbc Int
            -num_mechanic_dbc Int
            -mesh_statistics Flag
            -save_permute Flag
            -save_mapping Flag
            -mapping_mode Short
            ...
            -rt_lib String
            '-external_imp[Int]' String
            -external_imp '{ num_external_imp x String }'
            -num_external_imp Int


and help for a specific feature is queried by

.. code-block:: bash

   carpentry +Help stimulus[0].strength

   stimulus[0].strength:
           stimulus strength

           type:   Float
           default:(Float)(0.)
           units:  uA/cm^2(2D current), uA/cm^3(3D current), or mV
           Depends on: {
                   stimulus[PrMelem1]
           }

More verbose explanation of command line parameters is obtained by using the ``+Doc`` parameter
which prints the same output as ``+Help``, but provides in addition more details on each individual parameter:

.. code-block:: bash

   carpentry +Doc

   iout_idx_file:
           path to file containing intracellular indices to output

           type:   RFile
           default:(RFile)("")


   eout_idx_file:
           path to file containing extracellular indices to output

           type:   RFile
           default:(RFile)("")


   output_level:
           Level of terminal output; 0 is minimal output

           type:   Int
           default:(Int)(1)
           min:    (Int)(0)
           max:    (Int)(10)


   num_emission_dbc:
           Number of optical Dirichlet boundary conditions

           type:   Int
           default:(Int)(0)
           min:    (Int)(0)
           max:    (Int)(2)
           Changes the allocation of: {
                   emission_dbc
                   emission_dbc[PrMelem1].bctype
                   emission_dbc[PrMelem1].geometry
                   emission_dbc[PrMelem1].ctr_def
                   emission_dbc[PrMelem1].zd
                   emission_dbc[PrMelem1].yd
                   emission_dbc[PrMelem1].xd
                   emission_dbc[PrMelem1].z0
                   emission_dbc[PrMelem1].y0
                   emission_dbc[PrMelem1].x0
                   emission_dbc[PrMelem1].strength
                   emission_dbc[PrMelem1].dump_nodes
                   emission_dbc[PrMelem1].vtx_file
                   emission_dbc[PrMelem1].name
           }
           Influences the value of: {
                   emission_dbc
           }


Details on the executable and the main features it was compiled with are queried by

.. code-block:: bash

   carpentry -revision

   svn revision:             2974
   svn path:                 https://carpentry.medunigraz.at/carp-dcse-pt/branches/mechanics/CARP
   dependency svn revisions: PT_C=338,elasticity=336,eikonal=111



Basic Experiment Definition
---------------------------

Various types of experiments are supported by :ref:`carpentry <carpentry>`.
A list of experimental modes is obtained by

.. code-block:: bash

   carpentry +Help experiment

yielding the following list:


.. _tab-carpentry-experiments:

.. table:: Available experiment options to select simulation action

   +-------+-------------------------------------------------------+
   | Value | Description                                           |
   +=======+=======================================================+
   | 0     | **NORMAL RUN** (default)                              |
   +-------+-------------------------------------------------------+
   | 1     | Output FEM matrices only                              |
   +-------+-------------------------------------------------------+
   | 2     | Laplace solve                                         |
   +-------+-------------------------------------------------------+
   | 3     | Build model only                                      |
   +-------+-------------------------------------------------------+
   | 4     | Post-processing only                                  |
   +-------+-------------------------------------------------------+
   | 5     | Unloading (compute stress-free reference configation) |
   +-------+-------------------------------------------------------+
   | 6     | Eikonal solve (compute activation sequence only)      |
   +-------+-------------------------------------------------------+
   | 7     | Apply deformation                                     |
   +-------+-------------------------------------------------------+
   | 8     | memory test (obsolete?)                               |
   +-------+-------------------------------------------------------+

A representation of the tissue or organ to be modelled must be provided
in the form of a finite element mesh
that adheres to the primitive :ref:`carpentry mesh format <file-formats>`.
The duration of activity to be simulated is chosen by `-tend`
which is specified in milli-seconds.
Specifying these minimum number of parameters already suffices
to launch a basic simulation

.. code-block:: bash

   carpentry -meshname slab -tend 100

Due to the lack of any electrical stimulus this experiment only models electrophysiology in quiescent tissue
and, in most cases, such an experiment will be of very limited interest.
However, :ref:`carpentry <carpentry>` is designed based on the notion
that any user should be able to run a simple simulation by providing a minimum of parameters.
This was achieved by using the input parser :ref:`prm <PrM>`
through which reasonable default settings for all parameters must be provided
and inter-dependency of parameters and potential inconsistencies are caught immediately
when launching a simulation.
This concept makes it straight forward to define an initial experiment easily
and implement then step-by-step incremental refinements
until the desired experimental conditions are all matched.


Picking a Model and Discretization Scheme
-----------------------------------------

.. code-block:: bash

   carpentry +Help bidomain

# explain operator splitting, mass lumping,

The time stepping for the parabolic PDE is chosen with `-parab_solv`.
There are three options available.


Picking a Solver
----------------

The carpentry solver infrastructure relies largely on preconditioners and solvers
made available through PETSc. However, in many cases there are alternative solvers available
implemented in the parallel toolbox (PT).
Whenever PT alternatives are available we typically prefer PT for several reasons:

* Algorithms underlying PT are developed by our collaborators, thus it is easier to ask for
  specific modifications/improvements, if needed
* PT is optimized for strong scalability, in many cases we achieve better performance
  with lightweight PT solvers then with PETSc solvers. In general, PT needs more iteration,
  but each iteration is cheaper.
* PT is fully GPU enabled.

Switching between PETSc/PT solvers is achieved with


PETSc solvers can be configured by feeding in option files at the command line.
These option files are passed in using the following command line arguments

+-------------------------------------+
| ellip_options_file                  +
| parab_options_file                  +
| purk_options_file                   +
+-------------------------------------+

There are three options available.

After selecting a solver, for each physical problem convergence criteria are chosen
and the maximum number of allowed iterations can be defined.


Configuring IO
--------------
TBA



Post-Processing Mode
====================

Quantities appearing directly as variables in the equations being solved are
directly solved for during a simulation.
Other quantities which are indirectly derived from the primal variables
can also be computed on the fly during a simulation,
or, alternatively, subsequent to a simulation in a post-processing steps.
On the fly computation is often warranted for derived quantities such as the
instant of activation when high temporal resolution is important.
For many derived parameters this may not be the case and it may be more convenient then
to compute these quantities in a post-processing step from the output data.
The computation of such auxiliary quantities such as, for instance, the :ref:`electrocardiogram <ecg>`
is available as postprocessing options.
The post-processing mode is triggered by selecting

.. code-block:: bash

   carpentry -experiment 4 -simID experiment_S1_2_3

In this case, the simulation run is not repeated,
but the output of the experiment specified with the ``simID`` option is read in
and the sought after quantities are computed.
Output of data computed in post-processing mode is written to a subdirectory
of the original simulation output directory.
The data files in the output directory of the simulations are opened for reading,
but other than that are left unchanged.
A directory name can be passed in with ``ppID``. If no directory name is specified
the default name ``POST_PROC`` will be used.
