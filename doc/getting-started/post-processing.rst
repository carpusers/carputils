:orphan:

.. _carpentry-post-processing:

***************
Post-processing
***************

Quantities appearing directly as variables in the equations being solved are
directly solved for during a simulation.
Other quantities which are indirectly derived from the primal variables 
can also be computed on the fly during a simulation,
or, alternatively, subsequent to a simulation in a post-processing steps.
On the fly computation is often warranted for derived quantities such as the 
instant of activation when high temporal resolution is important.
For many derived parameters this may not be the case and it may be more convenient then
to compute these quantities in a post-processing step from the output data.
The computation of such auxiliary quantities such as, for instance, the :ref:`electrocardiogram <ecg>`
is available as postprocessing options. 
The post-processing mode is triggered by selecting 

.. code-block:: bash

   carpentry -experiment 4 -simID experiment_S1_2_3

In this case, the simulation run is not repeated, 
but the output of the experiment specified with the ``simID`` option is read in 
and the sought after quantities are computed. 
Output of data computed in post-processing mode is written to a subdirectory
of the original simulation output directory. 
The data files in the output directory of the simulations are opened for reading, 
but other than that are left unchanged.
A directory name can be passed in with ``ppID``. If no directory name is specified
the default name ``POST_PROC`` will be used.
