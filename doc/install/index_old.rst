:orphan:

.. _carpentry-installation:

********************************
|carp| **- The Simulation Core**
********************************

.. To install |carp|, you must first create an account with our |carp| community: :ref:`Create Account <create-account>`.
.. A basic knowledge of unix-based commands is also required.

Versions and OS Compatibility
=============================

The CARPentry Modelling Environment (CME) is currently available and supported
for both Mac OS X and Linux. It has also been used on Windows 10 with Windows Subsystem for Linux (WSL).

There are currently two versions of CME:

* **Free version** with electrophysiology support on up to 16 cores [`Linux download <https://carp.medunigraz.at/carpentry/cme.linux.tar.gz>`_ , `Mac OS X download <https://carp.medunigraz.at/carpentry/cme.free.osx.tar.gz>`_]. Available for all public use by researchers.

* **Full version** with support of electrophysiology, mechanics, and fluids. No limitation on compute cores. The full version is only available to specific collaborators and partners.

System specific installation instructions are detailed for :ref:`Mac OS X <macosx-install>` and :ref:`Linux <linux-install>`.
``Please note that a source installation is no longer available.``

<!--
.. include:: carputils-basic-install_slim.rst

|br|
|br|
-->

**************
**meshalyzer**
**************

You will also need to install meshalyzer (which depends on fltk) if you want to
properly view the tutorial outputs.

.. code-block:: bash

  # installation of FLTK
  cd $HOME/install
  wget  http://fltk.org/pub/fltk/1.3.4/fltk-1.3.4-1-source.tar.gz
  tar -xzvf fltk-1.3.4-1-source.tar.gz

  cd fltk-1.3.4-1
  ./configure --enable-xft --enable-shared
  make
  sudo make install

.. code-block:: bash

  # installation of Meshalyzer
  cd $HOME/install
  git clone https://github.com/cardiosolv/meshalyzer.git
  cd meshalyzer
  make
