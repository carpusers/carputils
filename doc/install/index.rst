:orphan:

.. _carpentry-installation:

********************************
|carp| **- The Simulation Core**
********************************

.. To install |carp|, you must first create an account with our |carp| community: :ref:`Create Account <create-account>`.
.. A basic knowledge of unix-based commands is also required.

The CARPentry Modelling Environment (CME) is currently available and supported
for both Mac OS X and Linux. It has also been used on Windows 10 with Windows Subsystem for Linux (WSL). CME comes with electrophysiology support on up to 16 cores and is available for public use by researchers with citation. To install, please download the relevant CME package [`Linux download <https://carp.medunigraz.at/carpentry/cme.linux.tar.gz>`_ , `Mac OS X download <https://carp.medunigraz.at/carpentry/cme.free.osx.tar.gz>`_] and follow the system specific installation instructions detailed below.

``The source CARP with support of electrophysiology, mechanics, and fluids with no limitation on compute cores is only available to specific collaborators and partners.``


OSX
=============
====

.. include:: macosx.rst


Linux
============
====

.. include:: linux.rst


Testing
==========
====

After reloading your shell environment, please test that the following binaries are executable outside the CME bin directory.

* bench
* carpentry
* meshtool
* meshalyzer
