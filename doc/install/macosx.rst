.. _macosx:

-------------------
System Requirements
-------------------
Installations have only been tested on OS X 10.11 to 10.13. General system requirements include:

* homebrew

.. code-block:: bash

    /usr/bin/ruby -e "$(curl -fsFL https://raw.githubusercontent.com/Homebrew/install/master/install)"

* Xcode: download from App Store. If working with an older OSX, Xcode can be downloaded from `apple developer <https://developer.apple.com/xcode/>`_.

.. code-block:: bash

    xcode-select --install

------------
Installation
------------

The CME Mac OSX package is a tarball consisting of:

* CME Mac OSX binaries
* Installer script: **install.sh**
* Empty settings file: **settings.yaml**

Within a terminal window, please create an installation directory ($INSTALL_DIR). For example, a suggested location is $INSTALL_DIR=$HOME/install/cme. Then move the cme.free.osx.tar.gz to the install directory and unpackage it:

.. code-block:: bash

    mkdir $INSTALL_DIR
    cd $INSTALL_DIR
    cp ~/Downloads/cme.free.osx.tar.gz .
    tar -xvf cme.free.osx.tar.gz

Navigate into cme.free.osx directory and run the installation script.

.. code-block:: bash

    cd $INSTALL_DIR/cme.free.osx
    ./install.sh

If not already installed, the install script will install and update necessary dependencies including wget and libpng. Throughout installation, input is required:

* root passwoord for system linking of fltk (dependency for meshalyzer)
* Permission for manual configuration of environment in bashrc file (~/.bashrc)

After completion, the shell environment needs to be reloaded. Please test the installation using the testing instructions below. If you experience troubles with the installation, please send the build.log to Christoph. 
