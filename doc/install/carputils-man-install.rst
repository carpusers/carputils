.. _bitbucket-ssh-keys: https://bitbucket.org/account/ssh-keys/
.. _carputils-basic-install:

************************************
Carputils **- The Python Framework**
************************************
Installation of the CARP examples requires retrieving several git repositories,
editing an environment variable and editing a settings file.

Git Sources
===========
First, you will need to download the sources hosted at https://bitbucket.org/carpusers/carputils


.. _carputils-manual-installation:

Manual Installation
===================
First, clone this repository to your system, in a location of your choice:

.. code-block:: bash

    cd ~/software
    git clone git@bitbucket.org:carpusers/carputils.git

Create a directory for examples, and clone the example repo(s) there:

.. code-block:: bash

    mkdir ~/software/carp-examples
    cd ~/software/carp-examples
    git clone git@bitbucket.org:carpusers/tutorials.git
    git clone git@bitbucket.org:carpusers/shellfun.git
    git clone git@bitbucket.org:carpusers/limpetgui.git
    

Add both the ``carputils`` repo and the directory you made for examples to your
PYTHONPATH in ``.bashrc``:

.. code-block:: bash

    export PYTHONPATH="$PYTHONPATH:$HOME/software/carputils:$HOME/software/carp-examples"

Finally, use the ``cusettings`` executable in ``carputils/bin`` to generate a
carputils settings file. This can be placed in one of:

* A location specified by the ``CARPUTILS_SETTINGS`` environment variable
* ``./settings.yaml`` (i.e. in the current working directory)
* ``~/.config/carputils/settings.yaml``
* In the root of the ``carputils`` git repository, called ``settings.yaml``

The order above is also the order in which the settings are searched for, so
setting a settings file path with the environment variable will override all
the others, for example. The third option, under ``~/.config``, is recommended
in most cases. To generate the settings file there run:

.. code-block:: bash

    cusettings ~/.config/carputils/settings.yaml

You will finally need to modify this file so that the paths match your system
installation. Note that in yaml indentation has meaning, so take care when
editing!

It would not be entirely unexpected if you run into version issues with python.
If this is the case, you'll find the ``auto-pip`` shell script in 
``shellfun/updating``. Run this script once to upgrade all relevant packages 
to a sufficiently recent revision.
It is recommended to add or upgrade Python packages in your local userspace by calling 
the script

.. code-block:: bash

   ./auto_pip

.. warning:: In some cases, the previous approach to directly update Python's system packages has produced system inconsistencies and is not recommended anymore.
