
.. _linux-install:


------------------
System Requirments
------------------

All linux distributions are currently supported. The requirements for building PETSc and MPICH are:

* C and Fortran compilers
* cmake
* git

Please install using your specific system’s package management software.

------------
Installation
------------

The Linux CME releases are provided as tarballs consisting of:

* CME binaries
* Carp-examples: tutorials and carputils
* MPICH MPI sources
* PETSc sources
* Installer script: **install-linux.sh**

The CME binaries have been compiled using PETSc (with external packages) and MPICH MPI.
The provided installer script can be used to build PETSc and MPI and install the
CME binaries. When installing multiple releases, PETSc and MPI do not need to be installed multiple times. Instead, the associated setup steps can be
skipped.

Then, the installer script should be executed. After completion, the shell environment needs to be reloaded. Please test the installation using the instructions below.
