#!/usr/bin/env python

"""
Generate documentation about the example repositories in doctests.
"""
import os
import sys
import json
import argparse
from glob import glob
from warnings import warn
from carputils import testing

# provide python2 and python3 compatibility
if sys.version_info.major > 2:
    from itertools import zip_longest as _izip_longest
    isPy2 = False
    xrange = range
else:
    from itertools import izip_longest as _izip_longest
    isPy2 = True


def format_heading(level, text):
    underlining = ['*','=', '-', '^', '"'][level - 1] * len(text)
    if level == 1:
        # just needed for H1
        overlining = ['*', '=', '-', '^', '"'][level - 1] * len(text)
        header = '{}\n{}\n{}\n\n'.format(overlining, text, underlining)
    else:
        header = '{}\n{}\n\n'.format(text, underlining)
    return header

def runinfo(result):
    """
    Generate a string summarising the CARP build used and other factors.
    """
    dep_revs = []
    tpl = '{}, revision {}, dependency revisions {{{}}}'

    # collect previous runs (code under svn control)
    if 'dep_svn_revisions' in dir(result):
        if result.dep_svn_revisions is not None:
            for dep in sorted(result.dep_svn_revisions):
                dep_revs.append('{}: {}'.format(dep, result.dep_svn_revisions[dep]))

        info = tpl.format(result.timestamp, result.dep_svn_revisions, ','.join(dep_revs))

    # collect previous runs (code under any vcs)
    if 'dep_revisions' in dir(result):
        if result.dep_revisions is not None:
            for dep in sorted(result.dep_revisions):
                dep_revs.append('{}: {}'.format(dep, result.dep_revisions[dep]))

        info = tpl.format(result.timestamp, result.revision, ','.join(dep_revs))

    return info

class History(object):
    """
    Load the testing history on demand from a directory.
    """

    MISSING_RESULTS = 1

    def __init__(self, directory):
        
        pattern = os.path.join(directory, '*-*-*_*-*-*',
                               'test_suite_results.json')

        # Store files names in reverse order
        self._files = sorted(glob(pattern))[::-1]
        self._data  = []

    def __iter__(self):
        """
        Iterate, in reverse time order, over the results in the history
        """
        for fname, dat in _izip_longest(self._files, self._data):
            if dat is None:
                # Yet to be loaded
                dat = self._load_next(fname)
            if dat == self.MISSING_RESULTS:
                continue
            else:
                yield dat

    def _load_next(self, filename):
        """
        Load the next file in the history, store and return it
        """
        with open(filename) as fp:
            try:
                data = json.load(fp)
            except ValueError:
                warn('Could not load JSON file {}'.format(filename))
                results = self.MISSING_RESULTS
            else:
                results = testing.TestSuiteResults.deserialise(data)
        self._data.append(results)
        return results

    def latest(self):
        """
        Return the most recent results in the history. Ignore empty directories.
        """
        idx = 0
        if len(self._data) == 0:
            results = self._load_next(self._files[idx])
            while results == self.MISSING_RESULTS:
                idx += 1
                self._data.pop() # remove history entries showing no results file
                results = self._load_next(self._files[idx])
        return self._data[0]

    def last_passed(self, test):
        """
        Search back through the history for the last time the test passed.
        """
        for results in self:
            try:
                res = results.search(test)
            except ValueError:
                continue

            if res.passed():
                return runinfo(res)

        # No passes in history
        msg = 'No passes found in history (earliest {})'
        return msg.format(res.timestamp)

_progress_length = 0

def progress(string):
    """
    Print a self-overwriting progress update line.
    
    Parameters
    ----------
    target : str
        Name of object currently being generated
    """
    
    # Prepare message
    msg = '\rGenerating documentation for {}'.format(string)

    # Ensure we overwrite previous message
    global _progress_length
    new_len = len(msg)
    if _progress_length > new_len:
        msg += ' ' * (_progress_length - new_len)

    # Write the message
    sys.stdout.write(msg)

    # Store length
    _progress_length = new_len

def module_heading(module_name, title=None):
    """
    Generate an rst formatted heading for a module, opt. with a nice title.

    Parameters
    ----------
    module_name : str
        The name of the module to generate a heading for
    title : str, optional
        A descriptive name to put in the title

    Returns
    -------
    str
        The formatted title
    """

    if title is None:
        return format_heading(1, ':mod:`{}`'.format(module_name))
    else:
        tpl = ':mod:`{} <{}>`'
        heading = format_heading(1, tpl.format(title, module_name))
        heading += '*Module:* :mod:`{}`\n\n'.format(module_name)
        return heading

def literal_block(content):
    """
    Put some fixed-width content in a literal block.

    Parameters
    ----------
    content : str
        The fixed-width content to be included

    Returns
    -------
    str
        The content in a literal block
    """
    
    # Put in a code-block without syntax highlighting
    out = '.. code-block:: none\n\n'
    for line in str(content).split('\n'):
        out += '   ' + line + '\n'
    return out + '\n'

def test_link_slug(modname, testname):
    """
    Standarised generation of links for specific tests in the docs.

    Parameters
    ----------
    modname : str
        The name of the module of the test
    testname : str
        The name of the test
    """
    return 'test_{}_{}'.format(modname, testname)

def test_link(test):
    mod = test.module
    tst = test.name
    return ':ref:`{}/{} <{}>`'.format(mod, tst, test_link_slug(mod, tst))

def tag_link_slug(tag):
    return 'carputils_tag_{}'.format(tag.name)

def tag_link(tag):
    return ':ref:`{}`'.format(tag_link_slug(tag))

def should_write(filename, content):

    if not os.path.exists(filename):
        # Definitely need to write it then!
        return True

    with open(filename) as fp:
        return fp.read() != content

def write_if_newer(filename, content):
    
    if should_write(filename, content):
        with open(filename, 'w') as fp:
            fp.write(content)

def generate_exampledoc(packages, dirname, heading=None, prefix=None, history=None):
    """
    Generate documentation for the specified example repositories.

    Parameters
    ----------

    packages : list of str
        List of the packages to generate docs for
    dirname : str
        Path of the directory to generate docs in
    secname : str
        Label of sections we are looking for
    heading: str
        Package header using in documentation
    prefix: rst file (no extension)
        To be added at the beginning of current document.
    history : History, optional
        History to add test result info from to the output
    """

    # Make sure output directory exists
    print('output directory: {}'.format(dirname))
    if not os.path.exists(dirname):
        os.mkdir(dirname)


    # title
    index = '.. _regression-testing-results:\n\n'

    if heading is not None:
        index += format_heading(1, heading)
    else:
        index += format_heading(1, 'Examples Documentation')


    # read in package prefix
    if prefix is not None:
        f = open('{}.rst'.format(prefix), 'r')
        index += f.read()
        f.close()

    # Add results summary is results passed
    if history is not None:
        index += generate_results_summary(history)
    
    # Generate the content for each package and add the generated table of
    # contents to the index file
    for pack in packages:
        index += generate_package(pack, dirname, history=history)

    # Make index file empty if no packages passed, so nothing appears in table
    # of contents
    if len(packages) == 0:
        index = ''

    # Write out the index file
    filename = os.path.join(dirname, 'index.rst')
    write_if_newer(filename, index)

def generate_results_summary(history):
    """
    Generate an rst formatted summary of the provided carptests results.

    Parameters
    ----------
    history : History, optional
        History to add test result info from to the output

    Returns
    -------
    str
        The rst formatted results summary section
    """

    # Title
    text = format_heading(2, 'Test Suite Results')

    # Introduction
    tpl = 'Below is a summary of the test suite ran on {}\n'
    text += tpl.format(history.latest().hostname)
    text += 'at {}.\n\n'.format(history.latest().timestamp)

    # Results summary
    text += literal_block(history.latest().summary()) + '\n'

    # Collect a list of links to failed tests
    failed = []
    for result in history.latest().results:
        if not result.passed():
            modname = result.test.module
            testname = result.test.name
            failed.append((test_link_slug(modname, testname),                
                           '{}/{}'.format(modname, testname)))

    # Add a section with failed tests, if there were failures
    if len(failed) > 0:

        # Sort alphabetically by module and test name
        failed = sorted(failed, key=lambda x: x[0])

        # Title
        text += format_heading(3, 'Failed Tests')

        # List of failed tests
        for link, label in failed:
            text += '* :ref:`{} <{}>`\n'.format(label, link)
        
        text += '\n'

    return text

def generate_package(package_name, dirname, history=None):
    """
    Generate the documentation for the specified package.

    The documentation file for each example is generated, as is the table of
    contents for this package, which is returned.

    Parameters
    ----------
    package_name : str
        The package to import and generate documentation for
    dirname : str
        The directory to generate example documentation files in
    history : History, optional
        History to add test result info from to the output

    Returns
    -------
    str
        The table of contents for this package
    """
    
    # Ensure directory exists for package files
    package_dir = os.path.join(dirname, package_name)
    if not os.path.exists(package_dir):
        os.mkdir(package_dir)

    # Table of contents beginning
    newtoc  = '.. toctree::\n'
    newtoc += '   :maxdepth: 1\n\n'
    
    # Generate toc template
    toc_tpl = '   {}/{{}}\n'.format(package_name)

    toccount = 0
    toc = ''
    
    # Get all the example modules in this package
    modules = testing.find(package_name, return_modules=True)

    for mod in sorted(list(modules), key=lambda m: m.__name__.lower()):

        #if hasattr(mod, 'EXAMPLE_DESCRIPTIVE_NAME'):
        if hasattr(mod, 'PACKAGE_DESCRIPTIVE_NAME'):
            # Start new toc
            toccount = 0

            # Print section
            level = len(mod.__name__.split('.'))
            toc += '\n'
            toc += format_heading(level+1, mod.PACKAGE_DESCRIPTIVE_NAME)

        if hasattr(mod, '__tests__') or hasattr(mod, 'EXAMPLE_DESCRIPTIVE_NAME'):

            if toccount == 0:
                toc += newtoc

            # Generate the documentation file for this module
            generate_module(mod, package_dir, history=history)

            # Add a toc entry for this module
            toc += toc_tpl.format(mod.__name__)

            toccount += 1

    toc += '\n'

    return toc

def generate_module(module, dirname, history=None):
    """
    Generate the documentation file for the specified example module.

    Parameters
    ----------
    module : module
        The example module to generate documentation for
    dirname : str
        The directory to generate example documentation files in
    history : History, optional
        History to add test result info from to the output

    Returns
    -------
    str
        The path of the generated file
    """
    
    # Print a progress message
    progress('module {}'.format(module.__name__))

    # Title
    title = getattr(module, 'EXAMPLE_DESCRIPTIVE_NAME', None)
    text = module_heading(module.__name__, title)

    # Author
    if hasattr(module, 'EXAMPLE_AUTHOR'):
        text += '.. sectionauthor:: {}\n\n'.format(module.EXAMPLE_AUTHOR)
    
    # Get Sphinx to add module docstring
    text += '.. automodule:: {}\n'.format(module.__name__)
    text += '   :no-members:\n'
    text += '   :no-inherited-members:\n\n'

    # Tests
    if hasattr(module, '__tests__'):

        # Section heading
        text += format_heading(2, 'Tests')

        # Add each test
        for test in module.__tests__:
            text += generate_test(test, history=history)
    
    # Write to file
    filename = '{}.rst'.format(module.__name__)
    filename = os.path.join(dirname, filename)
    
    write_if_newer(filename, text)

    return filename

def generate_test(test, history=None):
    """
    Generate documentation for this test.

    Parameters
    ----------
    test : carputils.testing.Test
        The test to generate documentation for
    history : History, optional
        History to add test result info from to the output

    Returns
    -------
    str
        The rst formatted test documentation
    """

    # Print a progress message
    progress('test {}/{}'.format(test.module, test.name))
    
    # Start with a link anchor
    text = '.. _' + test_link_slug(test.module, test.name) + ':\n\n'

    # Heading
    text += format_heading(3, test.name)

    # Description
    if test.description is not None:
        text += test.description + '\n\n'

    # Tags
    if len(test.tags) > 0:
        text += '**Tags:**'
        for t in test.tags:
            text += ' ' + tag_link(t)
        text += '\n\n'

    # Checks
    text += '**Checks:**\n\n'
    for check in test.checks:
        text += '* {}\n'.format(check.summary())
    text += '\n'
    
    # Test results
    try:
        result = history.latest().search(test=test)
    except (AttributeError, ValueError):
        pass
    else:
        # Add last run info
        text += '**Last run:** ' + runinfo(result) + '\n\n'
        text += literal_block(result.report())
        text += '\n'

        # Add info about when last passed
        if not result.passed():
            text += '**Last passed:** '
            text += history.last_passed(test)
            text += '\n\n'

    return text

def generate_tagdoc(outdir):
    """
    Generate the tag documentation file.
    """
    
    # Heading
    content = '.. _tags:\n\n'
    content += format_heading(1, 'Tags')

    # Include module docstring
    content += '.. automodule:: carputils.testing.tag\n\n'

    # Add tag documentation
    for tag in testing.tag:
        content += generate_tag(tag)
    
    # Write the documentation
    filename = os.path.join(outdir, 'tags.rst')
    write_if_newer(filename, content)

def generate_tag(tag):
    """
    Generate the documentation for a single tag.

    Parameters
    ----------
    tag : Tag
        The carputils tag to document

    Returns
    -------
    str
        The rst formatted documentation
    """
    
    # Tag link anchor
    text = '.. _{}:\n\n'.format(tag_link_slug(tag))

    # Tag name
    text += format_heading(2, tag.name)

    # Tag description
    if tag.description is not None:
       text += '*{}*\n\n'.format(tag.description)

    # List of tag tests
    tests = tag.tests()
    if len(tests) > 0:
        text += '**Assigned to tests:**\n\n'
        for test in tag.tests():
            text += '* {}\n'.format(test_link(test))
        text += '\n'

    text += '\n'

    return text

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('packages',
                        nargs='*', default=[],
                        help='Packages to generate docs for')
    parser.add_argument('--outdir',
                        default='examples',
                        help='Generated doc output')
    parser.add_argument('--heading',
                        default=None,
                        type=str,
                        help='Provide descriptive html section header.')
    parser.add_argument('--prefix',
                        default=None,
                        type=str,
                        help='Include rst-file containing package introduction.')
    parser.add_argument('--skip-tags',
                        action='store_true',
                        help='Skip creation of tag summary derived from __tests__.')
    parser.add_argument('--history',
                        type=History,
                        help='Test results history directory')

    args = parser.parse_args()


    generate_exampledoc(args.packages, args.outdir, heading=args.heading,
                        prefix=args.prefix, history=args.history)

    if not args.skip_tags:
        generate_tagdoc(args.outdir)

    sys.stdout.write('\n')
