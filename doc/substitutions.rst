:orphan:

.. |date| date::
.. |time| date:: %H:%M
.. |reST| replace:: reStructuredText
.. |copy| unicode:: 0xA9 .. copyright sign
.. |br| unicode:: 0x0D .. line break

.. |carp| replace:: CARPentry
.. |openCARP| replace:: `openCARP <https://opencarp.org>`__

.. |sphinx| replace:: `Sphinx <http://www.sphinx-doc.org>`__
.. |MGsell| replace:: `Matthias Gsell <matthias.gsell@medunigraz.at>`__
.. |EKarabelas| replace:: `Elias Karabelas <elias.karabelas@medunigraz.at>`__
.. |GPlank| replace:: `Gernot Plank <gernot.plank@medunigraz.at>`__
.. |APrassl| replace:: `Anton J Prassl <anton.prassl@medunigraz.at>`__
.. |KGillette| replace:: `Karli Gillette <karli.gillette@medunigraz.at>`__
.. |LMarx| replace:: `Laura Marx <laura.marx@medunigraz.at>`__
.. |ANeic| replace:: `Aurel Neic <aurel.neic@numericor.at>`__
.. |CAugustin| replace:: `Christoph Augustin <christoph.augustin@medunigraz.at>`__
.. |JBayer| replace:: `Jason Bayer <jason.bayer@ihu-liryc.fr>`__
.. |EVigmond| replace:: `Edward Vigmond <edward.vigmond@u-bordeaux.fr>`__

