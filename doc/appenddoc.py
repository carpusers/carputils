#!/usr/bin/env python

"""
Reads the index.rst file and checks if there are other anonymously
generated packages (to be indentified through index.rst-files in
subdirectories to be included.
"""

import os

indexFile = 'index.rst'
with open(indexFile, 'r') as fp:
    data = fp.readlines()

dirs  = os.listdir(os.getcwd())
for directory in dirs:
    rstFile = os.path.join(directory, 'index.rst')
    tagFile = os.path.join(directory, 'tags.rst' )

    if not os.path.isfile(rstFile):
        # there is no index.rst in this directory
        continue


    # check if current index.rst has been included
    # otherwise include it
    IncludeFound = False
    for line in data:
        if rstFile in line.strip()+'.rst':
            IncludeFound = True
            break

    if not IncludeFound:
        # append new section
        #print 'Adding section: {}'.format(directory.title())
        with open(indexFile, 'a') as fp:
            fp.write('\n')
            fp.write('{}\n'.format(directory.title()))
            fp.write('{}\n'.format('='*len(directory)))
            fp.write('\n')
            fp.write('.. toctree::\n')
            fp.write('   :hidden:\n')
            fp.write('   :maxdepth: 1\n')
            fp.write('\n')
            fp.write('   {}/index\n'.format(directory))
            fp.write('   {}\n'.format(directory))
            if os.path.isfile(tagFile):
                fp.write('   {}/tags\n'.format(directory))
            fp.write('\n')
            fp.write('\n')
