.. _single-cell-EM:

**Single Cell Scale**
=====================

For coupling between EP and deformation three distinct cellular mechanisms are implemented 
which are chosen on the fly depending on the myofilament chosen.

.. _activation-time-ecc:

*Activation-based coupling with phenomenological myofilament models*
--------------------------------------------------------------------

These myofilament models trigger a twitch force transients depending on a prescribed
instant of activation. 
Explicit prescription of activation times is not implemented in CARPentry,
rather the instant of activation is derived from the upstroke of the action potential 
of the cell model. For instance

.. _calcium-weak-ecc:

*Calcium-based weak coupling with biophysical active stress models*
-------------------------------------------------------------------

In this scenario the active stress plugin is driven by the intracellular Calcium trace
:math:`\text{Ca}_{\rm i}(t)`. As myocyte and myofilament are *weakly unidirectionally coupled*,
any effects due to stretch onto Calcium are ignored.
That is, enforcing a step change in length does not change intracellular Calcium
by binding more or less Calcium to the Troponin-C buffer.
That is, length and velocity dependence are accounted for, but not their feedback 
on the electrophysiological state.

.. _calcium-strong-ecc:

*Calcium-based weak coupling with biophysical active stress models*
-------------------------------------------------------------------

In this scenario myocyte and myofilament model are *strongly bidirectionally coupled*,
that is, enforcing a step change in length does change intracellular Calcium
by binding more or less Calcium to the Troponin-C buffer.



A tutorial on how to configure single cell electromechanics is found :ref:`here <single-cell-EM-tutorial>`.

.. _ecc:

Excitation-Contraction Coupling (ECC)
-------------------------------------

At the single cell scale ECC and MEF mechanisms are steered through turning on specific plugins
which account for 


Essentially, there are three ways of how models of cellular dynamics can be coupled 
with active stress models:

1. The easiest 

.. _mef:

Mechano-electric Feedback (MEF)
-------------------------------

TBA