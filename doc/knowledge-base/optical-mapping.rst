.. _optical-mapping:

Optical Mapping
===============

To run an optical mapping experiment in post-processing, 
both illuminating and emitting (or recording) surfaces need to be defined.
These are setup and defined like electrical electrodes being termed
``illum_dbc`` and ``emission_dbc`` as they represent Dirichlet boundary conditions.
Like electrodes, they can be defined as a collection of points with boxes or with a list of vertices.
However, note that these points **must** lie on the surface of the tissue that you want to illuminate.
The ``illum_dbc.strength`` parameter should just be set to 1, although the magnitude does not really matter.

For now the ``em_dbc.strength`` is set to 0 on the surfaces from which you would like to compute the signal.
This is the most natural(ish) choice.
We assume that exactly just outside the tissue, there is no scattering, so photons zoom off to infinity, 
thus leaving zero photon density just the other side of the boundary.
There is a more accurate mixed Neumann/Dirichlet boundary condition that could be implemented here, 
but the differences are minimal (< 5 % between this and the more simple case of :math:`\Phi_{\mathrm {em}}=0`.
In later versions, this other boundary condition may be added.

.. _tab-optical-map-input:

.. table:: Input parameters for optical mapping experiment

   +-------------------+-------------+----------------------------------------------------------------------------------+
   | Variable          | Type        | Description                                                                      |
   +===================+=============+==================================================================================+
   | num_illum_dbc     | Int         | Number of illumination Dirichlet boundary conditions                             |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | num_emission_dbc  | Int         | Number of emmission Dirichlet boundary conditions                                |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | optDT             | Float       | time step for optics in live mode,``0=no`` optics                                | 
   +-------------------+-------------+---------+------------------------------------------------------------------------+
   | optics_mode       | Int         | Value   | Description                                                            |
   |                   |             +---------+------------------------------------------------------------------------+
   |                   |             |    0    | Do not simulate optics                                                 |
   |                   |             +---------+------------------------------------------------------------------------+
   |                   |             |    1    | illuminate (for optogenetics simulations)                              |
   |                   |             +---------+------------------------------------------------------------------------+
   |                   |             |    2    | illumination and emission (optical maps)                               |
   +-------------------+-------------+---------+------------------------------------------------------------------------+
   | phi_illum         | Wfile       | illumination photon source density, :math:`\Phi_{\mathrm illum}`                 |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | phi_em            | Wfile       | emission photon source density, :math:`\Phi_{\mathrm em}`                        |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | w                 | Wfile       | volumetric photon source density                                                 |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | Vopt              | Wfile       | optical fluorescence signal                                                      |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | mu_a_illum        | Float       | optical absorpitivity for illumination, :math:`\mu_{\mathrm a,illum}`            |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | Dopt_illum        | Float       | optical diffusitiviy for illumination, :math:`D_{\mathrm illum}`                 |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | mu_a_em           | Float       | optical absorpitivity for emission, :math:`\mu_{a,em}`                           |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | Dopt_em           | Float       | optical diffusitiviy for emission, :math:`D_{\mathrm em}`                        |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | illum_dbc         | Optics_dbc* | boundary conditions for illumination                                             |
   +-------------------+-------------+----------------------------------------------------------------------------------+
   | emission_dbc      | Optics_dbc* | boundary conditions for photon emission  hline                                   |
   +-------------------+-------------+----------------------------------------------------------------------------------+


Boundary Conditions
-------------------

Like in the bidomain, on any surfaces which do not have a specific illumination or emission boundary condition imposed 
we assume the standard zero flux condition of :math:`\nabla \Phi = 0`.
This is ok for the sides of a slab, for example, which is being illuminated from its top side.
However, for the base of the slab this is not adequate, 
and additional boundary conditions during both illumination and emission should be specified here.
Specifically, :math:`\Phi=0` should be defined on these surfaces.
This should only be worried about for thin slabs.
For anything thicker than :math:`\sim 2 mm` or so, it does not really matter, 
as the illuminating photon density will have decayed to almost zero here anyway 
(although this should be adjusted for different illuminating wavelengths 
which may penetrate deeper, as appropriate).

.. _tab-optical-map-bcs:

.. table:: Definition of boundary conditions in optical mapping experiment 

   +-------------------+-------------+------------------------------------------------------+
   | Variable          | Type        | Description                                          |
   +===================+=============+======================================================+
   | vtx_file          | String      | If specified, affected points given in this file     |
   +-------------------+-------------+------------------------------------------------------+
   | strength          | Float       | photon density on boundary                           |
   +-------------------+-------------+------------------------------------------------------+
   | x0                | Float       | lower z-ordinate of electrode volume                 | 
   +-------------------+-------------+------------------------------------------------------+
   | y0                | Float       | lower y-ordinate of electrode volume                 | 
   +-------------------+-------------+------------------------------------------------------+
   | z0                | Float       | lower z-ordinate of electrode volume                 | 
   +-------------------+-------------+------------------------------------------------------+
   | xd                | Float       | x-dimension of electrode volume                      |                      
   +-------------------+-------------+------------------------------------------------------+
   | yd                | Float       | y-dimension of electrode volume                      |                      
   +-------------------+-------------+------------------------------------------------------+
   | zd                | Float       | z-dimension of electrode volume                      |                      
   +-------------------+-------------+------------------------------------------------------+
   | ctr_def           | Flag        | if set limits are [x0-xd x0+xd] etc, [x0 x0+xd] o.w. | 
   +-------------------+-------------+------------------------------------------------------+
   | geometry          | Int         | ID of dbc geometry definition                        | 
   +-------------------+-------------+------------------------------------------------------+
   | bctype            | Short       | 0=inomogeneous,1=homogeneous                         |
   +-------------------+-------------+------------------------------------------------------+
   | dump_nodes        | Flag        | dump affected nodes in file                          | 
   +-------------------+-------------+------------------------------------------------------+

