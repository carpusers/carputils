:orphan:

.. _knowledge-base-ep:

###############
General Aspects
###############

.. toctree::
    :maxdepth: 2

    modeling-aspects


##################################
Modeling Cardiac Electrophysiology
##################################

.. toctree:: 
    :maxdepth: 2
    
    single-cell-ep
    tissue-scale-ep




.. _knowledge-base-mech:

##########################
Modeling Cardiac Mechanics
##########################

.. toctree::
    :maxdepth: 2

    preamble-em
    single-cell-em
    tissue-scale-em




.. _knowledge-base-fluid:

#####################################
Modelling Cardiovascular Hemodynamics
#####################################


