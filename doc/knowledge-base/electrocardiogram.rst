.. _ecg:

Electrocardiogram
=================

ECG signals can be either computed directly when running bidomain simulations
or by using the integral solution of Poisson's equation when running monodomain simulations
given as

.. math::
   :nowrap:
   :label: eq-phie-recovery

   \begin{equation}
    \phi_{\rm e}( \mathbf{x}_{\rm f},t) = \frac{1}{4 \pi \sigma} \int_{\Omega_{\rm i}} \frac{I_{\rm m} (\mathbf{x}_{\rm s},t)}{r_{\rm sf}} d \Omega_{\rm i}
    \nonumber
   \end{equation}


where :math:`\phi_{\mathrm e}` are the potentials at a given field point, :math:`\mathbf{x}_{\rm f}`,
:math:`I_{\rm m}` is the source density at a given source point, :math:`\mathbf{x}_{\rm s}`,
and :math:`r_{\rm sf} = ||\mathbf{x}_{\rm s}-\mathbf{x}_{\rm f}||` is the distance between source and field point.
A detailed description of the method and its limitation has been given in 
(`Bishop et al, IEEE Trans Biomed Eng 58(8), 2011 <https://www.ncbi.nlm.nih.gov/pubmed/21536529>`_),


Bidomain simulations are the most appropriate approach
when the heart is immersed in a volume conductor of limited size
and the volume conductor can be represented.
ECG recovery techniques are very well suited
when the heart is immersed in an unbounded volume conductor.
That is, ECG signals from a heart immersed in a torso can be approximated very well
with a monodomain simulation using ECG recovery.
On the other hand, ECG signals recorded at the surface of a Langendorff preparation
are better approximated with a bidomain simulation when the heart is not immersed in a fluid bath.
An alternative in this scenario is to use recovery method 3
where we use a monodomain simulation, but solve the elliptic PDE infrequently before outputting.
Memorywise this simulation approach is equivalent to a bidomain simulations,
in terms of execution speed the simulation is roughly equivalent to a monodomain simulation.


.. table:: Input parameters for computing ECGs based on the :math:`\phi_{\mathrm e}`-recovery method
    :name: tab-ecg

    +------------------------+------------+-----------------------------------------------------------------------+
    | field                  | type       | description                                                           |
    +========================+============+=======================================================================+
    | phie_rec_ptf           | string     | file of recording sites (see Sref{sec:ptf} for format)                | 
    +------------------------+------------+-----------------------------------------------------------------------+
    | phie_recovery_file     | Wfile      | output file of ECG data                                               |
    +------------------------+------------+-----------------------------------------------------------------------+
    | phie_rec_meth          | Int        | method to recover :math:`phi_{mathrm e}`                              |
    +------------------------+------------+-----------------------------------------------------------------------+

A tutorial on how to use this ECG feature is found :ref:`here <tutorial-ecg>`.
