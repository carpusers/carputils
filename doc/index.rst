:orphan:

.. raw:: html

  <style> .red {color:#aa0060; font-weight:bold} </style>

.. role:: red

.. _carpentry-modeling-environment:

|

.. only:: DEVELPAGE

  ============================================
  CARPentry Modeling Environment Documentation
  ============================================

:red:`Support for the binary version of CARPentry will expire in June 2021!`
#############################################################################

As we are contributing to the community modeling project |openCARP|, the support for a binary version of CARPentry will be phased out. For those who are interested in using CARPentry for modeling cardiac EP in future projects, we recommend switching to |openCARP| which is available in source code, but packaged binary and container version are also provided. |openCARP| and CARPentry are compatible and can be used interchangeably; there is no need for adjusting any workflows to facilitate a smooth transition. Some most advance simulator features are currently not provided by |openCARP|, such as an integrated eikonal solver, reaction-eikonal modeling, lead-field based ECG modeling or Purkinje models. If these features are of interest to you, contact Prof. |GPlank| @MedUniGraz, Dr. |EVigmond| @LIRYC or Dr. |aneic| @NumeriCor to inquire academic and commercial licensing options.

|

The CARPentry Modeling Enviroment (CME) provides a set of tools for modeling electrophysiological, mechanical and fluidic aspects of cardiac function. CME comprises a set of tools for specific applications within a packaged binary.

.. figure:: /images/cme_components.png 
   :align: center

|

.. include:: research-groups.rst
