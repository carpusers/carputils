********************
CARPentry Literature
********************

The methodological underpinnings of virtually all features and concepts implemented in CARPentry
were reported in detail in various journal publications.
A selected non-exhaustive list of papers is found below.

.. [#bayer_ldrb]  Bayer JD, Blake RC, Plank G, Trayanova NA.
   **A Novel Rule-Based Algorithm for Assigning Myocardial Fiber Orientation to Computational Heart Models**,
   Ann Biomed Eng 40(10):2243-54, 2012. `[PubMed] <https://www.ncbi.nlm.nih.gov/pubmed/22648575>`_

.. [#plank_amg] Plank G, Liebmann M, Weber dos Santos R, Vigmond EJ, Haase G.
   **Algebraic multigrid preconditioner for the cardiac bidomain model**,
   IEEE Trans Biomed Eng. 54(4):585-96, 2007.
   `[PubMed] <https://www.ncbi.nlm.nih.gov/pubmed/17405366>`_

.. [#vigmond_solvers] Vigmond EJ, Weber dos Santos R, Prassl AJ, Deo M, Plank G.
   **Solvers for the cardiac bidomain equations**,
   Prog Biophys Mol Biol. 96(1-3):3-18, 2008.
   `|PubMed| <https://www.ncbi.nlm.nih.gov/pubmed/17900668>`_

.. [#augustin_hrmech] Augustin CM, Neic A, Liebmann M, Prassl AJ, Niederer SA, Haase G, Plank G.
   **Anatomically accurate high resolution modeling of human whole heart electromechanics:
   A strongly scalable algebraic multigrid solver method for nonlinear deformation**,
   J Comput Phys. 305:622-646, 2016. `[PubMed] <https://www.ncbi.nlm.nih.gov/pubmed/26819483>`_

