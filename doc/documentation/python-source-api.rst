*******************************************
Sphinx Documentation of carputils Framework
*******************************************

.. todo: needs some more includes

**********************
:mod:`carputils.tools`
**********************
.. automodule:: carputils.tools
    :no-members:
    :no-inherited-members:
    :show-inheritance:

.. currentmodule:: carputils

.. autosummary::
    :toctree: ../generated/
    :template: function.rst
    
    tools.carp_cmd
    tools.carpexample
    tools.standard_parser
    tools.clean_directory

********************
:mod:`carputils.job`
********************
.. automodule:: carputils.job
    :no-members:
    :no-inherited-members:

.. currentmodule:: carputils

.. rubric:: Classes
.. autosummary::
    :toctree: ../generated/
    :template: class.rst

    job.Job

*********************
:mod:`carputils.mesh`
*********************
.. automodule:: carputils.mesh
    :no-members:
    :no-inherited-members:

.. currentmodule:: carputils

.. rubric:: Classes
.. autosummary::
    :toctree: ../generated/
    :template: class.rst

    mesh.bivslice.BiVSlice
    mesh.Block
    mesh.Cable
    mesh.Grid
    mesh.Ring
    mesh.Ellipsoid
    mesh.BoxRegion
    mesh.pipe
    mesh.SphereRegion
    mesh.CylinderRegion

.. rubric:: Functions
.. autosummary::
    :toctree: ../generated/
    :template: function.rst

    mesh.generate
    mesh.block_boundary_condition
    mesh.block_region
    mesh.linear_fibre_rule

***********************
:mod:`carputils.carpio`
***********************
.. automodule:: carputils.carpio
    :no-members:
    :no-inherited-members:


:mod:`carputils.carpio.igb`
===========================
.. automodule:: carputils.carpio.igb
    :no-members:
    :no-inherited-members:
    
.. currentmodule:: carputils.carpio

.. rubric:: Classes
.. autosummary::
    :toctree: ../generated/
    :template: class.rst

    igb.IGBFile

.. rubric:: Functions
.. autosummary::
    :toctree: ../generated/
    :template: function.rst

    igb.open


:mod:`carputils.carpio.txt`
===========================
.. automodule:: carputils.carpio.txt
    :no-members:
    :no-inherited-members:
    
.. currentmodule:: carputils.carpio

.. rubric:: Classes
.. autosummary::
    :toctree: ../generated/
    :template: class.rst

    txt.TxtFile

.. rubric:: Functions
.. autosummary::
    :toctree: ../generated/
    :template: function.rst

    txt.open

:mod:`carputils.carpio.sv`
==========================
.. automodule:: carputils.carpio.sv
    :no-members:
    :no-inherited-members:
    
.. currentmodule:: carputils.carpio

.. rubric:: Classes
.. autosummary::
    :toctree: ../generated/
    :template: class.rst

    sv.SVFile
    sv.SVSeries

.. rubric:: Functions
.. autosummary::
    :toctree: ../generated/
    :template: function.rst

    sv.open
    sv.open_series


:mod:`carputils.carpio.lut`
===========================
.. automodule:: carputils.carpio.lut
    :no-members:
    :no-inherited-members:
    
.. currentmodule:: carputils.carpio

.. rubric:: Classes
.. autosummary::
    :toctree: ../generated/
    :template: class.rst

    lut.LUTFile

.. rubric:: Functions
.. autosummary::
    :toctree: ../generated/
    :template: function.rst

    lut.open

**********************
:mod:`carputils.model`
**********************
.. automodule:: carputils.model
    :no-members:
    :no-inherited-members:
    
.. currentmodule:: carputils

.. rubric:: Modules
.. autosummary::
    :toctree: ../generated/

    model.activetension
    model.ionic
    model.mechanics

.. rubric:: Classes
.. autosummary::
    :toctree: ../generated/
    :template: class.rst

    model.ConductivityRegion
    model.EikonalRegion
    model.Stimulus

.. rubric:: Functions
.. autosummary::
    :toctree: ../generated/
    :template: function.rst

    model.optionlist

*****************************
:mod:`carputils.divertoutput`
*****************************
.. automodule:: carputils.divertoutput
    :no-members:
    :no-inherited-members:

.. currentmodule:: carputils

.. autosummary::
    :toctree: ../generated/
    :template: function.rst
    
    divertoutput.call
    divertoutput.subprocess_exceptions

***********************
:mod:`carputils.stream`
***********************
.. automodule:: carputils.stream
    :no-members:
    :no-inherited-members:

.. currentmodule:: carputils

.. autosummary::
    :toctree: ../generated/
    :template: function.rst
    
    stream.merge
    stream.divert_std
