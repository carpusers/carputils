
The main objective of the tutorials compiled in this section is to provide
basic examples for most (ideal all) CARPentry features.
This examples are intended to transfer basic user know-how in an efficient way.
The scripts for building the tutorials are designed as mini-experiments
which should also serve as a basic building block for building more complex experiments.

There is no clear separation between *plain* and *carputils* philosophy, 
the tutorials are intended to teach both aspects. 
There is a number of tutorials dedicated to teaching *plain usage* of CARPentry
as this is considered necessary fundamental know-how for those 
who are interested in building more complex experiments from scratch themselves
or even for extending pre-existing *carputils* experiments.
However, all the executable tutorials are coded up in *carputils* 
to facilitate an easy execution of all experiments 
without the significant additional time-consuming hassle of assembling long and complex command lines.


**Intended use**

Most tutorials can be run by simply copying the *carputils* run command from the tutorial webpages.
It is recommended to inspect the generated command lines to understand 
what the simulation looks like in the *plain* command line by adding the option ``--dry`` to the run script command line.
A more detailed background on carputils command line options are given in the :ref:`Carputils usage section <carputils-usage>`.

To run the examples ``cd`` into the respective section relative to the location of your tutorials repository. 
The structure of the tutorial repository should look similar to the following 

.. code-block:: bash

   ├── 01_EP_single_cell
   │   ├── 01_basic_bench
   │   ├── 02_restitution
   │   ├── 02B_APD_restitution
   │   ├── 03_state_clamp
   │   ├── 03A_voltage_clamp
   │   ├── 04_limpet_fe
   │   ├── 05_EasyML
   │   └── 10_fromCellML
   ├── 02_EP_tissue
   │   ├── 01_basic_usage
   │   ├── 02_stimulation
   │   ├── 03_study_preparation
   │   ├── 03A_study_prep_tuneCV
   │   ├── 03B_study_prep_init
   │   ├── 03C_tuning_wavelength
   │   ├── 03D_conduction_velocity_restitution
   │   ├── 04_tagging
   │   ├── 05_heterogeneities
   │   ├── 05A_Regions_vs_Gradients
   │   ├── 05B_Conductive_Heterogeneity
   │   ├── 05C_Cellular_Dynamics_Heterogeneity
   │   ├── 05D_Region_Reunification
   │   ├── 05E_Smooth_Gradient_Heterogeneities
   │   ├── 06_eikonal
   │   ├── 07_extracellular
   │   ├── 07A_augmentation
   │   ├── 07B_periodic
   │   ├── 08_lats
   │   ├── 09_filaments
   │   ├── 10_optical
   │   ├── 11_discretization
   │   ├── 12_tuning_wavelength
   │   ├── 13_laplace
   │   ├── 14_bidm_rotation
   │   ├── 15_bidm_distribution
   │   ├── 16_bidm_dogbone
   │   ├── 17_bidm_pacing
   │   ├── 18_tdr
   │   └── 19_Pkje
   ├── 03_EM_single_cell
   │   └── 01_EM_coupling
   ├── 04_EM_tissue
   │   ├── 01_boundary_conditions
   │   ├── 02_laplace
   │   ├── 03_unloading
   │   ├── 04_EM_coupling
   │   ├── 05_MEF
   │   ├── 06_afterload_fitting
   │   ├── 07_constitutive_fitting
   │   ├── 08_ring
   │   └── 09_bivslice
   ├── 05_pre_post_processing
   │   ├── 01_basic_mesher
   │   ├── 03_Laplace_Dirichlet_Fibers
   │   ├── 04_output_restriction
   │   ├── 05_igbutils
   │   ├── 06_UVC_bullseye
   │   └── 07_UVC_applied
   ├── 06_fluid
   │   └── 01_HagenPouseille
   └── visualization
       ├── limpetGUI
       ├── meshalyzer
       ├── meshalyzer_adv
       └── paraview

To run the single cell EP tutorials therefore

.. code-block:: bash

   cd EM_tissue/04_EM_coupling
   ./run.py --help

The output data from the tutorial persists after the experiment. If meshes were generated with the *mesher*
program, there will be a ``meshes`` directory with
further subdirectories containing the meshes, each directory name having the date embedded within it.
You need to determine which one was used for the last experiment. 
To be sure of the mesh, you can recursively remove the *meshes* directory before you run the experiment, or simply run the script with the ``--clean`` option.

CARPentry results will be placed in a subdirectory within the experiment directory. 
The name of the subdirectory may be formed from a combinatio of 
the date, experiment conditions and number of processes so there may be several.
Within each results subdirectory, you should see IGB files (the outputs), possibly intracellular grids, as 
well as a file ``parameters.par`` which has all the parameter settings, encapsulating even the commandline optionss.
This file may be used to rerun the simulation from the main experiment directory.

.. code-block:: bash

   carp.pt +F parameters.par

