
from datetime import datetime, timedelta

DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'

def datetime_serialise(dtobj):
    return dtobj.strftime(DATETIME_FORMAT)

def datetime_deserialise(dtstring):
    return datetime.strptime(dtstring, DATETIME_FORMAT)

def timedelta_serialise(tdobj):
    return tdobj.total_seconds()

def timedelta_deserialise(seconds):
    return timedelta(seconds=seconds)
