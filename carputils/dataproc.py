
import numpy
import scipy.signal


class DataProcessor:

    def apply(self, data):
        raise NotImplementedError('Method "DataProcessor.apply" not implemented!')


class AverageData(DataProcessor):

    def apply(self, data):
        data = numpy.array(data, dtype=float, copy=False, subok=True)
        return data-data.mean()


class LowPassBesselFilter(DataProcessor):

    def __init__(self, signal_nyqfreq, filter_order, filter_freq):
        signal_nyqfreq = float(signal_nyqfreq)
        filter_order = int(filter_order)
        filter_freq = float(filter_freq)
        critical_freq = filter_freq / signal_nyqfreq

        self._filter = scipy.signal.bessel(filter_order, critical_freq, output='ba',
                                           btype='lowpass', analog=False)
        self._padlen = 3*max(len(self._filter[0]), len(self._filter[1]))

    def apply(self, data):
        return scipy.signal.filtfilt(self._filter[0], self._filter[1], data,
                                     method='pad', padtype='even',
                                     padlen=self._padlen)


class LowPassButterFilter(DataProcessor):

    def __init__(self, signal_nyqfreq, filter_order, filter_freq):
        signal_nyqfreq = float(signal_nyqfreq)
        filter_order = int(filter_order)
        filter_freq = float(filter_freq)
        critical_freq = filter_freq / signal_nyqfreq

        self._filter = scipy.signal.butter(filter_order, critical_freq, output='ba',
                                           btype='lowpass', analog=False)
        self._padlen = 3*max(len(self._filter[0]), len(self._filter[1]))

    def apply(self, data):
        return scipy.signal.filtfilt(self._filter[0], self._filter[1], data,
                                     method='pad', padtype='even',
                                     padlen=self._padlen)


class HighPassBesselFilter(DataProcessor):

    def __init__(self, signal_nyqfreq, filter_order, filter_freq):
        signal_nyqfreq = float(signal_nyqfreq)
        filter_order = int(filter_order)
        filter_freq = float(filter_freq)
        critical_freq = filter_freq/signal_nyqfreq

        self._filter = scipy.signal.bessel(filter_order, critical_freq, output='ba',
                                           btype='highpass', analog=False)
        self._padlen = 3*max(len(self._filter[0]), len(self._filter[1]))

    def apply(self, data):
        return scipy.signal.filtfilt(self._filter[0], self._filter[1], data,
                                     method='pad', padtype='even',
                                     padlen=self._padlen)


class HighPassButterFilter(DataProcessor):

    def __init__(self, signal_nyqfreq, filter_order, filter_freq):
        signal_nyqfreq = float(signal_nyqfreq)
        filter_order = int(filter_order)
        filter_freq = float(filter_freq)
        critical_freq = filter_freq/signal_nyqfreq

        self._filter = scipy.signal.butter(filter_order, critical_freq, output='ba',
                                           btype='highpass', analog=False)
        self._padlen = 3*max(len(self._filter[0]), len(self._filter[1]))

    def apply(self, data):
        return scipy.signal.filtfilt(self._filter[0], self._filter[1], data,
                                     method='pad', padtype='even',
                                     padlen=self._padlen)


class BandStopBesselFilter(DataProcessor):

    def __init__(self, signal_nyqfreq, filter_order, filter_freq_low, filter_freq_high):
        signal_nyqfreq = float(signal_nyqfreq)
        filter_order = int(filter_order)
        filter_freq_low = float(filter_freq_low)
        filter_freq_high = float(filter_freq_high)
        critical_freq = (filter_freq_low/signal_nyqfreq,
                         filter_freq_high/signal_nyqfreq)

        self._filter = scipy.signal.bessel(filter_order, critical_freq, output='ba',
                                            btype='bandstop', analog=False)
        self._padlen = 3*max(len(self._filter[0]), len(self._filter[1]))

    def apply(self, data):
        return scipy.signal.filtfilt(self._filter[0], self._filter[1], data,
                                     method='pad', padtype='even',
                                     padlen=self._padlen)


class BandStopButterFilter(DataProcessor):

    def __init__(self, signal_nyqfreq, filter_order, filter_freq_low, filter_freq_high):
        signal_nyqfreq = float(signal_nyqfreq)
        filter_order = int(filter_order)
        filter_freq_low = float(filter_freq_low)
        filter_freq_high = float(filter_freq_high)
        critical_freq = (filter_freq_low/signal_nyqfreq,
                         filter_freq_high/signal_nyqfreq)

        self._filter = scipy.signal.butter(filter_order, critical_freq, output='ba',
                                            btype='bandstop', analog=False)
        self._padlen = 3*max(len(self._filter[0]), len(self._filter[1]))

    def apply(self, data):
        return scipy.signal.filtfilt(self._filter[0], self._filter[1], data,
                                     method='pad', padtype='even',
                                     padlen=self._padlen)
