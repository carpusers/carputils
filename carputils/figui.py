
from matplotlib import pyplot
from matplotlib import colors
from matplotlib import widgets
from matplotlib.lines import Line2D

class ArtistMenu(object):

    # Linestyle names
    LS_NAMES = ('solid', 'dashed', 'dashdot', 'dotted', 'none')

    # Linestyle labels
    LS_LABELS = ('-', '--', '-.', ':', 'None')

    # Linestyle map
    LS_MAP = dict(zip(LS_NAMES, LS_LABELS))

    def __init__(self, figui, size=(4.0, 3.0)):
        assert isinstance(figui, FigureUI)

        # Parent figure
        self._parent = figui
        # Menu figure
        self._fig = pyplot.figure(figsize=size)
        self._fig.canvas.set_window_title('MENU')
        self._fig.set_visible(False)
        # Selected artist
        self._selection = None
        # GUI controls
        self._controls = self.__create_controls()
        # Connect events
        self.__connect_events()

    def raise_key_press_event(self, event):
        self.__key_press_event(event)

    def raise_key_release_event(self, event):
        self.__key_release_event(event)

    @property
    def fig(self):
        return self._fig

    def __create_controls(self):
        shift = 0.05
        height = 0.125

        # Create color slider
        slider = dict()
        # Create red slider
        axes = pyplot.axes([0.1, 0.45+shift, 0.8, height])
        slider_r = widgets.Slider(axes, 'red', 0.0, 1.0, 0.5)
        slider_r.on_changed(self.__on_color_slider_change)
        slider['red'] = slider_r
        # Create green slider
        axes = pyplot.axes([0.1, 0.3+shift, 0.8, height])
        slider_g = widgets.Slider(axes, 'green', 0.0, 1.0, 0.5)
        slider_g.on_changed(self.__on_color_slider_change)
        slider['green'] = slider_g
        # Create blue slider
        axes = pyplot.axes([0.1, 0.15+shift, 0.8, height])
        slider_b = widgets.Slider(axes, 'blue', 0.0, 1.0, 0.5)
        slider_b.on_changed(self.__on_color_slider_change)
        slider['blue'] = slider_b
        # Create alpha slider
        axes = pyplot.axes([0.1, 0.0+shift, 0.8, height])
        slider_a = widgets.Slider(axes, 'alpha', 0.0, 1.0, 0.5)
        slider_a.on_changed(self.__on_color_slider_change)
        slider['alpha'] = slider_a
        slider['rgba'] = [slider_r, slider_g, slider_b, slider_a]

        # Create linestyle choices
        axes = pyplot.axes([0.1, 0.6+shift, 0.2, 2.0*height])
        choices_ls = widgets.RadioButtons(axes, self.LS_NAMES,
                                          activecolor='#000000')
        choices_ls.on_clicked(self.__on_linestyle_choice_change)

        # Create controls dictionary
        controls = dict()
        controls['colslider'] = type('Namespace', (object,), slider)
        controls['lschoices'] = choices_ls
        # Return object
        return type('Namespace', (object,), controls)

    def set_selection(self, selection):
        # Set selection
        self._selection = selection
        # Check if selection is None
        if self._selection is None:
            # Hide figure
            self._fig.set_visible(False)
        else:
            # Get color and linestyle
            rgba = colors.to_rgba(self._selection.get_color())
            alpha = self._selection.get_alpha()
            lsidx = self.LS_LABELS.index(self._selection.get_linestyle())
            # If alpha value is given, overwrite in rgba tuple
            if alpha is not None:
                rgba = (rgba[0], rgba[1], rgba[2], alpha)
            # Update GUI controls
            self._controls.lschoices.set_active(lsidx)
            self.__update_color_slider(rgba)
            # Show figure
            self._fig.set_visible(True)

        # Redraw figure
        self._fig.canvas.draw_idle()

    def __update_color_slider(self, rgba):
        for i in (0, 1, 2, 3):
            slider = self._controls.colslider.rgba[i]
            # Set slider color
            slider.poly.set_color(rgba)
            slider.poly.set_alpha(rgba[3])
            # Set slider value
            slider.set_val(rgba[i])
            # Set value for intial value indicator
            slider.vline.set_xdata([rgba[i], rgba[i]])

    def __on_color_slider_change(self, value):
        # Collect rgba value
        rgba = tuple([slider.val for slider in self._controls.colslider.rgba])
        # Set slider color
        for slider in self._controls.colslider.rgba:
            slider.poly.set_color(rgba)
            slider.poly.set_alpha(rgba[3])
        # Set selection color
        artist = self._selection
        if artist is not None:
            artist.set_color(rgba)
            artist.set_alpha(rgba[3])
            # Get artist index
            ax = artist.axes
            index = getattr(artist, 'legendID', -1)
            if ax.legend_ is not None and index > -1:
                # Change legend entry
                ax.legend_.get_lines()[index].set_color(rgba)
                ax.legend_.get_lines()[index].set_alpha(rgba[3])
            # Redraw parent figure
            self._parent.fig.canvas.draw_idle()

    def __on_linestyle_choice_change(self, label):
        # Set selection linestyle
        artist = self._selection
        if artist is not None:
            ls = self.LS_MAP.get(label, 'None')
            artist.set_linestyle(ls)
            # Get artist index
            ax = artist.axes
            index = getattr(artist, 'legendID', -1)
            if ax.legend_ is not None and index > -1:
                # Change legend entry
                ax.legend_.get_lines()[index].set_linestyle(ls)
            # Redraw parent figure
            self._parent.fig.canvas.draw_idle()

    def __on_legend_state_change(self, value):
        print(value)

    def __close_event(self, event):
        pyplot.close(self._parent.fig)

    def __key_press_event(self, event):
        pass

    def __key_release_event(self, event):
        self._parent.raise_key_release_event(event)

    def __connect_events(self):
        # Connect callback functions to events
        self._fig.canvas.mpl_connect('key_press_event', self.__key_press_event)
        self._fig.canvas.mpl_connect('key_release_event', self.__key_release_event)
        self._fig.canvas.mpl_connect('close_event', self.__close_event)


class FigureUI(object):

    def __init__(self, fig):
        assert isinstance(fig, pyplot.Figure)
        # Figure
        self._fig = fig
        self._fig.canvas.set_window_title('CAVPLOT')
        # Selected artist
        self._selection = None
        # Artist menu
        self._menu = ArtistMenu(self)
        # Connect events
        self.__connect_events()

    def raise_key_press_event(self, event):
        self.__key_press_event(event)

    def raise_key_release_event(self, event):
        self.__key_release_event(event)

    @property
    def fig(self):
        return self._fig

    def __close_event(self, event):
        pyplot.close(self._menu.fig)

    def __key_press_event(self, event):
        pass

    def __key_release_event(self, event):
        if self._selection is None:
            return

        known_keys = []
        known_keys += ['delete', 'escape']
        known_keys += ['right', 'left']
        if event.key not in known_keys:
            return

        # Get stored artist and corresponding axes
        artist = self._selection
        ax = artist.axes

        # Check if delete key was pressed
        if event.key == 'delete':
            # Remove selected artist from axes
            artist.remove()
            # Reset selection information
            self._selection = None
            # Redraw axes legend if there was one
            legend = ax.get_legend()
            if legend is not None:
                # Get original legend location, fontsize and framealpha
                location = legend._loc
                fontsize = legend._fontsize
                framealpha = legend.get_frame().get_alpha()
                # Reset axes legend
                ax.legend(loc=location, fontsize=fontsize, framealpha=framealpha)

        # Check if escape key was pressed
        elif event.key == 'escape':
            # Get selected artist's line width
            linewidth = artist.get_lw()
            # Restore old line width
            artist.set_lw(linewidth*0.5)
            # Reset selection information
            self._selection = None

        elif event.key in ('right', 'left'):
            # Get all lines on current axes
            lines = ax.get_lines()
            num_lines = len(lines)
            # Get index of selected artist
            old_index = lines.index(artist)
            # Create new index
            index_shift = 1 if event.key == 'right' else -1
            new_index = (old_index + index_shift) % num_lines
            # Get selected artist's line width
            linewidth = artist.get_lw()
            # Restore old line width
            artist.set_lw(linewidth*0.5)
            # Get new artist
            artist = lines[new_index]
            # Skip not pickable artists
            while not artist.pickable():
                old_index = new_index
                new_index = (old_index + index_shift) % num_lines               
                artist = lines[new_index]
            # Set new selection
            self._selection = artist
            # Get selected artist's line width
            linewidth = artist.get_lw()
            # Highlight selected artist
            artist.set_lw(linewidth*2.0)

        # Update menu
        self._menu.set_selection(self._selection)
        # Redraw figure
        self._fig.canvas.draw_idle()

    def __pick_event(self, event):
        if not isinstance(event.artist, Line2D):
            return
        # Check if an artist is already selected
        if self._selection is not None:
            # Get stored artist and line width
            artist = self._selection
            linewidth = artist.get_lw()
            # Restore original line width
            artist.set_lw(linewidth*0.5)
            # Reset selection information
            self._selection = None

        # Get selected artist and line width
        artist = event.artist
        linewidth = event.artist.get_lw()
        # Set selected artist
        self._selection = artist
        # Highlight selected artist
        artist.set_lw(linewidth*2.0)
        # Update menu
        self._menu.set_selection(self._selection)

        # Redraw figure
        self._fig.canvas.draw_idle()

    def __connect_events(self):
        # Connect callback functions to events
        self._fig.canvas.mpl_connect('key_press_event', self.__key_press_event)
        self._fig.canvas.mpl_connect('key_release_event', self.__key_release_event)
        self._fig.canvas.mpl_connect('pick_event', self.__pick_event)
        self._fig.canvas.mpl_connect('close_event', self.__close_event)

