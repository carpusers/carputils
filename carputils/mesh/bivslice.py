"""
Generate a simple bi-ventricular slice geometry, providing a few parameters.
"""

from sys import version_info
import numpy as np
from carputils.mesh.general import Mesh3D, closest_to_rangles

# Python2 - Python3 compatibility
if version_info.major > 2:
    xrange = range

def circle_intersections(centre_1, centre_2, radius_1, radius_2):
    """

                 i
                 *
               * *  *
         r1  *   *     *   r2
           *     *h       *
         *       *           *
    c1 ************************** c2
            a           b
              d = a + b

    From Pythagoras:

    r1^2 = a^2 + h^2
    r2^2 = b^2 + h^2

    Rearranging and combining gives:

    a^2 = b^2 + r1^2 - r2^2

    Substituting b = d - a gives:

    a = (r1^2 - r2^2 + d^2) / 2d

    And h is found by:

    h^2 = r1**2 - a**2
    """

    centre_1 = np.array(centre_1)
    centre_2 = np.array(centre_2)

    d = np.linalg.norm(centre_1 - centre_2)
    a = (radius_1**2 - radius_2**2 + d**2) / (2*d)

    h = np.sqrt(radius_1**2 - a**2)

    vec_d = centre_2 - centre_1
    vec_d_norm = vec_d / d
    vec_a = a * vec_d_norm

    rot90 = np.array([[0, -1], [1, 0]])
    vec_h = np.dot(rot90, vec_d_norm) * h

    return (centre_1 + vec_a + vec_h,
            centre_1 + vec_a - vec_h)

class BiVSlice(Mesh3D):

    def __init__(self, r_lv, r_rv, thickness_lv, thickness_rv,
                 height, separation=1.,
                 div_lvfreewall=30, div_rvfreewall=25, div_septum=15,
                 div_lvtrans=3, div_rvtrans=2, div_height=2,
                 *args, **kwargs):

        Mesh3D.__init__(self, *args, **kwargs)

        self.r_inner_lv = float(r_lv)
        self.r_inner_rv = float(r_rv)

        self._separation = float(separation)

        self._thick_lv = float(thickness_lv)
        self._thick_rv = float(thickness_rv)

        self._height = float(height)

        self._div_lvfree = int(div_lvfreewall)
        self._div_rvfree = int(div_rvfreewall)
        self._div_septum = int(div_septum)

        self._div_lvtrans = int(div_lvtrans)
        self._div_rvtrans = int(div_rvtrans)

        self._div_height = int(div_height)

        # Check assumptions are valid
        centre_separation = np.linalg.norm(self.lv_centre - self.rv_centre)
        assert centre_separation < self.r_inner_lv + self.r_inner_rv
        assert centre_separation + self.r_inner_lv > self.r_outer_lv

    @property
    def r_outer_lv(self):
        return self.r_inner_lv + self._thick_lv

    @property
    def r_outer_rv(self):
        return self.r_inner_rv + self._thick_rv

    @property
    def lv_centre(self):
        return np.array([0., 0.])

    @property
    def rv_centre(self):
        return np.array([-self._separation * self.r_inner_lv, 0.])

    @property
    def lv_volume(self):
        return (self.r_inner_lv*self.r_inner_lv*np.pi)*self._height

    @property
    def rv_volume(self):
        pos_x_rv, pos_x_lv = self.rv_centre[0], self.lv_centre[0]
        r_rv, r_lv = self.r_inner_rv, self.r_outer_lv

        # determine x-component of intersection point
        tmp0 = pos_x_lv*pos_x_lv-pos_x_rv*pos_x_rv
        tmp1 = r_lv*r_lv-r_rv*r_rv
        tmp2 = pos_x_lv-pos_x_rv
        s = 0.5*(tmp0-tmp1)/(tmp2*tmp2)-pos_x_rv/tmp2
        assert 0.0 < s < 1.0

        # compute circle cap heights
        cap_h_rv, cap_h_lv = r_rv-s*tmp2, r_lv-(1.0-s)*tmp2
        assert 0 < cap_h_rv < r_rv and 0 < cap_h_lv < r_lv

        # compute circle segment areas
        capA_rv = r_rv*r_rv*np.arccos(1.0-cap_h_rv/r_rv) \
                  -(r_rv-cap_h_rv)*np.sqrt(2.0*r_rv*cap_h_rv-cap_h_rv*cap_h_rv)
        capA_lv = r_lv*r_lv*np.arccos(1.0-cap_h_lv/r_lv) \
                  -(r_lv-cap_h_lv)*np.sqrt(2.0*r_lv*cap_h_lv-cap_h_lv*cap_h_lv)
        A_rv = r_rv*r_rv*np.pi

        return (A_rv-(capA_rv+capA_lv))*self._height

    def _theta_intersection(self, origin, r_lv, r_rv):

        inter = circle_intersections(self.lv_centre, self.rv_centre,
                                     r_lv, r_rv)

        rad0 = inter[0] - origin
        rad1 = inter[1] - origin

        theta0 = np.arctan2(rad0[1], rad0[0])
        theta1 = np.arctan2(rad1[1], rad1[0])

        # Sanity check
        # If the following fails, check that the assumption that the centres
        # and the provided origin have the same y coordinate holds
        assert theta0 == -theta1, 'Assumption failure'

        # Return the positive one
        return theta0 if theta0 > 0 else theta1

    @property
    def lvtheta_intersection_outer(self):
        return self._theta_intersection(self.lv_centre, self.r_outer_lv,
                                        self.r_outer_rv)

    @property
    def lvtheta_intersection_inner(self):
        return self._theta_intersection(self.lv_centre, self.r_outer_lv,
                                        self.r_inner_rv)

    def rvtheta_intersection(self, r_rv):
        return self._theta_intersection(self.rv_centre, self.r_outer_lv, r_rv)

    def _rv_layer_theta_range(self, i_trans):
        trans_norm = float(i_trans) / self._div_rvtrans
        r = self.r_inner_rv + trans_norm * self._thick_rv
        theta_start = self.rvtheta_intersection(r)
        theta_end = 2*np.pi - theta_start
        return theta_start, theta_end

    def _rv_layer_divisions(self, i_trans):

        # The commented code here is the important part for the

        #start, end = self._rv_layer_theta_range(0)
        #total_0 = end - start

        #start, end = self._rv_layer_theta_range(i_trans)
        #total_here = end - start

        ## Determine target theta step based on inner layer
        #target_theta_step = total_0 / self._div_rvfree

        ## Number of divisions for this layer
        #return int(round(total_here / target_theta_step))

        # Fudge
        return self._div_rvfree + i_trans

    def _rv_layer_thetas(self, i_trans):
        start, end = self._rv_layer_theta_range(i_trans)
        divs = self._rv_layer_divisions(i_trans)
        return np.linspace(start, end, divs + 1)

    def _generate_points(self):
        """
        Generate the points of the mesh.
        """

        coords = []

        # Determine all z values
        z_range = np.linspace(0., self._height, self._div_height + 1)

        # Determine all r values in the LV
        r_lv = np.linspace(self.r_inner_lv, self.r_outer_lv,
                           self._div_lvtrans + 1)

        # Determine all thetas in the LV
        theta_lvfree = np.linspace(-self.lvtheta_intersection_outer,
                                   self.lvtheta_intersection_outer,
                                   self._div_lvfree + 1)[:-1]
        theta_join1 = np.linspace(self.lvtheta_intersection_outer,
                                  self.lvtheta_intersection_inner,
                                  self._div_rvtrans + 1)[:-1]
        theta_septum = np.linspace(self.lvtheta_intersection_inner,
                                   2 * np.pi - self.lvtheta_intersection_inner,
                                   self._div_septum + 1)[:-1]
        theta_join2 = np.linspace(-self.lvtheta_intersection_inner,
                                  -self.lvtheta_intersection_outer,
                                  self._div_rvtrans + 1)[:-1]
        theta_lv_all = np.concatenate((theta_lvfree,
                                       theta_join1,
                                       theta_septum,
                                       theta_join2))

        # Generate all LV/septum/join points
        for theta in theta_lv_all:
            for z in z_range:
                for r in r_lv:
                    xy = self.lv_centre + r * np.array([np.cos(theta),
                                                        np.sin(theta)])
                    coords.append(np.concatenate((xy, [z])))

        # Determine all r values in the RV
        r_rv = np.linspace(self.r_inner_rv, self.r_outer_rv,
                           self._div_rvtrans + 1)

        # Generate all RV points
        for z in z_range:
            for i_trans, r in enumerate(r_rv):
                div_circ = self._rv_layer_divisions(i_trans)
                for i_circ, theta in enumerate(self._rv_layer_thetas(i_trans)):

                    if i_circ == 0 or i_circ == div_circ:
                        # Skip these nodes - corresponding points already exist
                        # in lv mesh
                        continue

                    xy = self.rv_centre + r * np.array([np.cos(theta),
                                                        np.sin(theta)])
                    coords.append(np.concatenate((xy, [z])))

        self._ptsarray = np.array(coords)

    def _section_points_start(self, section):
        """
        Determine the index of the first point of the specified section.

        Parameters
        ----------
        section : str
            One of ['lv', 'septum', 'rv', 'join1', 'join2']

        Returns
        -------
        int
            The first point of the specified section
        """

        # For convenience
        sec_pts = lambda circ, trns: circ * (trns + 1) * (self._div_height + 1)

        points = 0
        if section == 'lv':
            return points
        points += sec_pts(self._div_lvfree, self._div_lvtrans)
        if section == 'join1':
            return points
        points += sec_pts(self._div_rvtrans, self._div_lvtrans)
        if section == 'septum':
            return points
        points += sec_pts(self._div_septum, self._div_lvtrans)
        if section == 'join2':
            return points
        points += sec_pts(self._div_rvtrans, self._div_lvtrans)
        if section == 'rv':
            return points

    def _generate_elements(self):
        """
        Generate the mesh connectivity, tesselated into hexahedra and prisms.
        """

        self._clear_mixedelem_data()

        # Start with LV free wall
        start = self._section_points_start('lv')

        step_circ = (self._div_lvtrans + 1) * (self._div_height + 1)
        step_height = self._div_lvtrans + 1
        step_trans = 1

        # Work out offsets for a hex
        offsets = []
        for h in [0, step_height]:
            for c in [0, step_circ]:
                for t in [0, step_trans]:
                    offsets.append(h + c + t)
        offsets = np.array(offsets)

        blocks = [(self._section_points_start('lv'), self._div_lvfree,
                   ['lvepi', 'epi']),
                  (self._section_points_start('join1'), self._div_rvtrans,
                   []),
                  (self._section_points_start('septum'), self._div_septum,
                   ['rvendo']),
                  (self._section_points_start('join2'), self._div_rvtrans,
                   [])]

        wrap_size = self._section_points_start('rv')

        for start, div_circ, outside_labels in blocks:

            for i_circ in xrange(div_circ):
                for i_height in xrange(self._div_height):
                    for i_trans in xrange(self._div_lvtrans):

                        first = start + i_circ   * step_circ \
                                      + i_height * step_height \
                                      + i_trans  * step_trans

                        nodes = first + offsets

                        # Wrap nodes at end of ring on to start
                        while any(nodes >= wrap_size):
                            nodes[nodes >= wrap_size] -= wrap_size

                        self._mixedelem['hexa'].append(nodes)

                        if i_height == 0:
                            self._register_face('hexa', 'bottom')
                        if i_height == self._div_height - 1:
                            self._register_face('hexa', 'top')
                        if i_trans == 0:
                            self._register_face('hexa', 'front', 'lvendo')
                        if i_trans == self._div_lvtrans - 1:
                            for lab in outside_labels:
                                self._register_face('hexa', 'back', lab)

        # Now do the RV
        rv_start = self._section_points_start('rv')

        # Determine nodes per slice in rv
        step_slice = 0
        for i_trans in xrange(self._div_rvtrans + 1):
            # Number of nodes in each row is the number of divisions minus 1
            # (and not plus one as normal) as the first and last nodes in each
            # row are not generated - the equivalent nodes generated for the LV
            # are used instead to ensure the mesh is properly connected
            step_slice += self._rv_layer_divisions(i_trans) - 1

        for i_height in xrange(self._div_height):

            slice_start = rv_start + i_height * step_slice

            lower_start = slice_start - 1

            for i_trans in xrange(self._div_rvtrans):

                # Get number of divisions for the lower and upper rows
                div_lower = self._rv_layer_divisions(i_trans)
                div_upper = self._rv_layer_divisions(i_trans + 1)

                # Get the start of the upper row
                upper_start = lower_start + (div_lower - 1)

                # Get theta values
                theta_lower = self._rv_layer_thetas(i_trans)
                theta_upper = self._rv_layer_thetas(i_trans + 1)

                # Initialise counters
                i_lower = 0
                i_upper = 0

                def end_edge(section, i_circ_section):
                    """
                    Function to determine the nodes for an edge on the RV-LV
                    join.

                    Parameters
                    ----------
                    section : str
                        The name of the section to get the nodes from
                    i_circ_section : int
                        The i_circ index within the above section
                    """

                    # The i_trans index within this section will always be
                    # equal to the mesh's div_lvtrans parameter
                    i_trans_section = self._div_lvtrans

                    # Determine index-steps
                    step_height = self._div_lvtrans + 1
                    step_circ = (self._div_height + 1) * step_height

                    # Get the starting node for this section
                    start = self._section_points_start(section)

                    # Add on the necessary steps to get to the first node of the
                    # edge
                    start += i_circ_section * step_circ
                    start += i_height * step_height
                    start += i_trans_section

                    # The edge's second node is the equivalent one in the next
                    # height-level
                    edge = np.array([start, start + step_height])

                    # Wrap any nodes outside the LV into the LV
                    while any(edge >= wrap_size):
                        edge[edge >= wrap_size] -= wrap_size

                    return edge

                while i_lower < div_lower or i_upper < div_upper:

                    e_0 = [lower_start + i_lower,
                           lower_start + i_lower + step_slice]
                    e_1 = [upper_start + i_upper,
                           upper_start + i_upper + step_slice]

                    # Replace edges with existing nodes in the LV at the joins
                    if i_lower == 0:
                        e_0 = end_edge('join1', self._div_rvtrans - i_trans)
                    if i_upper == 0:
                        e_1 = end_edge('join1', self._div_rvtrans - i_trans - 1)
                    if i_lower == div_lower:
                        e_0 = end_edge('join2', i_trans)
                    if i_upper == div_upper:
                        e_1 = end_edge('join2', i_trans + 1)

                    # Determine which layer to take third edge from
                    if i_lower == div_lower:
                        # No lowers left, use upper
                        take_lower = False
                    elif i_upper == div_upper:
                        # No uppers left, use lower
                        take_lower = True
                    elif theta_lower[i_lower + 1] < theta_upper[i_upper + 1]:
                        # Lower is first in terms of angle
                        take_lower = True
                    else:
                        # Upper is first in terms of angle
                        take_lower = False

                    # Work out the third edge
                    if take_lower:
                        i_lower += 1
                        if i_lower == div_lower:
                            # Last edge in row, map to LV
                            e_2 = end_edge('join2', i_trans)
                        else:
                            e_2 = [lower_start + i_lower,
                                   lower_start + i_lower + step_slice]
                        etype = 'prism2'
                    else:
                        i_upper += 1
                        if i_upper == div_upper:
                            # Last edge in row, map to LV
                            e_2 = end_edge('join2', i_trans + 1)
                        else:
                            e_2 = [upper_start + i_upper,
                                  upper_start + i_upper + step_slice]
                        etype = 'prism1'

                    # Work out offset for a prism
                    nodes = np.concatenate((e_2, e_0, e_1))

                    self._mixedelem[etype].append(nodes)

                    if i_height == 0:
                        self._register_face(etype, 'front', 'bottom')
                    if i_height == self._div_height - 1:
                        self._register_face(etype, 'back', 'top')
                    if i_trans == 0 and take_lower:
                        self._register_face(etype, 'base', 'rvendo')
                    if i_trans == self._div_rvtrans - 1 and not take_lower:
                        self._register_face(etype, 'upperright', 'rvepi')
                        self._register_face(etype, 'upperright', 'epi')

                lower_start = upper_start

    def _bc_nodes(self):
        """
        Determine nodes at 90 degree intervals on bottom face for BCs.

        Returns
        -------
        list
            Indices of nodes at 0, 90, 180 and 270 degrees
        """

        def points_in_faces(face1, face2):

            pts_face1 = [pts.flatten() for _, pts in self.faces(face1)]
            pts_face1 = np.unique(np.concatenate(pts_face1))

            pts_face2 = [pts.flatten() for _, pts in self.faces(face2)]
            pts_face2 = np.unique(np.concatenate(pts_face2))

            return np.intersect1d(pts_face1, pts_face2)

        # LV Endo
        candidates_lv = points_in_faces('lvendo', 'bottom')
        coords_lv = self.points()[candidates_lv]
        theta_lv = np.arctan2(coords_lv[:, 1], coords_lv[:, 0])
        bc_lv = candidates_lv[closest_to_rangles(theta_lv)]

        # RV Endo
        candidates_rv = points_in_faces('rvendo', 'bottom')
        coords_rv = self.points()[candidates_rv]
        theta_rv = np.arctan2(coords_rv[:, 1], coords_rv[:, 0])
        bc_rv = candidates_rv[closest_to_rangles(theta_rv)]

        bc_nodes = [bc_lv[0], # +x
                    bc_lv[1], # +y
                    bc_rv[2], # -x
                    bc_lv[3]] # -y

        return bc_nodes

    def _element_transmural_distance(self):

        centres = self.element_centres()[:, :2]

        lv_r = np.linalg.norm(centres - self.lv_centre, axis=1)
        in_lv = lv_r <= self.r_outer_lv
        in_rv = np.logical_not(in_lv)

        lv_r = lv_r[in_lv]

        rv_centres = centres[in_rv]
        rv_r = np.linalg.norm(rv_centres - self.rv_centre, axis=1)

        lv_rnorm = (lv_r - self.r_inner_lv) / self._thick_lv
        rv_rnorm = (rv_r - self.r_inner_rv) / self._thick_rv

        trans = np.zeros(centres.shape[0])
        trans[in_lv] = lv_rnorm
        trans[in_rv] = rv_rnorm

        return trans

    def _element_circum_direction(self):

        centres = self.element_centres()[:, :2]

        lv_r = np.linalg.norm(centres - self.lv_centre, axis=1)
        in_lv = lv_r <= self.r_outer_lv
        in_rv = np.logical_not(in_lv)

        lv_centres = centres[in_lv]
        rv_centres = centres[in_rv]

        lv_rvec = lv_centres - self.lv_centre
        rv_rvec = rv_centres - self.rv_centre

        lv_rvec /= np.linalg.norm(lv_rvec, axis=1)[:, np.newaxis]
        rv_rvec /= np.linalg.norm(rv_rvec, axis=1)[:, np.newaxis]

        circ = np.zeros((centres.shape[0], 3))

        circ[in_lv, 0] = -lv_rvec[:, 1]
        circ[in_lv, 1] = lv_rvec[:, 0]

        circ[in_rv, 0] = -rv_rvec[:, 1]
        circ[in_rv, 1] = rv_rvec[:, 0]

        return circ

    def _element_apical_basal_direction(self):
        return np.array([0., 0., 1.])

if __name__ == '__main__':

    from carputils.mesh.fibres import linear_fibre_rule

    # Generate a plus 60 - minus 60 fibre rule
    p60m60 = linear_fibre_rule(60, -60)


    mesh = BiVSlice(25, 30, 10, 5, 5,
                    #tetrahedrise=False,
                    div_rvtrans=3,
                    div_height=3, div_lvtrans=3,#)#, div_rvtrans=5, div_lvtrans=10)
                    #separation=0.41)
                    fibre_rule=p60m60)
    mesh.generate_carp('biv', faces=['bottom', 'top', 'lvendo', 'rvendo',
                                     'lvepi',
                                     'rvepi',
                                     'epi'])
    mesh.generate_vtk('biv.vtk')
