"""
Classes and routines for the generation of geometric meshes.

For a detailed description of how to use the mesh generation functionality,
see :ref:`carputils-mesh`.
"""

__all__ = ['Mesh', 'Block', 'Cable', 'Grid', 'Ring', 'Ellipsoid', 'Pipe',
           'BoxRegion', 'SphereRegion', 'CylinderRegion',
           'generate', 'linear_fibre_rule', 'block_boundary_condition']

# Import all the geometry classes
from carputils.mesh.base import Mesh
from carputils.mesh.block import *
from carputils.mesh.cable import Cable
from carputils.mesh.grid import Grid
from carputils.mesh.ring import Ring
from carputils.mesh.ellipsoid import Ellipsoid
from carputils.mesh.pipe import Pipe

# Import the utility functions
from carputils.mesh.generate import generate
from carputils.mesh.region import *
from carputils.mesh.fibres import linear_fibre_rule
