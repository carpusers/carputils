import numpy as np
from carputils.mesh.general import Mesh2D

class Grid(Mesh2D):
    """
    Generate a two dimensional mesh of a regular grid.

    The generated mesh is aligned along the x-y plane, with z set to 0. Nodes
    are arranged on a regularly spaced grid and connected by quad elements. The
    mesh is defined by the x,y coordinates of the lower left and upper right
    corners and the mesh resolution, in mm.

    To generate a mesh from 0,0 to 10,10, with 0.1mm spacing:

    >>> geom = Grid((0,0), (10,10), 0.1)
    >>> geom.generate_carp('mesh/grid')

    Constant fibre vectors are set by the fibres optional command line
    argument, which defaults to be aligned with the positive x direction. The
    mesh may also be triangulated by use of the ``triangulate`` argument.


    For more information on defining mesh regions etc., please see the
    :ref:`mesh_generation` section of the documentation.

    Args:
        lowerleft : array-like
            The lower left (x,y) coordinate of the mesh, in mm
        upperright : array-like
            The upper right (x,y) coordinate of the mesh, in mm
        resolution : float or array-like, optional
            The resolution of the mesh in mm, either as a scalar for the same in
            both directions or as a list or array when in 2 directions, defaults to
            0.1
        fibres : array-like, optional
            Specify a fibre vector, defaults to (1.0, 0.0)
    """

    def __init__(self, lowerleft, upperright, resolution=0.1,
                 fibres=(1.0, 0.0), triangulate=False):

        Mesh2D.__init__(self, triangulate)

        self._lower = np.array(lowerleft, dtype=float)
        self._upper = np.array(upperright, dtype=float)
        self._res = np.array(resolution, dtype=float)

        assert len(fibres) in [2, 3]
        if len(fibres) == 2:
            fibres = np.concatenate((fibres, [0.]))
        self._fibres = fibres

        # See if resolution was a scalar
        if self._res.ndim == 0:
            self._res = np.array([self._res, self._res])

    def _num_divs(self):
        """
        Return the number of divisions in each direction in the mesh.

        Returns
        -------
        array
            The number of line segments/divisions in each direction
        """
        divs = (self._upper - self._lower) / self._res
        return np.array(np.ceil(divs), dtype=int)

    def _generate_points(self):
        """
        Generate the node points of the mesh and store internally.
        """

        n_x, n_y = self._num_divs() + 1

        x_vec = np.linspace(self._lower[0], self._upper[0], n_x)
        y_vec = np.linspace(self._lower[1], self._upper[1], n_y)

        coords = []
        for y_coord in y_vec:
            for x_coord in x_vec:
                coords.append((x_coord, y_coord, 0.))

        self._ptsarray = np.array(coords)

    def _generate_elements(self):
        """
        Generate the elements of the mesh and store internally.
        """

        d_x, d_y = self._num_divs()
        x_per_line = d_x + 1

        elems = []
        for i_y in range(d_y):
            for i_x in range(d_x):
                corner = i_y * x_per_line + i_x
                nodes = [corner,
                         corner + 1,
                         corner + x_per_line,
                         corner + x_per_line + 1]
                elems.append(nodes)

        self._mixedelem = {'quad': np.array(elems)}

    def _calc_fibres(self):
        """
        Generate the fibre vectors of the mesh and store internally.
        """
        self._lonarray = np.array([self._fibres] * self.n_elem())
