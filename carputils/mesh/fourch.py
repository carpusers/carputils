"""
Generate a simple four-chamber like geometry, providing a few parameters.
"""

from sys import version_info
import numpy as np

from carputils.mesh.general import  Mesh3D
from carputils.mesh.general import ELEM_NUM_NODES
from carputils.mesh.general import VTK_HEX_REORDER
from carputils.mesh.general import VTK_PRISM_REORDER
from carputils.mesh.general import VTK_CELL_TYPES
from carputils.mesh.general import TET_SUBDIVISIONS

from .ring import Ring
from .bivslice import BiVSlice

from .region import BoxRegion


# Python2 - Python3 compatibility
if version_info.major > 2:
    xrange = range


class FourChamber(Mesh3D):

    def __init__(self, r_lv, r_rv, thickness_lv, thickness_rv,
                 r_la, r_ra, thickness_la, thickness_ra,
                 height_biv, height_la, height_ra,
                 separation=1.0, gap=None,
                 div_freewall_lv=30, div_freewall_rv=25, div_septum=15,
                 div_trans_lv=3, div_trans_rv=2, div_trans_la=1, div_trans_ra=1,
                 div_circum_la = None, div_circum_ra=None,
                 div_height_biv=2, div_height_la=1, div_height_ra=1,
                 *args, **kwargs):

        Mesh3D.__init__(self, *args, **kwargs)

        # create BiV-slice mesh
        self._bivslice = BiVSlice(r_lv, r_rv, thickness_lv, thickness_rv,
                                  height_biv, separation, div_freewall_lv,
                                  div_freewall_rv, div_septum, div_trans_lv,
                                  div_trans_rv, div_height_biv, *args, **kwargs)

        # create LA-ring mesh
        self._laring = Ring(r_la, thickness_la, height_la, div_trans_la,
                            div_circum_la, div_height_la, *args, **kwargs)

        # create RA-ring mesh
        self._raring = Ring(r_ra, thickness_ra, height_ra, div_trans_ra,
                            div_circum_ra, div_height_ra, *args, **kwargs)

        self._gap_biv_la = height_la
        self._gap_la_ra = height_ra
        if gap is not None:
            self._gap_biv_la = float(gap)
            self._gap_la_ra = float(gap)

        self._bounding_boxes = None

    def __reorder_element_data(self, biv_data, la_data, ra_data):

        # make sure that all data sets have the same shape
        biv_shape, la_shape, ra_shape = biv_data.shape, la_data.shape, ra_data.shape
        assert len(biv_shape) == len(la_shape) == len(ra_shape)
        shape_len = len(biv_data.shape)
        assert shape_len == 1 or shape_len > 1 and biv_shape[1:] == la_shape[1:] == ra_shape[1:]

        biv_pos, la_pos, ra_pos = 0, 0, 0
        elem_data = []

        elem_types = self._mixedelem.keys()
        for eltype in elem_types:

            subdiv = len(TET_SUBDIVISIONS[eltype]) if self._tetrahedrise else 1

            biv_data_size = len(self._bivslice._mixedelem[eltype])*subdiv
            elem_data.extend(biv_data[biv_pos:biv_pos+biv_data_size])
            biv_pos += biv_data_size

            la_data_size = len(self._laring._mixedelem[eltype])*subdiv
            elem_data.extend(la_data[la_pos:la_pos+la_data_size])
            la_pos += la_data_size

            ra_data_size = len(self._raring._mixedelem[eltype])*subdiv
            elem_data.extend(ra_data[ra_pos:ra_pos+ra_data_size])
            ra_pos += ra_data_size

        return np.array(elem_data)

    def _generate_points(self):

        # generate BiV-slice points
        self._bivslice._generate_points()
        biv_points = self._bivslice._ptsarray
        biv_bbox = np.array([np.amin(biv_points, axis=0), np.amax(biv_points, axis=0)])

        # generate LA-ring points
        self._laring._generate_points()
        la_points = self._laring._ptsarray
        la_bbox = np.array([np.amin(la_points, axis=0), np.amax(la_points, axis=0)])

        # generate RA-ring points
        self._raring._generate_points()
        ra_points = self._raring._ptsarray
        ra_bbox = np.array([np.amin(ra_points, axis=0), np.amax(ra_points, axis=0)])

        # determine z-shift of LA-points and RA-points
        la_z_shift = self._bivslice._height + self._gap_biv_la
        ra_z_shift = la_z_shift + self._laring._height + self._gap_la_ra

        # get LV and RV centers
        lv_center = self._bivslice.lv_centre
        rv_center = self._bivslice.rv_centre

        # Translation for LA and RA
        la_translation = np.append(lv_center, la_z_shift)
        ra_translation = np.append(rv_center, ra_z_shift)

        la_bbox += la_translation
        ra_bbox += ra_translation
        self._bounding_boxes = [biv_bbox, la_bbox, ra_bbox]

        # Translate points for LA and RA and concatenate
        # all point clouds
        self._ptsarray = np.concatenate((biv_points, la_points+la_translation, ra_points+ra_translation), axis=0)


    def _generate_elements(self):

        # clear element cache and selection cache
        self._clear_mixedelem_data()

        # list holding all element types
        elem_types = self._mixedelem.keys()

        # generate BiV-slice elements and selections
        self._bivslice._generate_elements()
        biv_mixedelem = self._bivslice._mixedelem
        biv_mixedelem_selections = self._bivslice._mixedelem_selections
        # number of points in BiV-slice mesh
        biv_np = self._bivslice.n_pts()

        # generate LA-ring elements and selections
        self._laring._generate_elements()
        la_mixedelem = self._laring._mixedelem
        la_mixedelem_selections = self._laring._mixedelem_selections
        # number of points in LA-ring mesh
        la_np = self._laring.n_pts()
        # node-index shift factor for LA-node indices
        la_idxshift = biv_np

        # generate RA-ring elements and selections
        self._raring._generate_elements()
        ra_mixedelem = self._raring._mixedelem
        ra_mixedelem_selections = self._raring._mixedelem_selections
        # node-index shift factor for RA-node indices
        ra_idxshift = biv_np + la_np

        # dictionaries to hold the number of elements
        # for each element type
        biv_num_elems = {}
        la_num_elems = {}
        ra_num_elems = {}

        for eltype in elem_types:
            # number of nodes of current element type
            num_nodes = ELEM_NUM_NODES[eltype]

            # convert element lists of current type to numpy-arrays for all three geometries
            biv_mixedelem[eltype] = np.array(biv_mixedelem[eltype], dtype=int)
            la_mixedelem[eltype] = np.array(la_mixedelem[eltype], dtype=int)
            ra_mixedelem[eltype] = np.array(ra_mixedelem[eltype], dtype=int)

            # get element lists of current type for all three geometries
            biv_elems = np.array(biv_mixedelem.get(eltype, []), dtype=int)
            la_elems = np.array(la_mixedelem.get(eltype, []), dtype=int)
            ra_elems = np.array(ra_mixedelem.get(eltype, []), dtype=int)

            # get number of elements of current type for all three geometries
            biv_num_elems[eltype] = len(biv_elems)
            la_num_elems[eltype] = len(la_elems)
            ra_num_elems[eltype] = len(ra_elems)

            # reshape element lists if empty,
            # needed for concatenation
            if biv_elems.size == 0:
                biv_elems = biv_elems.reshape((0, num_nodes))
            if la_elems.size == 0:
                la_elems = la_elems.reshape((0, num_nodes))
            if ra_elems.size == 0:
                ra_elems = ra_elems.reshape((0, num_nodes))

            # shift node indices of all LA and RA elements and
            # concatenate element lists
            elements = np.concatenate((biv_elems, la_elems+la_idxshift, ra_elems+ra_idxshift), axis=0)

            # set element list
            self._mixedelem[eltype] = elements

        for eltype in elem_types:
            # get all selections for current element type
            biv_selections = biv_mixedelem_selections.get(eltype, {})
            la_selections = la_mixedelem_selections.get(eltype, {})
            ra_selections = ra_mixedelem_selections.get(eltype, {})

            # element index shift factor for LA and RA elements
            la_elem_idxshift = biv_num_elems[eltype]
            ra_elem_idxshift = la_elem_idxshift + la_num_elems[eltype]

            selection = {}
            for selname, sides in biv_selections.iteritems():
                # create new selection name for all BiV-slice selections
                name = 'biv'+selname
                # add selections
                selection[name] = sides

            for selname, sides in la_selections.iteritems():
                # create new selection name for all LA-ring selections
                name = 'la'+selname
                selection[name] = {}
                # iterate over all sides and shift element index
                for side, indices in sides.iteritems():
                    selection[name][side] = np.array(indices, dtype=int)+la_elem_idxshift

            for selname, sides in ra_selections.iteritems():
                # create new selection name for all RA-ring selections
                name = 'ra'+selname
                selection[name] = {}
                # iterate over all sides and shift element index
                for side, indices in sides.iteritems():
                    selection[name][side] = np.array(indices, dtype=int)+ra_elem_idxshift

            # set selection list
            self._mixedelem_selections[eltype] = selection

    def _element_transmural_distance(self):
        # get BiV-slice distances
        biv_dist = self._bivslice._element_transmural_distance()
        # get LA-ring distances
        la_dist = self._laring._element_transmural_distance()
        # get RA-ring distances
        ra_dist = self._raring._element_transmural_distance()
        # return concatenated distances
        return self.__reorder_element_data(biv_dist, la_dist, ra_dist)

    def _element_circum_direction(self):
        # get BiV-slice directions
        biv_dir = self._bivslice._element_circum_direction()
        # get LA-rint directions
        la_dir = self._laring._element_circum_direction()
        # get RA-rint directions
        ra_dir = self._raring._element_circum_direction()
        # return concatenated directions
        return self.__reorder_element_data(biv_dir, la_dir, ra_dir)

    def _element_apical_basal_direction(self):
        return np.array([0., 0., 1.])

    def _bc_nodes(self):
        biv_bc_nodes = self._bivslice._bc_nodes()
        biv_np = self._bivslice.n_pts()
        la_idxshift = biv_np
        la_bc_nodes = self._laring._bc_nodes() + la_idxshift
        la_np = self._laring.n_pts()
        ra_idxshift = biv_np + la_np
        ra_bc_nodes = self._raring._bc_nodes() + ra_idxshift
        return [biv_bc_nodes, la_bc_nodes, ra_bc_nodes]

    @property
    def biv_bounding_box(self):
        if self._bounding_boxes is None:
            self._generate_points()
        return self._bounding_boxes[0]

    @property
    def la_bounding_box(self):
        if self._bounding_boxes is None:
            self._generate_points()
        return self._bounding_boxes[1]

    @property
    def ra_bounding_box(self):
        if self._bounding_boxes is None:
            self._generate_points()
        return self._bounding_boxes[2]

    def generate_carp_rigid_dbc(self, basename):

        node_list = self._bc_nodes()
        prefix_list = ['biv', 'la', 'ra']

        for nodes, prefix in zip(node_list, prefix_list):

            # BC file for two nodes along the x axis
            fname = '{}_{}xaxis.vtx'.format(basename, prefix)

            with open(fname, 'w') as f_p:
                # Header
                f_p.write('2\nintra\n')
                # Content
                f_p.write('{}\n'.format(nodes[0]))
                f_p.write('{}\n'.format(nodes[2]))

            # BC file for one node on y axis
            fname = '{}_{}yaxis.vtx'.format(basename, prefix)

            with open(fname, 'w') as f_p:
                # Header
                f_p.write('1\nintra\n')
                # Content
                f_p.write('{}\n'.format(nodes[1]))


    def generate_vtk(self, filename, pointdata={}, celldata={}):
        """
        Generate VTK file of mesh.

        Parameters
        ----------
        filename : str
            Filename to write out to
        """

        f_p = open(filename, 'w')

        # Header
        f_p.write('# vtk DataFile Version 3.0\n')
        f_p.write('Auto generated ring model\n')
        f_p.write('ASCII\n')
        f_p.write('DATASET UNSTRUCTURED_GRID\n')

        # Write points
        pts = self.points()
        n_pnts = pts.shape[0]

        f_p.write('POINTS {} double\n'.format(n_pnts))
        for point in pts:
            f_p.write('{0[0]} {0[1]} {0[2]}\n'.format(point))

        # Write elements
        elem_groups = self.elements()
        n_elem = 0
        n_vals = 0
        for _, elems in elem_groups:
            n_elem += elems.shape[0]
            n_vals += elems.shape[0] * (elems.shape[1] + 1)

        f_p.write('CELLS {} {}\n'.format(n_elem, n_vals))
        for etype, elems in elem_groups:
            for elem in elems:
                f_p.write(str(len(elem)) + ' ')
                if etype == 'hexa':
                    elem = elem[VTK_HEX_REORDER]
                elif etype.startswith('prism'):
                    elem = elem[VTK_PRISM_REORDER]
                f_p.write(' '.join([str(i) for i in elem]) + '\n')

        # Write cell types
        f_p.write('CELL_TYPES {}\n'.format(n_elem))
        for etype, elems in elem_groups:
            celltype = '{}\n'.format(VTK_CELL_TYPES[etype])
            for i in xrange(elems.shape[0]):
                f_p.write(celltype)

        if len(pointdata) > 0:
            # Write out some data for visualisation
            f_p.write('POINT_DATA {}\n'.format(n_pnts))

            for name, data in pointdata.iteritems():
                if len(data.shape) == 1 and data.shape[0] == n_pnts:
                    f_p.write('SCALARS {} float 1\n'.format(name))
                    f_p.write('LOOKUP_TABLE default\n')
                    for value in data:
                        f_p.write('{0}\n'.format(value))
                elif len(data.shape) == 2 and data.shape[0] == n_pnts and data.shape[1] == 3:
                    f_p.write('VECTORS {} float\n'.format(name))
                    for value in data:
                        f_p.write('{0[0]} {0[1]} {0[2]}\n'.format(value))
                elif len(data.shape) == 2 and data.shape[0] == n_pnts and data.shape[1] == 9:
                    f_p.write('TENSORS {} float\n'.format(name))
                    for value in data:
                        f_p.write('{0[0]} {0[1]} {0[2]} {0[3]} {0[4]} {0[5]} {0[6]} {0[7]} {0[8]}\n'.format(value))
                elif len(data.shape) == 3 and data.shape[0] == n_pnts and data.shape[1] == data.shape[2] == 9:
                    f_p.write('TENSORS {} float\n'.format(name))
                    for value in data:
                        f_p.write('{0[0,0]} {0[0,1]} {0[0,2]} {0[1,0]} {0[1,1]} {0[1,2]} {0[2,0]} {0[2,1]} {0[2,2]}\n'.format(value))
                else:
                    print('Failed to write point data "{}", shape {} not supported!'.format(name, data.shape))
                    continue

        # Write out some data for visualisation
        f_p.write('CELL_DATA {}\n'.format(n_elem))

        # Write IDs
        elem_tags = self.element_tags()
        f_p.write('SCALARS IDs int 1\n')
        f_p.write('LOOKUP_TABLE default\n')
        for tag in elem_tags:
            f_p.write('{0}\n'.format(tag))

        # Write fibres
        lon = self.fibres()
        f_p.write('VECTORS fibres float\n')
        for vec in lon:
            f_p.write('{0[0]} {0[1]} {0[2]}\n'.format(vec))

        # Write sheets
        if lon.shape[1] >= 6:
            f_p.write('VECTORS sheets float\n')
            for vec in lon:
                f_p.write('{0[3]} {0[4]} {0[5]}\n'.format(vec))

        for name, data in celldata.iteritems():
            if len(data.shape) == 1 and data.shape[0] == n_elem:
                f_p.write('SCALARS {} float 1\n'.format(name))
                f_p.write('LOOKUP_TABLE default\n')
                for value in data:
                    f_p.write('{0}\n'.format(value))
            elif len(data.shape) == 2 and data.shape[0] == n_elem and data.shape[1] == 3:
                f_p.write('VECTORS {} float\n'.format(name))
                for value in data:
                    f_p.write('{0[0]} {0[1]} {0[2]}\n'.format(value))
            elif len(data.shape) == 2 and data.shape[0] == n_elem and data.shape[1] == 9:
                f_p.write('TENSORS {} float\n'.format(name))
                for value in data:
                    f_p.write('{0[0]} {0[1]} {0[2]} {0[3]} {0[4]} {0[5]} {0[6]} {0[7]} {0[8]}\n'.format(value))
            elif len(data.shape) == 3 and data.shape[0] == n_elem and data.shape[1] == data.shape[2] == 9:
                f_p.write('TENSORS {} float\n'.format(name))
                for value in data:
                    f_p.write('{0[0,0]} {0[0,1]} {0[0,2]} {0[1,0]} {0[1,1]} {0[1,2]} {0[2,0]} {0[2,1]} {0[2,2]}\n'.format(value))
            else:
                print('Failed to write cell data "{}", shape {} not supported!'.format(name, data.shape))
                continue

        f_p.close()


if __name__ == '__main__':

    from carputils.mesh.fibres import linear_fibre_rule

    # Generate a plus 60 - minus 60 fibre rule
    p60m60 = linear_fibre_rule(60, -60)

    r_lv = 25.0
    r_rv = 30.0
    r_la = 15.0
    r_ra = 20.0
    thickness_lv = 10.0
    thickness_rv = 5.0
    thickness_la = 3.0
    thickness_ra = 3.0
    height_biv = 15.0
    height_la = 12.5
    height_ra = 12.5

    mesh = FourChamber(r_lv, r_rv, thickness_lv, thickness_rv,
                       r_la, r_ra, thickness_la, thickness_ra,
                       height_biv, height_la, height_ra, gap=2.0,
                       fibre_rule=p60m60)

    surfaces = ['bivbottom', 'bivtop', 'bivlvendo', 'bivrvendo', 'bivlvepi', 'bivrvepi', 'bivepi',
                'latop', 'labottom', 'lainside', 'laoutside',
                'ratop', 'rabottom', 'rainside', 'raoutside']

    bbox_biv = mesh.biv_bounding_box
    bbox_la = mesh.la_bounding_box
    bbox_ra = mesh.ra_bounding_box

    reg_box_biv = BoxRegion(bbox_biv[0], bbox_biv[1], 1)
    reg_box_la = BoxRegion(bbox_la[0], bbox_la[1], 2)
    reg_box_ra = BoxRegion(bbox_ra[0], bbox_ra[1], 2)

    mesh.add_region(1, reg_box_biv)
    mesh.add_region(2, reg_box_la)
    mesh.add_region(3, reg_box_ra)

    mesh.generate_vtk('fourchmbr.vtk')
    """
    mesh.generate_carp_rigid_dbc('fourchmbr')
    mesh.generate_carp('fourchmbr', faces=surfaces)

    dist_trans = mesh._element_transmural_distance()
    dist_circum = mesh._element_circum_direction()

    mesh.generate_vtk('fourchmbr.vtk', celldata={'trans': dist_trans, 'circum': dist_circum})    
    for surface in surfaces:
        mesh.generate_vtk_face('fourchmbr_{}.vtk'.format(surface), surface)
    """


