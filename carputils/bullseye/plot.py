
import math
import matplotlib
import numpy as np
from matplotlib import pyplot
from matplotlib import colors
from matplotlib import colorbar
import matplotlib.patches as mpatches
from matplotlib.collections import PatchCollection
from mpl_toolkits.axes_grid1 import make_axes_locatable


carpcolmapRGB = colors.LinearSegmentedColormap('CARPCOLMAPRGB', {'red':   ((0.00, 1.0, 1.0), (0.25, 1.0, 1.0), (0.50, 0.0, 0.0), (1.00, 0.0, 0.0)),
                                                                 'green': ((0.00, 0.0, 0.0), (0.25, 1.0, 1.0), (0.75, 1.0, 1.0), (1.00, 0.0, 0.0)),
                                                                 'blue':  ((0.00, 0.0, 0.0), (0.50, 0.0, 0.0), (0.75, 1.0, 1.0), (1.00, 1.0, 1.0))})

carpcolmapBGR = colors.LinearSegmentedColormap('CARPCOLMAPBGR', {'blue':  ((0.00, 1.0, 1.0), (0.25, 1.0, 1.0), (0.50, 0.0, 0.0), (1.00, 0.0, 0.0)),
                                                                 'green': ((0.00, 0.0, 0.0), (0.25, 1.0, 1.0), (0.75, 1.0, 1.0), (1.00, 0.0, 0.0)),
                                                                 'red':   ((0.00, 0.0, 0.0), (0.50, 0.0, 0.0), (0.75, 1.0, 1.0), (1.00, 1.0, 1.0))})


class BEPlot(object):

    LINEWIDTH = 2.0

    def __init__(self, data, title=None, unit=None, fontsize=14.0,
                 labels=False, labelfontsize=12.0,
                 colorbar=False, colorbarlevels=0,
                 latex=False, showrv=False):

        self._data = np.array(data, dtype=float).squeeze()
        # make sure that we have enough data to plot
        assert len(self._data.shape) == 1 and self._data.shape[0] >= 17
        self._title = None if title is None else str(title)
        self._unit = None if unit is None else str(unit)
        self._fontsize = float(fontsize)
        self._labels = bool(labels)
        self._labelfontsize = float(labelfontsize)
        self._colorbar = bool(colorbar)
        self._colorbarlevels = int(colorbarlevels)
        self._latex = bool(latex)
        self._showrv = bool(showrv)

    @classmethod
    def fromfile(cls, filename, column=0, **kwargs):

        data = np.loadtxt(str(filename), dtype=float, usecols=column)
        return cls(data, **kwargs)

    def bullseye(self, figuresize=(6.0, 6.0), cmap=carpcolmapBGR):

        if self._latex:
            matplotlib.rc_params['text.latex.preamble'] = ['\\usepackage{gensymb}', '\\usepackage{amsmath}']
            matplotlib.rc_params['text.usetex'] = True
            matplotlib.rc_params['font.family'] = 'serif'

        fig = pyplot.figure(figsize=figuresize)
        if self._title:
            fig.suptitle(self._title, fontsize=self._fontsize, fontweight='bold')
        ax = fig.add_subplot(1, 1, 1)
        divider = make_axes_locatable(ax)

        # hide all the lines and tick-lines
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.spines['left'].set_visible(False)
        pyplot.setp(ax.get_xticklabels(), visible=False)
        pyplot.setp(ax.get_yticklabels(), visible=False)
        ax.xaxis.set_ticks_position('none')
        ax.yaxis.set_ticks_position('none')
        pyplot.rc('ytick', labelsize=self._labelfontsize)

        radius = 1.0
        linewidth = BEPlot.LINEWIDTH
        norm = colors.Normalize(vmin=self._data.min(), vmax=self._data.max())

        rvradius = 0.0
        rvxpos = radius
        if self._showrv:
            rvradius = 0.875*radius
            gamma = math.asin(math.sin(2.0*np.pi/3.0)*radius/rvradius)
            rvxpos = -(radius*math.cos(2.0*np.pi/3.0)-rvradius*math.cos(gamma))
            rvpatch = mpatches.Wedge((-rvxpos, 0.0), rvradius, gamma*180.0/np.pi, 360.0-gamma*180.0/np.pi, width=0.1, color='#AAAAAA', alpha=0.5, ec='none', zorder=-1.0)
            ax.add_patch(rvpatch)

        # draw areas
        patches = []
        for i in range(0, 6):
            patches.append(mpatches.Wedge((0.0, 0.0), radius, 60.0+i*60.0, 60.0+(i+1)*60.0, width=0.25))
        for i in range(0, 6):
            patches.append(mpatches.Wedge((0.0, 0.0), radius*0.75, 60.0+i*60.0, 60.0+(i+1)*60.0, width=1.0/3.0))
        for i in range(0, 4):
            patches.append(mpatches.Wedge((0.0, 0.0), radius*0.5, 45.0+i*90.0, 45.0+(i+1)*90.0, width=0.5))
        patches.append(mpatches.Circle((0.0, 0.0), radius*0.25))
        collection = PatchCollection(patches, cmap=cmap)
        collection.set_array(norm(self._data))
        ax.add_collection(collection)

        # draw edges
        ax.add_artist(pyplot.Circle((0.0, 0.0), radius*0.25, fill=False, edgecolor='#000000', lw=linewidth))
        ax.add_artist(pyplot.Circle((0.0, 0.0), radius*0.5, fill=False, edgecolor='#000000', lw=linewidth))
        ax.add_artist(pyplot.Circle((0.0, 0.0), radius*0.75, fill=False, edgecolor='#000000', lw=linewidth))
        ax.add_artist(pyplot.Circle((0.0, 0.0), radius, fill=False, edgecolor='#000000', lw=linewidth))
        for i in range(0, 4):
            x = math.cos((45.0+i*90.0)*np.pi/180.0)
            y = math.sin((45.0+i*90.0)*np.pi/180.0)
            ax.add_artist(pyplot.Line2D([radius*0.25*x, radius*0.5*x], [radius*0.25*y, radius*0.5*y], color='#000000', lw=linewidth, solid_capstyle='butt'))
        for i in range(0, 6):
            x = math.cos((60.0+i*60.0)*np.pi/180.0)
            y = math.sin((60.0+i*60.0)*np.pi/180.0)
            ax.add_artist(pyplot.Line2D([radius*0.5*x, radius*x], [radius*0.5*y, radius*y], color='#000000', lw=linewidth, solid_capstyle='butt'))

        limfac = 1.01
        ax.set_xlim([-limfac*(rvradius+rvxpos), limfac*radius])
        ax.set_ylim([-limfac*radius, limfac*radius])
        ax.set_aspect('equal')

        # draw labels
        if self._labels:
            rad1, rad2, rad3 = 3.0/8.0*radius, 5.0/8.0*radius, 7.0/8.0*radius
            ax.text(0.0, 0.0, '17', ha='center', va='center', fontsize=self._labelfontsize, clip_on=True)
            for i in range(0, 4):
                x = math.cos((90.0+i*90.0)*np.pi/180.0)
                y = math.sin((90.0+i*90.0)*np.pi/180.0)
                ax.text(rad1*x, rad1*y, str(13+i), ha='center', va='center', fontsize=self._labelfontsize, clip_on=True)
            for i in range(0, 6):
                x = math.cos((90.0+i*60.0)*np.pi/180.0)
                y = math.sin((90.0+i*60.0)*np.pi/180.0)
                ax.text(rad2*x, rad2*y, str(7+i), ha='center', va='center', fontsize=self._labelfontsize, clip_on=True)
                ax.text(rad3*x, rad3*y, str(1+i), ha='center', va='center', fontsize=self._labelfontsize, clip_on=True)

        # draw colorbar
        if self._colorbar:
            cbax = divider.append_axes('right', '5%', pad=0.05)
            ticks = None
            if self._colorbarlevels > 0:
                bounds = np.linspace(self._data.min(), self._data.max(), num=self._colorbarlevels+1, endpoint=True)
                norm = colors.BoundaryNorm(bounds, cmap.N)
                tickstepsize = int(math.ceil(self._colorbarlevels/10.0))
                ticks = 0.5*(bounds[:-1]+bounds[1:])[::tickstepsize]

            cb = colorbar.ColorbarBase(cbax, cmap=cmap, norm=norm, orientation='vertical')
            if self._unit is not None:
                cbax.set_xlabel(self._unit, fontsize=self._labelfontsize)

            if ticks is not None:
                cb.set_ticks(ticks)

        return fig
