"""
Load the my_switches.def settings from the CARP repository
"""

import subprocess
from warnings import warn

from carputils.settings.namespace import SettingsNamespace
from carputils.settings.exceptions import *


class CARPUtilsMissingMakevar(CARPUtilsSettingsError):
    def __init__(self, makevar):
        tpl = ('makevar "{}" missing from carp -buildinfo - maybe you need to '
               'update CARP and rebuild')
        super(CARPUtilsMissingMakevar, self).__init__(tpl.format(makevar))

def revision(execs):
    """
    Return the SVN revision number of the CARP executable.

    Returns
    -------
    int
        The revision number
    """

    cmd = [str(execs.CARP), '-buildinfo']
    revisions = {}

    output = subprocess.check_output(cmd).decode('utf8').split('\n')

    # check revision output for specific keywords, raise error otherwise
    for line in output:
        if 'hash' in line:
            break

    # default CARP may use SVN or GIT versioning, we check for both
    if ('svn revision' in line) or ('git commit' in line) or ('GIT' in line):
        revisions['base'] = str(line.split()[-1])
        return revisions
    else:
        raise CARPUtilsSettingsError('Could not correctly detect CARP revision')

def dependency_revisions(execs):

    cmd = [str(execs.CARP), '-buildinfo']
    revisions = {}

    output = subprocess.check_output(cmd).decode('utf8').split('\n')

    for line in output:
        if 'dependency' in line:
            break
    else:
        raise CARPUtilsSettingsError('Could not correctly determine CARP '
                                     'dependency revisions')

    #if there are no dependency plugins then string contains "revisions:"
    string = line.split()[-1]
    revisions = {}

    for part in string.split(','):
        # check if part can be split in key-value pairs
        if part.count('=') == 1:
            key, val = part.split('=')
            revisions[key] = str(val)

    return revisions

def makevars(execs):

    makevars = SettingsNamespace(errortype=CARPUtilsMissingMakevar)

    # consider no plugins to be available, which is the default for openCARP
    makevars.MECH = 0
    makevars.LIMPET_CUDA = 0
    makevars.FEMLIB_CUDA = 0
    makevars.PT_CUDA = 0
    makevars.EIKONAL = 0
    makevars.FLUID = 0
    makevars.CIRCADAPT = 0
    makevars.FSM = 0
    makevars.WITH_PURK = 0
     
    cmd = [str(execs.CARP), '-buildinfo']
    try:
        output = subprocess.check_output(cmd).decode('utf8').split('\n')

    except subprocess.CalledProcessError:
        warn('Error retrieving -buildinfo from CARP - assuming no mechanics '
             'or CUDA')
        return makevars

    for line in output:
        if 'make flags' not in line:
            continue

        string = line.split(': ')[-1].strip()

        for part in string.split(','):
            key, value = part.strip().split('=')
            makevars[key] = bool(int(value))

    return makevars
