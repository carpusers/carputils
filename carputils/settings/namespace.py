#!/usr/bin/env python

import os
import sys
from collections import OrderedDict
from contextlib import contextmanager
from carputils.settings.exceptions import CARPUtilsMissingPathError

isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False

class SettingsNamespace(object):
    """
    Settings namespace object

    Objects of this class provide a case-insensitive namespace, which can be
    access either through attributes or as a dict. For example, an object
    created like:

    >>> sn = SettingsNamespace()

    can then be assigned values like::

    >>> sn.MYPATH = '/some/path'
    >>> sn['otherpath'] = '/other/path'

    and accessed like:

    >>> print sn['MYPATH']
    '/some/path'
    >>> print sn.otherpath
    '/other/path'

    Attributes/keys are case insensitive:

    >>> print sn.mypath
    '/some/path'
    >>> print sn['OtherPath']
    '/other/path'

    Use of the attribute method of accessing values is recommended, as this
    reflects the object's intended use as a container of 'constants' in the
    rest of the code. dict-like access is implemented for backwards
    compatibility.

    Parameters
    ----------
    missing_error : bool, optional
        Raise an Exception when a requested attribute is missing or None
        (default: True)
    errortype : Exception, optional
        Type of exception to raise when an item is missing or None (default;
        AttributeError)
    """

    def __init__(self, missing_error=True, errortype=AttributeError):

        self._data = OrderedDict()
        self._missingerr = missing_error
        self._exception = errortype

    def get(self, key, fallback=None):
        """
        Get an item without raising errors when missing, like dict().get()

        Parameters
        ----------
        key : str
            Key of item to return (case-insensitive)
        fallback : object, optional
            Value to return when key is missing (default: None)
        """
        return self._data.get(key.upper(), fallback)

    def __getattr__(self, attr):
        """
        Custom attribute access code providing case-insensitivity.
        """

        # Case insensitive
        attr = attr.upper()

        val = self._data.get(attr)

        if val is None:
            return val
        elif isinstance(val, (str, int, dict, bool, list)):
            return val

        if self._missingerr:
            raise self._exception(attr)

        return val

    def __getitem__(self, key):
        """
        Allow object to be used like dict
        """
        return getattr(self, key)

    def __setattr__(self, attr, value):
        """
        Custom attribute access code providing case-insensitivity and order
        logging.
        """

        # Any 'private' variables with leading underscore should be left alone
        if attr.startswith('_'):
            self.__dict__[attr] = value

        else:
            # Case insensitive
            attr = attr.upper()
            self._data[attr] = value

    def __setitem__(self, key, value):
        """
        Allow object to be used like dict
        """
        setattr(self, key, value)

    def __iter__(self):
        """
        Return iterator over settings
        """
        if isPy2: it = self._data.iteritems
        else:     it = self._data.items

        for key, item in it():
            if isinstance(item, Exception):
                yield key, None
            else:
                yield key, item

    def __str__(self):
        rep_str = ''
        for key, value in self.__iter__():
            rep_str += '{0:<20s} {1}\n'.format(key+':', value)
        return rep_str
