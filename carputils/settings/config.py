"""
Functionality for the parsing of a carputils settings file
"""

import os
import sys
import errno
import subprocess
from collections import OrderedDict

from carputils import cml
from carputils.settings.solver import MECH_ELEMENT
from carputils.settings.namespace import SettingsNamespace
from carputils.settings.exceptions import *
from carputils import resources

BACKWARDS_COMPAT_MAPPING = {'MPI_EXEC':       'MPIEXEC',
                            'HW_DEFAULT':     'PLATFORM',
                            'SILENT_DEFAULT': 'SILENT',
                            'VIS_DEFAULT':    'VISUALIZE',
                            'FLAVOR_DEFAULT': 'FLAVOR',
                            'DIRECT_DEFAULT': 'DIRECT_SOLVER',
                            'PURK_DEFAULT':   'PURK_SOLVER',
                            'LIMPET_GUI_DIR': 'LIMPETGUI_DIR'}

BACKWARDS_COMPAT_REMOVED = ['HOME',
                            'CARP_TYPE',
                            'WITH_MECH',
                            'DEBUG']

VALIDATION_SINGLE = {'MESHALYZER_DIR':  str,
                     'LIMPETGUI_DIR':   str,
                     'TUNING_DIR':      str,
                     'MESH_DIR':        str,
                     'MESHTOOL_DIR':    str,
                     'REGRESSION_TEMP': str,
                     'REGRESSION_REF':  str,
                     'MPIEXEC':         str,
                     'SILENT':          bool,
                     'VISUALIZE':       bool,
                     'FLAVOR':          str,
                     'PLATFORM':        str,
                     'BUILD':           str,
                     'DIRECT_SOLVER':   str,
                     'PURK_SOLVER':     str,
                     'EMAIL':           str,
                     'MAX_NP':          int}

VALIDATION_SEQUENCE = {'CARP_EXE_DIR':   str,
                       'REGRESSION_PKG': str}

PATHS = ['CARP_EXE_DIR',
         'MESHALYZER_DIR',
         'LIMPETGUI_DIR',
         'TUNING_DIR',
         'MESH_DIR',
         'REGRESSION_TEMP',
         'REGRESSION_REF']

NOSETTINGS_TEMPLATE = """ERROR: No carputils settings file was found.
The resolution order is:
    - The full path (not just the directory) set by the environment variable
      CARPUTILS_SETTINGS
    - ./settings.yaml
    - ~/.config/carputils/settings.yaml
    - settings.yaml in the root of the carputils repository
The following paths were tried:
    - {}
Creating your settings file at ~/.config/carputils/settings.yaml is recommended.
To do so, please run:
    cusettings ~/.config/carputils/settings.yaml
"""

def carputils_root_dir():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), '..', '..'))

def filename():
    """
    Determine carputils settings file name

    Returns
    -------
    str
        The settings file path
    """
    repo_root = carputils_root_dir()
    candidates = []

    try:
        candidates.append(os.environ['CARPUTILS_SETTINGS'])
    except KeyError:
        msg  = '\n\nCould not find environment variable $CARPUTILS_SETTINGS\n'
        msg += 'The env variable should point to {}!'.format(os.path.join(repo_root, 'settings.yaml'))
        msg += '\n\n'
        sys.stdout.write(msg)
        pass

    candidates += ['./settings.yaml',
                   os.path.expanduser('~/.config/carputils/settings.yaml'),
                   os.path.join(repo_root, 'settings.yaml')]

    for fname in candidates:
        if os.path.exists(fname):
            return fname

    msg = NOSETTINGS_TEMPLATE.format('\n    - '.join(candidates))
    sys.stderr.write(msg)
    sys.exit(errno.ENOENT) # No such file or directory

def assert_type(name, value, type, sequence=False):
    """
    Raise an error if the value is not of the correct type

    Parameters
    ----------
    name : str
        Name of the variable, used for error messages
    value : object
        Object to test type of
    type : type
        Type to check value is
    sequence : bool, optional
        If True, expect value to be a sequence of type
    """

    if not sequence:
        if isinstance(value, type):
            return
    else:
        if isinstance(type, list):
            # there is a list of possible datatypes available
            if (hasattr(type, '__iter__') # list of possible datatypes
              and any([isinstance(value, t) for t in type])):
              return

        if isinstance(value, dict):
            # Check values
            value = value.values()
        if (hasattr(value, '__iter__') # Is a sequence
            and all([isinstance(e, type) for e in value])):
            return

    tpl = 'settings.yaml: {} should be a {}{}'
    msg = tpl.format(name, 'sequence of ' if sequence else '', type)
    raise CARPUtilsSettingsError(msg)

def load():
    """
    Load the settings file

    Returns
    -------
    SettingsNamespace
        The loaded settings
    """

    # Load data from file
    fname = filename()
    with open(fname) as fp:
        data = cml.load(fp)

    # Determine directory
    dirname = os.path.dirname(fname)

    # Path resolution function
    pathresolve = lambda p: os.path.abspath(os.path.join(dirname, os.path.expandvars(p)))

    # Check if settings file needs update
    old_fields = BACKWARDS_COMPAT_REMOVED + list(BACKWARDS_COMPAT_MAPPING)
    old_fields += ['CARP_DIR', 'CARP_GPU_DIR']
    for field in old_fields:
        if field in data:
            tpl = ('Your settings file is out of date - please run '
                   '"cusettings {}" (found in carputils/bin) to update and '
                   'try again.')
            raise CARPUtilsSettingsError(tpl.format(fname))

    # Create namespace instance
    err = lambda a: CARPUtilsMissingSettingError(a, fname)
    obj = SettingsNamespace(errortype=err)

    # Validate settings and add them to Settings instance
    for name, value in data.items():

        if name in VALIDATION_SINGLE:
            assert_type(name, value, VALIDATION_SINGLE[name])

        elif name in VALIDATION_SEQUENCE:
            assert_type(name, value, VALIDATION_SEQUENCE[name], True)

        if name in PATHS:
            if isinstance(value, OrderedDict):
                for key in value:
                    value[key] = pathresolve(value[key])
            elif isinstance(value, list):
                value = [pathresolve(v) for v in value]
            else:
                value = pathresolve(value)

        obj[name] = value

    # set some default values
    obj['CARPUTILS_ROOT_DIR'] = carputils_root_dir()
    obj['MECH_ELEMENT'] = str(MECH_ELEMENT[0])

    return obj

def default_settings(software_root=None, regression_ref=None,
                     regression_pkg=['devtests', 'benchmarks'],
                     email=None):
    """
    Determine the default configuration options for the settings.yaml file

    Parameters
    ----------
    software_root : str, optional
        Default path for built software packages
    regression_ref : str, optional
        Location of regression reference solution repositories
    regression_pkg : list, optional
        Default packages to load tests from

    Returns
    -------
    dict
        The default settings
    """

    if software_root is None:
        software_root = os.path.join(os.environ['HOME'], 'software')
    if regression_ref is None:
        regression_ref = os.path.join(os.environ['HOME'], 'data/carp-tests-reference')

    fields = {}

    # Defaults for paths
    # CARP
    carpexe = OrderedDict()
    carpexe['CPU'] = os.path.join(software_root, 'carp', 'bin')
    carpexe['GPU'] = os.path.join(software_root, 'carp-gpu', 'bin')
    fields['CARP_EXE_DIR'] = carpexe
    # Others
    fields['MESHALYZER_DIR'] = os.path.join(software_root, 'meshalyzer')
    fields['LIMPETGUI_DIR']  = os.path.join(software_root, 'limpetGUIpyQt')
    fields['TUNING_DIR']     = os.path.join(software_root, 'pyUtils')
    fields['MESH_DIR']       = os.path.join(software_root, 'carp-meshes')
    fields['MESHTOOL_DIR']   = carpexe['CPU']

    # Setting defaults
    fields['MPIEXEC']       = 'mpiexec'
    fields['SILENT']        = False
    fields['VISUALIZE']     = False
    fields['FLAVOR']        = 'pt'
    fields['DIRECT_SOLVER'] = 'MUMPS'
    fields['PURK_SOLVER']   = 'GMRES'
    fields['PLATFORM']      = 'desktop'
    fields['BUILD']         = 'CPU'
    fields['MAX_NP']        = None

    # Regression settings
    fields['REGRESSION_REF']  = regression_ref
    fields['REGRESSION_PKG']  = regression_pkg
    fields['REGRESSION_TEMP'] = '/scratch/temp'

    # Personal information
    if email is None:
        try:
            cmd = ['git', 'config', '--get', 'user.email']
            email = subprocess.check_output(cmd)
        except subprocess.CalledProcessError:
            email = 'you@example.com'
    fields['EMAIL'] = email

    return fields

def new(filename, *args, **kwargs):
    """
    Generate a new settings file with the default settings

    Extra arguments are passed through to default_settings

    Parameters
    ----------
    filename : str
        The path of the settings file to generate
    """
    dirname = os.path.dirname(filename)
    if len(dirname) and not os.path.exists(dirname):
        os.makedirs(dirname)
    write_settings_yaml(default_settings(*args, **kwargs), filename)

def update(filename):
    """
    Update the keys in the settings file using the backwards compatability list

    Parameters
    ----------
    filename : str
        The path of the settings file to update
    """

    # Load current settings
    with open(filename) as fp:
        settings = cml.load(fp)

    # Load default settings
    new = default_settings()

    # Handle CARP_EXE_DIR backwards compatability first
    if 'CARP_DIR' in settings or 'CARP_GPU_DIR' in settings:

        exedir = new['CARP_EXE_DIR']

        if 'CARP_DIR' in settings:
            exedir['CPU'] = os.path.join(settings['CARP_DIR'], 'bin')
            del settings['CARP_DIR']

        if 'CARP_GPU_DIR' in settings:
            exedir['GPU'] = os.path.join(settings['CARP_GPU_DIR'], 'bin')
            del settings['CARP_GPU_DIR']

    for key, value in settings.items():

        # Removed settings
        if key in BACKWARDS_COMPAT_REMOVED:
            continue

        # Renamed settings
        if key in BACKWARDS_COMPAT_MAPPING:
            new[BACKWARDS_COMPAT_MAPPING[key]] = settings[key]
            continue

        new[key] = settings[key]

    # Write out file
    write_settings_yaml(new, filename)

def yaml_format_seq(seq):
    """
    Format a sequence in YAML syntax
    """
    if len(seq) == 0:
        return ' []'
    string = ''
    for item in seq:
        string += '\n    - {}'.format(item)
    return string

def yaml_format_dict(seq):
    """
    Format a dictionary in YAML syntax
    """
    if len(seq) == 0:
        return ' {}'
    string = ''
    for key, value in seq.items():
        string += '\n    {}: {}'.format(key, value)
    return string

def write_settings_yaml(fields, filename):
    """
    Write the settings file with the provided fields.

    Parameters
    ----------
    fields : dict
        Data to insert into configuration file template
    filename : str
        Filename to write file to
    """
    # Format entries in YAML syntax
    for key, value in fields.items():
        # Booleans
        if isinstance(value, bool):
            fields[key] = 'Yes' if value else 'No'
        # Lists/Tuples
        elif isinstance(value, (list, tuple)):
            fields[key] = yaml_format_seq(value)
        # Dictionaries/OrderedDict
        elif isinstance(value, dict):
            fields[key] = yaml_format_dict(value)

    # Load Template
    content = resources.load('settings.yaml.tpl')

    # Insert data
    content = content.format(**fields)

    # Write out file
    with open(filename, 'w') as fp:
        fp.write(content)
