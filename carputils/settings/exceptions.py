
class CARPUtilsSettingsError(Exception):
    pass

class CARPUtilsMissingSettingError(CARPUtilsSettingsError):
    def __init__(self, setting, path, *args, **kwargs):
        msg = ('The required setting \'{}\' is missing from your settings file'
               ' (path: {})').format(setting, path)
        Exception.__init__(self, msg)

class CARPUtilsMissingPathError(CARPUtilsSettingsError):
    def __init__(self, attr, path=None, *args, **kwargs):
        msg = 'The path for {} was not found'.format(attr)
        if path is not None:
            msg += ' ({})'.format(path)
        Exception.__init__(self, msg)

class CARPUtilsMissingLicenseError(CARPUtilsSettingsError):
    def __init__(self, module, *args, **kwargs):
        msg =  ('\n\nChosen set of parameters requires the \'{}\' option to be enabled.\n').format(module)
        msg += ('Please recompile CARPentry or apply for the missing licence!\n')
        Exception.__init__(self, msg)

class CARPUtilsSettingsWarning(UserWarning):
    pass

