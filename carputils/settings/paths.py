
import os
import shutil
import sys
from carputils.settings.namespace import SettingsNamespace
from carputils.settings.exceptions import *

if sys.version_info.major > 2:
    isPy2 = False
else:
    isPy2 = True

class Path(object):

    def __init__(self, *parts):
        self._parts = parts

    def exists(self):
        return os.path.exists(str(self))

    def __str__(self):
        return os.path.join(*self._parts)

    def __add__(self, other):
        return Path(*(self._parts + other._parts))

    def __iadd__(self, other):
        self._parts += other._parts
        return self

class PathSettingsNamespace(SettingsNamespace):
    """
    Modified namespace with extra error handling for paths
    """

    def __init__(self, missing_error=True):
        SettingsNamespace.__init__(self, missing_error, CARPUtilsMissingPathError)

    def __getattr__(self, attr):
        # Case insensitive
        attr = attr.upper()

        path = self._data.get(attr)

        if self._missingerr:
            if path is None:
                raise self._exception(attr)
            if not path.exists():
                raise self._exception(attr, path)

        return path

def join_add(*parts):
    """
    Build path from supplied parts using os.path.join for OS independence.
    Returns None if the specified path does not exist, otherwise returns the
    path.
    """

    if None in parts:
        return None

    path = os.path.join(*parts)

    if os.path.exists(path):
        return path
    else:
        return None

def carp_executable(exedir, start='carp.'):
    """
    Select the most recent CARPentry/bench/etc. executable in the exe dir

    Parameters
    ----------
    exedir : str or Path
        The path of the CARPentry bin directory
    start : str, optional
        The beginning of the executable name (default: 'carpentry.')

    Returns
    -------
    str
        The path of the file (or link) to the most recently built executable
        starting with the specified string
    """

    exedir = str(exedir)

    candidates = []

    for fname in os.listdir(exedir):

        start_lc = start.lower()
        fname_lc = fname.lower()
        if not start_lc in fname_lc:
            continue

        # Resolve links
        real = os.path.realpath(os.path.join(exedir, fname))

        # Check is an executable
        if not os.path.exists(real) or not os.access(real, os.X_OK):
            continue

        # Store orig path and exe mod time
        candidates.append((fname, os.path.getmtime(real)))

    if len(candidates) == 0:
        # No suitable executable, return cleaned dummy exe name
        return start.rstrip('.')

    # Pick newest executable
    candidates = sorted(candidates, key=lambda t: t[1])
    return candidates[-1][0]

def dirs(config, cli):
    """
    Determine the directories to look for executables.
    """

    build = getattr(cli, 'build', config.BUILD)

    dirs = PathSettingsNamespace()
    dirs.CARP       = Path(config.CARP_EXE_DIR[build])
    dirs.MESHTOOL   = Path(config.MESHTOOL_DIR)
    dirs.MESHALYZER = Path(config.MESHALYZER_DIR)
    dirs.LIMPETGUI  = Path(config.LIMPETGUI_DIR)
    dirs.TUNING     = Path(config.TUNING_DIR)
    dirs.CARPUTILS  = Path(config.CARPUTILS_ROOT_DIR)

    return dirs

def execs(config, dirs):
    """
    Generate the executable paths for the current run configuration
    """

    # Prepare namespace
    execs = PathSettingsNamespace()

    # CARP
    carp = carp_executable(dirs.get('CARP'), start='carp')
    execs.CARP   = dirs.get('CARP') + Path(carp)
    execs.MESHER = dirs.get('CARP') + Path('mesher')
    execs.FSM = dirs.get('CARP') + Path('fsm_fcc')

    # LIMPET
    bench = carp_executable(dirs.get('CARP'), 'bench.')
    execs.BENCH = dirs.get('CARP') + Path(bench)

    # FEMLIB
    for name in ['GlInterpolate', 'GlGradient', 'GlElemCenters',
                 'GlMeshConvert', 'GlVTKConvert', 'GlTransfer',
                 'GlRuleFibers', 'GlRuleFibersMult', 'EllipticSolver',
                 'ParabolicSolver', 'AdjointSolver', 'GlFilament',
                 'GlIGBProcess', 'SurfaceMesh', 'GlSurfaceConvert',
                 'correspondance']:
        execs[name] = dirs.get('CARP') + Path(name)

    # Elasticity
    execs.ELASTICITY = dirs.get('CARP') + Path('ElasticityTest')

    # FluidTest
    execs.FLUIDSOLVE = dirs.get('CARP') + Path('FluidSolve')

    # FluidTest
    execs.CVSTOOL = dirs.get('CARP') + Path('cvstool')

    # IGB Utils
    for name in ['igbhead', 'igbops', 'igbextract', 'igbapd', 'igbdft']:
        execs[name] = dirs.get('CARP') + Path(name)

    # Meshalyzer
    execs.MESHALYZER = dirs.get('MESHALYZER') + Path('meshalyzer')

    # MODH5 -> convert carp models to hdf5 format
    execs.MODH5 = dirs.get('MESHALYZER') + Path('utils/modh5conv/bin/modh5conv')

    # DATH5 -> convert carp data to hdf5 format
    execs.DATH5 = dirs.get('MESHALYZER') + Path('utils/package_data/package_data')

    # LIMPET_GUI
    execs.LIMPETGUI = dirs.get('LIMPETGUI') + Path('main.py')
    execs.SV2H5B = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'bin2h5.py'))
    execs.SV2H5T = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'txt2h5.py'))

    # MISC
    execs.CLOSEST_HC = dirs.get('CARPUTILS') + Path(os.path.join('bin', 'closest_hc'))

    # EIKONAL
    execs.EKBATCH = dirs.get('CARP') + Path('ekbatch')
    execs.EKSOLVE = dirs.get('CARP') + Path('eksolve')

    # MESHTOOL
    execs.MESHTOOL = dirs.get('MESHTOOL') + Path('meshtool')

    # LIMPET code generation
    execs.LIMPETFE = dirs.get('CARP') + Path('limpet_fe.py')
    execs.MAKEDYNAMICMODEL = dirs.get('CARP') + Path('make_dynamic_model.sh')

    # CARPUTILS
    execs.TUNECV = dirs.get('TUNING') + Path('tuneCV')

    # BEM
    execs.BEMECG = dirs.get('CARP') + Path('bem-ecg')

    return execs
