
import sys
from carputils.settings.settings import SettingsModule

# Replace this module with the Settings instance
# This hack is 'officially' supported, as outlined in this message by Gudio van
# Rossum, the creator of Python:
#       https://mail.python.org/pipermail/python-ideas/2012-May/014969.html
sys.modules[__name__] = SettingsModule()
