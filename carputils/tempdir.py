"""
Code to help with devising temporary directory names.
"""

import tempfile

from carputils import settings
from carputils.settings.exceptions import CARPUtilsSettingsError

def tempdir(suffix=''):
    """
    Generate a temporary directory with the specified suffix.
    """

    try:
        # Attempt to load regression temporary directory from settings file
        root = settings.config.REGRESSION_TEMP
    except CARPUtilsSettingsError:
        # No root temporary directory specified, use system dir
        tempdir = tempfile.mkdtemp(suffix=suffix)
    else:
        # Temp dir is specified, use it!
        tempdir = tempfile.mkdtemp(suffix=suffix, dir=root)
    
    return tempdir
