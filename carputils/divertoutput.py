"""
Execute external processes with output buffering and error checking.
"""

from __future__ import print_function
import sys
import subprocess
import errno
import threading
from contextlib import contextmanager
from functools import wraps

_subprocess_raise_exceptions_counter = 0

class DivertOutputError(Exception):
    pass

def intercept_OSError(func):
    """
    Decorator to intercept file not found OSErrors
    """
    @wraps(func)
    def wrapped(cmd, *args, **kwargs):
        try:
            return func(cmd, *args, **kwargs)
        except OSError as err:
            if err.errno == errno.ENOENT:
                # Executable not found
                tpl = 'the executable "{}" was not found'
                raise DivertOutputError(tpl.format(cmd[0]))
            else:
                raise
    return wrapped

@intercept_OSError
def call(cmd, interactive=False, timeout=None, detached=False,
         stdout=None, stderr=None, *args, **kwargs):
    """
    Run the specified command.

    This function manually captures the output and writes it to sys.stdout,
    meaning that replacing sys.stdout with a StringIO stream can be used to
    handle combined subprocess and python output more easily.

    Example usage::

        from carputils import divertoutput
        divertoutput.call(['ls', '-ltr', 'mydir/'])

    When used with the :func:`subprocess_exceptions` context manager, checks
    the return code of the finished process and raises an exception if it did
    not exit correctly.

    See also the :func:`carputils.stream.divert_std` context manager, which
    allows control of the destination of the process output streams.

    Args:
        cmd : list of str
            List of command line arguments to execute.
    """

    # Ensure all strings
    #cmd = [str(o) for o in cmd]
    cmd_mod = []
    for i, s in enumerate(cmd):
        command = str(s)
        # remove leading apostrophes for popen
        if (command.startswith('"') or command.startswith("'")):
            command = command.replace("'", "")
            command = command.replace('"', "")
        cmd_mod.append(command)

    cmd = cmd_mod

    if interactive:
        # Execute the command without pipes or threading to ensure interactive
        # shell works correctly (in debugger, etc)
        proc = subprocess.Popen(cmd, stderr=stderr, *args, **kwargs)
        proc.wait()

    elif detached:
        # Don't wait for it to finish
        subprocess.Popen(cmd, stderr=stderr, *args, **kwargs)
        return

    else:
        # No stream specified for stderr - put in with stdout
        if stderr is None:
            stderr = subprocess.STDOUT
        # Open process
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=stderr,
                                universal_newlines=True,
                                *args, **kwargs)
        if stdout is None:
            stdout = sys.stdout
        if timeout is None:
            # Write all subprocess output to python stdout
            for line in iter(proc.stdout.readline, ''):
                # assume the line to hold junk if its larger than XXX characters
                if len(line) < 256:
                    stdout.write(line)
        else:
            with process_timeout(proc, timeout):
                for line in iter(proc.stdout.readline, ''):
                    stdout.write(line)

        # Wait for process to finish
        remainder = proc.communicate()[0]

        # All output should have been returned in the for loop above, or in the
        # case of interactive execution there should be no output at all. If there
        # is any remaining output returned by communicate(), this represents a bug
        if remainder is not None and len(remainder) > 0:
            raise DivertOutputError('implementation not working '
                                    +'as expected')

    # Get module variable
    global _subprocess_raise_exceptions_counter

    if _subprocess_raise_exceptions_counter == 0:
        # Return the process return code, like subprocess.call
        return proc.returncode
    elif proc.returncode == 0:
        return 0
    else:
        if len(cmd) <= 10:
            raise subprocess.CalledProcessError(proc.returncode, cmd)
        else:
            raise subprocess.CalledProcessError(proc.returncode, cmd[0])

@contextmanager
def subprocess_exceptions():
    """
    Make :func:`call` raise an exception on process non-zero exit status.

    Example usage:

    >>> from carputils import divertoutput
    >>> with divertoutput.subprocess_exceptions():
    >>>     diveroutput.call(['ls', '-e'])
    Traceback (most recent call last):
        ...
    CalledProcessError: Command '['ls', '-e']' returned non-zero exit status 2
    """

    # Get module variable
    global _subprocess_raise_exceptions_counter

    # Change setting
    _subprocess_raise_exceptions_counter += 1
    try:
        yield
    finally:
        # Restore setting
        _subprocess_raise_exceptions_counter -= 1

@contextmanager
def process_timeout(process, timeout):

    # Track errors in dict
    error = {'timeout': False}

    # Define a function to kill the process if it times out
    def kill():
        process.kill()
        error['timeout'] = True

    # Set up the timer
    timer = threading.Timer(timeout, kill)

    # Start the timer and return to continue process
    timer.start()
    try:
        yield
    finally:
        timer.cancel()
        if error['timeout']:
            tpl = 'Process exceeded max runtime of {} seconds'
            raise DivertOutputError(tpl.format(timeout))

def mp_one_run(run):
    '''
    Define how to start a single run with multiprocessing

    Args:
        run = (num, total, cmd) where
            num: run#; total: total number of runs; cmd: command line to run
    '''
    rnum, total, cmd = run
    print('\n\nStarting run ' + str(rnum) + ' of ' + str(total) + '\n')
    cmdlist = [cmd]
    for cmd in cmdlist:
        subprocess.call(cmd, shell=True)
