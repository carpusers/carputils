#!/usr/bin/env python

"""
tuning

tune conductivity settings to match prescribed conduction velocities
"""


import os

from carputils import format
from carputils import settings
from carputils import tools
from carputils import ep
from carputils import stream
from carputils import mesh

from numpy import nonzero, mean, loadtxt


class CVtuning():
    """
    class for managing CV tuning

    Parameters
    ----------

    Mesh parameters
    ---------------

    sres : float
           resolution of strand in um

    slen : float
           length of strand in cm


    Ionic model parameters
    ----------------------

    model  : name of ionic model
             default name is MBRDR

    mpar   : model parameter list
             default is empty

    plugin : name of plugin
             default is empty

    ppar   : plugin parameter list
             default is empty

    svinit : initial state vector file
             default is empty

    Tuning parameters
    -----------------

    vel : float
          desired velocity in m/s

    gi  : float
          intracellular conductivity in S/m
          default is 0.174 S/m

    ge  : float
          extracellular conductivity in S/m
          default is 0.625 S/m

    beta : float
           surface-to-volume ratio in 1/cm

    conv : bool
           converge to prescribed velocity

    ctol : float
           stop tolerance for CV tuning

    Numerical parameters
    --------------------

    dt   : float
           integration time step in us

    ts   : int
           time stepping method
           0: explicit
           1: Crank-Nicolson

    stol : float
           solver tolerance

    lump : bool
           toggle mass lumping flag

    opsplt : bool
             toggle operator splitting flag

    srcMod : int
             select source model
             0: monodomain
             1: bidomain
             2: pseudo-bidomain
    """

    def __init__(self, vel=0.6, logfile='tuneCV.log', cvfile='results.dat'):

        # geometry for testing CVs
        self.sres       = 100.0
        self.slen       =   1.0
        self.geom       = None
        self.meshfiles  = None

        # ionic setup
        self.model       = 'MBRDR'
        self.mpar        = ''
        self.plugin      = ''
        self.ppar        = ''
        self.svinit      = ''

        # conductivity/velocity tuning
        self.vel         =  vel    # m/s
        self.gi          =  0.174  # S/m
        self.ge          =  0.625  # S/m
        self.beta        =  0.14   # 1/cm
        self.conv        =  False
        self.ctol        =  0.01   # relative tolerance for converged CV
        self.max_it      = 20.     # maximum number of iterations

        # tuned parameters
        self.vel_        = -1.
        self.gi_         = self.gi
        self.ge_         = self.ge

        # numerical settings
        self.dt          = 10.     # us
        self.ts          =  0      # time stepper
        self.stol        = 1e-9    # solver tol
        self.lump        = False   # mass lumping
        self.opsplt      = True    # operator splitting on
        self.srcMod      = 'monodomain'
        self.stimS       = 250.    # Itr in uA/cm^2

        # logging
        self.log         = logfile
        self.last_it     = -1

        # results
        self.dat         = cvfile

        # visualization
        self.visualize   = False   # visualize CV experiment

        # remove old log file
        if os.path.isfile(logfile):
            os.remove(logfile)

        ## remove old results file
        #if os.path.isfile(cvfile):
            #os.remove(cvfile)

    def config(self,args):

        # geometry for testing CVs
        self.sres       = args.resolution
        self.slen       = args.length

        # ionic setup
        self.model       = args.model
        self.mpar        = args.modelpar
        self.plugin      = args.plugin
        self.ppar        = args.plugpar
        self.svinit      = args.svinit

        # conductivity/velocity tuning
        self.vel         = args.velocity # m/s
        self.gi          = args.gi       # S/m
        self.ge          = args.ge       # S/m
        self.gi_         = args.gi       # S/m
        self.ge_         = args.ge       # S/m
        self.beta        = args.beta     # 1/cm
        self.conv        = args.converge
        self.ctol        = args.tol      # relative tolerance for converged CV
        self.max_it      = args.maxit    # maximum number of iterations


        # numerical settings
        self.dt          = args.dt       # us
        self.ts          = args.ts       # time stepper
        self.stol        = args.stol     # solver tol
        self.lump        = args.lumping  # mass lumping
        self.opsplt      = args.opsplit  # operator splitting on
        self.srcMod      = args.sourceModel
        self.stimS       = args.stimS    # Strength of the stimulus

        # logging
        self.log         = args.log

        # visualization
        self.visualize   = args.visualize

    def configCells(self):
        """
        setup ionic model we want to use for CV tuning
        """

        ion_set = ['-imp_region[0].cellSurfVolRatio', self.beta,
                   '-imp_region[0].im',               self.model,
                   '-imp_region[0].im_param',         self.mpar,
                   '-imp_region[0].plugins',          self.plugin,
                   '-imp_region[0].plug_param',       self.ppar ]

        if self.svinit != '':
            # check whether file exists
            if not os.path.isfile(self.svinit):
                print('Given {} does not exist! Aborting...'.format(self.svinit))
                exit(1)
            ion_set += ['-imp_region[0].im_sv_init', self.svinit]

        return ion_set

    def hmean(self):
        """
        Compute harmonic mean conductivity
        """

        if self.conv:
          gm = self.gi_*self.ge_/(self.gi_ + self.ge_)
        else:
          gm = self.gi*self.ge/(self.gi + self.ge)

        return gm

    def configConductivities(self):
        """
        Define myocardial conductivities
        """

        if self.conv==True:
            gi = self.gi_
            ge = self.ge_
        else:
            gi = self.gi
            ge = self.ge

        cset = ['-gregion[0].g_il', gi,
                '-gregion[0].g_el', ge]

        return cset

    def configNumerics(self):
        """
        Define numerical settings for tuning experiment
        """
        num_set  = [ '-dt',                 self.dt,
                     '-parab_solve',        self.ts,
                     '-cg_tol_parab',       self.stol,
                     '-mass_lumping',       int(self.lump),
                     '-operator_splitting', int(self.opsplt) ]

        return num_set

    def summary(self):
        """
        Output summarized user settings
        """
        print('\n\n')
        print('Conduction velocity testing - parameter summary')
        print('_'*format.terminal_width() + '\n')

        print('# mesh settings')
        print('resolution   : {} um'.format(self.sres))
        print('length       : {} cm'.format(self.slen))
        print('\n')

        print('# model settings')
        print('model        : {}'.format(self.model))
        print('model-par    : {}'.format(self.mpar))
        print('plugin       : {}'.format(self.plugin))
        print('plug-par     : {}'.format(self.ppar))
        print('svinit       : {}'.format(self.svinit))
        print('\n')

        print('# conductivity/velocity settings')
        print('velocity     : {} m/s '.format(self.vel))
        print('gil          : {} S/m '.format(self.gi))
        print('gel          : {} S/m '.format(self.ge))
        print('beta         : {} 1/cm'.format(self.beta))
        print('\n')

        print('# numerical settings')
        print('dt           : {} us'.format(self.dt))
        print('ts           : {}'.format(self.ts))
        print('solver tol   : {}'.format(self.stol))
        print('mass lumping : {}'.format(self.lump))
        print('op splitting : {}'.format(self.opsplt))
        print('source model : {}'.format(self.srcMod))
        print('stimulus strength : {}'.format(self.stimS))

        print('\n')

    def report(self):
        """
        fill dictionaries and output to log file
        """
        # check whether the report file exists
        tf = os.path.exists(self.log)

        header = '{:10s}\t{:7s}\t{:8s}\t' \
                 '{:12s}\t{:12s}\t{:12s}\t'\
                 '{:12s}\t{:8s}\t{:6s}\t'\
                 '{:6s}\t{:4s}\t{:4s}\t'\
                 '{:12s}\t{:10s}\t{:12s}\t'\
                 '{:12s}\t{:6s}\n'.format('resolution', 'length', 'imp',
                                          'imp_param', 'plugin', 'plug_par',
                                          'sv_init', 'velocity', 'gil',
                                          'gel', 'beta', 'dt',
                                          'parab_solve', 'solver_tol', 'mass_lumping',
                                          'op_splitting', 'CV')

        tpl = '{:9.1f}\t{:7.2f}\t{:8s}\t' \
              '{:12s}\t{:12s}\t{:12s}\t' \
              '{:12s}\t{:8.2f}\t{:6.4f}\t' \
              '{:6.4f}\t{:4.2f}\t{:4d}\t' \
              '{:12d}\t{:10.3e}\t{:12s}\t' \
              '{:12s}\t{:6.4f}\n'

        with open(self.log, 'a') as fp:
            # append to parameters/results file
            if not tf:
                fp.write(header)

            # export current results
            fp.write(tpl.format(self.sres, self.slen, self.model,
                                self.mpar, self.plugin, self.ppar,
                                self.svinit, self.vel_, self.gi_,
                                self.ge_, self.beta, int(self.dt),
                                self.ts, self.stol, str(self.lump),
                                str(self.opsplt), self.vel_))

            if not settings.cli.silent:
                print('Conduction velocity: {:.4f} m/s [gi={:.4f}, ge={:.4f}, gm={:.4f}]'.format(self.vel_, self.gi_, self.ge_, self.hmean()))
        return

    def saveResults(self):
        """
        write tuning results to .dat file
        """

        #cvf = '{:.0f}\t{:.4f}\t{:.4f}\t{:.4f}\t{:.4f}\n'

        #with open(self.dat, 'a') as fcv:
	    #fcv.write(cvf.format(self.sres, self.vel_, self.gi_, self.ge_, self.hmean()))

        cvf = '{:.4f}\n{:.4f}\n{:.4f}\n'

        with open(self.dat, 'a') as fcv:
            fcv.write(cvf.format(self.vel_,self.gi_,self.ge_))


    def geomStrand(self):
        """
        Build strand-like FE mesh
        """
        # resolution in mm
        res = float(self.sres/1000.)

        # define strand size
        cm2mm = 10.
        self.geom  = mesh.Block(size=(self.slen*cm2mm, res, res), resolution=res)

        # Set fibre angle to 0, sheet angle to 0
        self.geom.set_fibres(0, 0, 90, 90)

        with open(self.log, 'a') as fp:
            with stream.divert_std(fp):
                self.meshfiles = mesh.generate(self.geom)

        if not settings.cli.silent:
            print('\n\n')

        return self.geom


    def configStimLHFace(self):
        """
        Add stimulus definition to initiate propagation at the lhs face of the strand
        """

        lhface = mesh.block_boundary_condition(self.geom, 'stimulus', 0, 'x', True)

        idx  = 0
        stim = ['-stimulus[%d].stimtype'%(idx),    0,
                '-stimulus[%d].strength'%(idx),  self.stimS,
                '-stimulus[%d].duration'%(idx),    2.,
                '-stimulus[%d].start'   %(idx),    2.,
                '-stimulus[%d].npls'    %(idx),    1  ]

        return ['-num_stim', 1 ] + stim + lhface


    def configLATs(self):
        """
        Set up detection of local activation times
        """

        lats = ['-num_LATs', 1,
                '-lats[0].measurand',   0,
                '-lats[0].method',      1,
                '-lats[0].all',        int(False),
                '-lats[0].threshold', -20.,
                '-lats[0].mode',        0,
                '-lats[0].ID',        'lats']

        return lats


    def measureCV(self, actsFile):
        """
        Measure conduction velocity along a strand

        As input the given mesh of a strand and the recorded activation times are used.
        Arrival times are computed at the interfaces of the quarters Q1/Q2 and Q3/Q4.
        """
        # read ub strand
        strand = mesh.Mesh.from_files(self.meshfiles)
        pts    = strand.points()

        # figure out nodes which fall into transitions
        # between quarter 1-2 and 3-4
        lo, up = self.geom.tissue_limits()
        slen   = up[0]-lo[0]
        dx     = self.geom.resolution()
        q12    = lo[0] + slen*0.25
        q34    = lo[0] + slen*0.75

        # find points around q12 +/- dx/2
        nq12 = nonzero((pts[:,0]>= (q12-dx/2)) & (pts[:,0] < (q12 + dx/2)))
        # find points around q34 +/- dx/2
        nq34 = nonzero((pts[:,0]>= (q34-dx/2)) & (pts[:,0] < (q34 + dx/2)))


        # load local activation times
        lats = loadtxt(actsFile)

        x12  = mean(pts[nq12,0])
        x34  = mean(pts[nq34,0])
        at12 = mean(lats[nq12])
        at34 = mean(lats[nq34])

        # make sure we had capture and tissue activated
        if (at12 == -1) or (at34 == -1):
            if (at34 > 0.):
                if not settings.cli.silent:
                    print('Error: Duration of simulation too short')
                    print('Distal tissue not activated')
            else:
                if not settings.cli.silent:
                    print('No capture, proximal tissue not activated')

            CV = 0.

        else:
            CV = (x34 - x12)/(at34 - at12)

        return CV

    # build command line
    def buildCmd(self):

        cmd = tools.carp_cmd()

        cmd += [ '-meshname', self.meshfiles ]
        cmd += ep.model_type_opts(self.srcMod)
        cmd += self.configCells()
        cmd += self.configStimLHFace()
        cmd += self.configNumerics()
        cmd += self.configLATs()

        if self.visualize:
            cmd += ['-gridout_i', 3]
            cmd += ['-spacedt',   1]

        # adjust simulation time to cable length and velocity
        cableLen_mm = self.slen*10
        delay       = cableLen_mm / self.vel

        # make simulated time a factor >=1 of the target delay
        cmd += ['-tend', delay*3 ]

        return cmd


    # iterate to converge conduction velocity
    def iterateCV(self, job):

        # build strand
        self.geomStrand()

        # build command line
        cmd = self.buildCmd()


        # Run simulation
        err_flg = False
        i     =  0
        v_new = -1  # do at least one run

        # start tuning with chosen start values
        if not settings.cli.silent:
            print('Start iterating')
            print('-'*format.terminal_width() + '\n')

        if not self.conv:
            self.max_it = 1

        while abs(self.vel - self.vel_) > self.ctol and i < self.max_it:

            # rerun simulation
            g       = self.configConductivities()
            ItJobID = '{}_It_{}'.format(job.ID, i)

            with open(self.log, 'a') as fp:
               with stream.divert_std(fp):
                    job.carp(cmd + g + ['-simID', ItJobID ])

            # measure macroscopic conduction velocity
            self.vel_ = self.measureCV(os.path.join(ItJobID, 'init_acts_lats-thresh.dat'))

            # export CV to keep track of parameter changes
            self.report()

            # adjust conductivities only if converge is True
            if self.max_it > 1 and self.vel_ > 0:
                alpha    = (self.vel/self.vel_)**2.0
                self.gi_ = alpha * self.gi_
                self.ge_ = alpha * self.ge_

            self.last_it = i
            i = i+1


        if not settings.cli.silent:
            print('\n\n')
            print('Tuning results')
            print('-'*format.terminal_width() + '\n')

        if self.conv and i==self.max_it and i!=1:
            err_flg = True
            print('CV tuning not converged with given max_it number of iterations!')

        if not settings.cli.silent:
            print('\n\n')

        # write tuning results to ascii file
        self.saveResults()

        return self.gi_, self.ge_, self.vel, self.hmean(),i, err_flg
