"""
This package most importantly defines the :class:`~carputils.job.Job` class,
which provides methods for executing bash commands.
"""

from carputils.job.job import Job
