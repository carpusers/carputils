"""
Functions for cleaning directories and
"""

from __future__ import print_function, division, unicode_literals
import os
import sys
import shutil
from glob import glob
import re

from carputils.prompt import query_yes_no
from carputils.prompt import query_folder
from carputils import settings

OVERWRITE_CHECK = True
CLEAN_BATCH = False
CLEAN_REMOVED_COUNT = 0

def clean_directory(filelist=None, globstr=None, regex=None):
    """
    Remove the specified files.

    This convenience method is provided to minimise duplication of directory
    cleanup code in run scripts. Files and directories can be specified by an
    explicit list, glob expression or regular expression. The user is prompted
    with a list of files to be removed before anything is done. Exits the
    program when done.

    Args:
        filelist : list of str
            List of file and directory names to be removed.
        globstr : str
            Glob expression (e.g. ``'output_*'`` to match files and directories for
            removal).
        regex : str
            Regular expression (e.g. ``'^output_\\d{4}'``) to match files and
            directories for removal.
    """

    assert filelist is not None or globstr is not None or regex is not None, \
           "Must provide either a glob string or regular expression"

    # Build list of files/dirs to be removed
    remove = []

    # Add any existing files in filelist to remove list
    if filelist is not None:
        for filename in filelist:
            if os.path.exists(filename):
                remove.append(filename)

    # Add any files matching the glob string to remove list
    if globstr is not None:
        remove += glob(globstr)

    # Add any files matching the regex to remove list
    if regex is not None:
        for filename in os.listdir('.'):
            if re.search(regex, filename):
                remove.append(filename)

    # Add Python bytecode
    for filename in os.listdir('.'):
        if re.search(u'(.*\.py[cwo])|(__pycache__)', filename):
            remove.append(filename)

    # Filter out any python files, just in case
    remove = [fn for fn in remove if not fn.endswith('.py')]

    if not remove:
        # Nothing to be done
        print('No files found for removal')
        sys.exit(0)

    # Print summary of files to be removed
    print('\nFiles found for removal:')
    for filename in remove:
        if os.path.isdir(filename):
            print('    {0} (directory)'.format(filename))
        else:
            print('    {0}'.format(filename))

    # Ask the question and do the removal if yes
    question = 'Do you really want to remove these files/directories?'

    if CLEAN_BATCH or query_yes_no(question, None):
        for filename in remove:
            if os.path.isdir(filename):
                shutil.rmtree(filename)
            else:
                os.remove(os.path.join(os.getcwd(), filename))
    else:
        remove = []

    # Count number removed in this session
    global CLEAN_REMOVED_COUNT
    CLEAN_REMOVED_COUNT += len(remove)

    # Quit script
    sys.exit(0)

def overwrite_mode_prompt(name, directory):
    if directory:
        name_tag = "Directory"
    else:
        name_tag = "File"

    question = '{0} "{1}" already exists. ' \
               'Do you want to overwrite(y), quit(n), or append(a)?'
    try:
        overwrite = query_folder(question.format(name_tag, name), None)
    except EOFError:
        msg = ('There is a strange error related to using Python\'s raw_input()'
               ' after using mpiexec with gdb. Please rename the {} {} '
               'manually or force an overwrite with the --overwrite-behaviour '
               'option.')
        raise Exception(msg.format(name_tag.lower(), name))

    if overwrite == 0:
        print('Overwriting {} ...'.format(name_tag.lower()))
        return True

    elif overwrite == 1:
        print('Quitting ...')
        sys.exit(0)
    elif overwrite == 2:
        print('Appending to {} ... '.format(name_tag.lower()))
        if directory:
            print('Please be aware files may be overwritten.')
        else:
            print('Please be aware that options may be overwritten.')
        return False
    else:
        print('Quitting...')
        sys.exit(0)

def overwrite_mode_error(name, directory):
    if directory:
        tpl = 'Simulation directory "{}" already exists'
    else:
        tpl = 'File "{}" already exists'
    raise Exception(tpl.format(name))

OVERWRITE_MODES = {'prompt':    overwrite_mode_prompt,
                   'error':     overwrite_mode_error,
                   'delete':    lambda n, d: True,
                   'overwrite': lambda n, d: False,
                   'append':    lambda n, d: False}

def overwrite_check(name, mode='prompt'):
    """
    Make sure the user does not overwrite an existing output directory

    Args:
        simID : str
            The simulation output directory.
        mode : str, optional
            The overwrite mode - one of 'append','prompt', 'error', 'delete' or 'overwrite'
    """

    assert mode in OVERWRITE_MODES, 'Invalid mode {}'.format(mode)
    directory = False
    if os.path.isdir(name):
        directory = True

    # Never prompt in batch mode
    if settings.platform.BATCH and mode == 'prompt':
        mode = 'error'

    if directory:
        # Clean up simulation directory, if it already exists
        if (OVERWRITE_CHECK and os.path.exists(name) and os.listdir(name)):
            if OVERWRITE_MODES[mode](name, directory):
                shutil.rmtree(name)
    else:  # file
        if (OVERWRITE_CHECK and os.path.isfile(name)):
            if OVERWRITE_MODES[mode](name, directory):
                os.remove(name)
