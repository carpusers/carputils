
import sys
import subprocess
import numpy as np

def terminal_width(fallback=70):
    """
    Get the width of the terminal, in characters

    Parameters
    ----------
    fallback, int
        The default width to use when the width cannot be determined (default:
        100)

    Returns
    -------
    int
        The number of columns in the output
    """

    try:
        # Not using divertoutput since we just want to ignore any errors
        # completely
        _, cols = subprocess.check_output(['stty', 'size']).split()
        return int(cols)

    except Exception as err:
        # If any error, use fallback
        return fallback

def hline(char='-', fallback=70):
    return char * terminal_width(fallback) + '\n'

def header(description, char='-', fallback=70):

    cols = terminal_width(fallback)-1
    num_spaces = int((cols - len(description)) / 2)

    header = '#' + char * cols + '\n'
    header += '#' + ' ' * num_spaces + description + '\n'
    header += '#' + char * cols + '\n'

    return header

def dot_pad(string, length):

    if len(string) < length:
        string += ' '

    while len(string) < length:
        string += '.'

    return string

def stringtable(strlist, ncolumns=4, padding=3, rowprefix=''):
    tablestr, tablerow = '', ''
    maxstrlen = max(map(len, strlist))
    for i, string in enumerate(strlist):
        tablerow += string.ljust(maxstrlen+padding)
        if (i+1)%ncolumns == 0:
            tablestr += rowprefix+tablerow+'\n'
            tablerow = ''
    if len(strlist)%ncolumns > 0:
        tablestr += rowprefix+tablerow+'\n'
    return tablestr

def linewrap(content, length):

    # Retain any initial indent on the content
    indent = ' ' * (len(content) - len(content.lstrip()))

    lines = [indent]

    for word in content.split():

        space = ' ' if len(lines[-1].strip()) > 0 else ''

        # Normal word, fits on line
        if len(lines[-1]) + len(space) + len(word) <= length:
            lines[-1] += space + word

        # Very long word, not enough space on single line
        elif len(indent) + len(word) > length:

            # Add as much as possible to this line
            nchar = length - len(lines[-1]) - len(space)
            lines[-1] += space + word[:nchar]

            # Add rest on new lines
            remainder = word[nchar:]
            while len(remainder) > 0:
                nchar = length - len(indent)
                lines.append(indent + remainder[:nchar])
                remainder = remainder[nchar:]

        else:
            # Start new line
            lines.append(indent + word)

    return lines

def command_line(cmd, optionals=True, cols=None, prefix=''):
    """
    Nicely format a command line for output to terminal.

    If joining the arguments with single spaces results in a string that will
    fit on one line, this is returned. Otherwise, a string containing the
    command line nicely displayed over many lines is returned.

    Parameters
    ----------
    cmd : list
        list of strings representing command line arguments
    optionals : bool, optional
        If True (default), flags starting with '-' start new lines. If False,
        all arguments are on new lines.
    cols : int, optional
        Specify the number of columns to fit inside, default: terminal width
    prefix : str, optional
        A string to prefix each line of the formatted output

    Returns
    -------
    str
        The formatted command line
    """

    if cols is None:
        # Get the terminal width
        cols = terminal_width()

    # Ensure cmd entries are strings
    cmd = [str(e) for e in cmd]

    # NOTE: commented out - cannot guarante that this has broken something
    ## Make sure any strings with spaces are quoted
    #cmd = [repr(s) if ' ' in s else s for s in cmd]

    # If it will fit, format as single line
    joined = prefix + ' '.join(cmd)
    if len(joined) <= cols:
        return joined

    # Otherwise, format nicely over many lines

    formatted = prefix + cmd[0]
    next_new_line = False
    nl = ' \\\n{}  '.format(prefix)

    for entry in cmd[1:]:

        if next_new_line:
            formatted += nl
            next_new_line = False

        elif not optionals:
            # Put all arguments on new line
            formatted += nl

        elif ((entry.startswith('-') and entry != '-n')
              or entry in ['+F', ':', '>', '1>', '2>', '&>']):
            # Put any flags on a new line
            # Check that entry does not start with '-' because it's a number
            try:
                float(entry)
            except ValueError:
                # Not a number
                formatted += nl
            else:
                # Is a number, no new line
                formatted += ' '

            next_new_line = entry == ':'

        else:
            # Just put a space
            formatted += ' '

        # Add the entry itself
        formatted += entry

    return formatted

class Cell(object):

    def __init__(self, content):
        self.content = content.split('\n')

    def width(self):
        return max([len(line) for line in self.content])

    def wrapped(self, width):
        for line in self.content:
            for wrappedline in linewrap(line, width):
                yield wrappedline

class Table(object):

    def __init__(self, data, header=None, column_ratios=None):

        self.data = data
        self.header = header

        if column_ratios is None:
            ncols = len(data[0])
            self.column_ratios = np.ones(ncols, dtype=float) / ncols
        else:
            self.column_ratios = np.array(column_ratios, dtype=float)
            self.column_ratios = self.column_ratios / np.sum(self.column_ratios)

        # Caches
        self._col_widths = None

    def column_widths(self):

        if self._col_widths is not None:
            return self._col_widths

        # Initialise to header widths
        widths = [cell.width() for cell in self.header]

        # Update with successive rows
        for row in self.data:
            widths = [max([cell.width(), w]) for cell, w in zip(row, widths)]

        # Get terminal width
        term_width = terminal_width(fallback=90)

        # Space needed for inter-column spacers
        spacers = len(widths) - 1

        while sum(widths) + spacers > term_width:
            ratios = np.array(widths, dtype=float) / np.sum(widths)
            largest = (ratios / self.column_ratios).argmax()
            widths[largest] -= 1

        self._col_widths = widths
        return self._col_widths

    def hline(self, char='-'):
        parts = [char*w for w in self.column_widths()]
        return ' '.join(parts) + '\n'

    def format_row(self, cells):

        iterators = [cell.wrapped(width) for cell, width
                     in zip(cells, self.column_widths())]

        tpl = ' '.join(['{:' + str(w) + '}' for w in self.column_widths()])

        content = ''

        if sys.version_info[0] < 3:
            from itertools import izip_longest
            for lineset in izip_longest(*iterators, fillvalue=''):
                content += tpl.format(*lineset) + '\n'
        else:
            from itertools import zip_longest
            for lineset in zip_longest(*iterators, fillvalue=''):
                content += tpl.format(*lineset) + '\n'

        return content

    def __str__(self):

        content = ''

        content += self.hline('=')

        if self.header is not None:
            content += self.format_row(self.header)
            content += self.hline('=')

        for i, row in enumerate(self.data):
            content += self.format_row(row)
            if i != len(self.data) - 1:
                content += self.hline()

        content += self.hline('=')

        return content

class Summary(object):

    BLANKLINE = (None, None)

    def __init__(self, title):
        self.title = str(title)
        self.lines = []
        self.list_heading = None
        self.list_content = []

    def add_line(self, description, value):
        self.lines.append((str(description), str(value)))

    def add_blank_line(self):
        self.lines.append(self.BLANKLINE)

    def set_list(self, heading, content):
        self.list_heading = str(heading)
        self.list_content = [str(e) for e in content]

    def formatted_lines(self):

        nlen = lambda v: 0 if v is None else len(v)
        desc_width = max([nlen(desc) for desc, _ in self.lines]) + 3

        for desc, value in self.lines:

            if desc is None:
                yield ''
            else:
                yield '  {} {}  '.format(dot_pad(desc, desc_width), value)

        if self.list_heading is not None:
            yield ''
            yield '  {}:  '.format(self.list_heading)
            for entry in self.list_content:
                yield '    {}  '.format(entry)

    def __str__(self):

        content = list(self.formatted_lines())

        width = max([len(line) for line in content])
        hline = '=' * width + '\n'

        tpl = '{:^' + str(width) + '}\n'
        header = hline + tpl.format(self.title) + hline

        return header + '\n' + '\n'.join(content) + '\n\n' + hline
