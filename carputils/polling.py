"""
Functions for generating and executing CARP polling files.
"""

from __future__ import print_function, division, unicode_literals
import sys
import os
import re
from carputils import settings
from carputils.clean import overwrite_check

# -----------------------------------------------------------------------------
def process_polling_file(polling_file, polling_param, polling_range, sampling):
    """ Generate polling input file if polling_param and polling_range is
        set.
        Check polling file if polling_param is None
    Args:
        polling_file: polling input or output file name
        polling_param: parameter for polling experiment
        polling_range: range of polling parameter in the format min max nr_points
        sampling: sampling type; latin-hypercube ('lhs') or 'linear' sampling
    """
    if polling_file is None and polling_param is None:
        return

    # Check if polling file exists when loading polling file
    if polling_file is not None and polling_param is None:
        if not os.path.isfile(polling_file):
            sys.exit("Error: polling file {} does not exist"\
                    .format(settings.cli.polling_file))
        return

    # Check if file exists when generating polling file
    overwrite_check(polling_file)

    # Check if polling range was set on the command line
    if not polling_param:
        raise Exception('Polling Error: Must specify range when creating polling file!')

    # Check if polling range and param length are the same
    p_length = len(polling_param)
    if len(polling_range) != p_length:
        raise Exception('Polling Error: Length of parameters and polling range'
                        ' must be the same!')

    # Generate file
    param_range = []
    num_samples = polling_range[0][2]

    for i in range(p_length):
        p_range = polling_range[i]
        # Check polling range logics
        if p_range[0] > p_range[1]:
            sys.exit("Polling Error: the minimum of the polling interval should "
                     " be small than the maximum")
        if p_range[2] < 1:
            sys.exit("Polling Error: the number of polling samples should be "
                     "larger or equal to one.")

        if p_range[2] != num_samples:
            sys.exit("Polling Error: the number of polling samples should be "
                     "the same for each parameter.")

    if sampling == 'linear':
        # import linspace from numpy
        from numpy import linspace
        for i in range(p_length):
            p_range = polling_range[i]
            param_range.append(linspace(float(p_range[0]),
                                        float(p_range[1]), p_range[2]))
    elif sampling == 'lhs':
        # import latin hypercube from pyDOE
        from pyDOE import lhs
        lhs_samples = lhs(p_length, num_samples)
        for i in range(p_length):
            p_range = polling_range[i]
            min_param = p_range[0]
            max_param = p_range[1]
            param_range.append(min_param + lhs_samples[:,i]*(max_param - min_param))

    # Open and write polling file
    p_file = open(polling_file, "a")
    for num in range(num_samples):
        for i in range(p_length):
            p_param = polling_param[i]
            p_file.write("-{} {:.6f} ".format(p_param, param_range[i][num]))
        p_file.write("\n")
    p_file.close()

    sys.exit("Created polling file {}".format(polling_file))

# -----------------------------------------------------------------------------
def set_polling_options(cmd, polling_file, gen_subdir=True):
    """
    Set polling options and new sim ID
    """
    if polling_file is None:
        return None

    cmd = [x.encode('utf-8') for x in cmd]

    # Check if SimID is given
    if "-simID".encode('utf8') in cmd:
        sim_id_idx = cmd.index("-simID".encode('utf8'))
        sim_id = cmd[sim_id_idx + 1].decode('utf8')
    else:
        sim_id = "OUTPUT"

    # Create SimID for polling
    if not os.path.exists(sim_id):
        os.system("mkdir -p {}".format(sim_id))

    # Initialize polling options
    opts = []

    # Read polling file
    with open(settings.cli.polling_file) as src:
        lines = src.readlines()

    for line in lines:
        params = line.split()
        id_given = False
        if "-simID" in params:
            id_given = True

        len_line = 0 if params is None else len(params)

        id_ext = 'p'  # sim ID extension

        # check if polling parameter is in command
        entry = ''
        for i in range(0, len_line, 2):
            if not params[i].encode('utf8') in cmd:
                sys.exit("Polling parameter \"{}\" not found in command!"\
                        .format(params[i]))

            # write parameters to opts
            entry += "{} {} ".format(params[i], params[i+1])

            # create sim ID extension
            id_ext += '_'
            id_ext += re.sub('[^A-Za-z0-9]+', '', params[i])
            try:
                value = float(params[i+1])
                id_ext += '.{:.6f}'.format(value)
            except:
                id_ext += re.sub('[^A-Za-z0-9._-]+', '', params[i+1])

        new_sim_id = "{}/{}".format(sim_id, id_ext)
        if gen_subdir and not id_given:
            entry += " -simID {}\n".format(new_sim_id)
        opts.append(entry)

    return opts
