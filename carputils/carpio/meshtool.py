import os, sys

from contextlib import contextmanager

@contextmanager
def suppress_stdout():
    with open(os.devnull,'w') as devnull:
        _stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = _stdout
    return


def execute(job, cmd, msg=None, outdir=None, silent=False):
    if not silent:
        if outdir is not None:
            job.mkdir(outdir, parents=True)
        job.meshtool(cmd, msg)
    else:
        with suppress_stdout():
            if outdir is not None:
                job.mkdir(outdir, parents=True)
            job.meshtool(cmd, msg)
    return

def generate_splitsurf(job,msh,surfs,split_ofile,silent=False):

    """
    MESHTOOL    generate a split file from a given surface.

    parameters:
        msh:   (input) path to basename of the input mesh
        surf:   (input) list of surfaces seperated with a comma
        ifmt:   (optional) input format. (carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow)
        split:		 (output) path to the split list file

        The supported input formats are:
        carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
    """

    cmd = ['generate',' surfsplit',
          '-msh='+msh,
          '-surf='+surfs,
           '-split='+split_ofile]

    execute(job, cmd, outdir=None, silent=silent)

    # [job.mv(x,split_ofile+'.'+x) for x in ['nod_selection.dat','surface_alignment.vec'] if os.path.isfile(x)]

    return


def generate_split(job,msh,ops,split_ofile,silent=False):

    """
    MESHTOOL    generate a split file from a given surface.

    parameters:
        msh:   (input) path to basename of the input mesh
        op:     (input) sequence of split operations.
        ifmt:   (optional) input format. (carp_txt, carp_bin, vtk, vtk_bin, mmg, neu, purk, obj, stellar, vcflow)
        split:		 (output) path to the split list file

        The supported input formats are:
        carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
    """

    cmd = ['generate','split',
          '-msh='+msh,
          '-op='+ops,
           '-split='+split_ofile]

    execute(job, cmd, outdir=None, silent=silent)

    return


def convert(job, imsh, ifmt, omsh, ofmt, silent=False):
    """
    MESHTOOL     Convert a mesh between different formats

    parameters:
        imsh:   (input) path to basename of the input mesh
        ifmt:   (input) format of the input mesh
        omsh:   (output) path to basename of the output mesh
        ofmt:   (input) format of the output mesh

        The supported input formats are:
        carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar

        The supported output formats are:
        carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar
    """

    # check if output directory exists
    outdir = os.path.dirname(omsh)
    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd = ['convert', '-imsh=' + imsh,
                      '-ifmt=' + ifmt,
                      '-omsh=' + omsh,
                      '-ofmt=' + ofmt]

    execute(job, cmd, outdir=outdir, silent=silent)
    return

def UVC_localize(job,msh,pts2find,scale_thr=1,ofile=None,silent=False):

    """
    MESHTOOL     Localize uvc pts to nodes on the mesh.

    parameters:
        msh:   (input) path to basename of the input mesh. Mesh must contain +'.pts_t' file describing the UVC Combined Coordinates.
        pts2find:   (input) list of uvcpts to localize with included thresholds. Format = {z,rho,phi,ven,thr_rho,thr_ep}
        scale_thr:   (output) scaling the sizing of the stimulus sites.
        ofile:   (optional) format of the output mesh

    """

    if ofile is None: ofile=msh+'.MT.pts'

    with open(ofile,'w') as f:
        f.write(str(len(pts2find))+'\n')
        for k,entry in enumerate(pts2find):
            f.write(' '.join((str(entry[0]),str(entry[1]),str(entry[2]),str(entry[3]),
                              str(round(scale_thr*entry[4])),
                              str(entry[5]),'\n')))

    cmd=['query','idxlist_uvc',
          '-msh='+msh,
          '-coord='+ofile]

    execute(job, cmd, outdir=None, silent=silent)

    f=open(ofile+'.out.txt','r')
    uvc_mt_data=f.readlines()[1::]
    f.close()

    return uvc_mt_data

def insert_submesh(job, submsh, msh, outmsh, ofmt=None, silent=False):
    """ MESHTOOL
    # -------------------------------------------------------------------------
    insert submesh: a given submesh is inserted back into a mesh and written to an output mesh
    parameters:
        -submsh=<path>	... (input) path to basename of the submesh to insert from
        -msh=<path>	    ... (input) path to basename of the mesh to insert into
        -ofmt=<format>	... (optional) mesh output format. may be: carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar
        -outmsh=<path>	... (output) path to basename of the output mesh

    Note that the files defining the submesh must include a *.eidx and a *.nod file.
    This files define how elements and nodes of the submesh map back into the original mesh.
    The *.eidx and *.nod files are generated when using the "extract mesh" mode.

    # -------------------------------------------------------------------------
    insert meshdata: the fiber and tag data of a mesh is inserted into another mesh
                     based on vertex locations.
    parameters:
        -imsh=<path>		... (input) path to basename of the mesh to insert from
        -msh=<path>		... (input) path to basename of the mesh to insert into
        -op=<path>		... (input) Operation index: 0 = only tags, 1 = only fibers, 2 = both
        -ifmt=<format>		... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        -ofmt=<format>		... (optional) mesh output format. may be: carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar
        -trsp=<vec-file>	... (optional) element-based transport gradient
        -trsp_dist=<float>	... (optional) transport distance threshold
        -grad_thr=<float>	... (optional) gradient correlation threshold. Must be in (0, 1). Default is 0.99.
        -corr_thr=<float>	... (optional) correspondance linear search fallback threshold. Default is 1000.
        -con_thr=<float>	... (optional) Connectivity threshold. Required number of nodes an element of mesh1
		                                   needs to share with an element of mesh2 in order for the data to be inserted. Default is 1.
        -outmsh=<path>		... (output) path to basename of the output mesh

    # -------------------------------------------------------------------------
    insert data: data defined on a submesh is inserted back into a mesh
    parameters:
        -msh=<path>		... (input) path to basename of the mesh
        -submsh=<path>		... (input) path to basename of the submesh
        -submsh_data=<path>	... (input) path to submesh data
        -msh_data=<path>	... (input) path to mesh data
        -odat=<path>	... (output) path to output data

    Note that the files defining the submesh must include a *.nod file.
    This file defines how the nodes of the submesh map back into the original mesh.
    The *.nod file is generated when using the "extract mesh" mode.

    # -------------------------------------------------------------------------
    insert points: points of a submesh are inserted back into a mesh and written to an output file
    parameters:
        -submsh=<path>	... (input) path to basename of the submesh to insert from
        -msh=<path>	... (input) path to basename of the mesh to insert into
        -outmsh=<path>	... (output) path to basename of the output mesh

    Note that the files defining the submesh must include a *.nod file.
    This file defines how the nodes of the submesh map back into the original mesh.
    The *.nod file is generated when using the "extract mesh" mode.
    """

    # check if output directory exists
    outdir = os.path.dirname(outmsh)
    if os.path.exists(outdir):
        # reset variable
        outdir = None


    cmd  = ['insert', 'submesh']
    cmd += ['-submsh=' + submsh]
    cmd += ['-msh='    + msh   ]
    cmd += ['-outmsh=' + outmsh]
    if ofmt is not None:
        cmd += ['-ofmt=' + ofmt]

    execute(job, cmd, outdir=outdir, silent=silent)
    return

def extract_mesh(job, msh, submsh, tags, tag_file=None, ifmt=None, ofmt=None, silent=False):
    """ MESHTOOL
    extract mesh: a submesh is extracted from a given mesh based on given element tags
    parameters:
        -msh=<path>	        ... (input) path to basename of the mesh to extract from
        -tags=tag1,tag2	    ... (input) ","-seperated list of tags
        -tag_file=<path>    ... (optional) path to an alternative tag file {*.tags, *.btags}
        -ifmt=<format>	    ... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        -ofmt=<format>	    ... (optional) mesh output format. may be: carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar
        -submsh=<path>	    ... (output) path to basename of the submesh to extract to
    """

    # check if output directory exists
    if submsh.find(','):
        outdir = os.path.dirname(submsh.split(',')[-1])
    else:
        outdir = os.path.dirname(submsh)

    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd  = ['extract', 'mesh'  ]
    cmd += ['-msh='    + msh   ]
    cmd += ['-tags='   + tags  ]
    cmd += ['-submsh=' + submsh]

    if tag_file is not None:
        cmd += ['-tag_file={}'.format(tag_file)]
    if ifmt is not None:
        cmd += ['-ifmt=' + ifmt]
    if ofmt is not None:
        cmd += ['-ofmt=' + ofmt]

    execute(job, cmd, outdir=outdir, silent=silent)
    return

def interpolate(job, cmode, omsh=None, idat=None, odat=None, imsh=None, dynpts=None, pts=None, mode=None, silent=False):
    """ MESHTOOL
    interpolate nodedata: interpolate nodal data from one mesh onto another
    parameters:
    -omsh=<path>	 (input) path to basename of the mesh we interpolate to
    -imsh=<path>	 (input) path to basename of the mesh we interpolate from
    -idat=<path>	 (input) path to input data.
    -odat=<path>	 (output) path to output data.
    -dynpts=<path>	 (optional) Dynpts describing point cloud movement.

    interpolate elemdata: interpolate element data from one mesh onto another
    parameters:
    -omsh=<path>	 (input) path to basename of the mesh we interpolate to
    -imsh=<path>	 (input) path to basename of the mesh we interpolate from
    -idat=<path>	 (input) path to input data.
    -odat=<path>	 (output) path to output data.
    -dynpts=<path>	 (optional) Dynpts describing point cloud movement.

    interpolate clouddata: interpolate data from a pointcloud onto a mesh using radial basis function interpolation
    parameters:
    -omsh=<path>	 (input) path to basename of the mesh we interpolate to
    -pts=<path>	 (input) path to the coordinates of the point cloud
    -idat=<path>	 (input) path to input data.
    -odat=<path>	 (output) path to output data.
    -mode=<int>	 (optional) Choose between localized Shepard (=0), global Shepard (=1), and RBF interpolation (=2). Default is 2.
    -dynpts=<path>	 (optional) Dynpts describing point cloud movement.

    interpolate elem2node: interpolate data from elements onto nodes
    parameters:
    -omsh=<path>	 (input) path to basename of the mesh we interpolate to
    -idat=<path>	 (input) path to input data.
    -odat=<path>	 (output) path to output data.

    interpolate node2elem: interpolate data from nodes onto elements
    parameters:
    -omsh=<path>	 (input) path to basename of the mesh we interpolate to
    -idat=<path>	 (input) path to input data.
    -odat=<path>	 (output) path to output data.
    """

    MODES  = ['nodedata', 'elemdata', 'clouddata', 'elem2node', 'node2elem']
    assert cmode in MODES, 'meshtool interpolate detected incorrect mode: {}'.format(cmode)

    # check if output directory exists
    outdir = os.path.dirname(omsh)

    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd = ['interpolate', cmode]
    # -------------------------------------------------------------------------
    if 'nodedata' in cmode or 'elemdata' in cmode:
        cmd += ['-imsh={}'.format(imsh)]
        cmd += ['-omsh={}'.format(omsh)]
        cmd += ['-idat={}'.format(idat)]
        cmd += ['-odat={}'.format(odat)]
        if dynpts is not None:
            cmd += ['-dynpts={}'.format(dynpts)]
    # -------------------------------------------------------------------------
    elif 'clouddata' in cmode:
        cmd += ['-omsh={}'.format(omsh)]
        cmd += ['-pts={}'.format(pts)]
        cmd += ['-idat={}'.format(idat)]
        cmd += ['-odat={}'.format(odat)]
        if mode is not None:
            cmd += ['-mode={}'.format(mode)]
        if dynpts is not None:
            cmd += ['-dynpts={}'.format(dynpts)]
    # -------------------------------------------------------------------------
    elif 'elem2node' in cmode or 'node2elem' in cmode:
        cmd += ['-omsh={}'.format(omsh)]
        cmd += ['-idat={}'.format(idat)]
        cmd += ['-odat={}'.format(odat)]
    # -------------------------------------------------------------------------
    else:
        assert False, 'Unknown mode: {}'.format(cmode)

    execute(job, cmd, outdir=outdir, silent=silent)
    return

def extract_surface(job, msh, surf, op=None, tag_file=None, ifmt=None, ofmt=None, edge=None, ang_thr=None,
                    coord=None, size=None, lower=None, silent=False):
    """ MESHTOOL
    extract surface: extract a sequence of surfaces defined by set operations on element tags
    parameters:
    -msh=<path>		... (input) path to basename of the mesh
    -surf=<path>		(output) list of names associated to the given operations
    -op=operations		(optional) list of operations to perform. By default, the
                        surface of the full mesh is computed.
    -tag_file=<path>    (optional) path to an alternative tag file {*.tags, *.btags}
    -edge=<deg. angle>	(optional) surface elements connected to sharp edges will
                        be removed. A sharp edge is defined by the nodes which connect elements
                        with normal vectors at angles above the given threshold.
    -coord=<xyz>:<xyz>:.(optional) restrict surfaces to those elements reachable by
                        surface edge-traversal from the surface vertices closest to the given
                        coordinates. If -edge= is also provided, sharp edges will block
                        traversal, thus limit what is reachable.
    -size=<float>		(optional) surface edge-traversal is limited to the given
                        radius from the initial index.
    -lower_size=<float>	(optional) surface edge-traversal limitation lower size (for extracting bands).
    -ifmt=<format>		(optional) mesh input format.
    -ofmt=<format>		(optional) mesh output format. If set, the surfaces will also
                        be written as surface meshes.

    The supported input formats are:
    carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, obj, stellar, vcflow
    The supported output formats are:
    carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, obj, stellar, vcflow

    The format of the operations is:
    tagA1,tagA2,[surfA1,surfA2..]..[+-:]tagB1,tagB2,[surfB1..]..;tagA1,..[+-:]tagB1..;..
    Tag regions separated by "," will be unified into submeshes and their surface computed.
    Alternatively, surfaces can be provided directly by .surf surface files (only basename, no extension).
    If two surfaces are separated by "-", the rhs surface will be removed from the
    lhs surface (set difference). Similarly, using "+" will compute the surface union.
    If the submeshes are separated by ":", the set intersection of the two submesh surfaces will be computed.
    Individual operations are separated by ";".

    The number of names provided with "-surf=" must match the number of operations. If no operations are provided,
    the surface of the whole geometry will be extracted. Individual names are separated by ",".

    Further restrictions can be added to the surface extraction with the -edge= , -coord= , -size= options.
    """


    # check if output directory exists
    if surf.find(',') >= 0:
        outdir = os.path.dirname(surf.split(',')[-1])
    else:
        outdir = os.path.dirname(surf)

    if os.path.exists(outdir):
        # reset variable
        outdir = None

    # TODO: this error check may be removed sometime soon
    #       just put it here to more quickly account for meshtool changes
    if isinstance(op, str) and ',' in surf:
        if not ';' in op:
            os.error('Individual operations NOW need to be separated by ";"! You may need to update meshtool.')
    # -------------------------------------------------------------------------

    cmd  = ['extract', 'surface']
    cmd += ['-msh={}'.format(msh)]
    cmd += ['-surf={}'.format(surf)]

    if op is not None:
        if isinstance(op, list):
            # op needs to be a string
            op   = ';'.join(str(x) for x in op)
            cmd += ['-op={}'.format(op)]
        else:
            cmd += ['-op={}'.format(op)]
    if tag_file is not None:
        cmd += ['-tag_file={}'.format(tag_file)]
    if edge is not None:
        cmd += ['-edge={}'.format(edge)]
    if ang_thr is not None:
        cmd += ['-ang_thr={}'.format(ang_thr)]
    if coord is not None:
        cmd += ['-coord={}'.format(coord)]
    if size is not None:
        cmd += ['-size={}'.format(size)]
    if lower is not None:
        cmd += ['-lower_size={}'.format(lower)]
    if ifmt is not None:
        cmd += ['-ifmt={}'.format(ifmt)]
    if ofmt is not None:
        cmd += ['-ofmt={}'.format(ofmt)]

    execute(job, cmd, outdir=outdir, silent=silent)
    print('Done')

    return

def query(job, mode, msh, tags=None, op=None, ifmt=None, ofmt=None, edge=None, coord=None, uvc=None, thr=None, surf=None, silent=False):
    """ MESHTOOL

    query bbox: print the bounding box of a given mesh
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to query
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar


    query edges: print statistics of the node-to-node graph
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to query
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        -tags=tag1,tag2 ... (optional) List of region tags to compute the edge statistics on.


    query graph: print the nodal connectivity graph
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to query
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar


    query idx: print indices in a proximity to a given coordinate
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to query
        -coord=x,y,z	... (input) triple of x,y,z coordinates
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        -surf=<surface>	... (optional) index query is restricted to this surface
        -thr=<float>	... (optional input) threshold defining the proximity to the coordinate


    query idxlist: generate an index list file from a given coord list file.
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to query
        -coord=<file>	... (input) the coordinates file
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        -surf=<surface>	... (optional) index query is restricted to this surface


    query idxlist_uvc: apply 'query idx' for all coordinates in a file based on uvc coordinates
    parameters:
        -msh=<path>	    ... (input) path to basename of the uvc points file to perform query on (must be .uvc_pts)
        -coord=<file>	... (input) the uvc coordinates file holding the query points in uvc
        -uvc=<file>     ... (optional input) path to the ucv points file (default `basename`.uvc_pts)

    query coords_xyz: get the XYZ coordinates for all UVC coordinates provided in a file
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to perform query on
        -coord=<file>	... (input) the uvc coordinates file holding the query points in uvc
        -uvc=<file>	    ... (optional input) path to the ucv points file (default `basename`.uvc_pts)

    query quality: print mesh quality statistics
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to query
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        -thr=<float>	... (optional) output exact number of elements with quality above this threshold


    query tags: print the tags present in a given mesh
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to query
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar


    query smoothness: compute the nodal smoothness
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh to query
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
    """

    outdir = None
    MODES  = ['bbox', 'edges', 'graph', 'idx', 'idxlist', 'idxlist_uvc', 'coords_xyz', 'quality', 'tags', 'smoothness']
    assert mode in MODES, 'meshtool query detected incorrect mode: {}'.format(mode)

    cmd =  ['query', mode]
    cmd += ['-msh={}'.format(msh)]

    # -------------------------------------------------------------------------
    if 'bbox' in mode:
        pass
    # -------------------------------------------------------------------------
    elif 'edges' in mode:
        cmd += ['>', msh + '.mtquery']
    # -------------------------------------------------------------------------
    elif 'graph' in mode:
        assert False, 'bbox mode not yet implemented'
    #  ------------------------------------------------------------------------
    elif 'idxlist_uvc' in mode:
        cmd += ['-coord={}'.format(coord)]
        if uvc is not None:
            cmd += ['-uvc={}'.format(uvc)]
    #  ------------------------------------------------------------------------
    elif 'coords_xyz' in mode:
        cmd += ['-coord={}'.format(coord)]
        if uvc is not None:
            cmd += ['-uvc={}'.format(uvc)]
    #  ------------------------------------------------------------------------
    elif 'idxlist' in mode:
        cmd += ['-coord={}'.format(coord)]
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
        if surf is not None:
            cmd += ['-surf={}'.format(surf)]
    #  ------------------------------------------------------------------------
    elif 'idx' in mode:
        cmd += ['-coord={}'.format(coord)]
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
        if surf is not None:
            cmd += ['-surf={}'.format(surf)]
        if thr is not None:
            cmd += ['-thr={}'.format(thr)]
        assert False, 'bbox mode not yet implemented'
    # -------------------------------------------------------------------------
    elif 'quality' in mode:
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
        if thr is not None:
            cmd += ['-op={}'.format(thr)]
    # -------------------------------------------------------------------------
    elif 'tags' in mode:
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
    # -------------------------------------------------------------------------
    else:
        assert False, 'Unknown mode: {}'.format(mode)

    print(' '.join(cmd))
    execute(job, cmd, outdir=outdir, silent=silent)
    return

def smooth(job, mode, msh, surf=None, thr=None, lvl=None, iter=None, smth=None,
                           edge=None, ifmt=None, ofmt=None, outmsh=None,
                           tags=None, idat=None, nodal=None, odat=None,
                           silent=False):

    """smooth surface: Smooth one or multiple surfaces of a mesh.
    parameters:
        -msh=<path>		        ... (input) path to basename of the mesh
        -surf=<path1>,<path2>	... (input) list of surfaces to smooth.
        -thr=<float>	        ... (optional) Maximum allowed element quality metric. default is 0.9. Set to 0 to disable quality checking.
        -lvl=<int>		        ... (optional) Number of volumetric element layers to to use when smoothing surfaces. Default is 2.
        -iter=<int>		        ... (optional) Number of smoothing iter (default 100).
        -smth=<float>	        ... (optional) Smoothing coefficient (default 0.15).
        -edge=<float>	        ... (optional) Normal-vector angle difference (in degrees, default 0) defining a sharp edge.
                                    If set to 0, edge detection is turned off.
        -ifmt=<format>		    ... (optional) mesh input format. (carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar)
        -ofmt=<format>		    ... (optional) mesh output format. (carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar)
        -outmsh=<path>		    ... (output) path to basename of the output mesh

    smooth mesh: Smooth a mesh (surfaces and volume).
    parameters:
        -msh=<path>		        ... (input) path to basename of the mesh
        -tags=ta1,ta2/tb1,tb2	... (input) List of tag sets. The tags in one set have a common surface.
                                    Surfaces between different tag sets will be smoothed. Use * to include all tags, one
                                    tag per set. Use + to include all tags into one set.
        -thr=<float>		    ... (optional) Maximum allowed element quality. default is 0.9. Set to 0 to disable quality checking.
        -iter=<int>	    	    ... (optional) Number of smoothing iter (default 100).
        -smth=<float>		    ... (optional) Smoothing coefficient (default 0.15).
        -edge=<float>		    ... (optional) Normal-vector angle difference (in degrees, default 0) defining a sharp edge.
                                    If set to 0, edge detection is turned off.
        -ifmt=<format>		    ... (optional) mesh input format. (carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar)
        -ofmt=<format>		    ... (optional) mesh output format. (carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar)
        -outmsh=<path>		    ... (output) path to basename of the output mesh

    smooth data: Smooth data.
    parameters:
        -msh=<path>	    ... (input) path to basename of the mesh
        -idat=<path>	... (input) path to the input data file
        -iter=<int>	    ... (optional) Number of smoothing iter (default 100).
        -smth=<float>	... (optional) Smoothing coefficient (default 0.15).
        -nodal=<0|1>	... (optional) Set data representation: 0 = element data, 1 = nodal data. (default is 1).
        -ifmt=<format>	... (optional) mesh input format. may be: carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        -odat=<path>	... (output) path to the output data file
    """

    cmd =  ['smooth', mode]
    cmd += ['-msh={}'.format(msh)]

    if 'surface' in mode:
        pass
    # -------------------------------------------------------------------------
    elif 'mesh' in mode:
        cmd += ['-tags={}'.format(tags)]
        if thr is not None:
            cmd += ['-thr={}'.format(thr)]
        if iter is not None:
            cmd += ['-iter={}'.format(iter)]
        if smth is not None:
            cmd += ['-smth={}'.format(smth)]
        if edge is not None:
            cmd += ['-edge={}'.format(edge)]
        if ifmt is not None:
            cmd += ['-ifmt={}'.format(ifmt)]
        if ofmt is not None:
            cmd += ['-ofmt={}'.format(ofmt)]
        cmd += ['-outmsh={}'.format(outmsh)]
        outdir = os.path.dirname(outmsh)
    # -------------------------------------------------------------------------
    elif 'data' in mode:
        print ('Warning: smooth data not yet tested!')
    # -------------------------------------------------------------------------
    else:
        assert False, 'Unknown mode: {}'.format(mode)

    # check if output directory exists
    if os.path.exists(outdir):
        # reset variable
        outdir = None

    execute(job, cmd, outdir=outdir, silent=silent)
    return

def resample_surfmesh(job, msh, min, max, outmsh, surf_corr=None,
                    fix_bnd=None, uniform=None, ifmt=None, ofmt=None,
                    tags=None, silent=False):
    """ MESHTOOL
    refine surfmesh: refine or coarsen a surface triangle mesh to fit a given edge size range
    parameters:
        -msh=<path>		    ... (input) path to basename of the input mesh
        -min=<float>		... (input) min edge size
        -max=<float>		... (input) max edge size
        -outmsh=<path>		... (output) path to basename of the output mesh
        -surf_corr=<float>	... (optional) surface correlation parameter (default 0.85).
		                	    Edges connecting surface nodes with surface normals correlating greater
			                    than the specified amount can be collapsed.
        -fix_bnd=<int>		... (optional) Fix boundary of non-closed surfaces. 0 = no, 1 = yes. Default is 1.
        -uniform=<int>		... (optional) Edge-refinement is applied uniformly on selected tags.
                			    0 = no, 1 = yes. Default is 0.
        -ifmt=<format>		... (optional) format of the input mesh
        -ofmt=<format>		... (optional) format of the output mesh
        -tags=<tag lists>	... (optional) element tag lists specifying the regions
		                	    to perform refinement on

        The supported input formats are:
        carp_txt, carp_bin, vtk, vtk_bin, mmg, purk, stellar
        The supported output formats are:
        carp_txt, carp_bin, vtk, vtk_bin, vtk_polydata, mmg, stellar

        The tag lists have the following syntax: multiple lists are seperated by a "/",
        while the tags in one list are seperated by a "," character. The surface smoothness
        of the submesh formed by one tag list will be preserved.
    """

    # check if output directory exists
    outdir = os.path.dirname(outmsh)
    if os.path.exists(outdir):
        # reset variable
        outdir = None

    cmd =  ['resample', 'surfmesh']
    cmd += ['-msh={}'.format(msh)]
    cmd += ['-min={}'.format(min)]
    cmd += ['-max={}'.format(max)]
    cmd += ['-outmsh={}'.format(outmsh)]

    # -------------------------------------------------------------------------
    if surf_corr is not None:
        cmd+= ['-surf_corr={}'.format(surf_corr)]
    if fix_bnd is not None:
        cmd += ['-fix_bnd={}'.format(fix_bnd)]
    if uniform is not None:
        cmd += ['-uniform={}'.format(uniform)]
    if tags is not None:
        cmd += ['-tags={}'.format(tags)]
    if ifmt is not None:
        cmd += ['-ifmt={}'.format(ifmt)]
    if ofmt is not None:
        cmd += ['-ofmt={}'.format(ofmt)]
    # -------------------------------------------------------------------------



    execute(job, cmd, outdir=outdir, silent=silent)
    return


def map(job, submsh, files, outdir, mapName=None, silent=False):

        """ MESHTOOL
        map: map .vtx, .surf and .neubc files to the indexing of a submesh (obtained from 'meshtool extract mesh')
        parameters:
        submsh      	(input) path to basename of the submesh
        files	        (input) files to map to submesh. Can include patterns using *
        outdir          (output) directory to place mapped files
        mapName         (optional) Change the basename of the files mapped (Default= basename of the submsh)

        'patterns' is a comma separated list of files or glob patterns, e.g.:
        mesh/endo.vtx,mesh/endo.surf,mesh/*.neubc
        Any mapped files that would overwrite their source file are skipped.

        """

        mcmd=['map',
             '-submsh='+ submsh,
             '-files=' + files,
             '-outdir='+ outdir]

        execute(job, mcmd, silent=silent)

        if mapName is not None:
            filesbasename = os.path.basename(files)
            for char2strp in ['*','.vtx','.surf']:
                if char2strp in files:
                    filesbasename = filesbasename.strip(char2strp)

            for item in os.listdir(outdir):
                if filesbasename in item:
                    ifile = os.path.join(outdir, item)
                    print(filesbasename)
                    print(mapName)
                    ofile = ifile.replace(filesbasename, mapName)
                    job.mv(ifile,ofile)
        return

def clean_quality(job,imsh,surfs=None,omsh=None,thr=0.98,silent=False):

    """ MESHTOOL
        clean quality: deform mesh elements to reach a certain quality threshold value. Provided surfaces will be preserved.

        parameters:
        imsh      	  (input) path to basename of the mesh
        omsh	       (optional) output meshname. If not specified, overwrites imsh.
        thr          (optional) directory to place mapped files

        """

    if omsh is None: omsh = imsh

    mcmd = ['clean','quality',
            '-msh='+imsh,
            '-thr='+str(thr),
            '-outmsh='+omsh]

    if surfs is not None:
       # Warning('Surfaces are not currently supported!')
         surflist = ','.join((['{}.{}'.format(imsh,entry) for entry in surfs.split(',')]))
         mcmd += ['-surf={}'.format(surflist)]

    execute(job, mcmd, silent=silent)

    return
