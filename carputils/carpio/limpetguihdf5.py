
import tables
import numpy as np
from carputils.carpio.filelike import FileLikeMixin

DEFAULT_TITLE = 'carputils generated file for limpetgui'

class LimpetGUIHDF5File(FileLikeMixin):
    """
    limpetgui HDF5 file IO class.

    Parameters
    ----------
    filename : str
        The filename to open
    mode : str, optional
        'r' for read mode (default), 'w' for write mode
    """

    def __init__(self, filename, mode='r', title=DEFAULT_TITLE):
        self._h5file = tables.open_file(filename, mode, title)

    def close(self):
        """
        Close the file object.
        """
        self._h5file.close()

    def write(self, times=None, traces=[], named_traces={}):
        """
        Write data to the file.

        Parameters
        ----------
        times : array-like
            Array of times for the traces
        traces : list of array-like
            List of unnamed traces to write to file
        named_traces : dict of array-like
            Dict of name-trace pairs to write to file
        """

        if times is not None:
            # Check data sizes match
            trace = np.asarray(traces)
            for items in named_traces.values():
                assert len(np.concatenate((trace, items))) == len(times), 'trace does not match times'

        group = self._h5file.create_group('/', 'protocol_1', 'protocol 1')
       
        if times is not None:
            # Write times
            self._h5file.create_array(group, 'time', times, 'Sample Times')
        
        # Auto name the traces
        for i, trace in enumerate(traces):
            self._h5file.create_array(group, 'trace_{:03d}'.format(i+1), trace,
                                      'Trace {}'.format(i+1))

        # Write named traces
        for name, trace in named_traces.items():
            self._h5file.create_array(group, name.replace(' ', '_'), trace, name)

        self._h5file.flush()

def open(*args, **kwargs):
    """
    Open a limpetgui HDF5 file.

    Convenience method to provide normal python style interface to create a
    file type object.

    Parameters
    ----------
    filename : str
        The filename to open
    mode : str, optional
        'r' for read mode (default), 'w' for write mode
    """
    return LimpetGUIHDF5File(*args, **kwargs)
