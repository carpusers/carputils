#!/usr/bin/env python

"""
Provide functions and methods to create
a json import file for carp-viz
"""

import json
import subprocess
from carputils import settings
from datetime  import date


class CARPentryVIZ(object):
    """
    Collect carp parameters and group them for subsequent json file export.

    Args:
        author : string
                 Author of json file
        created : date string
                  Date of initial json file creation
        description : string
                      Detailed description of experiment
        last-edited : date string
                      Date of last modification
        title : string
                Short title
        parameter : class
                    List of parameters
    """

    def __init__(self, author=None, description='', lastedited=None, title=''):

        self.author      = author if author else self.get_author()
        self.created     = self.get_date()
        self.description = description
        self.lastedited  = self.get_date() if not lastedited else lastedited
        self.title       = title
        self.id_arg      = 0
        self.parameter   = []

    def get_date(self):
        today = date.today().isoformat()
        return today

    def get_author(self):

        # query with the help of git for a user name
        cmd    = ['git', 'config', 'user.name']
        string = subprocess.check_output(cmd).rstrip()
        author = string if string else None

        return author

    def __iadd__(self, item):
        # update global number of added parameters
        #self.id_arg += 1

        # increment id_arg automatically
        item.id_arg = self.id_arg

        if not hasattr(self, '_params'):
            setattr(self, '_params', [item])
        else:
            self._params.append(item)
        return self

    def append(self, item):
        if item is None:
            return self
        if type(item) == grp_param:
            # not planning to add empty group parameter
            if len(item.parameter):
                print('...adding carpviz group "{}"'.format(item.name))
                self.parameter.append(item)
            else:
                print('...skipping carpviz group "{}"'.format(item.name))
        else:
            # add any other given parameter
            self.parameter.append(item)
        return self

    def create_json(self):
        jdict = {}

        # add header to carpviz file
        jdict['author']      = self.author
        jdict['created']     = self.created
        jdict['description'] = self.description
        jdict['last-edited'] = self.lastedited
        jdict['title']       = self.title
        jdict['parameter']   = []

        # loop over all parameters
        for item in self.parameter:
            subjdict = item.to_json()
            jdict['parameter'].append(subjdict)
            del subjdict

        return jdict

    @staticmethod
    def json_dump(data, file):

        if not file.endswith('.json'):
            file += '.json'

        with open(file,'w') as out:
            json.dump(data, out, sort_keys=False, indent=2, ensure_ascii=False)
        return


class std_param(object):
    """
    Note: these parameters may be of type textfield or file.
    """
    def __init__(self, argname=None, gui=None, name=None, datatyp=None,
                       value=None, min=None, max=None, default=None,
                       info=None, unit=None,
                       isVisible=True, isValid=True, query=True):

        self.name         = name if name else argname.lstrip('-')    # descriptive gui name
        self.argumentname = argname                 # carp parameter name
        self.isVisible    = isVisible
        self.isValid      = isValid
        self.datatyp      = datatyp
        self.value        = value       # user specified value for carp argument
        self.min          = min
        self.max          = max
        self.default      = default     # default value of given carp argument
        self.gui          = gui         # file, textfield,
        self.units        = unit        # parameter units according to carp help
        self.info         = info        # more elaborate explanations in carp-viz gui
        self.id_arg       = -1          # position identifier within carp-viz gui

        # try to gather more information of
        if query:
            self.query_carp_param(argname)

        # add some additional checks to make sure that for file parameters
        # self.gui is of type 'file'
        if self.gui == 'textfield':
            if self.argumentname and 'file' in self.argumentname:
                self.gui = 'file'
                return
            if self.datatyp and self.datatyp.lower() == 'string':
                self.gui = 'file'
                return

    def pretty_print(self):
        print('name:         {}'.format(self.name))
        print('argumentname: {}'.format(self.argumentname))
        print('isVisible:    {}'.format(self.isVisible))
        print('isValid:      {}'.format(self.isValid))
        print('datatype:     {}'.format(self.datatyp))
        print('value:        {}'.format(self.value))
        print('min:          {}'.format(self.min))
        print('max:          {}'.format(self.max))
        print('default:      {}'.format(self.default))
        print('gui:          {}'.format(self.gui))
        print('units:        {}'.format(self.units))
        print('info:         {}'.format(self.info))
        print('id_arg:       {}'.format(self.id_arg))

    def to_json(self):
        jdict = {}
        jdict['name']         = self.name
        jdict['argumentname'] = self.argumentname if self.argumentname else ''
        jdict['datatyp']     = self.datatyp
        jdict['default']      = self.default if self.default else ''
        jdict['gui']          = self.gui
        jdict['info']         = self.info
        if self.id_arg >= 0:
            jdict['id_arg']   = self.id_arg
        jdict['isVisible']    = self.isVisible
        jdict['isValid']      = self.isValid
        if self.min is not None:
            if self.min or len(str(self.min)):  # need to include 0
                jdict['min'] = self.min
        if self.max is not None:
            if self.max or len(str(self.max)):  # need to include 0
                jdict['max'] = self.max
        if self.value is not None:              # need to include 0
            if self.value or len(str(self.value)):
                jdict['value'] = self.value
        if self.units:
            jdict['unit'] = self.units

        return jdict


    def query_carp_param(self, param):
        result = None

        exe    = settings.execs.CARP.__str__()
        param  = param.lstrip('-')
        cmd    = [exe, '+Help', param]

        try:
            process = subprocess.Popen(cmd, stdout=subprocess.PIPE)
            output, unused_err = process.communicate()
            string = output.lstrip().rstrip()
            string = string.replace('\t','').split('\n')
        except:
            string = ''

        #if len(string) > 50:
        #    # unsuccessful query showing default (all) CARP parameters
        #    return self

        for substring in string:

            if '{}:'.format(param) in substring:
                # start of the help output
                continue
            elif not substring:
                # skip empty lines
                continue
            elif 'type:'    in substring:
                self.datatyp = substring.split(':')[1]
                continue
            elif 'default:' in substring:
                substring    = substring[substring.rfind('('):]
                substring    = substring.lstrip('(').rstrip(')')
                substring    = substring.lstrip('"').rstrip('"')
                self.default = substring
                continue
            elif 'min:' in substring:
                substring = substring[substring.rfind('('):]
                substring = substring.lstrip('(').rstrip(')')
                substring = substring.lstrip('"').rstrip('"')
                self.min  = substring
                continue
            elif 'max:' in substring:
                substring = substring[substring.rfind('('):]
                substring = substring.lstrip('(').rstrip(')')
                substring = substring.lstrip('"').rstrip('"')
                self.max  = substring
                continue
            elif 'units:' in substring:
                self.units = substring.split(':')[1]
                continue
            elif 'Depends on:' in substring:
                # would need to collect the string between {...}
                return self
            elif 'Changes the default value of:' in substring:
                # would need to collect the string between {...}
                return self
            else:
                # assuming information on this parameter
                self.info = substring
                continue

        return self


class grp_param(std_param):
    """
    """
    def __init__(self, name='', isVisible=True, isValid=True, typ='group'):
        self.name      = name
        self.isVisible = isVisible
        self.isValid   = isValid
        self.typ       = typ
        self.parameter = []
        self.id_arg    = -1

    def __iadd__(self, item):
        if not hasattr(self, '_selections'):
            setattr(self, '_selections', [item])
        else:
            self._selections.append(item)
        return self

    def append(self, item):
        self.parameter.append(item)
        return self

    def to_json(self):
        jdict = {}
        jdict['name']      = self.name
        jdict['isVisible'] = self.isVisible
        jdict['isValid']   = self.isValid
        jdict['typ']       = self.typ
        if self.id_arg >= 0:
            jdict['id_arg']    = self.id_arg
        jdict['parameter'] = []

        # check for entries in this group
        if self.parameter:
            for item in self.parameter:
                jjdict = item.to_json()
                jdict['parameter'].append(jjdict)
                del jjdict
        return jdict


class dropdown_param(object):
    """
    """
    def __init__(self, value=None, typ='parameter', default=None, info='', name='',
                 id_arg=-1, isVisible=True, isValid=True, datatyp='selection',
                 gui='dropdown'):

        self.name         = name
        self.gui          = gui
        self.typ          = typ
        self.isVisible    = isVisible
        self.isValid      = isValid
        self.id_arg       = id_arg
        self.datatyp      = datatyp
        self.value        = value
        self.default      = default
        self.info         = info
        self.selections   = []

    def append(self, item):
        self.selections.append(item)

    def to_json(self):
        jdict = {}
        jdict['name']       = self.name
        jdict['gui']        = self.gui
        jdict['typ']        = self.typ
        jdict['isVisible']  = self.isVisible
        jdict['isValid']    = self.isValid
        jdict['datatyp']    = self.datatyp
        jdict['default']    = self.default if self.default else ''
        jdict['value']      = self.value
        jdict['info']       = self.info
        jdict['selections'] = []

        # check for entries in this group
        for item in self.selections:
            jjdict = item.to_json()
            jdict['selections'].append(jjdict)
            del jjdict

        return jdict


class select_param(object):
    """
    """
    def __init__(self, name='', info='', selected=[], notselected=[],
                 gui_visible=[], gui_not_visible=[]):
        self.name                     = name
        self.info                     = info
        self.selected                 = []
        self.notselected              = []
        self.selected_gui_visible     = gui_visible
        self.selected_gui_not_visible = gui_not_visible

        if selected:
            self.selected.append(selected)
        if notselected:
            self.notselected.append(notselected)

    def append(self, item):
        self.selected.append(item)

    def to_json(self):
        jdict = {}
        jdict['name']                     = self.name
        jdict['info']                     = self.info
        jdict['selected']                 = []
        jdict['notselected']              = []
        jdict['selected_gui_visible']     = self.selected_gui_visible     # name of this dictionary must be available
        jdict['selected_gui_not_visible'] = self.selected_gui_not_visible # name of this dictionary must be available

        # check for entries in this group
        for item in self.selected:
            jdict['selected'].append(item)

        return jdict


class arglist_param(object):
    """
    """
    def __init__(self, info='', name='', isVisible=True, isValid=True, datatyp='argumentlist',):

        self.name         = name
        self.isVisible    = isVisible
        self.isValid      = isValid
        self.datatyp      = datatyp
        self.info         = info
        self.arguments    = []

    def append(self, item):
        self.arguments.append(item)

    def to_json(self):
        jdict = {}
        jdict['name']      = self.name
        jdict['isVisible'] = self.isVisible
        jdict['isValid']   = self.isValid
        jdict['datatyp']   = self.datatyp
        jdict['info']      = self.info
        jdict['arguments'] = []

        # check for entries in this group
        for arg in self.arguments:
            jdict['arguments'].append(arg)

        return jdict


def search_dict(dict, keystr=None, valuestr=None):

    key_found   = None
    value_found = None

    for key, value in dict.items():
        if keystr is not None:
            # search based on a (partial) key string
            if keystr in key:
                key_found   = key
                value_found = value
                break

        if valuestr is not None:
            # need to make sure we are comparing the same data types
            if not isinstance(value, (str,unicode)):
                continue

            # search based on a (partial) value string
            if valuestr in value:
                key_found   = key
                value_found = value
                break

    # remove entries from list
    if key_found or value_found:
        del dict[key_found]

    return key_found, value_found, dict


def build_interface(job, args, cmd, title=None):

    # create main carp-viz object
    vizobj = CARPentryVIZ(title=title)

    # make a copy of the main options structure
    mainopts = cmd._mainopts[1:]
    mainopts = dict(zip(mainopts[::2], mainopts[1::2]))

    # -------------------------------------------------------------------------
    # Enforcing a structure of the json file
    # -------------------------------------------------------------------------


    # Launcher ----------------------------------------------------------------
    # mpirun or mpiexec
    # process count
    launchobj = grp_param(name='Launcher')
    launchobj.append(std_param(name    = 'MPI Launcher',
                               gui     = 'file',
                               argname = '', # skip this entry for the launcher
                               value   = settings.config.MPIEXEC,
                               default = settings.config.MPIEXEC,
                               datatyp = 'string',
                               info    = '',
                               query   = False))

    launchobj.append(std_param(name    = 'Process Count',
                               argname = '-np',
                               gui     = 'textfield',
                               value   = settings.cli.np,
                               default = 1,
                               datatyp = 'int',
                               info    = 'How many cores should be used in the simulation.',
                               query   = False))
    vizobj.append(launchobj)


    # Executable --------------------------------------------------------------
    exeobj = std_param(name  = 'Executable',
                       gui   = 'file',
                       value = settings.execs.CARP.__str__(),
                       info  = 'Path to the CARP executable',
                       query = False)
    vizobj.append(exeobj)


    # CARPentry parameter file ------------------------------------------------
    parobj = None
    _key, _value, mainopts = search_dict(mainopts, keystr='+F')
    if _key is not None:
        parobj = std_param(name    = 'Parameter File',
                           argname = _key,
                           gui     = 'file',
                           value   = _value,
                           info    = 'Parameter File',
                           query   = False)
    del _key, _value
    vizobj.append(parobj)


    # Meshname ----------------------------------------------------------------
    mshobj = None
    _key, _value, mainopts = search_dict(mainopts, keystr='meshname')
    if _key is not None:
        mshobj = std_param(name    = 'Mesh File',
                           argname = _key,
                           gui     = 'file',
                           value   = _value,
                           info    = 'Path to the mesh used for the simulation.')
    del _key, _value
    vizobj.append(mshobj)


    # Simulation Type ---------------------------------------------------------
    #   - monodomain, bidomain, pseudo_bidomain (augment_depth)
    simtypeobj = None
    _key, _value, mainopts = search_dict(mainopts, keystr='bidomain')
    if _key is not None:
        if   _value == 0: name = 'Monodomain'
        elif _value == 1: name = 'Bidomain'
        else :            name = 'Pseudo-bidomain'

        simtypeobj = dropdown_param(name    = 'Simulation Type',
                                    default = name,
                                    value   = name,
                                    info    = 'Select type of source model.')
        # Note: parameter "selected" might need to be a list of dicts
        simtypeobj.append(select_param(name='Monodomain',
                                       selected={'-bidomain': 0}))
        simtypeobj.append(select_param(name = 'Bidomain',
                                       selected={'-bidomain': 1}))
        simtypeobj.append(select_param(name='Pseudo-bidomain',
                                       selected={'-bidomain': 2}))

    del _key, _value
    vizobj.append(simtypeobj)


    # ---Simulation Control----------------------------------------------------
    # Note: the keys need to be listed below
    simobj = grp_param(name='Simulation Control')
    # keywords to be listed within this group (add more if you wish)
    keys = ['simID', 'tend', 'timedt', 'spacedt', 'dt']
    for key in keys:
        _key, _value, mainopts = search_dict(mainopts, keystr=key)
        if _key is not None:
            simobj.append(std_param(argname = _key,
                                    value   = _value,
                                    gui     = 'textfield'))
        del _key, _value
    del key
    vizobj.append(simobj)


    # ---Stimulus Control------------------------------------------------------
    stimobj = grp_param(name='Stimulation Control')
    # keywords to be listed within this group (add more if you wish)
    keys = ['num_stim'] + ['stimulus[']*30 + ['ground']*3
    for key in keys:
        _key, _value, mainopts = search_dict(mainopts, keystr=key)
        if _key is not None:
            stimobj.append(std_param(argname = _key,
                                     value   = _value,
                                     gui     = 'textfield'))
        del _key, _value
    del key
    vizobj.append(stimobj)


    # ---Stimulus Control------------------------------------------------------
    elecobj = grp_param(name='Electrical Parameters')
    # keywords to be listed within this group (add more if you wish)
    keys = ['gregion'] * 70
    for key in keys:
        _key, _value, mainopts = search_dict(mainopts, keystr=key)
        if _key is not None:
            elecobj.append(std_param(argname=_key,
                                     value=_value,
                                     gui='textfield'))
        del _key, _value
    del key
    vizobj.append(elecobj)


    # =========================================================================
    # Note: PETSc and PT
    #       Both flavors may be mixed. This makes exclusive (un-)hide of
    #       'PT Solver Options' and 'PETSC Solver Options' complicated!
    #   -optics_use_pt  Short
    #   -mech_use_pt    Short
    #   -purk_use_pt    Short
    #   -parab_use_pt   Short
    #   -ellip_use_pt   Short
    # =========================================================================
    solvtypeobj = grp_param(name='Solver Options')
    keys  = ['use_pt'] * 10 + ['parab_option']*5 + ['ellip_options']*5
    keys += ['num_io']*5 + ['_tol_']*5 + ['ode']*5
    for key in keys:
        _key, _value, mainopts = search_dict(mainopts, keystr=key)

        if _key is not None:
            solvtypeobj.append(std_param(argname = _key,
                                         value   = _value,
                                         gui     = 'textfield'))
        del _key, _value
    del key
    vizobj.append(solvtypeobj)


    # ---Options for Mechanics-------------------------------------------------
    mechobj = grp_param(name='Elasticity Options')
    # keywords to be listed within this group (add more if you wish)
    keys = ['mech']*50
    for key in keys:
        _key, _value, mainopts = search_dict(mainopts, keystr=key)
        if _key is not None:
            mechobj.append(std_param(argname = _key,
                                     value   = _value,
                                     gui     = 'textfield'))
        del _key, _value
    del key
    vizobj.append(mechobj)


    # ---Options for Mechanics-------------------------------------------------
    purkobj = grp_param(name='Purkinje Options')
    # keywords to be listed within this group (add more if you wish)
    keys = ['purk']*50 + ['PMJ']*10 + ['His_']*10 + ['PurkIon']*10 + ['pkje_']*10 + ['pmj']*5
    for key in keys:
        _key, _value, mainopts = search_dict(mainopts, keystr=key)
        if _key is not None:
            purkobj.append(std_param(argname = _key,
                                     value   = _value,
                                     gui     = 'textfield'))
        del _key, _value
    vizobj.append(purkobj)


    # ---Options for Mechanics-------------------------------------------------
    ekobj = grp_param(name='Eikonal Options')
    # keywords to be listed within this group (add more if you wish)
    keys = ['ekparams'] * 10 + ['ek_'] * 10 + ['ekregion'] * 50
    for key in keys:
        _key, _value, mainopts = search_dict(mainopts, keystr=key)
        if _key is not None:
           ekobj.append(std_param(argname = _key,
                                  value   = _value,
                                  gui     = 'textfield'))
           del _key, _value
    del key
    vizobj.append(ekobj)



    # ---Options for Visualisation---------------------------------------------
    visualobj = grp_param(name='Visualisation Settings')
    # keywords to be listed within this group (add more if you wish)
    keys = ['gridout'] * 10
    for key in keys:
        _key, _value, mainopts = search_dict(mainopts, keystr=key)
        if _key is not None:
            visualobj.append(std_param(argname=_key,
                                   value=_value,
                                   gui='textfield'))
            del _key, _value
    del key
    vizobj.append(visualobj)


    # ---Collecting everything not yet handled---------------------------------


    # Unhandled options will be collected as "expert options"
    nerdobj = grp_param(name='Expert Options')
    for _key, _value in mainopts.items():
        nerdobj.append(std_param(argname = _key,
                                 value   = _value,
                                 gui     = 'textfield'))
        del mainopts[_key], _key, _value

    #nerdobj = arglist_param(name='Expert Options')
    #for _key, _value in mainopts.items():
    #    nerdobj.append({_key: _value})
    #    del mainopts[_key], _key, _value
    vizobj.append(nerdobj)


    print('Done.')
    return vizobj
