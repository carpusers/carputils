
"""
Each module in this package defines a class for reading and writing a specific
data type. Some convenience methods are also provided for common file type
conversions.
"""

import numpy as np
from carputils import settings
from . import txt, igb, bin, quantity

READERS = {'.igb':      igb.open,
           '.igb.gz':   igb.open,
           '.dynpt':    igb.open,
           '.dynpt.gz': igb.open,
           '.dat':      txt.open,
           '.dat.gz':   txt.open,
           '.vec':      txt.open,
           '.vec.gz':   txt.open,
           '.vtx':      txt.open,
           '.vtx.gz':   txt.open,
           '.bin':      bin.open}

def show(msg, mode=1):
    if mode < 0:
        return

    try:
        _, c = os.popen('stty size','r').read().split()
    except:
        c = 80
    finally:
        c = max(int(c), 1)

    if mode == 0:
        print('#' + '='*(c-1))
        print('# ' + msg.upper())
        print('#' + '='*(c-1)+'\n')
    elif mode == 1:
        print('#' + '-'*(c-1))
        c_shift = 1 if len(msg) > (c+1) else int((c-len(msg))/2)
        print('#' + ' '*c_shift + msg)
        print('#' + '-'*(c-1)+'\n')
    else:
        print(msg)

def dat2adj(datfile, adjfile, grid='intra'):
    """
    Convert a .dat file to a nodal adjustment vector file.

    Parameters
    ----------
    datfile : str
        Path of .dat file to convert
    adjfile : str
        Path to write nodal adjustment vector file
    grid : str, optional
        String to enter in output file header as grid type
    """

    # Load dat file
    with txt.open(datfile) as fp:
        data = fp.data()

    # Add indicies
    data = np.column_stack((np.arange(len(data)), data))

    # Write nodal adjustment file
    with open(adjfile, 'w') as fp:

        # Write header
        fp.write('{}\n'.format(len(data)))
        fp.write('{}\n'.format(grid))

        # Write data
        np.savetxt(fp, data, fmt='%g')
