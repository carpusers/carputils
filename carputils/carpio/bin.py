"""
Provides a class for bench .bin file IO to/from python numpy arrays.
"""

import numpy as np
from carputils.carpio.filelike import FileLikeMixin

# Save original file open
fopen = open

class BinFile(FileLikeMixin):
    """
    Bench .bin format file IO class.

    This class is little more than an elaborate wrapper for the numpy fromfile
    and tofile functions, but is included for completeness in the
    :mod:`carputils.carpio` module.

    Parameters
    ----------
    filename : str
        The filename to open
    mode : str, optional
        'r' for read mode (default), 'w' for write mode
    """
    DTYPE = np.dtype('<d')

    def __init__(self, filename, mode='r'):
        
        self._filename = filename
        self._mode = mode

    def close(self):
        """
        Close the file object.
        """
        pass

    def data(self):
        """
        Return a numpy array of the file contents.

        Returns
        -------
        numpy.ndarray
            A numpy array with the file contents
        """

        assert self._mode == 'r'

        return np.fromfile(self._filename, dtype=self.DTYPE)

    def write(self, data):
        """
        Write a numpy array to a .dat or .vec file.

        Parameters
        ----------
        data : numpy.ndarray
            The array to be written to the IGB file
        """

        assert self._mode == 'w'

        np.array(data, dtype=self.DTYPE).tofile(self._filename)

def open(*args, **kwargs):
    """
    Open a bench .bin file.

    Convenience method to provide normal python style interface to create a
    file type object.

    Parameters
    ----------
    filename : str
        The filename to open
    mode : str, optional
        'r' for read mode (default), 'w' for write mode
    """
    return BinFile(*args, **kwargs)
