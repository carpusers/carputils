"""
Provides a class for .dat and .vec IO to/from python numpy arrays.
"""
import os
import numpy as np
from carputils.carpio.filelike import FileLikeMixin
from carputils import settings

# Save original file open
fopen = open


class TxtFile(FileLikeMixin):
    """
    file IO class for reading ascii files with extensions
    - dat(.gz)
    - elem
    - vec(.gz)
    - vtx
    - pts

    Direct read/write to gzipped files is possible. The class automatically
    determines if the file is gzipped by its extension.

    This class is little more than an elaborate wrapper for the numpy loadtxt
    and savetxt functions, but is included for completeness in the
    :mod:`carputils.carpio` module.

    Args:
        filename : str
            The filename to open
        mode : str, optional
            'r' for read mode (default), 'w' for write mode
    """

    def __init__(self, filename, mode='r'):
        
        self._filename = filename
        self._mode     = mode

        # assert that file exists

    def count(self):

        if self.check_ext('.dat.gz') or self.check_ext('.dat'):
            return range(2)
        elif self.check_ext('.vtx'):
            return range(2)
        elif self.check_ext('.pts'):
            return range(2)
        elif self.check_ext('.elem'):
            return range(3)
        elif self.check_ext('.vec.gz') or self.check_ext('.vec'):
            return self.__read_vec_file()


    def close(self):
        """
        Close the file object.
        """
        pass

    def data(self):
        """
        Return a numpy array of the file contents.

        Returns:
            numpy.ndarray
                A numpy array with the file contents
        """

        assert self._mode == 'r'

        if self.check_ext('.dat.gz') or self.check_ext('.dat'):
            return self.__read_dat_file()
        elif self.check_ext('.vtx'):
            return self.__read_vtx_file()
        elif self.check_ext('.pts'):
            return self.__read_pts_file()
        elif self.check_ext('.elem'):
            return self.__read_elem_file()
        elif self.check_ext('.vec.gz') or self.check_ext('.vec'):
            return self.__read_vec_file()
        else:
            raise Exception('Not yet implemented reader for {}'.format(self._filename))


    def __read_dat_file(self, **kwargs):
        """
        Read dat from .dat file

        Returns:
            1D numpy array
        """
        np.set_printoptions(suppress=True) # suppress exponential notation

        if self.check_ext('.gz'):
            # loadtxt automatically determines compression from file extension, so
            # no need to handle this ourselves
            data = np.loadtxt(self._filename)
        else:
            with fopen(self._filename, self._mode) as fp:
                data = fp.read()
                data = np.fromstring(data, dtype=np.float, sep='\n')

        return data


    def __write_dat_file(self, data, **kwargs):
        """
        Write data into .dat file

        Returns:
        """

        assert self._filename.endswith('.dat')
        assert self._mode == 'w'

        with fopen(self._filename, self._mode) as fp:
            # export data
            data_str = np.asarray(data, dtype=np.str)
            data_str = '\n'.join(data_str)
            fp.write(data_str)

        return


    def __read_vtx_file(self):
        """
        Read data from .vtx file

        Returns:
            data ... 1D numpy array of
            domain ... 'intra' or 'extra

        """
        assert self._filename.endswith('.vtx')
        assert self._mode == 'r'

        with fopen(self._filename, self._mode) as fp:
            num = np.int(fp.readline().strip())
            domain = fp.readline().strip()
            data = fp.readlines()
            data = np.asarray(data, dtype=np.int)

        return data, domain


    def __write_vtx_file(self, data, **kwargs):
        """
        Write vtx data fo file

        Returns:
            data ... 1D numpy array of
            domain ... 'intra' or 'extra

        """
        assert self._filename.endswith('.vtx')
        domain = kwargs['domain'] if 'domain' in kwargs.keys() else 'extra'

        if not isinstance(data, np.ndarray):
            # input may not always be an numpy array
            data = np.asarray(data, dtype=np.int)
        else:
            data = data.astype(np.int)

        with fopen(self._filename, self._mode) as fp:
            # export header
            fp.write('{}\n'.format(len(data)))
            fp.write('{}\n'.format(domain))
            # export data
            data_str = np.asarray(data, dtype=np.str)
            data_str = '\n'.join(data_str)
            fp.write(data_str)

        return


    def __read_pts_file(self):
        """
        Read data from .pts file

        Returns:
            pts ... 2D numpy array of node coordinates
            numpts ... number of points

        """
        np.set_printoptions(suppress=True) # suppress exponential notation

        assert self._filename.endswith('.pts')
        assert self._mode == 'r'

        with fopen(self._filename, self._mode) as fp:
            npts = np.int(fp.readline().strip())
            data = fp.read().replace('\n',' ')
            data = np.fromstring(data, dtype=np.float, sep=' ')
            data = np.reshape(data, (npts,3))
        return data, npts


    def __write_pts_file(self, data, **kwargs):
        """
        Write data into .pts file

        Returns:

        """

        assert self._filename.endswith('.pts')
        assert self._mode == 'w'
        np.set_printoptions(suppress=True)
        np.set_printoptions(threshold=np.prod(data.shape)) # avoid truncation of strings

        with fopen(self._filename, self._mode) as fp:

            data_str = np.array2string(data)
            data_str = data_str.replace('[','').replace(']','')
            data_str = data_str.replace("'",'')
            # enforce single blanks between numbers
            data_str = data_str.replace("\n",'n')
            data_str = ' '.join(data_str.split())
            data_str = data_str.replace("n ", chr(10))
            # add header
            data_str = str(data.shape[0]) + chr(10) + data_str
            # export entire string at once
            fp.write(data_str)
        return


    def __write_pts_file_(self, data, **kwargs):
        """
        Write data into .pts file

        Returns:

        """
        assert self._filename.endswith('.pts')
        assert self._mode == 'w'
        npoints = data.shape[0]

        # ready for export
        if self._mode == 'w':
            header = str(npoints)
        else:
            header = ''

        np.savetxt(self._filename, data, fmt='%f %f %f',    comments='', header=header)
        return


    def __write_lon_file_(self, data, **kwargs):
        """
        Write data into .lon file

        Returns:

        """
        assert self._filename.endswith('.lon')
        assert self._mode == 'w'
        nfibers = data.shape[0]
        header = ''

        np.savetxt(self._filename, data, fmt='%f %f %f',    comments='', header=header)


    def __read_elem_file(self):
        """
        Read data from .elem file
        Only tets are properly supported by now
        It should be easily extendable to elements of just one kind

        Returns:
            elem ... 2D numpy array of element indices
            tags ... 1D numpy array with element labels
            numelems ... number of elements in file

        """
        assert self._filename.endswith('.elem')
        assert self._mode == 'r'
        tt_found = False # found tetrahedron?
        qd_found = False # found quads?

        with fopen(self._filename, self._mode) as fp:
            nelems = np.int(fp.readline().strip())
            data = fp.read()
            if 'Tt' in data:
                tt_found = True
                data  = data.replace('Tt','')
                ncols = 5
            elif 'Qd' in data:
                qd_found = True
                data  = data.replace('Qd','')
                ncols = 5
            else:
                assert 0, 'txt.read current works just for tetrahedral and quadrilateral meshes'

            data = data.replace('\n',' ')
            data = np.fromstring(data, dtype=np.int, sep=' ')
            data = np.reshape(data, (nelems,ncols))

            # assuming tags are available
            tags = np.copy(data[:,-1])

            data = np.delete(data, [data.shape[1]-1], axis=1)
        return data, tags, nelems


    def __write_elem_file(self, elems, **kwargs):
        """
        Write element array (indices into pts file)
        into .elem file

        Note: Currently just implemented for tetrahedral element export

        Returns:
        """
        assert self._filename.endswith('.elem')
        assert self._mode == 'w'
        assert elems.shape[1] == 4, 'Export of only tetrahedra is currently supported!'

        etags = kwargs['etags'] if 'etags' in kwargs.keys() else None
        if etags is None:
            print('Assuming the element file is saved without labels!')
            elems_mod = elems
        else:
            assert elems.shape[0] == len(etags), 'Dimension of element list and label information does not match!'
            elems_mod = np.column_stack((elems, etags))

        # read for export
        np.savetxt(self._filename, elems_mod, fmt='Tt %u %u %u %u %i', comments='', header=str(len(etags)))

        return


    def __write_surf_file(self, surfs, **kwargs):
        """
        Write element array (indices into pts file)
        into .surf file

        Note: Currently just implemented for triangular element export

        Returns:
        """
        isTris  = False
        isQuads = False
        nsurfs = surfs.shape[0]
        if surfs.shape[1] == 3:
            isTris  = True
        elif surfs.shape[1] == 4:
            isQuads = True
        else:
            os.error('Export of only triangles and quads is supported!')

        assert self._filename.endswith('.surf')
        assert self._mode == 'w' or self._mode == 'a'

        etags = kwargs['etags'] if 'etags' in kwargs.keys() else None

        if etags is None:
            print('Assuming the element file is saved without labels!')
        else:
            assert nsurfs == len(etags), 'Dimension of element list and label information does not match!'
            elems_mod = np.column_stack((surfs, etags))

        # ready for export
        if self._mode == 'w':
            header = str(nsurfs)
        else:
            header = ''

        if isTris:
            np.savetxt(self._filename, surfs, fmt='Tr %u %u %u',    comments='', header=header)
        else:
            np.savetxt(self._filename, surfs, fmt='Qd %u %u %u %u', comments='', header=header)

        return


    def __read_vec_file(self):
        """
        Read data from .vec file

        Returns:
            vec ... 2D numpy array of node coordinates
            numpts ... number of points

        """
        np.set_printoptions(suppress=True) # suppress exponential notation

        assert self._filename.endswith('.vec')
        assert self._mode == 'r'

        with fopen(self._filename, self._mode) as fp:
            data = fp.read().replace('\n',' ')
            data = np.fromstring(data, dtype=np.float, sep=' ')
            nvec = len(data)//3     # floor division
            data = np.reshape(data, (nvec,3))
        return data, nvec


    def __write_vec_file(self, data, **kwargs):
        """
        Write data into .vec file

        Returns:

        """
        assert self._filename.endswith('.vec')
        assert self._mode == 'w'
        np.set_printoptions(suppress=True) # avoid scientific notation
        np.set_printoptions(threshold=np.prod(data.shape)) # avoid truncation of strings

        with fopen(self._filename, self._mode) as fp:

            data_str = np.array2string(data)
            data_str = data_str.replace('[','').replace(']','')
            data_str = data_str.replace("'",'')
            # enforce single blanks between numbers
            data_str = data_str.replace("\n",'n')
            data_str = ' '.join(data_str.split())
            data_str = data_str.replace("n ", chr(10))
            fp.write(data_str)
        return


    def write(self, data, **kwargs):
        """
        Write a numpy array to a .dat or .vec file.

        Args:
            data : numpy.ndarray
                The array to be written to the IGB file
        """
        assert self._mode == 'w'

        if self.check_ext('.dat.gz') or self.check_ext('.dat'):
            return self.__write_dat_file(data)

        elif self.check_ext('.vtx'):
            return self.__write_vtx_file(data, **kwargs)

        elif self.check_ext('.pts'):
            #return self.__write_pts_file_(data, **kwargs)
            return self.__write_pts_file(data, **kwargs)

        elif self.check_ext('.lon'):
            return self.__write_lon_file_(data, **kwargs)

        elif self.check_ext('.elem'):
            return self.__write_elem_file(data, **kwargs)

        elif self.check_ext('.surf'):
            return self.__write_surf_file(data, **kwargs)

        elif self.check_ext('.vec.gz') or self.check_ext('.vec'):
            return self.__write_vec_file(data)
        else:
            raise Exception('Not yet implemented writer for {}'.format(self._filename))

        return


    def check_ext(self, ext):
        return self._filename.endswith(ext)


def open(*args, **kwargs):
    """
    Open an .dat or .vec file.

    Convenience method to provide normal python style interface to create a
    file type object.

    Args:
        filename : str
            The filename to open
        mode : str, optional
            'r' for read mode (default), 'w' for write mode
    """
    return TxtFile(*args, **kwargs)


def read(*args, **kwargs):
    """
    read data based on file extension

    Handled file types: .dat, .vec, .vtx, .pts

    Args:
        filename : str
            The filename to open
        mode : str, optional
            'r' for read mode (default), 'w' for write mode

    Returns:
        data ... 1D or 2D array of integers or float
	    nelems ... in the case of an .elem file
	    tags ... in the case of an .elem file
        npts ... in case of a .pts file
        domain ... in case of .vtx file
    """
    if not settings.cli.silent:
            print('Reading file: '+str(*args))

    if not settings.cli.dry:
        return TxtFile(*args, **kwargs).data()
    else:
        return TxtFile(*args,**kwargs).count()


def write(filename, data, **kwargs):

    if not settings.cli.silent:
            print('Writing file: '+filename)

    if not settings.cli.dry:
        mode = kwargs['mode'] if 'mode' in kwargs.keys() else 'w'

        TxtFile(filename, mode=mode).write(data, **kwargs)

    return
