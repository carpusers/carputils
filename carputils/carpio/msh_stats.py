#!/usr/bin/python

import os
import numpy as np
from carputils.carpio import txt, show


def queryEdges(basemesh):
    """ QUERYEDGES
    query min/mean/max sizes of with given mesh (obtained from 'meshtool query edges')

    parameters:
    basemesh     (input) path to basename of the mesh

    returns:
        tuple of minimum, mean and maximum existing edge length

    """
    min_el  = -1
    mean_el = -1
    max_el  = -1

    queryFile = basemesh + '.mtquery'

    if not os.path.exists(queryFile):

        # compile meshtool command
        cmd = 'meshtool query edges -msh={} > {}'.format(basemesh, queryFile)
        # Note: no need to silence command
        os.system(cmd)

    with open(queryFile, 'r') as fid:
        lines = fid.readlines()

        rows  = -1
        for line in lines:
            rows += 1

            if "Edge lengths" in line:
                strOI   = lines[rows+2].strip().rstrip(')').split(',')

                mean_el = float(strOI[1].split(':')[1])
                min_el  = float(strOI[2].split(':')[1])
                max_el  = float(strOI[3].split(':')[1])
                break

    return min_el, mean_el, max_el


def write(msh):
    """
    Write the mesh stats (m_el and centroid) to msh+'.stats.out.txt

    params:
        msh     (input) Meshname
    outputs:
        msh+'.stats.txt'

    """

    stats_file = msh + '.stats.txt'

    if not os.path.exists(stats_file):
        show('Writing stats to file: {}'.format(stats_file),mode=2)
        msh_pts, _ = txt.read(msh+'.pts')
        centroid = np.array(np.matrix(msh_pts).mean(0))[0]
        _ , m_el, _ = queryEdges(msh)

        with open(stats_file, 'w') as f:
            f.write(' '.join(([str(x) for x in centroid]))+'\n')
            f.write(str(m_el))

        f.close()


def read(msh):
    """
    Read a mesh stats file (msh+'.stats.txt) for centroid and mean edge length.

    params
    msh:    (input) Meshname of a mesh with an already generated stats file.

    return:
        m_el, centroid
    """
    stats_file = msh + '.stats.txt'

    if not os.path.isfile(stats_file):
        raise IOError('Mesh stat file does not yet exist. Please write the file first.')

    with open(stats_file, 'r') as f:
        centroid = np.array(f.readline().split(' '), dtype=float)
        m_el = float(f.readline())
    f.close()

    show('Centroid: {}'.format(centroid))
    show('Mean edge length: {}'.format(m_el))

    return m_el, centroid



