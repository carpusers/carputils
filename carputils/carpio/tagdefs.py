"""
Provides a class for .dat and .vec IO to/from python numpy arrays.
"""

from carputils import dspace

# Save original file open
fopen = open


class tagsdict(dspace):
    """
    Generate the tag and option dictionary for simulation based on the
    argument flags. Reads and parses the tags file provided by --tags.
    Then operations are constructed for mesh and surface generation.
    """

    def __init__(self, filename, mode='r'):
        super(tagsdict, self).__init__()

        with open(filename, mode) as fp:
            tmp = [line.split(' ')[0].strip('\n') for line in fp.readlines() if 'T_' in line and line.split(' ')[0].split('=')[1] is not '']

        list = dspace(**dict([x.split('=') for x in tmp]))

        self.add('list', list)
        self.add('filename', filename)