"""
Implement a file-like mixin to implement self-closing in context managers.
"""


class FileLikeMixin(object):
    """
    Implements a mixin that essentially allows self-closing when exiting a with
    block.

    For information on context managers see:
    https://docs.python.org/2/library/stdtypes.html#context-manager-types
    """

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # Close the file
        self.close()
        # Do not suppress any exceptions
        return False
