import sys
import time
import functools

# provide python2 to python3 compatibility
isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False


def stopwatch(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        t0 = time.time()
        return_val = func(*args, **kwargs)
        t1 = time.time()
        print('"{}" done in {} sec'.format(func.__name__, t1-t0))
        return return_val
    return wrapper


class dspace(object):

    def __init__(self, **kwargs):
        self._attrdict = {}
        self.extend(kwargs)

    def add(self, attrname, attrvalue):
        if not attrname[0].isalpha():
            raise SyntaxError("attribute names have to begin with an alphabetic "
                              "character (`{}`)!".format(attrname))
        self._attrdict[attrname] = attrvalue

    def extend(self, attrdict):
        if isPy2: it = attrdict.iteritems
        else:     it = attrdict.items
        for attrname, attrvalue in it():
            self.add(attrname, attrvalue)

    def iterattr(self):
        if isPy2: it = self._attrdict.iteritems
        else:     it = self._attrdict.items
        return it()

    def __getattr__(self, attrname):
        return self._attrdict[attrname]

    def __getitem__(self, attrname):
        return self._attrdict[attrname]

    def __setitem__(self, attrname, attrvalue):
        self.add(attrname, attrvalue)
        
    def __delitem__(self, attrname):
        del self._attrdict[attrname]

    def __len__(self):
        return len(self._attrdict)

    def __str__(self):
        return str(self._attrdict)

    def prettify(self, level=0, indent=3):
        pstr, istr = '', ' '*indent*level

        if isPy2: it = self._attrdict.iteritems
        else:     it = self._attrdict.items

        for attrname, attrvalue in it():
            if isinstance(attrvalue, dspace):
                pstr += istr + '{}:\n'.format(attrname)
                pstr += attrvalue.prettify(level+1, indent)
            else:
                pstr += istr + '{}: {}\n'.format(attrname, attrvalue)
        return pstr

    __repr__ = __str__

    def __iadd__(self, other):
        if isinstance(other, dict):
            self.extend(other)
            return self
        elif isinstance(other, dspace):
            self.extend(other._attrdict)
            return self
        else:
            raise TypeError('can only add dictionaries of dictspaces to a dictspace!')
