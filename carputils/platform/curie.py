
from carputils.platform.general import BatchPlatform

TEMPLATE = """#!/bin/bash
#MSUB -r TBunnyC2     # Request name
#MSUB -n {nproc}      # Number of tasks to use
#MSUB -T {walltime}   # Elapsed time limit in seconds
#MSUB -o {jobID}_%I.o # Standard output. %I is the job id
#MSUB -e {jobID}_%I.e # Error output. %I is the job id
#MSUB -A pa1332       # Project ID
#MSUB -q standard     # Choosing standard nodes

set -x
cd $BRIDGE_MSUB_PWD
"""

class Curie(BatchPlatform):
    """
    Run on the Curie supercomputer

    Info: http://www-hpc.cea.fr/en/complexe/tgcc-curie.htm
    """

    SUBMIT    = 'ccc_msub'
    LAUNCHER  = 'ccc_mprun'
    BATCH_EXT = '_Script'

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):
        return TEMPLATE.format(jobID=jobID, nproc=nproc, walltime=walltime)
