
from carputils.platform.general import BatchPlatform

__all__ = ['SuperMUCFat',
           'SuperMUCThin',
           'SuperMUCInteractive']

TEMPLATE = """#!/bin/bash
##
## optional: energy policy tags
#@ energy_policy_tag = carp_energy_tag
#@ minimize_time_to_solution = yes
#
#@ job_type = MPICH
#@ class = {llcls}
{islandline}#@ node = {nnode}
#@ tasks_per_node = {procpernode}
#@ wall_clock_limit = {walltime}
#@ job_name = {jobID}
#@ network.MPI = sn_all,not_shared,us
#@ initialdir = .
#@ output = {jobID}_$(jobid).out
#@ error = {jobID}_$(jobid).err
#@ notification=always
#@ notify_user={email}
#@ queue
. /etc/profile
. /etc/profile.d/modules.sh

# Setup of environment
module load prace

# Use Intel MPI
module unload mpi.ibm
module load mpi.intel
"""

ISLAND_TPL = '#@ island_count = {}\n'

class SuperMUCAbstract(BatchPlatform):

    SUBMIT    = 'llsubmit'
    LAUNCHER  = 'mpiexec'
    BATCH_EXT = '.ll'

    PROCPERNODE   = None
    TYPESHORTNAME = None

    @classmethod
    def _class(cls, nproc):
        raise NotImplementedError()

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):

        nproc = int(nproc)

        # Check sensible number of nodes
        tpl = 'Use a multiple of {} processes on SuperMUC {} nodes'
        assert nproc % cls.PROCPERNODE == 0, tpl.format(cls.PROCPERNODE,
                                                        cls.TYPESHORTNAME)

        nnode = int(nproc / cls.PROCPERNODE)

        # Get the class and island settings
        llcls, islands = cls._class(nnode)
        islandline = '' if islands is None else ISLAND_TPL.format(islands)
        
        return TEMPLATE.format(llcls=llcls, islandline=islandline, jobID=jobID,
                               nnode=nnode, procpernode=cls.PROCPERNODE, 
                               walltime=walltime, email=email)

class SuperMUCFat(SuperMUCAbstract):
    """
    Run on fat nodes at the SuperMUC petascale system in Germany

    Info: https://www.lrz.de/services/compute/supermuc/
    """
    
    PROCPERNODE   = 40
    TYPESHORTNAME = 'fat'
    
    @classmethod
    def _class(cls, nnode):
        return 'fat', None

class SuperMUCThin(SuperMUCAbstract):
    """
    Run on thin nodes at the SuperMUC petascale system in Germany

    Info: https://www.lrz.de/services/compute/supermuc/
    """

    PROCPERNODE   = 16
    TYPESHORTNAME = 'thin'

    @classmethod
    def _class(cls, nnode):
        if nnode <= 32:
            return 'micro',   '1,2'
        elif nnode <= 512:
            return 'general', '1,2'
        else:
            return 'special', '2,4'

class SuperMUCInteractive(SuperMUCAbstract):
    """
    Run interatively at the SuperMUC petascale system in Germany

    Info: https://www.lrz.de/services/compute/supermuc/
    """

    BATCH    = False
    SUBMIT   = None
    LAUNCHER = 'llrun'
