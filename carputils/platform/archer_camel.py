"""
ARCHER project camel platform script
"""
from carputils.platform.general import BatchPlatform

TEMPLATE = """#!/bin/bash --login
#PBS -N {jobID:.15s}
#PBS -l select={nnode}
#PBS -l walltime={walltime}
#PBS -m bea
#PBS -M {email}
#PBS -A pr1u1362

# Make sure any symbolic links are resolved to absolute path
export PBS_O_WORKDIR=$(readlink -f $PBS_O_WORKDIR)

# Change to the directory that the job was submitted from
# (remember this should be on the /work filesystem)
cd $PBS_O_WORKDIR

# Set the number of threads to 1
#   This prevents any system libraries from automatically
#   using threading.
export OMP_NUM_THREADS=1

"""

class ArcherCamel(BatchPlatform):
    """
    Run jobs on the ARCHER UK National Supercomputing Service

    Info: http://archer.ac.uk/
    """

    SUBMIT = 'qsub'
    LAUNCHER = 'aprun'
    BATCH_EXT = '.pbs'

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):

        nproc = int(nproc)

        # Check sensible number of nodes
        assert nproc % 24 == 0, 'Use a multiple of 24 processes on ARCHER'

        nnode = int(nproc / 24)

        return TEMPLATE.format(jobID=jobID, nnode=nnode, walltime=walltime,
                               email=email)

    @classmethod
    def polling(cls, polling_opts, nproc, nproc_job, script):

        if polling_opts is None:
            return None

        poll_tmp = "poll=60  # Seconds. Few percent of typical execution time.\n"

        # Get number of nodes
        nnodes = int(nproc / 24)

        # Check sensible number of nodes per job
        # Note that you cannot use aprun to run more than one application
        # on a single node at the same time and that none of the single aprun
        # commands can ask for more nodes than the job has in total
        assert nproc_job % 24 == 0,\
            'Use a multiple of 24 processes per job on ARCHER. aprun does not '\
            'allow to run more than one application on a single node.'
        assert nproc_job <= nproc,\
            'Number of Processes per job has to be smaller than the total '\
            'number of processes!'

        poll_tmp = "p={}\n\n".format(nnodes)

        # Add polling file options
        poll_tmp += "\n\n" + "runopts=(\n"

        nruns = len(polling_opts)
        for i in range(nruns):
            poll_tmp += '  \"' + \
                        str(polling_opts[i]).replace('\n', '').replace('\r', '') + \
                        '\"\n'

        poll_tmp += ")\n"
        poll_tmp += "nruns=${#runopts[@]}\n\n"

        poll_tmp += "(\n" \
                    "    for ((r=0; r<$nruns; r++)); do\n" \
                    "        while (($(jobs -r | wc -l) >= p))\n" \
                    "        do\n" \
                    "            sleep $poll\n" \
                    "        done\n\n" \
                    "        # Launch the parallel job\n"
        poll_tmp += "        "
        poll_tmp += script + " ${runopts[$r]} &\n"
        poll_tmp += "    done\n" \
                    "    wait   # for the last p apruns to finish\n" \
                    ")\n"
        return poll_tmp
