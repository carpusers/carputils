
from __future__ import print_function
from carputils.platform.general import BatchPlatform
from carputils.format import terminal_width

TEMPLATE = """#!/bin/bash
#SBATCH --job-name={jobID}
#SBATCH --nodes={nnode}
#SBATCH --ntasks-per-node=36
#SBATCH --partition={partition}
#SBATCH --account=Pra15_3333
#SBATCH --error={jobID}.err
#SBATCH --output={jobID}.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user={email}
#SBATCH --time={walltime}

# Make sure any symbolic links are resolved to absolute path
export SLURM_SUBMIT_DIR=$(readlink -f $SLURM_SUBMIT_DIR)

# Change to the directory that the job was submitted from
# (remember this should be on the /work filesystem)
cd $SLURM_SUBMIT_DIR

# Set the number of threads to 1
#   This prevents any system libraries from automatically
#   using threading.
export OMP_NUM_THREADS=1

module load intel intelmpi
"""

class MarconiSlurm(BatchPlatform):
    """
    Run jobs on the MARCONI HPC Cluster

    Info: http://www.hpc.cineca.it/
    """

    SUBMIT = 'sbatch'
    LAUNCHER = 'srun'
    BATCH_EXT = '.slrm'
    PARTITION = 'bdw_usr_prod'

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, ddt=False, cuda=False,
                     *args, **kwargs):
        #cmd = [cls.LAUNCHER, '-n', nproc]
        cmd = [cls.LAUNCHER, '--mpi=pmi2']
        cmd += carp_cmd

        if ddt:
            # Run with ddt in reverse connection mode
            cmd = ['ddt', '--connect',
                   '--cuda' if cuda else '--no-cuda']+cmd

        return cmd

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):
        """
        Define header for submission script on Marconi
        """
        nproc = int(nproc)

        # Check sensible number of nodes
        assert nproc % 36 == 0, 'Use a multiple of 36 processes on MARCONI'

        nnode = int(nproc / 36)

        return TEMPLATE.format(jobID=jobID, nnode=nnode, walltime=walltime,
                               email=email, partition=MarconiSlurm.PARTITION)


class MarconiDebug(BatchPlatform):
    """
    Run jobs on the MARCONI HPC Cluster

    Info: http://www.hpc.cineca.it/
    """

    SUBMIT = 'sbatch'
    LAUNCHER = 'srun'
    BATCH_EXT = '.slrm'
    PARTITION = 'bdw_usr_dbg'

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, ddt=False, cuda=False,
                     *args, **kwargs):
        #cmd = [cls.LAUNCHER, '-n', nproc]
        cmd = [cls.LAUNCHER, '--mpi=pmi2']
        cmd += carp_cmd

        if ddt:
            # Run with ddt in reverse connection mode
            cmd = ['ddt', '--connect',
                   '--cuda' if cuda else '--no-cuda']+cmd

        return cmd

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):

        nproc = int(nproc)
        # Check sensible number of nodes
        assert nproc%36 == 0, 'Use a multiple of 36 processes on MARCONI'


        termw = terminal_width()
        decstr = '='*(termw-1)

        # Check if number of processes is greater than 144
        if nproc > 144:
            nproc = 144
            print('\n#{0}\n#\tWARNING: number of processes limited to 144 '
                  'on the debug partition!\n#{0}\n'.format(decstr))

        # Check if runtime is more than 30 minutes
        try:
            hours, mins, secs = map(int, walltime.split(':'))
        except ValueError:
            walltime = '00:30:00'
            print('\n#{0}\n#\tWARNING: runtime limited to 30 minutes '
                  'on the debug partition!\n#{0}\n'.format(decstr))
        else:
            if (hours*3600 + mins*60 + secs) > 1800:
                walltime = '00:30:00'
                print('\n#{0}\n#\tWARNING: runtime limited to 30 minutes '
                      'on the debug partition!\n#{0}\n'.format(decstr))

        nnode = int(nproc / 36)

        return TEMPLATE.format(jobID=jobID, nnode=nnode, walltime=walltime,
                               email=email, partition=MarconiDebug.PARTITION)
