"""
Platform-specific functionality such as MPI launcher generation.

To load a particular hardware profile:

>>> from carputils import platform
>>> profile = platforms.get('myplatform')

To add new hardware profiles, duplicate an existing one inside this directory,
adapt it to your needs, and add it to the PLATFORMS dictionary below.
"""

from collections import OrderedDict

from carputils.platform.desktop       import Desktop
from carputils.platform.archer        import Archer
from carputils.platform.archer_intel  import ArcherIntel
from carputils.platform.archer_camel  import ArcherCamel
from carputils.platform.curie         import Curie
from carputils.platform.marconi       import Marconi
from carputils.platform.marconi_slurm import *
from carputils.platform.medtronic     import Medtronic
from carputils.platform.mephisto      import Mephisto
from carputils.platform.supermuc      import *
from carputils.platform.vsc2          import VSC2
from carputils.platform.vsc3          import VSC3
from carputils.platform.vsc4          import VSC4
from carputils.platform.wopr          import Wopr

# Use ordered dictionary to store platforms, ensuring that desktop is first and
# others are in alphabetical order
PLATFORMS = OrderedDict()

PLATFORMS['desktop']       = Desktop
PLATFORMS['archer']        = Archer
PLATFORMS['archer_intel']  = ArcherIntel
PLATFORMS['archer_camel']  = ArcherCamel
PLATFORMS['curie']         = Curie
PLATFORMS['marconi']       = Marconi
PLATFORMS['marconi_slurm'] = MarconiSlurm
PLATFORMS['marconi_debug'] = MarconiDebug
PLATFORMS['medtronic']     = Medtronic
PLATFORMS['mephisto']      = Mephisto
PLATFORMS['smuc_f']        = SuperMUCFat
PLATFORMS['smuc_t']        = SuperMUCThin
PLATFORMS['smuc_i']        = SuperMUCInteractive
PLATFORMS['vsc2']          = VSC2
PLATFORMS['vsc3']          = VSC3
PLATFORMS['vsc4']          = VSC4
PLATFORMS['wopr']          = Wopr

def list():
    return PLATFORMS.keys()

def get(key):
    return PLATFORMS[key]
