
from carputils.platform.general import Platform
from carputils.debug import mpi_attach_debugger

class Desktop(Platform):
    """
    Desktop platform, providing extensive debugging and profiling capabilities
    """

    @classmethod
    def add_launcher(cls, carp_cmd, nproc, gdb_procs=None, ddd_procs=None,
                     lldb_procs=None, ddt=False, valgrind=None, map=False,
                     cuda=False):

        nproc = int(nproc)

        if gdb_procs is not None:

            if valgrind is not None:
                # Run with valgrind + gdb
                # gdb procs have --vgdb-error=1 set, others do not
                unsel = ['valgrind'] + ['--' + o for o in valgrind]
                sel = unsel + ['--vgdb-error=1']
                xterm = False

            else:
                # gdb only
                sel   = ['gdb', '--args']
                unsel = []
                xterm = True

            cmd = mpi_attach_debugger(cls.LAUNCHER, carp_cmd, nproc, gdb_procs,
                                      sel, unsel, xterm=xterm)

        elif valgrind is not None:

            # Run valgrind without gdb
            cmd = ['valgrind'] + ['--' + o for o in valgrind]

            if nproc > 1:
                cmd = [cls.LAUNCHER, '-n', nproc] + cmd

            cmd += carp_cmd

        elif ddd_procs is not None:

            # Normal launcher
            cmd = [cls.LAUNCHER, '-n', nproc]
            cmd += carp_cmd

            # Get PETSc to launch debugger
            cmd += ['+', '-start_in_debugger', 'ddd']

            # Set specific procs if requested
            if len(ddd_procs) != 0:
                cmd += ['-debugger_nodes',
                        ','.join(str(i) for i in ddd_procs)]

        elif lldb_procs is not None:

            # lldb only
            sel   = ['lldb', '--']
            unsel = []
            xterm = True

            cmd = mpi_attach_debugger(cls.LAUNCHER, carp_cmd, nproc, lldb_procs,
                                      sel, unsel, xterm=xterm)

        elif ddt:
            cmd = ['ddt',
                   '--cuda' if cuda else '--no-cuda',
                   cls.LAUNCHER, '-n', nproc]
            cmd += carp_cmd

        elif map:
            cmd = ['map',
                   cls.LAUNCHER, '-n', nproc]
            cmd += carp_cmd

        else:
            if nproc == 1:
                cmd = carp_cmd
            else:
                cmd = [cls.LAUNCHER, '-n', nproc]
                cmd += carp_cmd

        return cmd
