
from carputils.platform.general import BatchPlatform

TEMPLATE = """#!/bin/sh -f
#$ -V
#$ -cwd
#$ -j y
#$ -m be
#$ -pe mpich {nproc}
#$ -l h_rt={walltime}
#$ -o {jobID}_{nproc}.$JOB_ID
#$ -N {jobID}_{nproc}

#export I_MPI_DAT_LIBRARY=/usr/lib64/libdat2.so.2
#export OMP_NUM_THREADS=1
#export I_MPI_FABRICS=shm:dapl
#export I_MPI_FALLBACK=0
#export I_MPI_CPUINFO=proc
#export I_MPI_PIN_PROCESSOR_LIST=0-15
#export I_MPI_JOB_FAST_STARTUP=0
"""

class VSC2(BatchPlatform):
    """
    Run on VSC-2 at the Vienna Scientific Cluster

    Info: http://vsc.ac.at/systems/vsc-2/
    """

    SUBMIT    = 'qsub'
    LAUNCHER  = 'mpirun'
    BATCH_EXT = '.pbs'

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):
        return TEMPLATE.format(jobID=jobID, nproc=nproc, walltime=walltime)
