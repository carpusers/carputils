
from carputils.platform.general import BatchPlatform

TEMPLATE = """#!/bin/bash --login
#PBS -N {jobID:.15s}
#PBS -l nodes={nnodes}:ppn={ppn}
#PBS -l walltime={walltime}

SCRATCH_DIR=/usr/sci/cluster/username/$USER/$PBS_JOBID

# make sure the scratch directory is created
mkdir -p $SCRATCH_DIR

# copy datafiles from directory where I typed qsub, to scratch directory
cp -r $PBS_O_WORKDIR/* $SCRATCH_DIR/

#change to the scratch directory
cd $SCRATCH_DIR

# Set the number of threads to 1
#   This prevents any system libraries from automatically
#   using threading.
export OMP_NUM_THREADS=1

"""

class Wopr(BatchPlatform):
    """
    Run jobs on SCI GPU cluster wopr 

    Info: https://internal.sci.utah.edu/cluster 
    """

    SUBMIT    = 'qsub'
    LAUNCHER  = 'mpiexec'
    BATCH_EXT = '.pbs'

    @classmethod
    def header(cls, jobID, nproc, walltime, queue, cuda, email):

        nproc = int(nproc)
        ppn   = 8

        # Check sensible number of nodes
        assert nproc % ppn == 0, 'Use a multiple of 8 processes on WOPR'

        nnodes = int(nproc / ppn)
        
        return TEMPLATE.format(jobID=jobID, nnodes=nnodes, ppn=ppn, walltime=walltime)

    @classmethod
    def footer(cls):
        return "cp -r $SCRATCH_DIR/* $PBS_O_WORKDIR/"
