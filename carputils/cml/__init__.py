from carputils.cml.cmlconverter import *
from carputils.cml.cmlparser import *
from carputils.cml.cmltree import *

YAMLParser = CMLParserYAML(emptyValue='')


def load_as_dict(fp, parser=YAMLParser):
    root = parser.parsef(fp)
    return root.serialize()[1]


def loads_as_dict(string, parser=YAMLParser):
    root = parser.parses(string)
    return root.serialize()[1]


def load_as_dict_flat(fp, parser=YAMLParser):
    root = parser.parsef(fp)
    return CMLDictOperations.flatten(root.serialize()[1])


def loads_as_dict_flat(string, parser=YAMLParser):
    root = parser.parses(string)
    return CMLDictOperations.flatten(root.serialize()[1])


def load_as_object(fp, parser=YAMLParser):
    root = parser.parsef(fp)
    return root.objectify()[1]


def loads_as_object(string, parser=YAMLParser):
    root = parser.parses(string)
    return root.objectify()[1]


def load(fp, parser=YAMLParser):
    return load_as_dict(fp, parser=parser)


def loads(string, parser=YAMLParser):
    return loads_as_dict(string, parser=parser)