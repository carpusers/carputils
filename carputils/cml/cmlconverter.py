
class CMLConverter:

    def __init__(self):
        pass

    @staticmethod
    def is_valid_string(s):
        """
        Checks whether the given object is a valid string or not

        Parameters
        ----------
        s : string
            The string to be checked

        Returns
        -------
        True if the given object is a valid string,
        False otherwise
        """
        if type(s) != str:
            return False
        if len(s) == 0:
            return False
        return True

    @staticmethod
    def is_identifier(s):
        """
        Checks whether the given object is a valid identifier or not

        Parameters
        ----------
        s : string
            The string to be checked

        Returns
        -------
        True if the given object is a valid string,
        False otherwise
        """
        if not CMLConverter.is_valid_string(s):
            return False
        if not (s[0].isalpha() or s[0] == '_'):
            return False
        for c in s[1:]:
            if not (c.isalnum() or c == '_'):
                return False
        return True

    @staticmethod
    def make_identifier(s, substitute='_'):
        """
        Tries to convert the given object to a valid identifier

        Parameters
        ----------
        s : string
            The string to be converted

        Returns
        -------
        The identifier string
        """
        if not (CMLConverter.is_valid_string(substitute) and CMLConverter.is_identifier(substitute)):
            raise AttributeError('error, `{}` is not a valid substitute!'.format(substitute))
        if not CMLConverter.is_valid_string(s):
            raise AttributeError('error, `{}` is not a valid string!'.format(s))
        ids = ''
        if s[0].isalpha() or s[0] == '_':
            ids += s[0]
        elif s[0] == '-':
            ids += '_'
        else:
            ids += substitute
        for c in s[1:]:
            if c.isalnum() or c == '_':
                ids += c
            elif c == '-':
                ids += '_'
            else:
                ids += substitute
        return ids

    @staticmethod
    def to_int(s):
        """
        Tries to convert a given object to an integer

        Parameters
        ----------
        s : string
            The string to be converted

        Returns
        -------
        The state of the conversion (True or False) and
        the converted value (None if the state is False)
        """
        c = False
        try:
            v = int(s)
        except ValueError:
            v = None
        else:
            c = True
        finally:
            return c, v

    @staticmethod
    def to_float(s):
        """
        Tries to convert a given object to an float

        Parameters
        ----------
        s : string
            The string to be converted

        Returns
        -------
        The state of the conversion (True or False) and
        the converted value (None if the state is False)
        """
        c = False
        try:
            v = float(s)
        except ValueError:
            v = None
        else:
            c = True
        finally:
            return c, v

    @staticmethod
    def to_string(s):
        """
        Tries to convert a given object to a string

        Parameters
        ----------
        s : string
            The string to be converted

        Returns
        -------
        The state of the conversion (True or False) and
        the converted value (None if the state is False)
        """
        c = False
        try:
            v = str(s)
            if v[0] == v[-1] == '"':
                v = v[1:-1]
        except ValueError:
            v = None
        else:
            c = True
        finally:
            return c, v

    @staticmethod
    def convert(s):
        raise NotImplementedError('error, `convert` method not implemented!')


class CMLConverterYAML(CMLConverter):

    BOOL_DICT = {'true': True, 'yes': True, 'false': False, 'no': False}
    NONE_LIST = ('none', 'null', 'nil')

    def __init__(self):
        pass

    @staticmethod
    def to_bool(s):
        """
        Tries to convert a given object to a boolean

        Parameters
        ----------
        s : string
            The string to be converted

        Returns
        -------
        The state of the conversion (True or False) and
        the converted value (None if the state is False)
        """
        c, v = False, None
        if s.lower() in CMLConverterYAML.BOOL_DICT:
            v = CMLConverterYAML.BOOL_DICT[s.lower()]
            c = True
        return c, v

    @staticmethod
    def to_none(s):
        """
        Tries to convert a given object to None

        Parameters
        ----------
        s : string
            The string to be converted

        Returns
        -------
        The state of the conversion (True or False) and
        the converted value (None if the state is False)
        """
        c = s.lower() in CMLConverterYAML.NONE_LIST
        v = None
        return c, v

    @staticmethod
    def convert(s):
        """
        Convert a given string

        Parameters
        ----------
        s : string
            The string to be converted

        Returns
        -------
        The converted value or the value
        itself if no conversion was applied
        """
        fiter = iter([CMLConverter.to_int,
                      CMLConverter.to_float,
                      CMLConverterYAML.to_bool,
                      CMLConverterYAML.to_none,
                      CMLConverter.to_string,
                      lambda s: (True, s)])
        cv = (False, s)
        while not cv[0]:
            cv = next(fiter)(s)
        return cv[1]
