import sys
from collections import OrderedDict
from carputils.cml.cmlconverter import CMLConverter

isPy2 = not sys.version_info.major > 2

DictType = OrderedDict


class CMLDictOperations:

    def __init__(self):
        pass

    @staticmethod
    def __flatten_child(obj, prefix):
        it = obj.iteritems if isPy2 else obj.items
        childlist = []
        for key, value in it():
            globalkey = '{}.{}'.format(prefix, key)
            if isinstance(value, DictType):
                childlist += CMLDictOperations.__flatten_child(value, globalkey)
            else:
                childlist.append([globalkey, value])
        return childlist

    @staticmethod
    def flatten(obj):
        it = obj.iteritems if isPy2 else obj.items
        childlist = []
        for key, value in it():
            if isinstance(value, DictType):
                childlist += CMLDictOperations.__flatten_child(value, key)
            else:
                childlist.append([key, value])
        return DictType(childlist)


class CMLNamespace(object):

    def __init__(self, make_identifier=True, substitute='_', **kwargs):
        it = kwargs.iteritems if isPy2 else kwargs.items
        for key, value in it():
            self.add_attribute(key, value, make_identifier, substitute)

    def add_attribute(self, key, value, make_identifier=True, substitute='_'):
        if not CMLConverter.is_identifier(key):
            if not make_identifier:
                raise AttributeError('error, `{}` is not an identifier!'.format(key))
            else:
                key = CMLConverter.make_identifier(key, substitute=substitute)
        setattr(self, key, value)

    def to_string(self, level=0, indent=1):
        it = self.__dict__.iteritems if isPy2 else self.__dict__.items
        string = ''
        for key, value in it():
            if key[0].isalpha():
                string += ' '*level*indent + '{}:'.format(key)
                if isinstance(value, CMLNamespace):
                    string += '\n'+value.to_string(level+1, indent)
                else:
                    string += ' {}\n'.format(value)
        return string

    def __str__(self):
        return self.to_string()


class CMLNode(object):

    def __init__(self, parent, key):
        # parent node
        self.parent = parent
        # node level
        self.level = -1 if parent is None else parent.level+1
        # index of node in parent's child list
        self.index = -1
        # key of node
        self.key = None if key is None else str(key)

    @property
    def has_key(self):
        return CMLConverter.is_valid_string(self.key)

    def add_child(self, node):
        raise NotImplementedError('error, `add_child` method not implemented!')

    def serialize(self):
        raise NotImplementedError('error, `serialize` method not implemented!')

    def objectify(self):
        raise NotImplementedError('error, `objectify` method not implemented!')


class CMLLeaf(CMLNode):

    def __init__(self, parent, key, value):
        super(CMLLeaf, self).__init__(parent, key)
        self.value = value

    @staticmethod
    def from_node(node, value):
        if not isinstance(node, CMLNode):
            raise AttributeError('error, CMLNode expected but got {}!'.format(type(node)))
        leafnode = CMLLeaf(node.parent, node.key, value)
        leafnode.index = node.index
        return leafnode

    def add_child(self, node):
        raise NotImplementedError('error, `add_child` method not implemented!')

    def serialize(self):
        return [self.key, self.value] if self.has_key else self.value

    def objectify(self):
        return [self.key, self.value] if self.has_key else self.value


class CMLListBranch(CMLNode):

    def __init__(self, parent, key):
        super(CMLListBranch, self).__init__(parent, key)
        self.children = []

    @staticmethod
    def from_node(node):
        if not isinstance(node, CMLNode):
            raise AttributeError('error, CMLNode expected but got {}!'.format(type(node)))
        branchnode = CMLListBranch(node.parent, node.key)
        branchnode.index = node.index
        return branchnode

    def add_child(self, node):
        if not isinstance(node, CMLNode):
            raise AttributeError('error, CMLNode expected but got {}!'.format(type(node)))
        node.index = len(self.children)
        self.children.append(node)

    def serialize(self):
        childseries = [child.serialize() for child in self.children]
        return [self.key, childseries] if self.has_key else childseries

    def objectify(self):
        childobjects = [child.objectify() for child in self.children]
        return [self.key, childobjects] if self.has_key else childobjects


class CMLDictBranch(CMLNode):

    def __init__(self, parent, key):
        super(CMLDictBranch, self).__init__(parent, key)
        self.children = []

    @staticmethod
    def from_node(node):
        if not isinstance(node, CMLNode):
            raise AttributeError('error, CMLNode expected but got {}!'.format(type(node)))
        branchnode = CMLDictBranch(node.parent, node.key)
        branchnode.index = node.index
        return branchnode

    def add_child(self, node):
        if not isinstance(node, CMLNode) and not node.has_key:
            raise AttributeError('CMLNode with key expected but got {}!'.format(type(node)))
        node.index = len(self.children)
        self.children.append(node)

    def serialize(self):
        childseries = DictType([child.serialize() for child in self.children])
        return [self.key, childseries] if self.has_key else childseries

    def objectify(self):
        childobjects = DictType([child.objectify() for child in self.children])
        return [self.key, CMLNamespace(**childobjects)] if self.has_key else CMLNamespace(**childobjects)

