# -*- coding: utf-8 -*-
"""
carputils.testing

Tests are readily added in developer test run scripts. The simulation
setup and run code should be wrapped in a function that takes a list of
command line arguments and an (optional) output directory as arguments::

    def run(argv, outdir=None):
        if outdir is not None:
            simID = outdir # Must actually use the outdir argument

Tests may be defined after the run script definition::

    newtest = Test('new',                    # Test name
                    run,                     # Run function
                    ['--experiment', 'new']) # Command line args

And comparsions with the reference solution can be added::

    newtest.add_filecmp_test('x.dynpt.gz',       # File to compare
                             igb_max_error,      # Comparison function
                             0.1)                # Tolerance for error

Tests are considered to have passed when the error calculated by the
comparison function is less than the specified tolerance. Alternatively,
you can apply your own rules for when a test passes by passing a function
in place of the tolerance which returns a bool given the result of the
comparsion function.

Tests are registered by creating a list in the module called ``__tests__``::

    __tests__ = [newtest]

The test running code discovers tests to run by searching in the python
package hierarchy. If you make a new example in developer tests, be sure to
include it in the package hierarchy by adding a blank file called
``__init__.py`` in the same directory.

For example, if you add a new test ``new`` under ``mechanics``, the
directory structure should be::

    devtests
    ├── FEMLIB
    ├── LIMPET
    └── mechanics
        ├── __init__.py      <- Makes mechanics a package
        └── new
            ├── __init__.py  <- Makes mechanics.new a package
            └── run.py
"""

import sys

from .test import Test
from .result import TestResult
from .testsuite import TestSuite, TestSuiteResults
from .cost import *
from . import tag
from .find import find, find_multi

# Handy output flags
SUPPRESS = None
SHOW = sys.stdout
