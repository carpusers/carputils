
import os
import subprocess
import traceback

class CARPTestError(Exception):
    pass

class SerialisableException(object):

    def __init__(self, classname, message, **extras):
        self.classname = str(classname).strip()
        self.message = str(message).strip()
        self.extras = extras

    @classmethod
    def from_exception(cls, exception):

        extras = {}
        if isinstance(exception, subprocess.CalledProcessError):
            extras['returncode'] = int(exception.returncode)

        return cls(exception.__class__.__name__, str(exception), **extras)

    def serialise(self):
        data = {'classname': self.classname,
                'message': self.message,
                'extras': self.extras}
        return data

    @classmethod
    def deserialise(cls, data):
        return cls(data['classname'], data['message'], **(data['extras']))

    def __str__(self):

        if len(self.message) == 0:
            string = self.classname
        else:
            string = '{}: {}'.format(self.classname, self.message)

        if 'returncode' in self.extras:
            error_message = os.strerror(self.extras['returncode'])
            string += ' ({})'.format(error_message)

        return string


class SerialisableBacktrace(object):

    def __init__(self, content):
        self.content = [str(part) for part in content]

    @classmethod
    def from_backtrace(cls, backtrace):
        try:
            return cls(traceback.format_tb(backtrace))
        except:
            # empty lists, strings etc would throw this exception
            if not backtrace: return ''
            return backtrace

    def serialise(self):
        return {'content': self.content}

    @classmethod
    def deserialise(cls, data):
        if data is None:
            return None
        else:
            return cls(data['content'])


class SerialisableError(object):

    def __init__(self, exception, backtrace=None):

        if isinstance(exception, SerialisableException):
            self.exception = exception
        else:
            self.exception = SerialisableException.from_exception(exception)

        if isinstance(backtrace, SerialisableBacktrace):
            self.backtrace = backtrace
        else:
            self.backtrace = SerialisableBacktrace.from_backtrace(backtrace)

    def serialise(self):
        data = {'exception': self.exception.serialise(),
                'backtrace': self.backtrace.serialise()}
        return data

    @classmethod
    def deserialise(cls, data):
        if data is None:
            return None

        exception = SerialisableException.deserialise(data['exception'])
        backtrace = SerialisableBacktrace.deserialise(data['backtrace'])
        #print('exception: ', exception)
        #print('backtrace: ', backtrace)
        return cls(exception, backtrace)

    def report(self, pad=0, backtrace=True):

        lines = [str(self.exception)]

        if backtrace and self.backtrace is not None:
            lines.append('Backtrace:')
            for part in self.backtrace.content:
                lines += part.strip('\n').split('\n')

        # Apply padding
        lines = [' ' * pad + l for l in lines]

        return '\n'.join(lines)
