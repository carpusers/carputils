
import pickle
import sys
from .. import serialise
from .check import CheckResult
from .exception import SerialisableError

isPy2 = True
if sys.version_info.major > 2:
    isPy2  = False
    xrange = range

class TestResult(object):
    """
    CARP Test Results Object.
    """

    def __init__(self, test, timestamp=None):
        self.test = test
        self.timestamp = timestamp
        self.execution_time = None
        self.revision = None
        self.dep_revisions = None

        self.check_results = []
        self.error = None

    def serialise(self):

        datetime = serialise.datetime_serialise
        timedelta = serialise.timedelta_serialise

        data = {'test': self.test.serialise(),
                'timestamp': datetime(self.timestamp),
                'execution_time': timedelta(self.execution_time),
                'revision': self.revision,
                'dep_revisions': self.dep_revisions,
                'check_results': [c_r.serialise() for c_r in self.check_results],
                'error': None if self.error is None
                         else self.error.serialise()}

        return data

    @classmethod
    def deserialise(cls, data):

        from .test import Test

        datetime = serialise.datetime_deserialise
        timedelta = serialise.timedelta_deserialise

        obj = cls(Test.deserialise(data['test']), datetime(data['timestamp']))
        obj.execution_time = timedelta(data['execution_time'])

        if isPy2:
            if data.has_key('revision') and data['revision'] is not None:
                obj.revision = data['revision']
            if data.has_key('svn_revision') and data['svn_revision'] is not None:
                obj.revision = data['svn_revision']
            if data.has_key('git_revision') and data['git_revision'] is not None:
                obj.revision = data['git_revision']

            if data.has_key('dep_revisions') and data['dep_revisions'] is not None:
                obj.dep_revisions = data['dep_revisions']
            if data.has_key('dep_svn_revisions') and data['dep_svn_revisions'] is not None:
                obj.dep_revisions = data['dep_svn_revisions']
            if data.has_key('dep_svn_revisions') and data['dep_svn_revisions'] is not None:
                obj.dep_revisions = data['dep_svn_revisions']
        else:
            if 'revision' in data and data['revision'] is not None:
                obj.revision = data['revision']
            if 'svn_revision' in data and data['svn_revision'] is not None:
                obj.revision = data['svn_revision']
            if 'git_revision' in data and data['git_revision'] is not None:
                obj.revision = data['git_revision']

            if 'dep_revisions' in data and data['dep_revisions'] is not None:
                obj.dep_revisions = data['dep_revisions']
            if 'dep_svn_revisions' in data and data['dep_svn_revisions'] is not None:
                obj.dep_revisions = data['dep_svn_revisions']

        obj.check_results = [CheckResult.deserialise(c_r)
                             for c_r in data['check_results']]

        if 'error' in data:
            error = SerialisableError.deserialise(data['error'])
        else:
            # Backwards compatability
            if 'subprocess' in data['exception']:
                msg = 'subprocess error that could not be depickled'
                exception = Exception(msg)
            else:
                exception = pickle.loads(data['exception'])
            backtrace = data['backtrace']
            error = SerialisableError(exception, backtrace)

        obj.error = error

        return obj

    def add_result(self, result):
        self.check_results.append(result)

    def report(self, backtrace=False):

        report = ''

        etime = self.execution_time
        if etime is not None:
            report += 'Runtime: {0}\n'.format(etime)

        if self.error is not None:
            report += 'SIMULATION FAILED TO RUN\n'
            report += self.error.report(pad=2, backtrace=backtrace)

        elif len(self.check_results) == 0:
            report += 'No comparison results found.\n'
            report += 'Did you define any checks?'

        else:
            if self.checks_failed() == 0:
                report += 'ALL PASSED'
            elif self.checks_passed() == 0:
                report += '**** ALL FAILED ****'
            else:
                tpl = '**** {0}/{1} FAILED ****'
                report += tpl.format(self.checks_failed(), self.checks())

            tpl = '\n  {0} {1}({2}){3}'

            for result in self.check_results:
                report += '\n' + result.report(pad=2)

        return report


    def passed(self):
        return self.checks_failed() == 0 and self.error is None

    def failed(self):
        return not self.passed()

    def checks(self):
        return len(self.check_results)

    def checks_passed(self):
        passed = 0
        for result in self.check_results:
            if result.success:
                passed += 1
        return passed

    def checks_failed(self):
        return self.checks() - self.checks_passed()

    def checks_with_errors(self):
        errors = 0
        for result in self.check_results:
            if result.execerror is not None:
                errors += 1
        return errors
