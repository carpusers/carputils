"""
Functions to build checks and assist in the creation of new checks.

Some functions to quantify the error between the generated and reference
solutions. At present, these checks always take two arguments, the reference
and test files, which then compute some error and return it. A more complex
return value could be used when adding checks to your tests, but then an
appropriate function would need to be passed as the check's tolerance test.
"""

import numpy as np

__all__ = ['max_error',
           'error_norm']

def max_error(array1, array2):
    return np.max(np.abs(array1 - array2))

def error_norm(array1, array2):
    return np.sqrt(np.sum((array1 - array2)**2))
