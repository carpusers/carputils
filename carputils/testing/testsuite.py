#!/bin/python
import os
import pickle
import sys
import shutil
from datetime import datetime
import socket
from warnings import warn

from carputils.tempdir import tempdir

from .test import Test
from .result import TestResult
from .. import format
from .. import serialise

# provide Python2 to Python3 compatibility
isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False
    xrange = range

class TestSuite(object):

    def __init__(self, tests, output_directory=None, keep_output=False):
        self.tests = list(tests)
        self.output_directory = output_directory or tempdir('_test_suite')
        self.keep_output = bool(keep_output)

    def serialise(self):
        data = {'tests': [test.serialise() for test in self.tests],
                'output_directory': self.output_directory,
                'keep_output': self.keep_output}
        return data

    @classmethod
    def deserialise(cls, data):
        for test in data['tests']:
            try:
                pickle.loads(test['run'])
            except:
                warn('Querying for {} failed. Test may have been (re-)moved'.format(test['run']))
                continue
            Test.deserialise(test)

        return cls([Test], data['output_directory'], data['keep_output'])
        #return cls([Test.deserialise(test) for test in data['tests']],
        #           data['output_directory'], data['keep_output'])

    def table(self):

        header = [format.Cell('Module'),
                  format.Cell('Tests'),
                  format.Cell('Max Runtime')]

        groups = {}
        for test in self.tests:
            try:
                groups[test.module].append(test)
            except KeyError:
                groups[test.module] = [test]

        cells = []

        for modname in sorted(groups):

            names = []
            times = []

            for test in groups[modname]:
                names.append(test.name)
                runtime_tag = test.runtime_tag()
                if runtime_tag is None:
                    times.append('-')
                else:
                    times.append(runtime_tag.runtime_string())

            row = [format.Cell(modname),
                   format.Cell('\n'.join(names)),
                   format.Cell('\n'.join(times))]

            cells.append(row)

        return format.Table(cells, header)

    def execute(self, output_streams=None):
        """
        Execute all the simulations for this test suite.
        """

        for test in self.tests:

            # Logging
            tpl = 'Executing test \'{}\' in module {}...'
            sys.stdout.write(tpl.format(test.name, test.module))
            sys.stdout.flush()

            outdir = test.output_directory(self.output_directory)
            test.execute(outdir, output_streams)

            # Logging
            sys.stdout.write(' Done.\n')

            if not self.keep_output:
                shutil.rmtree(outdir)

    def test(self):

        results = TestSuiteResults(self)

        for test in self.tests:

            # Logging
            tpl = 'Performing checks for test \'{}\' in module {}...'
            sys.stdout.write(tpl.format(test.name, test.module))
            sys.stdout.flush()

            outdir = test.output_directory(self.output_directory)
            res = test.perform_checks(outdir)
            results.add(res)

            # Logging
            sys.stdout.write(' Done.\n')

        return results

    def execute_and_test(self, output_streams=None):
        """
        Run the tests.
        """

        start = datetime.now()
        results = TestSuiteResults(self, start)

        for test in self.tests:

            # Logging
            tpl = 'Running test \'{}\' in module {}...'
            sys.stdout.write(tpl.format(test.name, test.module))
            sys.stdout.flush()

            outdir = test.output_directory(self.output_directory)
            res = test.execute_and_check(outdir, output_streams)
            results.add(res)

            # Logging
            current = datetime.now()
            if res.passed():
                outstr = ' passed.\n'
            else:
                outstr = ' failed.\n'
            sys.stdout.write(outstr)

            time = res.execution_time
            sys.stdout.write('    Run time:     {}\n'.format(time))
            sys.stdout.write('    Elapsed time: {}\n\n'.format(current - start))

            if not self.keep_output and os.path.isfile(outdir):
                shutil.rmtree(outdir)

        current = datetime.now()
        results.runtime = current - start

        return results

    def generate_reference(self, output_streams=None):
        """
        Generate the reference solution files for this test suite.
        """

        results = TestSuiteReferenceGenerationResults(self)
        start = datetime.now()

        for test in self.tests:

            if not test.ref_generate:
                tpl = ('Test \'{}\' in module {} has reference generation '
                       'disabled, ignoring.\n')
                sys.stdout.write(tpl.format(test.name, test.module))
                sys.stdout.flush()
                continue

            # Logging
            tpl = 'Running test \'{}\' in module {}...'
            sys.stdout.write(tpl.format(test.name, test.module))
            sys.stdout.flush()

            outdir = test.output_directory(self.output_directory)
            test.execute(outdir, output_streams)
            results.files += test.copy_reference(outdir)

            # Logging
            current = datetime.now()
            sys.stdout.write(' Done.\n')
            sys.stdout.write('    Elapsed time: {}\n\n'.format(current - start))
            sys.stdout.flush()

            if not self.keep_output:
                shutil.rmtree(outdir)

        current = datetime.now()
        results.runtime = current - start

        return results

    def copy_reference(self, output_streams=None):
        """
        Only copy the references.
        """
        results = TestSuiteReferenceGenerationResults(self)
        start = datetime.now()

        for test in self.tests:
            if not test.ref_generate:
                tpl = ('Test \'{}\' in module {} has reference generation disabled, ignoring.\n')
                sys.stdout.write(tpl.format(test.name, test.module))
                sys.stdout.flush()
                continue

            # Logging
            outdir = test.output_directory(self.output_directory)
            results.files += test.copy_reference(outdir)

        current = datetime.now()
        results.runtime = current - start

        return results

class TestSuiteResults(object):

    def __init__(self, suite, timestamp=None):
        self.suite = suite
        self.timestamp = timestamp
        self.results = []
        self.hostname = socket.gethostname()
        self.runtime = None

    def serialise(self):
        data = {'suite':     self.suite.serialise(),
                'timestamp': serialise.datetime_serialise(self.timestamp),
                'results':   [result.serialise() for result in self.results],
                'hostname':  self.hostname,
                'runtime':   serialise.timedelta_serialise(self.runtime)}
        return data

    @classmethod
    def deserialise(cls, data):
        suite = TestSuite.deserialise(data['suite'])
        timestamp = serialise.datetime_deserialise(data['timestamp'])
        obj = cls(suite, timestamp)

        for d in data['results']:
            try:
                pickle.loads(d['test']['run'])
            except:
                warn('Querying for {} failed. Test may have been (re-)moved!\n'.format((d['test']['run']).replace('\n', ' ')))
                continue
            obj.results.append(TestResult.deserialise(d))
        #obj.results = [TestResult.deserialise(d) for d in data['results']]

        obj.hostname = data['hostname']
        obj.runtime = serialise.timedelta_deserialise(data['runtime'])
        return obj

    def add(self, result):
        self.results.append(result)

    def search(self, test=None):
        for result in self.results:
            if result.test == test:
                return result
        raise ValueError('no test {} found'.format(test))

    def passed(self):
        count = 0
        for result in self.results:
            if result.passed():
                count += 1
        return count

    def failed(self):
        count = 0
        for result in self.results:
            if result.failed():
                count += 1
        return count

    def partially_failed(self):
        count = 0
        for result in self.results:
            if result.failed() and result.checks_passed() > 0:
                count += 1
        return count

    def table(self, backtrace=False):

        headers = [format.Cell('Description'), format.Cell('Result')]

        rows = []
        for result in self.results:
            rows.append([format.Cell(result.test.summary()),
                         format.Cell(result.report(backtrace))])

        return format.Table(rows, headers, [1, 2])

    def summary(self):

        summary = format.Summary('Test Suite Summary')

        def pc(v1, v2, suff=''):
            try:
                frac = float(v1) / float(v2)
            except ZeroDivisionError:
                frac = 0.
            return '{} ({:.1%}{})'.format(v1, frac, suff)

        total = len(self.results)
        passed = self.passed()
        partial = self.partially_failed()
        failed = total - passed - partial

        summary.add_line('Tests run', total)
        summary.add_blank_line()
        summary.add_line('Tests passed', pc(passed, total))
        summary.add_line('Tests partially failed', pc(partial, total))
        summary.add_line('Tests completely failed', pc(failed, total))
        summary.add_blank_line()

        checks = 0
        chks_passed = 0
        chks_failed = 0
        chks_error = 0

        for result in self.results:
            checks += result.checks()
            chks_passed += result.checks_passed()
            chks_failed += result.checks_failed()
            chks_error  += result.checks_with_errors()

        summary.add_line('Checks performed', checks)
        summary.add_blank_line()
        summary.add_line('Checks passed', pc(chks_passed, checks))
        summary.add_line('Checks failed', pc(chks_failed, checks))
        summary.add_line('  of which had errors',
                         pc(chks_error, checks, ' of total'))

        if self.runtime is not None:
            summary.add_blank_line()
            summary.add_line('Suite runtime', self.runtime)

        return summary

class TestSuiteReferenceGenerationResults(object):

    def __init__(self, suite):
        self.suite = suite
        self.files = []
        self.runtime = None

    def summary(self):

        summary = format.Summary('Reference Files Generated')

        summary.add_line('Tests run', len(self.suite.tests))
        summary.add_line('Reference files generated', len(self.files))
        summary.add_line('Suite runtime', self.runtime)

        summary.set_list('Files generated', self.files)

        return summary
