"""
carputils.testing.find

Find tests in the package hierarchy.
"""
import pkgutil
import fnmatch
import warnings
from carputils.testing.exception import CARPTestError

def find(module, return_modules=False, *args, **kwargs):
    """
    Find all tests meeting the specified criteria.
    """

    # Load module if only its name was passed
    if isinstance(module, str):
        module = __import__(module, fromlist=[''])

    if return_modules:
        yield module

    # Load module hierarchy
    if is_package(module):
        mod_iter = recursive_modules(module)
    else:
        mod_iter = [module]

    for mod in mod_iter:
        if return_modules:
            yield mod
        else:
            for test in module_tests(mod):
                if matches_filter(test, *args, **kwargs):
                    yield test

def find_multi(module_list, *args, **kwargs):
    for module in module_list:
        for test in find(module, *args, **kwargs):
            yield test

def is_package(module):
    return hasattr(module, '__path__')

def recursive_modules(package):
    """
    Recursively import all modules in a given package.
    """

    for importer, name, ispkg in pkgutil.iter_modules(package.__path__):

        modname = '.'.join([package.__name__, name])

        try:
            module = __import__(modname, fromlist=[''])

        except Exception as err:
            tpl = '\n{} while importing {}'
            excp = err.__class__.__name__
            warnings.warn(tpl.format(excp, modname))
            # more verbose error handling when running regression tests
            warnings.warn('\nError message: ' + str(err)+'\n')
        else:

            yield module

            if ispkg:
                for sub in recursive_modules(module):
                    yield sub

def module_tests(module):
    """
    Iterate over all tests in a module.
    """

    try:
        test_list = module.__tests__
    except AttributeError:
        return

    # Check for duplicates
    count = {}
    for test in test_list:
        if test.name in count:
            count[test.name] += 1
        else:
            count[test.name] = 1
    duplicates = [name for name, num in count.items() if num > 1]

    if len(duplicates) > 0:
        tpl = 'Multiple tests in module {} have the same name: {}'
        teststr = ', '.join(['"{}"'.format(name) for name in duplicates])
        raise CARPTestError(tpl.format(module.__name__, teststr))

    # Iterate over the tests
    for test in test_list:
        yield test

def matches_filter(test, name=None, tags=[], combine=any, invert=False):

    filters = []
    if name is not None:
        filters.append(fnmatch.fnmatch(test.name, name))
    for tag in tags:
        filters.append(tag in test.tags)

    if len(filters) == 0:
        # No filters applied
        return True
    if not invert:
        return combine(filters)
    # else
    return not combine(filters)
