r"""
Theory
------

From Klotz's 2007 Nature Protocols paper 'A computational method of prediction
of the end-diastolic pressure-volume relationship by single beat', the
following equations are taken.

Equations 2 and 5 seem to describe the end-diastolic pressure-volume relation
(EDPVR) as:

.. math:

    P = A_n V_n^{B_n} 

where :math:`P` is cavity pressure, :math:`V_n` is normalised volume, and the
constants :math:`A_n` and :math:`B_n` were determined emperically in their
earlier study as 27.78 mmHg and 2.76 respectively.

Normalised volume :math:`V_n` is determined as explained in equation 4 as:

.. math:

    V_n = \frac{V - V_0}{V_30 - V_0}

where :math:`V` is the cavity volume, :math:`V_0` is the volume at zero
pressure and :math:`V_30` is the volume at a pressure of 30 mmHg.

The above can be arranged to equation 6, giving the :math:`V_{30}` constant:

.. math:

    V_{30} = V_0 + \frac{V - V_0}{\left(\frac{P_m}{A_n}\right)^{(1/B_n)}}

:math:`V_0` is determined using the emperical relation:

.. math:

    V_0 = V_m (0.6 - 0.006 P_m)

where :math:`V_m` and :math:`P_m` are a pressure-volume pair measured.

The equation :math:`P = \alpha V^\beta` is then fitted to the above
:math:`(V_m, P_m)` point, giving the following relations for the constants
:math:`\alpha` and :math:`\beta`:

.. math:

    \beta = \frac{\log\left(\tfrac{P_m}{30}\right)}
                 {\log\left(\tfrac{V_m}{V_{30}}\right)}

    \alpha = \frac{30}{V_{30}^\beta}

To avoid a singularity in the above equations when :math:`P_m \rightarrow 30`,
the above equations were reposed as:

.. math:

    \beta = \frac{\log\left(\tfrac{P_m}{15}\right)}
                 {\log\left(\tfrac{V_m}{V_{15}}\right)}

    \alpha = \frac{P_m}{V_m^\beta}

where :math:`V_{15}` is determined analytically as:

.. math:

    V_{15} = 0.8 (V_{30} - V_0) + V_0

The paper advises using the first form of the :math:`\alpha` and :math:`\beta`
equations (utilising :math:`V_{30}`) when using a :math:`P_m` value up to 22
mmHg and the second form (utilising :math:`V_{15}`) above 22 mmHg.
"""
import numpy as np

MMHG2KPA = 0.133322387415
KPA2MMHG = 1.0 / MMHG2KPA

AN = 27.78
BN = 2.76

def V0(Vm, Pm):
    """
    Determine the zero-pressure volume from the Klotz relation.

    Parameters
    ----------
    Vm : float
        The volume of a measured state on the EDPVR (ml)
    Pm : float
        The pressure of a measured state on the EDPVR (kPa)

    Returns
    -------
    float
        The zero-pressure volume (ml)
    """

    return Vm * (0.6 - 0.006 * Pm * KPA2MMHG)

def V30(Vm, Pm):
    """
    Determine the volume at 30 mmHg from the Klotz relation.

    Parameters
    ----------
    Vm : float
        The volume of a measured state on the EDPVR (ml)
    Pm : float
        The pressure of a measured state on the EDPVR (kPa)

    Returns
    -------
    float
        The volume at 30 mmHg (ml)
    """

    Pm_mmHg = Pm * KPA2MMHG
    return V0(Vm, Pm) + (Vm - V0(Vm, Pm)) / ((Pm_mmHg / AN) ** (1. / BN))

def V15(Vm, Pm):
    """
    Determine the volume at 15 mmHg from the Klotz relation.

    Parameters
    ----------
    Vm : float
        The volume of a measured state on the EDPVR (ml)
    Pm : float
        The pressure of a measured state on the EDPVR (kPa)

    Returns
    -------
    float
        The volume at 15 mmHg (ml)
    """

    return 0.8 * (V30(Vm, Pm) - V0(Vm, Pm)) + V0(Vm, Pm)

def beta(Vm, Pm):
    """
    Determine the beta parameter of the Klotz EDPVR.

    Parameters
    ----------
    Vm : float
        The volume of a measured state on the EDPVR (ml)
    Pm : float
        The pressure of a measured state on the EDPVR (kPa)

    Returns
    -------
    float
        The beta parameter
    """

    Pm_mmHg = Pm * KPA2MMHG

    if Pm_mmHg <= 22:
        return np.log(Pm_mmHg / 30.) / np.log(Vm / V30(Vm, Pm))

    else:
        return np.log(Pm_mmHg / 15.) / np.log(Vm / V15(Vm, Pm))

def alpha(Vm, Pm):
    """
    Determine the alpha parameter of the Klotz EDPVR.

    Parameters
    ----------
    Vm : float
        The volume of a measured state on the EDPVR (ml)
    Pm : float
        The pressure of a measured state on the EDPVR (kPa)

    Returns
    -------
    float
        The alpha parameter
    """

    Pm_mmHg = Pm * KPA2MMHG

    if Pm_mmHg <= 22:
        return 30. / (V30(Vm, Pm) ** beta(Vm, Pm))

    else:
        return Pm_mmHg / (Vm ** beta(Vm, Pm))

class KlotzRelation(object):
    """
    Represents the EDPVR determined by Klotz from a single volume, pressure
    measurement.

    To construct the relation, simply pass in a measured pressure-volume pair
    on the EDVPR:

    >>> V = 120 # ml
    >>> P = 4   # kPa
    >>> relation = KlotzRelation(V, P)

    You can then evaluate the relation by either calling it with volumes:

    >>> Vsamp = np.linspace(0, 120, 100)
    >>> Psamp = relation(Vsamp)

    or compute the inverse:

    >>> Psamp = np.linsapce(0, 4, 100)
    >>> Vsamp = relation.inverse(Psamp)

    Parameters
    ----------
    Vm : float
        The volume of a measured state on the EDPVR (ml)
    Pm : float
        The pressure of a measured state on the EDPVR (kPa)
    """
    
    def __init__(self, Vm, Pm):
        self._alpha = alpha(Vm, Pm)
        self._beta  = beta(Vm, Pm)

    def __call__(self, V):
        return self._alpha * (V ** self._beta) * MMHG2KPA

    def inverse(self, P):
        return (P * KPA2MMHG / self._alpha) ** (1 / self._beta)


if __name__ == '__main__':

    import sys
    from matplotlib import pyplot

    # Prepare plot
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    
    # Test with similar values to Fig. 7A in the paper
    #Vm, Pm = 159.045, 30 * MMHG2KPA
    Vm, Pm = 110, 10 * MMHG2KPA
    V = []
    if len(sys.argv) < 3:
        print('usage `{} Vm Pm [V0, [V1, ...]]`'.format(sys.argv[0]))
        print('\nwith measured volume Vm (ml)\n'
              'and measured pressure Pm (mmHg)\n')
        print('pressure values for the volumes V0, V1, ... are evaluated\n')
        print('using default values, Vm = {}, Pm = {}\n'.format(Vm, Pm))
    else:
        try:
            Vm = float(sys.argv[1])
            Pm = float(sys.argv[2]) * MMHG2KPA
            V = map(float, sys.argv[3:])
        except ValueError:
            print('real values for volume ({}) '
                  'pressure ({}) and volumes expected!'.format(sys.argv[1], sys.argv[2]))
            exit(0)
    
    # Construct the relation
    P = KlotzRelation(Vm, Pm)

    # Sample it   
    vdat = np.linspace(0, 170, 200)
    pdat = P(vdat)

    # Plot it
    ax.plot(vdat, pdat)
    
    # Mark som data points
    ax.scatter([Vm], [Pm], 40, c='r', label='Measured')
    ax.scatter([V0(Vm, Pm)],  [0.], 40, marker='x', c='b', label='V0')
    ax.scatter([V15(Vm, Pm)], [15. * MMHG2KPA], 40,marker='x', c='g', 
               label='V15')
    ax.scatter([V30(Vm, Pm)], [30. * MMHG2KPA], 40,marker='x', c='r',
               label='V30')
    
    # Plot presentation
    ax.set_xlabel('Volume (ml)')
    ax.set_ylabel('Pressure (kPa)')
    pyplot.legend(loc='upper left')

    print('For (Vm, Pm): ', (Vm, Pm), '(ml, kPa)')
    print('          V0: ', V0(Vm, Pm), 'ml')
    print('         V15: ', V15(Vm, Pm), 'ml')
    print('         V30: ', V30(Vm, Pm), 'ml')

    for v in V:
        print('V = {} ml, P = {} kPa, {} mmHg'.format(v, P(v), P(v) * KPA2MMHG))
        ax.scatter([v], [P(v)], marker='v', color='#FFAABB')
    
    # Display plot
    pyplot.show()
