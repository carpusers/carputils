
from .general import AbstractMechanicsMaterial as _AbstractMechanicsMaterial
from collections import OrderedDict as _OrderedDict
from carputils.format import stringtable


def grid_id():
    """
    Return mechanics grid ID
    """
    grid_id = 8  # grid ID
    return grid_id


class LinearMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Linear material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    E : float, optional
        Value of E, defaults to 100.0
    lambda : float, optional
        Value of lambda, defaults to 100.0
    nu : float, optional
        Value of nu, defaults to 0.33
    """
    SHORTNAME = 'linear'
    MODELID = 1
    PARAMETERS = ['E', 'lambda', 'nu']


class NeoHookeanMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Neo-Hooke material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    c : float, optional
        Value of c, defaults to 33.345
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'neohookean'
    MODELID = 2
    PARAMETERS = ['kappa', 'c']


class StVenantKirchhoffMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "St. Venant-Kirchhoff material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    lambda : float, optional
        Value of lambda, defaults to 100.0
    mu : float, optional
        Value of mu, defaults to 30.0
    """
    SHORTNAME = 'stvenantkirchhoff'
    MODELID = 3
    PARAMETERS = ['lambda', 'mu']


class MooneyRivlinMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Mooney-Rivlin material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    c_1 : float, optional
        Value of c_1, defaults to 30.0
    c_2 : float, optional
        Value of c_2, defaults to 20.0
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'mooneyrivlin'
    MODELID = 4
    PARAMETERS = ['kappa', 'c_1', 'c_2']


class DemirayMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Demiray material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a : float, optional
        Value of a, defaults to 33.345
    b : float, optional
        Value of b, defaults to 9.242
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'demiray'
    MODELID = 5
    PARAMETERS = ['kappa', 'a', 'b']


class CompressibleNeoHookeanMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Compressible neo-Hooke material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    lambda : float, optional
        Value of lambda, defaults to 100.0
    mu : float, optional
        Value of mu, defaults to 33.345
    """
    SHORTNAME = 'compneohookean'
    MODELID = 6
    PARAMETERS = ['lambda', 'mu']


class HolzapfelArterialMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Holzapfel et al. (2000) artery material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    c : float, optional
        Value of c, defaults to 33.345
    k_1 : float, optional
        Value of k_1, defaults to 2.3632
    k_2 : float, optional
        Value of k_2, defaults to 0.8393
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'holzapfelarterial'
    MODELID = 7
    PARAMETERS = ['kappa', 'c', 'k_1', 'k_2']


class HolzapfelOgdenMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Holzapfel-Ogden material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a : float, optional
        Value of a, defaults to 33.345
    a_f : float, optional
        Value of a_f, defaults to 18.535
    a_fs : float, optional
        Value of a_fs, defaults to 0.417
    a_s : float, optional
        Value of a_s, defaults to 2.564
    b : float, optional
        Value of b, defaults to 9.242
    b_f : float, optional
        Value of b_f, defaults to 15.972
    b_fs : float, optional
        Value of b_fs, defaults to 11.602
    b_s : float, optional
        Value of b_s, defaults to 10.446
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'holzapfelogden'
    MODELID = 8
    PARAMETERS = ['kappa', 'a', 'a_f', 'a_fs', 'a_s', 'b', 'b_f', 'b_fs', 'b_s']


class GuccioneMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Guccione et al. (1995) material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a : float, optional
        Value of a, defaults to 0.876
    b_f : float, optional
        Value of b_f, defaults to 18.48
    b_fs : float, optional
        Value of b_fs, defaults to 1.627
    b_t : float, optional
        Value of b_t, defaults to 3.58
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'guccione'
    MODELID = 9
    PARAMETERS = ['kappa', 'a', 'b_f', 'b_fs', 'b_t']


class AnisotropicMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Simple anisotropic material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a_f : float, optional
        Value of a_f, defaults to 18.535
    b_f : float, optional
        Value of b_f, defaults to 2.564
    c : float, optional
        Value of c, defaults to 33.345
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'anisotropic'
    MODELID = 10
    PARAMETERS = ['kappa', 'a_f', 'b_f', 'c']


class ReducedHolzapfelOgdenMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Reduced Holzapfel-Ogden material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a : float, optional
        Value of a, defaults to 33.345
    a_f : float, optional
        Value of a_f, defaults to 18.535
    b : float, optional
        Value of b, defaults to 9.242
    b_f : float, optional
        Value of b_f, defaults to 15.972
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'redholzapfelogden'
    MODELID = 11
    PARAMETERS = ['kappa', 'a', 'a_f', 'b', 'b_f']


class DispersionArterialMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Gasser et al. (2006) artery material with dispersion"
    mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    c : float, optional
        Value of c, defaults to 33.345
    delta_f : float, optional
        Value of delta_f, defaults to 0.314
    k_1 : float, optional
        Value of k_1, defaults to 180.0
    k_2 : float, optional
        Value of k_2, defaults to 100.0
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'disparterial'
    MODELID = 12
    PARAMETERS = ['kappa', 'c', 'delta_f', 'k_1', 'k_2']


class ReducedHolzapfelOgdenDispersionMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Reduced Holzapfel-Ogden material with dispersion" mechanics
    material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a : float, optional
        Value of a, defaults to 33.345
    a_f : float, optional
        Value of a_f, defaults to 18.535
    b : float, optional
        Value of b, defaults to 9.242
    b_f : float, optional
        Value of b_f, defaults to 15.972
    delta_f : float, optional
        Value of delta_f, defaults to 0.226
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'redholzapfelogdendisp'
    MODELID = 13
    PARAMETERS = ['kappa', 'a', 'a_f', 'b', 'b_f', 'delta_f']


class UsykMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Usyk et al. (2000) material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a : float, optional
        Value of a, defaults to 0.88
    b_ff : float, optional
        Value of b_ff, defaults to 5.0
    b_fn : float, optional
        Value of b_fn, defaults to 2.0
    b_fs : float, optional
        Value of b_fs, defaults to 10.0
    b_nn : float, optional
        Value of b_nn, defaults to 3.0
    b_ns : float, optional
        Value of b_ns, defaults to 2.0
    b_ss : float, optional
        Value of b_ss, defaults to 6.0
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'usyk'
    MODELID = 14
    PARAMETERS = ['kappa', 'a', 'b_ff', 'b_fn', 'b_fs', 'b_nn', 'b_ns', 'b_ss']


class CompressibleMooneyRivlinMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Compressible Mooney-Rivlin material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    c_1 : float, optional
        Value of c_1, defaults to 30.0
    c_2 : float, optional
        Value of c_2, defaults to 20.0
    lambda : float, optional
        Value of lambda, defaults to 100.0
    """
    SHORTNAME = 'compmooneyrivlin'
    MODELID = 15
    PARAMETERS = ['c_1', 'c_2', 'lambda']


class ModifiedMooneyRivlinMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Modified Mooney-Rivlin material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    c_1 : float, optional
        Value of c_1, defaults to 30.0
    c_2 : float, optional
        Value of c_2, defaults to 20.0
    lambda : float, optional
        Value of lambda, defaults to 100.0
    """
    SHORTNAME = 'modmooneyrivlin'
    MODELID = 16
    PARAMETERS = ['c_1', 'c_2', 'lambda']


class FullHolzapfelOgdenMaterial(_AbstractMechanicsMaterial):
    """
    Describes the "Full Holzapfel-Ogden material" mechanics material

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a : float, optional
        Value of a, defaults to 33.345
    a_f : float, optional
        Value of a_f, defaults to 18.535
    a_fn : float, optional
        Value of a_fn, defaults to 0.0
    a_fs : float, optional
        Value of a_fs, defaults to 0.417
    a_n : float, optional
        Value of a_n, defaults to 0.0
    a_ns : float, optional
        Value of a_ns, defaults to 0.0
    a_s : float, optional
        Value of a_s, defaults to 2.564
    b : float, optional
        Value of b, defaults to 9.242
    b_f : float, optional
        Value of b_f, defaults to 15.972
    b_fn : float, optional
        Value of b_fn, defaults to 0.0
    b_fs : float, optional
        Value of b_fs, defaults to 11.602
    b_n : float, optional
        Value of b_n, defaults to 0.0
    b_ns : float, optional
        Value of b_ns, defaults to 0.0
    b_s : float, optional
        Value of b_s, defaults to 10.446
    delta_f : float, optional
        Value of delta_f, defaults to 0.0
    delta_n : float, optional
        Value of delta_n, defaults to 0.0
    delta_s : float, optional
        Value of delta_s, defaults to 0.0
    kappa : float, optional
        Value of the incompressibility penalty, defaults to 1000.0
    """
    SHORTNAME = 'fullholzapfelogden'
    MODELID = 17
    PARAMETERS = ['kappa', 'a', 'a_f', 'a_fn', 'a_fs', 'a_n', 'a_ns', 'a_s', 
                  'b', 'b_f', 'b_fn', 'b_fs', 'b_n', 'b_ns', 'b_s', 'delta_f', 
                  'delta_n', 'delta_s']


MAPPING = _OrderedDict()
MAPPING['linear']                = LinearMaterial
MAPPING['neohookean']            = NeoHookeanMaterial
MAPPING['stvenantkirchhoff']     = StVenantKirchhoffMaterial
MAPPING['mooneyrivlin']          = MooneyRivlinMaterial
MAPPING['demiray']               = DemirayMaterial
MAPPING['compneohookean']        = CompressibleNeoHookeanMaterial
MAPPING['holzapfelarterial']     = HolzapfelArterialMaterial
MAPPING['holzapfelogden']        = HolzapfelOgdenMaterial
MAPPING['guccione']              = GuccioneMaterial
MAPPING['anisotropic']           = AnisotropicMaterial
MAPPING['redholzapfelogden']     = ReducedHolzapfelOgdenMaterial
MAPPING['disparterial']          = DispersionArterialMaterial
MAPPING['redholzapfelogdendisp'] = ReducedHolzapfelOgdenDispersionMaterial
MAPPING['usyk']                  = UsykMaterial
MAPPING['compmooneyrivlin']      = CompressibleMooneyRivlinMaterial
MAPPING['modmooneyrivlin']       = ModifiedMooneyRivlinMaterial
MAPPING['fullholzapfelogden']    = FullHolzapfelOgdenMaterial


def keys():
    """
    Return a list of string keys to be used with :func:`get`
    """
    return MAPPING.keys()


def get(*args, **kwargs):
    """
    Get a class from its key string from :func:`keys`
    """
    return MAPPING.get(*args, **kwargs)


def summary(indent=2, ncolumns=4, padding=2):
    """
    Return a string summarizing the models and their parameters
    """
    modelattr = ['SHORTNAME', 'MODELID']
    maxattrlen = max(map(len, modelattr+['PARAMETERS']))
    summarystr = 'MATERIAL MODELS\n\n'
    indentstr = ' '*indent
    for name, model in MAPPING.items():
        summarystr += indentstr+'{}\n'.format(name)
        for attr in modelattr:
            obj = getattr(model, attr, None)
            if obj is None:
                continue
            summarystr += indentstr*2+'{}: {}\n'.format(attr.lower().ljust(maxattrlen), str(obj))
        attr = 'PARAMETERS'
        obj = getattr(model, attr, None)
        if obj is not None and len(obj) > 0:
            tablestr = stringtable(obj, ncolumns, padding, rowprefix=' '*(maxattrlen+2*indent+2))
            summarystr += indentstr*2+attr.lower().ljust(maxattrlen)+': '+tablestr[len(attr)+2*indent+2:]
        summarystr += '\n'
    return summarystr
