
from .general import optionlist

from . import ionic
from . import activetension
from . import mechanics

from .stimulus import Stimulus
from .conductivity import ConductivityRegion
from .eikonal import EikonalRegion

# Make aliases for compactness
GRegion  = ConductivityRegion
EkRegion = EikonalRegion
