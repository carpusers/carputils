
from .general import AbstractIonicModel as _AbstractIonicModel
from collections import OrderedDict as _OrderedDict
from carputils.format import stringtable


class PassiveIonicRegion(_AbstractIonicModel):
    """
    Describes the passive ionic model configuration

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    """
    SHORTNAME = 'passive'
    MODELID = 'PASSIVE'
    PARAMETERS = []


class AlievPanfilovIonicModel(_AbstractIonicModel):
    """
    Describes the AlievPanfilov ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    mu1 : float, optional
        Value of mu1, defaults to 0.2
    mu2 : float, optional
        Value of mu2, defaults to 0.3
    """
    SHORTNAME = 'alievpanfilov'
    MODELID = 'AlievPanfilov'
    PARAMETERS = ['mu1', 'mu2']


class ARPFIonicModel(_AbstractIonicModel):
    """
    Describes the ARPF ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Ca_e : float, optional
        Value of Ca_e, defaults to 1.8
    Ca_handling : float, optional
        Value of Ca_handling, defaults to 1.0
    Cl_e : float, optional
        Value of Cl_e, defaults to 150.0
    Cl_i : float, optional
        Value of Cl_i, defaults to 30.0
    g_CaB : float, optional
        Value of g_CaB, defaults to 0.00035182
    g_CaL : float, optional
        Value of g_CaL, defaults to 0.27
    g_CaT : float, optional
        Value of g_CaT, defaults to 0.2
    g_Cl : float, optional
        Value of g_Cl, defaults to 0.3
    g_ClB : float, optional
        Value of g_ClB, defaults to 0.0009
    g_K1 : float, optional
        Value of g_K1, defaults to 0.5
    g_KB : float, optional
        Value of g_KB, defaults to 5e-05
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.015583333333333
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.011738183745949
    g_Na : float, optional
        Value of g_Na, defaults to 20.0
    g_NaB : float, optional
        Value of g_NaB, defaults to 2.97e-05
    g_NaL : float, optional
        Value of g_NaL, defaults to 0.0162
    g_to : float, optional
        Value of g_to, defaults to 0.112
    I_CaP_Hill : float, optional
        Value of I_CaP_Hill, defaults to 1.6
    I_CaP_Kmf : float, optional
        Value of I_CaP_Kmf, defaults to 0.5
    I_CaP_Vmf : float, optional
        Value of I_CaP_Vmf
    inacamax : float, optional
        Value of inacamax, defaults to 4.5
    K_e_init : float, optional
        Value of K_e_init, defaults to 5.4
    K_i : float, optional
        Value of K_i, defaults to 135.0
    kmcaact : float, optional
        Value of kmcaact, defaults to 0.000125
    kmcai : float, optional
        Value of kmcai, defaults to 0.0036
    kmcao : float, optional
        Value of kmcao, defaults to 1.3
    kmnai1 : float, optional
        Value of kmnai1, defaults to 12.3
    kmnao : float, optional
        Value of kmnao, defaults to 87.5
    ksat : float, optional
        Value of ksat, defaults to 0.27
    Mg_i : float, optional
        Value of Mg_i, defaults to 1.0
    Na_e : float, optional
        Value of Na_e, defaults to 140.0
    Na_i : float, optional
        Value of Na_i, defaults to 8.8
    nu : float, optional
        Value of nu, defaults to 0.35
    P_NaK : float, optional
        Value of P_NaK, defaults to 0.01833
    T : float, optional
        Value of T, defaults to 310.0
    """
    SHORTNAME = 'arpf'
    MODELID = 'ARPF'
    PARAMETERS = ['Ca_e', 'Ca_handling', 'Cl_e', 'Cl_i', 'g_CaB', 'g_CaL', 
                  'g_CaT', 'g_Cl', 'g_ClB', 'g_K1', 'g_KB', 'g_Kr', 'g_Ks', 
                  'g_Na', 'g_NaB', 'g_NaL', 'g_to', 'I_CaP_Hill', 'I_CaP_Kmf', 
                  'I_CaP_Vmf', 'inacamax', 'K_e_init', 'K_i', 'kmcaact', 
                  'kmcai', 'kmcao', 'kmnai1', 'kmnao', 'ksat', 'Mg_i', 'Na_e', 
                  'Na_i', 'nu', 'P_NaK', 'T']


class StewardIonicModel(_AbstractIonicModel):
    """
    Describes the Steward ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    g_bca : float, optional
        Value of g_bca, defaults to 0.000592
    g_bna : float, optional
        Value of g_bna, defaults to 0.00029
    g_CaL : float, optional
        Value of g_CaL, defaults to 3.98e-05
    g_f_K : float, optional
        Value of g_f_K, defaults to 0.0234346
    g_f_Na : float, optional
        Value of g_f_Na, defaults to 0.0145654
    g_K1 : float, optional
        Value of g_K1, defaults to 0.065
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.0918
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.2352
    g_Na : float, optional
        Value of g_Na, defaults to 130.5744
    g_pCa : float, optional
        Value of g_pCa, defaults to 0.1238
    g_pK : float, optional
        Value of g_pK, defaults to 0.0146
    g_sus : float, optional
        Value of g_sus, defaults to 0.0227
    g_to : float, optional
        Value of g_to, defaults to 0.08184
    K_NaCa : float, optional
        Value of K_NaCa, defaults to 1000.0
    P_NaK : float, optional
        Value of P_NaK, defaults to 2.724
    """
    SHORTNAME = 'steward'
    MODELID = 'Steward'
    PARAMETERS = ['g_bca', 'g_bna', 'g_CaL', 'g_f_K', 'g_f_Na', 'g_K1', 'g_Kr', 
                  'g_Ks', 'g_Na', 'g_pCa', 'g_pK', 'g_sus', 'g_to', 'K_NaCa', 
                  'P_NaK']


class PRd2IonicModel(_AbstractIonicModel):
    """
    Describes the PRd2 ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    gRyR3 : float, optional
        Value of gRyR3, defaults to 15.0
    RyR2_sens : float, optional
        Value of RyR2_sens, defaults to 1.0
    RyR2_tau : float, optional
        Value of RyR2_tau, defaults to 6.0
    """
    SHORTNAME = 'prd2'
    MODELID = 'PRd2'
    PARAMETERS = ['gRyR3', 'RyR2_sens', 'RyR2_tau']


class converted_COURTEMANCHEIonicModel(_AbstractIonicModel):
    """
    Describes the converted_COURTEMANCHE ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    blf_i_Kur : float, optional
        Value of blf_i_Kur, defaults to 1.0
    blf_i_up : float, optional
        Value of blf_i_up, defaults to 1.0
    Ca_o : float, optional
        Value of Ca_o, defaults to 1.8
    g_bCa : float, optional
        Value of g_bCa, defaults to 0.00113
    g_bNa : float, optional
        Value of g_bNa, defaults to 0.000674
    g_CaL : float, optional
        Value of g_CaL, defaults to 0.1238
    g_K1 : float, optional
        Value of g_K1, defaults to 0.09
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.0294
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.129
    g_Na : float, optional
        Value of g_Na, defaults to 7.8
    g_to : float, optional
        Value of g_to, defaults to 0.1652
    K_o : float, optional
        Value of K_o, defaults to 5.4
    Na_o : float, optional
        Value of Na_o, defaults to 140.0
    """
    SHORTNAME = 'converted_courtemanche'
    MODELID = 'converted_COURTEMANCHE'
    PARAMETERS = ['blf_i_Kur', 'blf_i_up', 'Ca_o', 'g_bCa', 'g_bNa', 'g_CaL', 
                  'g_K1', 'g_Kr', 'g_Ks', 'g_Na', 'g_to', 'K_o', 'Na_o']


class converted_DiFranNobleIonicModel(_AbstractIonicModel):
    """
    Describes the converted_DiFranNoble ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    C : float, optional
        Value of C
    G_Na : float, optional
        Value of G_Na
    kNaCa : float, optional
        Value of kNaCa, defaults to 0.02
    p_to : float, optional
        Value of p_to, defaults to 1.0
    pCaf : float, optional
        Value of pCaf, defaults to 1.0
    pFunny : float, optional
        Value of pFunny, defaults to 1.0
    pK : float, optional
        Value of pK, defaults to 1.0
    """
    SHORTNAME = 'converted_difrannoble'
    MODELID = 'converted_DiFranNoble'
    PARAMETERS = ['C', 'G_Na', 'kNaCa', 'p_to', 'pCaf', 'pFunny', 'pK']


class converted_LRDII_FIonicModel(_AbstractIonicModel):
    """
    Describes the converted_LRDII_F ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    g_NaK : float, optional
        Value of g_NaK, defaults to 1.0
    """
    SHORTNAME = 'converted_lrdii_f'
    MODELID = 'converted_LRDII_F'
    PARAMETERS = ['g_NaK']


class converted_MBRDRIonicModel(_AbstractIonicModel):
    """
    Describes the converted_MBRDR ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    APDshorten : float, optional
        Value of APDshorten, defaults to 1.0
    GNa : float, optional
        Value of GNa, defaults to 15.0
    Gsi : float, optional
        Value of Gsi, defaults to 0.09
    """
    SHORTNAME = 'converted_mbrdr'
    MODELID = 'converted_MBRDR'
    PARAMETERS = ['APDshorten', 'GNa', 'Gsi']


class converted_NYGRENIonicModel(_AbstractIonicModel):
    """
    Describes the converted_NYGREN ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    G_CaB : float, optional
        Value of G_CaB, defaults to 0.00157362
    G_CaL : float, optional
        Value of G_CaL, defaults to 0.135
    G_K1 : float, optional
        Value of G_K1, defaults to 0.06
    G_Kr : float, optional
        Value of G_Kr, defaults to 0.01
    G_KS : float, optional
        Value of G_KS, defaults to 0.02
    G_NaB : float, optional
        Value of G_NaB, defaults to 0.00121198
    G_sus : float, optional
        Value of G_sus, defaults to 0.055
    G_t : float, optional
        Value of G_t, defaults to 0.15
    I_NaKmax : float, optional
        Value of I_NaKmax, defaults to 1.416506
    k_NaCa : float, optional
        Value of k_NaCa, defaults to 0.000749684
    """
    SHORTNAME = 'converted_nygren'
    MODELID = 'converted_NYGREN'
    PARAMETERS = ['G_CaB', 'G_CaL', 'G_K1', 'G_Kr', 'G_KS', 'G_NaB', 'G_sus', 
                  'G_t', 'I_NaKmax', 'k_NaCa']


class converted_RNCIonicModel(_AbstractIonicModel):
    """
    Describes the converted_RNC ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Ca_o : float, optional
        Value of Ca_o, defaults to 1.8
    Cl_o : float, optional
        Value of Cl_o, defaults to 132.0
    g_bCa : float, optional
        Value of g_bCa, defaults to 0.00113
    g_bNa : float, optional
        Value of g_bNa, defaults to 0.000674
    g_CaL : float, optional
        Value of g_CaL, defaults to 0.24
    g_ClCa : float, optional
        Value of g_ClCa, defaults to 0.3
    g_K1 : float, optional
        Value of g_K1, defaults to 0.15
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.06984
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.0561
    g_Na : float, optional
        Value of g_Na, defaults to 7.8
    g_to : float, optional
        Value of g_to, defaults to 0.19824
    K_o : float, optional
        Value of K_o, defaults to 5.4
    maxCa_up : float, optional
        Value of maxCa_up, defaults to 15.0
    maxI_NaCa : float, optional
        Value of maxI_NaCa, defaults to 1600.0
    maxI_NaK : float, optional
        Value of maxI_NaK, defaults to 0.6
    maxI_pCa : float, optional
        Value of maxI_pCa, defaults to 0.275
    maxI_up : float, optional
        Value of maxI_up, defaults to 0.005
    Na_o : float, optional
        Value of Na_o, defaults to 140.0
    tau_tr : float, optional
        Value of tau_tr, defaults to 180.0
    tau_u : float, optional
        Value of tau_u, defaults to 8.0
    """
    SHORTNAME = 'converted_rnc'
    MODELID = 'converted_RNC'
    PARAMETERS = ['Ca_o', 'Cl_o', 'g_bCa', 'g_bNa', 'g_CaL', 'g_ClCa', 'g_K1', 
                  'g_Kr', 'g_Ks', 'g_Na', 'g_to', 'K_o', 'maxCa_up', 
                  'maxI_NaCa', 'maxI_NaK', 'maxI_pCa', 'maxI_up', 'Na_o', 
                  'tau_tr', 'tau_u']


class converted_TT2IonicModel(_AbstractIonicModel):
    """
    Describes the converted_TT2 ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Bufc : float, optional
        Value of Bufc, defaults to 0.2
    Bufsr : float, optional
        Value of Bufsr, defaults to 10.0
    Bufss : float, optional
        Value of Bufss, defaults to 0.4
    Cao : float, optional
        Value of Cao, defaults to 2.0
    CAPACITANCE : float, optional
        Value of CAPACITANCE, defaults to 0.185
    cell_type : float, optional
        Value of cell_type
    D_CaL_off : float, optional
        Value of D_CaL_off, defaults to 0.0
    Fconst : float, optional
        Value of Fconst, defaults to 96485.3415
    flags : float, optional
        Value of flags
    GbCa : float, optional
        Value of GbCa, defaults to 0.000592
    GbNa : float, optional
        Value of GbNa, defaults to 0.00029
    GCaL : float, optional
        Value of GCaL, defaults to 3.98e-05
    GK1 : float, optional
        Value of GK1, defaults to 5.405
    Gkr : float, optional
        Value of Gkr, defaults to 0.153
    Gks : float, optional
        Value of Gks
    GNa : float, optional
        Value of GNa, defaults to 14.838
    GpCa : float, optional
        Value of GpCa, defaults to 0.1238
    GpK : float, optional
        Value of GpK, defaults to 0.0146
    Gto : float, optional
        Value of Gto
    Kbufc : float, optional
        Value of Kbufc, defaults to 0.001
    Kbufsr : float, optional
        Value of Kbufsr, defaults to 0.3
    Kbufss : float, optional
        Value of Kbufss, defaults to 0.00025
    KmCa : float, optional
        Value of KmCa, defaults to 1.38
    KmK : float, optional
        Value of KmK, defaults to 1.0
    KmNa : float, optional
        Value of KmNa, defaults to 40.0
    KmNai : float, optional
        Value of KmNai, defaults to 87.5
    knaca : float, optional
        Value of knaca, defaults to 1000.0
    knak : float, optional
        Value of knak, defaults to 2.724
    Ko : float, optional
        Value of Ko, defaults to 5.4
    KpCa : float, optional
        Value of KpCa, defaults to 0.0005
    ksat : float, optional
        Value of ksat, defaults to 0.1
    Kup : float, optional
        Value of Kup, defaults to 0.00025
    n : float, optional
        Value of n, defaults to 0.35
    Nao : float, optional
        Value of Nao, defaults to 140.0
    pKNa : float, optional
        Value of pKNa, defaults to 0.03
    Rconst : float, optional
        Value of Rconst, defaults to 8314.472
    scl_tau_f : float, optional
        Value of scl_tau_f, defaults to 1.0
    T : float, optional
        Value of T, defaults to 310.0
    Vc : float, optional
        Value of Vc, defaults to 0.016404
    Vleak : float, optional
        Value of Vleak, defaults to 0.00036
    Vmaxup : float, optional
        Value of Vmaxup, defaults to 0.006375
    Vrel : float, optional
        Value of Vrel, defaults to 0.102
    Vsr : float, optional
        Value of Vsr, defaults to 0.001094
    Vss : float, optional
        Value of Vss, defaults to 5.468e-05
    xr2_off : float, optional
        Value of xr2_off, defaults to 0.0
    """
    SHORTNAME = 'converted_tt2'
    MODELID = 'converted_TT2'
    PARAMETERS = ['Bufc', 'Bufsr', 'Bufss', 'Cao', 'CAPACITANCE', 'cell_type', 
                  'D_CaL_off', 'Fconst', 'flags', 'GbCa', 'GbNa', 'GCaL', 'GK1', 
                  'Gkr', 'Gks', 'GNa', 'GpCa', 'GpK', 'Gto', 'Kbufc', 'Kbufsr', 
                  'Kbufss', 'KmCa', 'KmK', 'KmNa', 'KmNai', 'knaca', 'knak', 
                  'Ko', 'KpCa', 'ksat', 'Kup', 'n', 'Nao', 'pKNa', 'Rconst', 
                  'scl_tau_f', 'T', 'Vc', 'Vleak', 'Vmaxup', 'Vrel', 'Vsr', 
                  'Vss', 'xr2_off']


class converted_UCLA_RABIonicModel(_AbstractIonicModel):
    """
    Describes the converted_UCLA_RAB ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    BASE_Ki : float, optional
        Value of BASE_Ki, defaults to 140.0
    blf_ICaL : float, optional
        Value of blf_ICaL, defaults to 1.0
    blf_IKr : float, optional
        Value of blf_IKr, defaults to 1.0
    blf_IKs : float, optional
        Value of blf_IKs, defaults to 1.0
    blf_INa : float, optional
        Value of blf_INa, defaults to 1.0
    blf_INaCa : float, optional
        Value of blf_INaCa, defaults to 1.0
    Cao : float, optional
        Value of Cao
    constant_K_i : float, optional
        Value of constant_K_i, defaults to 0.0
    gK1 : float, optional
        Value of gK1, defaults to 0.3
    gKr : float, optional
        Value of gKr, defaults to 0.0125
    gKs : float, optional
        Value of gKs
    gNa : float, optional
        Value of gNa, defaults to 12.0
    gss : float, optional
        Value of gss
    gtof : float, optional
        Value of gtof, defaults to 0.11
    gtos : float, optional
        Value of gtos, defaults to 0.04
    Ko : float, optional
        Value of Ko
    m : float, optional
        Value of m
    Na_i : float, optional
        Value of Na_i
    Nao : float, optional
        Value of Nao
    T : float, optional
        Value of T, defaults to 308.0
    """
    SHORTNAME = 'converted_ucla_rab'
    MODELID = 'converted_UCLA_RAB'
    PARAMETERS = ['BASE_Ki', 'blf_ICaL', 'blf_IKr', 'blf_IKs', 'blf_INa', 
                  'blf_INaCa', 'Cao', 'constant_K_i', 'gK1', 'gKr', 'gKs', 
                  'gNa', 'gss', 'gtof', 'gtos', 'Ko', 'm', 'Na_i', 'Nao', 'T']


class UCLA_RAB_SCRIonicModel(_AbstractIonicModel):
    """
    Describes the UCLA_RAB_SCR ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Ca_o : float, optional
        Value of Ca_o, defaults to 1.8
    CSR : float, optional
        Value of CSR, defaults to 1200.0
    g_Ca : float, optional
        Value of g_Ca, defaults to 0.6
    g_K1 : float, optional
        Value of g_K1, defaults to 0.3
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.003
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.043
    g_Na : float, optional
        Value of g_Na, defaults to 12.0
    g_RyR : float, optional
        Value of g_RyR, defaults to 2.25
    g_sp : float, optional
        Value of g_sp
    g_tof : float, optional
        Value of g_tof, defaults to 0.1
    g_tos : float, optional
        Value of g_tos, defaults to 0.04
    i_NaCamax : float, optional
        Value of i_NaCamax, defaults to 0.3
    i_NaKmax : float, optional
        Value of i_NaKmax, defaults to 1.5
    K_o : float, optional
        Value of K_o, defaults to 5.4
    Na_o : float, optional
        Value of Na_o, defaults to 136.0
    rseed : float, optional
        Value of rseed, defaults to -1.0
    t_SCR : float, optional
        Value of t_SCR, defaults to -1.0
    v_Na : float, optional
        Value of v_Na, defaults to 0.0
    v_up : float, optional
        Value of v_up, defaults to 0.3
    """
    SHORTNAME = 'ucla_rab_scr'
    MODELID = 'UCLA_RAB_SCR'
    PARAMETERS = ['Ca_o', 'CSR', 'g_Ca', 'g_K1', 'g_Kr', 'g_Ks', 'g_Na', 
                  'g_RyR', 'g_sp', 'g_tof', 'g_tos', 'i_NaCamax', 'i_NaKmax', 
                  'K_o', 'Na_o', 'rseed', 't_SCR', 'v_Na', 'v_up']


class COURTEMANCHEIonicModel(_AbstractIonicModel):
    """
    Describes the COURTEMANCHE ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    blf_g_Kur : float, optional
        Value of blf_g_Kur, defaults to 1.0
    extern_Cab : float, optional
        Value of extern_Cab
    g_bCa : float, optional
        Value of g_bCa, defaults to 0.00113
    g_bNa : float, optional
        Value of g_bNa, defaults to 0.000674
    g_CaL : float, optional
        Value of g_CaL, defaults to 0.1238
    g_K1 : float, optional
        Value of g_K1, defaults to 0.09
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.0294
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.129
    g_Na : float, optional
        Value of g_Na, defaults to 7.8
    g_to : float, optional
        Value of g_to, defaults to 0.1652
    """
    SHORTNAME = 'courtemanche'
    MODELID = 'COURTEMANCHE'
    PARAMETERS = ['blf_g_Kur', 'extern_Cab', 'g_bCa', 'g_bNa', 'g_CaL', 'g_K1', 
                  'g_Kr', 'g_Ks', 'g_Na', 'g_to']


class DiFranNobleIonicModel(_AbstractIonicModel):
    """
    Describes the DiFranNoble ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    C : float, optional
        Value of C
    G_Na : float, optional
        Value of G_Na, defaults to 750.0
    kNaCa : float, optional
        Value of kNaCa, defaults to 0.02
    p_to : float, optional
        Value of p_to, defaults to 1.0
    pCaf : float, optional
        Value of pCaf, defaults to 1.0
    pFunny : float, optional
        Value of pFunny, defaults to 1.0
    pK : float, optional
        Value of pK, defaults to 1.0
    """
    SHORTNAME = 'difrannoble'
    MODELID = 'DiFranNoble'
    PARAMETERS = ['C', 'G_Na', 'kNaCa', 'p_to', 'pCaf', 'pFunny', 'pK']


class ECMEIonicModel(_AbstractIonicModel):
    """
    Describes the ECME ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Cao : float, optional
        Value of Cao, defaults to 2.0
    etSOD : float, optional
        Value of etSOD, defaults to 0.00143
    gNa : float, optional
        Value of gNa, defaults to 12.8
    H_KATP : float, optional
        Value of H_KATP, defaults to -0.001
    INaKmax : float, optional
        Value of INaKmax, defaults to 3.147
    j_Vt2SO2m : float, optional
        Value of j_Vt2SO2m, defaults to 0.1
    Ko : float, optional
        Value of Ko, defaults to 5.4
    Nao : float, optional
        Value of Nao, defaults to 140.0
    rhoF1 : float, optional
        Value of rhoF1, defaults to 0.05
    rhoREF : float, optional
        Value of rhoREF, defaults to 0.000375
    rhoREN : float, optional
        Value of rhoREN, defaults to 0.0001
    shunt : float, optional
        Value of shunt, defaults to 0.1
    sigma_KATP : float, optional
        Value of sigma_KATP, defaults to 0.6
    useCK : float, optional
        Value of useCK
    Vmuni : float, optional
        Value of Vmuni, defaults to 0.0275
    """
    SHORTNAME = 'ecme'
    MODELID = 'ECME'
    PARAMETERS = ['Cao', 'etSOD', 'gNa', 'H_KATP', 'INaKmax', 'j_Vt2SO2m', 'Ko', 
                  'Nao', 'rhoF1', 'rhoREF', 'rhoREN', 'shunt', 'sigma_KATP', 
                  'useCK', 'Vmuni']


class FHN_RMcCIonicModel(_AbstractIonicModel):
    """
    Describes the FHN_RMcC ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    eta1 : float, optional
        Value of eta1, defaults to 4.4
    eta2 : float, optional
        Value of eta2, defaults to 0.012
    eta3 : float, optional
        Value of eta3, defaults to 1.0
    G : float, optional
        Value of G, defaults to 1.5
    vp : float, optional
        Value of vp, defaults to 100.0
    vth : float, optional
        Value of vth, defaults to 13.0
    """
    SHORTNAME = 'fhn_rmcc'
    MODELID = 'FHN_RMcC'
    PARAMETERS = ['eta1', 'eta2', 'eta3', 'G', 'vp', 'vth']


class FOXIonicModel(_AbstractIonicModel):
    """
    Describes the FOX ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.0136
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.0245
    g_Na : float, optional
        Value of g_Na, defaults to 12.8
    P_Ca : float, optional
        Value of P_Ca, defaults to 2.26e-05
    """
    SHORTNAME = 'fox'
    MODELID = 'FOX'
    PARAMETERS = ['g_Kr', 'g_Ks', 'g_Na', 'P_Ca']


class GPBIonicModel(_AbstractIonicModel):
    """
    Describes the GPB ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Ca_i_init : float, optional
        Value of Ca_i_init, defaults to 0.08597401
    Cao : float, optional
        Value of Cao, defaults to 1.8
    cell_type : float, optional
        Value of cell_type
    Cli : float, optional
        Value of Cli, defaults to 15.0
    Clo : float, optional
        Value of Clo, defaults to 150.0
    flags : float, optional
        Value of flags
    Ki_init : float, optional
        Value of Ki_init, defaults to 120.0
    Ko : float, optional
        Value of Ko, defaults to 5.4
    markov_iks : float, optional
        Value of markov_iks, defaults to 0.0
    Mgi : float, optional
        Value of Mgi, defaults to 1.0
    Nai_init : float, optional
        Value of Nai_init, defaults to 9.06
    Nao : float, optional
        Value of Nao, defaults to 140.0
    """
    SHORTNAME = 'gpb'
    MODELID = 'GPB'
    PARAMETERS = ['Ca_i_init', 'Cao', 'cell_type', 'Cli', 'Clo', 'flags', 
                  'Ki_init', 'Ko', 'markov_iks', 'Mgi', 'Nai_init', 'Nao']


class GPVatriaIonicModel(_AbstractIonicModel):
    """
    Describes the GPVatria ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    AF : float, optional
        Value of AF, defaults to 0.0
    factor_g_CaL : float, optional
        Value of factor_g_CaL, defaults to 1.0
    factor_g_K1 : float, optional
        Value of factor_g_K1, defaults to 1.0
    factor_J_SRCarel : float, optional
        Value of factor_J_SRCarel, defaults to 1.0
    factor_J_SRleak : float, optional
        Value of factor_J_SRleak, defaults to 1.0
    factor_Vmax_SRCaP : float, optional
        Value of factor_Vmax_SRCaP, defaults to 1.0
    ISO : float, optional
        Value of ISO, defaults to 0.0
    markov_iks : float, optional
        Value of markov_iks, defaults to 0.0
    RA : float, optional
        Value of RA, defaults to 0.0
    usefca : float, optional
        Value of usefca, defaults to 0.0
    """
    SHORTNAME = 'gpvatria'
    MODELID = 'GPVatria'
    PARAMETERS = ['AF', 'factor_g_CaL', 'factor_g_K1', 'factor_J_SRCarel', 
                  'factor_J_SRleak', 'factor_Vmax_SRCaP', 'ISO', 'markov_iks', 
                  'RA', 'usefca']


class GPB_LandIonicModel(_AbstractIonicModel):
    """
    Describes the GPB_Land ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a : float, optional
        Value of a, defaults to 0.35
    A_1 : float, optional
        Value of A_1, defaults to -29.0
    A_2 : float, optional
        Value of A_2, defaults to 116.0
    alpha_1 : float, optional
        Value of alpha_1, defaults to 0.1
    alpha_2 : float, optional
        Value of alpha_2, defaults to 0.5
    beta_0 : float, optional
        Value of beta_0, defaults to 1.65
    beta_1 : float, optional
        Value of beta_1, defaults to -1.5
    Ca_50ref : float, optional
        Value of Ca_50ref, defaults to 0.52
    Ca_i_init : float, optional
        Value of Ca_i_init, defaults to 0.08597401
    Cao : float, optional
        Value of Cao, defaults to 1.8
    cell_type : float, optional
        Value of cell_type
    Cli : float, optional
        Value of Cli, defaults to 15.0
    Clo : float, optional
        Value of Clo, defaults to 150.0
    flags : float, optional
        Value of flags
    k_TRPN : float, optional
        Value of k_TRPN, defaults to 0.14
    k_xb : float, optional
        Value of k_xb, defaults to 0.0049
    Ki_init : float, optional
        Value of Ki_init, defaults to 120.0
    Ko : float, optional
        Value of Ko, defaults to 5.4
    lengthDep : float, optional
        Value of lengthDep, defaults to 0.0
    markov_iks : float, optional
        Value of markov_iks, defaults to 0.0
    Mgi : float, optional
        Value of Mgi, defaults to 1.0
    n_TRPN : float, optional
        Value of n_TRPN, defaults to 1.54
    n_xb : float, optional
        Value of n_xb, defaults to 3.38
    Nai_init : float, optional
        Value of Nai_init, defaults to 9.06
    Nao : float, optional
        Value of Nao, defaults to 140.0
    T_ref : float, optional
        Value of T_ref, defaults to 117.1
    TRPN_50 : float, optional
        Value of TRPN_50, defaults to 0.37
    """
    SHORTNAME = 'gpb_land'
    MODELID = 'GPB_Land'
    PARAMETERS = ['a', 'A_1', 'A_2', 'alpha_1', 'alpha_2', 'beta_0', 'beta_1', 
                  'Ca_50ref', 'Ca_i_init', 'Cao', 'cell_type', 'Cli', 'Clo', 
                  'flags', 'k_TRPN', 'k_xb', 'Ki_init', 'Ko', 'lengthDep', 
                  'markov_iks', 'Mgi', 'n_TRPN', 'n_xb', 'Nai_init', 'Nao', 
                  'T_ref', 'TRPN_50']


class GW_CANIonicModel(_AbstractIonicModel):
    """
    Describes the GW_CAN ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Cao : float, optional
        Value of Cao, defaults to 2.0
    CHF : float, optional
        Value of CHF, defaults to 0.0
    flags : float, optional
        Value of flags
    G_K1 : float, optional
        Value of G_K1
    G_Ks : float, optional
        Value of G_Ks
    G_Kv43 : float, optional
        Value of G_Kv43
    gNa : float, optional
        Value of gNa, defaults to 12.8
    kNaCa : float, optional
        Value of kNaCa
    Ko : float, optional
        Value of Ko, defaults to 4.0
    ksr : float, optional
        Value of ksr
    Nao : float, optional
        Value of Nao, defaults to 138.0
    PKv14 : float, optional
        Value of PKv14, defaults to 1.708681e-07
    type : float, optional
        Value of type
    """
    SHORTNAME = 'gw_can'
    MODELID = 'GW_CAN'
    PARAMETERS = ['Cao', 'CHF', 'flags', 'G_K1', 'G_Ks', 'G_Kv43', 'gNa', 
                  'kNaCa', 'Ko', 'ksr', 'Nao', 'PKv14', 'type']


class hAMrIonicModel(_AbstractIonicModel):
    """
    Describes the hAMr ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    ACh : float, optional
        Value of ACh, defaults to 1e-24
    blf_i_di : float, optional
        Value of blf_i_di, defaults to 1.0
    blf_i_NaCa : float, optional
        Value of blf_i_NaCa, defaults to 1.0
    blf_i_rel : float, optional
        Value of blf_i_rel, defaults to 1.0
    blf_i_tr : float, optional
        Value of blf_i_tr, defaults to 1.0
    Cm : float, optional
        Value of Cm, defaults to 50.0
    g_B_Ca : float, optional
        Value of g_B_Ca, defaults to 0.078681
    g_B_Na : float, optional
        Value of g_B_Na, defaults to 0.060599
    g_Ca_L : float, optional
        Value of g_Ca_L, defaults to 6.75
    g_K1 : float, optional
        Value of g_K1, defaults to 3.1
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.5
    g_Ks : float, optional
        Value of g_Ks, defaults to 1.0
    g_kur : float, optional
        Value of g_kur, defaults to 2.25
    g_t : float, optional
        Value of g_t, defaults to 8.25
    i_CaP_max : float, optional
        Value of i_CaP_max, defaults to 4.0
    i_NaK_max : float, optional
        Value of i_NaK_max, defaults to 68.55
    I_up_max : float, optional
        Value of I_up_max, defaults to 2800.0
    P_ACh : float, optional
        Value of P_ACh, defaults to 1.0
    P_Na : float, optional
        Value of P_Na, defaults to 0.0018
    phi_Na_en : float, optional
        Value of phi_Na_en, defaults to -1.68
    """
    SHORTNAME = 'hamr'
    MODELID = 'hAMr'
    PARAMETERS = ['ACh', 'blf_i_di', 'blf_i_NaCa', 'blf_i_rel', 'blf_i_tr', 
                  'Cm', 'g_B_Ca', 'g_B_Na', 'g_Ca_L', 'g_K1', 'g_Kr', 'g_Ks', 
                  'g_kur', 'g_t', 'i_CaP_max', 'i_NaK_max', 'I_up_max', 'P_ACh', 
                  'P_Na', 'phi_Na_en']


class HHIonicModel(_AbstractIonicModel):
    """
    Describes the HH ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    g_K : float, optional
        Value of g_K, defaults to 36.0
    g_L : float, optional
        Value of g_L, defaults to 0.3
    g_Na : float, optional
        Value of g_Na, defaults to 120.0
    """
    SHORTNAME = 'hh'
    MODELID = 'HH'
    PARAMETERS = ['g_K', 'g_L', 'g_Na']


class IION_SRCIonicModel(_AbstractIonicModel):
    """
    Describes the IION_SRC ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    AP_ID : float, optional
        Value of AP_ID, defaults to 0.0
    """
    SHORTNAME = 'iion_src'
    MODELID = 'IION_SRC'
    PARAMETERS = ['AP_ID']


class INADA_AVNIonicModel(_AbstractIonicModel):
    """
    Describes the INADA_AVN ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    cell_type : float, optional
        Value of cell_type
    Cm_cell : float, optional
        Value of Cm_cell
    E_b : float, optional
        Value of E_b
    E_CaL : float, optional
        Value of E_CaL
    flags : float, optional
        Value of flags
    g_b : float, optional
        Value of g_b
    g_CaL : float, optional
        Value of g_CaL
    g_f : float, optional
        Value of g_f
    g_K1 : float, optional
        Value of g_K1
    g_Kr : float, optional
        Value of g_Kr
    g_Na : float, optional
        Value of g_Na
    g_st : float, optional
        Value of g_st
    g_to : float, optional
        Value of g_to
    k_NaCa : float, optional
        Value of k_NaCa
    p_max : float, optional
        Value of p_max
    p_rel : float, optional
        Value of p_rel
    """
    SHORTNAME = 'inada_avn'
    MODELID = 'INADA_AVN'
    PARAMETERS = ['cell_type', 'Cm_cell', 'E_b', 'E_CaL', 'flags', 'g_b', 
                  'g_CaL', 'g_f', 'g_K1', 'g_Kr', 'g_Na', 'g_st', 'g_to', 
                  'k_NaCa', 'p_max', 'p_rel']


class IribeIonicModel(_AbstractIonicModel):
    """
    Describes the Iribe ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    K_i_init : float, optional
        Value of K_i_init, defaults to 138.22
    """
    SHORTNAME = 'iribe'
    MODELID = 'Iribe'
    PARAMETERS = ['K_i_init']


class KurataIonicModel(_AbstractIonicModel):
    """
    Describes the Kurata ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    ACh : float, optional
        Value of ACh, defaults to 0.0
    B_max : float, optional
        Value of B_max, defaults to 0.56
    Cm : float, optional
        Value of Cm, defaults to 32.0
    G_CaL : float, optional
        Value of G_CaL, defaults to 0.58
    G_h : float, optional
        Value of G_h, defaults to 0.357
    G_KACh_max : float, optional
        Value of G_KACh_max
    G_Kr : float, optional
        Value of G_Kr
    G_Ks : float, optional
        Value of G_Ks, defaults to 0.025
    G_Na : float, optional
        Value of G_Na, defaults to 0.0
    G_st : float, optional
        Value of G_st, defaults to 0.015
    G_sus : float, optional
        Value of G_sus, defaults to 0.02
    G_to : float, optional
        Value of G_to, defaults to 0.18
    """
    SHORTNAME = 'kurata'
    MODELID = 'Kurata'
    PARAMETERS = ['ACh', 'B_max', 'Cm', 'G_CaL', 'G_h', 'G_KACh_max', 'G_Kr', 
                  'G_Ks', 'G_Na', 'G_st', 'G_sus', 'G_to']


class LMCGIonicModel(_AbstractIonicModel):
    """
    Describes the LMCG ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Cao : float, optional
        Value of Cao, defaults to 2.5
    cell_type : float, optional
        Value of cell_type
    ECaL : float, optional
        Value of ECaL, defaults to 50.0
    flags : float, optional
        Value of flags
    gCaB : float, optional
        Value of gCaB, defaults to 0.03
    gCaL : float, optional
        Value of gCaL, defaults to 4.0
    gCaP : float, optional
        Value of gCaP, defaults to 9.509
    gCaT : float, optional
        Value of gCaT, defaults to 6.0
    gClB : float, optional
        Value of gClB, defaults to 0.0
    gK1 : float, optional
        Value of gK1, defaults to 10.0
    gKr : float, optional
        Value of gKr, defaults to 7.0
    gKs : float, optional
        Value of gKs, defaults to 5.0
    gNaB : float, optional
        Value of gNaB, defaults to 0.03
    gNaCa : float, optional
        Value of gNaCa, defaults to 0.02
    gsus : float, optional
        Value of gsus, defaults to 2.4
    gt : float, optional
        Value of gt, defaults to 35.0
    iNaKmax : float, optional
        Value of iNaKmax, defaults to 64.41
    Ki : float, optional
        Value of Ki, defaults to 139.88
    Ko : float, optional
        Value of Ko, defaults to 5.0
    Nai : float, optional
        Value of Nai, defaults to 8.438
    Nao : float, optional
        Value of Nao, defaults to 140.0
    pNa : float, optional
        Value of pNa, defaults to 0.0014
    """
    SHORTNAME = 'lmcg'
    MODELID = 'LMCG'
    PARAMETERS = ['Cao', 'cell_type', 'ECaL', 'flags', 'gCaB', 'gCaL', 'gCaP', 
                  'gCaT', 'gClB', 'gK1', 'gKr', 'gKs', 'gNaB', 'gNaCa', 'gsus', 
                  'gt', 'iNaKmax', 'Ki', 'Ko', 'Nai', 'Nao', 'pNa']


class LR1IonicModel(_AbstractIonicModel):
    """
    Describes the LR1 ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Ca_i_init : float, optional
        Value of Ca_i_init, defaults to 0.0002
    Gb : float, optional
        Value of Gb, defaults to 0.03921
    GK1bar : float, optional
        Value of GK1bar, defaults to 0.604
    GKbar : float, optional
        Value of GKbar, defaults to 0.282
    GKp : float, optional
        Value of GKp, defaults to 0.0183
    GNa : float, optional
        Value of GNa, defaults to 23.0
    Gsi : float, optional
        Value of Gsi, defaults to 0.09
    Ki : float, optional
        Value of Ki, defaults to 145.0
    Ko : float, optional
        Value of Ko, defaults to 5.4
    Nai : float, optional
        Value of Nai, defaults to 18.0
    Nao : float, optional
        Value of Nao, defaults to 140.0
    tau_d_factor : float, optional
        Value of tau_d_factor, defaults to 1.0
    tau_f_factor : float, optional
        Value of tau_f_factor, defaults to 1.0
    """
    SHORTNAME = 'lr1'
    MODELID = 'LR1'
    PARAMETERS = ['Ca_i_init', 'Gb', 'GK1bar', 'GKbar', 'GKp', 'GNa', 'Gsi', 
                  'Ki', 'Ko', 'Nai', 'Nao', 'tau_d_factor', 'tau_f_factor']


class LRDIIIonicModel(_AbstractIonicModel):
    """
    Describes the LRDII ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    *capulse_AP : float, optional
        Value of *capulse_AP
    *capulse_CaO : float, optional
        Value of *capulse_CaO
    *flags : float, optional
        Value of *flags
    caincyc : float, optional
        Value of caincyc
    cell_type : float, optional
        Value of cell_type, defaults to 1.0
    dvdt_thresh : float, optional
        Value of dvdt_thresh, defaults to 20.0
    extern_Cab : float, optional
        Value of extern_Cab, defaults to 0.0
    gCaL : float, optional
        Value of gCaL, defaults to 1.0
    gKr : float, optional
        Value of gKr, defaults to 0.02614
    gKs : float, optional
        Value of gKs, defaults to 1.0
    grel_AP : float, optional
        Value of grel_AP, defaults to 38.0
    grel_CaO : float, optional
        Value of grel_CaO, defaults to 3.0
    pulsecyc : float, optional
        Value of pulsecyc
    ratio[0] : float, optional
        Value of ratio[0], defaults to 1.0
    ratio[1] : float, optional
        Value of ratio[1], defaults to 1.0
    ratio[2] : float, optional
        Value of ratio[2], defaults to 1.0
    ratio[3] : float, optional
        Value of ratio[3]
    tauSR_AP : float, optional
        Value of tauSR_AP, defaults to 3.5
    tauSR_CaO : float, optional
        Value of tauSR_CaO, defaults to 6.0
    """
    SHORTNAME = 'lrdii'
    MODELID = 'LRDII'
    PARAMETERS = ['*capulse_AP', '*capulse_CaO', '*flags', 'caincyc', 
                  'cell_type', 'dvdt_thresh', 'extern_Cab', 'gCaL', 'gKr', 
                  'gKs', 'grel_AP', 'grel_CaO', 'pulsecyc', 'ratio[0]', 
                  'ratio[1]', 'ratio[2]', 'ratio[3]', 'tauSR_AP', 'tauSR_CaO']


class LRDII_FIonicModel(_AbstractIonicModel):
    """
    Describes the LRDII_F ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    *capulse_cicr : float, optional
        Value of *capulse_cicr
    *capulse_jsrol : float, optional
        Value of *capulse_jsrol
    cicr_pulsecyc : float, optional
        Value of cicr_pulsecyc
    ext_conc_dt : float, optional
        Value of ext_conc_dt, defaults to 0.0
    extern_Cab : float, optional
        Value of extern_Cab
    g_NaB : float, optional
        Value of g_NaB, defaults to 0.004
    g_NaK : float, optional
        Value of g_NaK, defaults to 1.0
    gamma : float, optional
        Value of gamma, defaults to 0.8
    jsrol_pulsecyc : float, optional
        Value of jsrol_pulsecyc
    scl_tau_f : float, optional
        Value of scl_tau_f, defaults to 1.0
    """
    SHORTNAME = 'lrdii_f'
    MODELID = 'LRDII_F'
    PARAMETERS = ['*capulse_cicr', '*capulse_jsrol', 'cicr_pulsecyc', 
                  'ext_conc_dt', 'extern_Cab', 'g_NaB', 'g_NaK', 'gamma', 
                  'jsrol_pulsecyc', 'scl_tau_f']


class MacCannell_FbIonicModel(_AbstractIonicModel):
    """
    Describes the MacCannell_Fb ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    g_b_Na : float, optional
        Value of g_b_Na, defaults to 0.0095
    g_K1 : float, optional
        Value of g_K1, defaults to 0.4822
    g_Kv : float, optional
        Value of g_Kv, defaults to 0.25
    i_bar_NaK : float, optional
        Value of i_bar_NaK, defaults to 1.4
    K_i_init : float, optional
        Value of K_i_init, defaults to 140.045865
    K_o : float, optional
        Value of K_o, defaults to 5.4
    Na_i_init : float, optional
        Value of Na_i_init, defaults to 9.954213
    Na_o : float, optional
        Value of Na_o, defaults to 130.0
    """
    SHORTNAME = 'maccannell_fb'
    MODELID = 'MacCannell_Fb'
    PARAMETERS = ['g_b_Na', 'g_K1', 'g_Kv', 'i_bar_NaK', 'K_i_init', 'K_o', 
                  'Na_i_init', 'Na_o']


class MitchellSchaefferIonicModel(_AbstractIonicModel):
    """
    Describes the MitchellSchaeffer ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    tau_close : float, optional
        Value of tau_close, defaults to 150.0
    tau_in : float, optional
        Value of tau_in, defaults to 0.3
    tau_open : float, optional
        Value of tau_open, defaults to 120.0
    tau_out : float, optional
        Value of tau_out, defaults to 5.0
    V_gate : float, optional
        Value of V_gate, defaults to 0.13
    V_max : float, optional
        Value of V_max, defaults to 1.0
    V_min : float, optional
        Value of V_min, defaults to 0.0
    """
    SHORTNAME = 'mitchellschaeffer'
    MODELID = 'MitchellSchaeffer'
    PARAMETERS = ['tau_close', 'tau_in', 'tau_open', 'tau_out', 'V_gate', 
                  'V_max', 'V_min']


class mMSIonicModel(_AbstractIonicModel):
    """
    Describes the mMS ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a_crit : float, optional
        Value of a_crit, defaults to 0.13
    tau_close : float, optional
        Value of tau_close, defaults to 150.0
    tau_in : float, optional
        Value of tau_in, defaults to 0.3
    tau_open : float, optional
        Value of tau_open, defaults to 120.0
    tau_out : float, optional
        Value of tau_out, defaults to 5.0
    V_gate : float, optional
        Value of V_gate, defaults to 0.13
    V_max : float, optional
        Value of V_max, defaults to 1.0
    V_min : float, optional
        Value of V_min, defaults to 0.0
    """
    SHORTNAME = 'mms'
    MODELID = 'mMS'
    PARAMETERS = ['a_crit', 'tau_close', 'tau_in', 'tau_open', 'tau_out', 
                  'V_gate', 'V_max', 'V_min']


class MBRDRIonicModel(_AbstractIonicModel):
    """
    Describes the MBRDR ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    APDshorten : float, optional
        Value of APDshorten, defaults to 1.0
    GNa : float, optional
        Value of GNa, defaults to 15.0
    Gsi : float, optional
        Value of Gsi, defaults to 0.09
    """
    SHORTNAME = 'mbrdr'
    MODELID = 'MBRDR'
    PARAMETERS = ['APDshorten', 'GNa', 'Gsi']


class MurineMouseIonicModel(_AbstractIonicModel):
    """
    Describes the MurineMouse ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    a : float, optional
        Value of a, defaults to 0.0625
    Acap : float, optional
        Value of Acap, defaults to 0.00013866
    aKss_init : float, optional
        Value of aKss_init, defaults to 0.287585636847048
    ato_f_init : float, optional
        Value of ato_f_init, defaults to 0.0142335908879204
    ato_s_init : float, optional
        Value of ato_s_init, defaults to 0.0443263407760382
    aur_init : float, optional
        Value of aur_init, defaults to 0.00346258606821817
    b : float, optional
        Value of b, defaults to 0.4
    Beta_Na4 : float, optional
        Value of Beta_Na4
    Beta_Na5 : float, optional
        Value of Beta_Na5
    Bmax : float, optional
        Value of Bmax, defaults to 109.0
    C_K1_init : float, optional
        Value of C_K1_init, defaults to 0.0011733433957123
    C_K2_init : float, optional
        Value of C_K2_init, defaults to 0.00105586824723736
    C_Na1_init : float, optional
        Value of C_Na1_init, defaults to 0.000420472760277688
    C_Na2_init : float, optional
        Value of C_Na2_init, defaults to 0.0240114508843199
    Ca_i_init : float, optional
        Value of Ca_i_init, defaults to 0.0949915068139801
    CaJSR_init : float, optional
        Value of CaJSR_init, defaults to 171.167969039613
    CaMKt_init : float, optional
        Value of CaMKt_init, defaults to 0.729027738385079
    CaNSR_init : float, optional
        Value of CaNSR_init, defaults to 404.825013216286
    Cao : float, optional
        Value of Cao, defaults to 1400.0
    Cass_init : float, optional
        Value of Cass_init, defaults to 0.0954184301907784
    Cm : float, optional
        Value of Cm, defaults to 1.0
    const5 : float, optional
        Value of const5, defaults to 8.2
    CSQN_tot : float, optional
        Value of CSQN_tot, defaults to 50000.0
    delta_V_L : float, optional
        Value of delta_V_L, defaults to 6.4489
    E_Cl : float, optional
        Value of E_Cl, defaults to -40.0
    eta : float, optional
        Value of eta, defaults to 0.35
    F : float, optional
        Value of F, defaults to 96.5
    g_Cab : float, optional
        Value of g_Cab, defaults to 0.0007
    g_ClCa : float, optional
        Value of g_ClCa, defaults to 10.0
    g_K1 : float, optional
        Value of g_K1, defaults to 0.35
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.0165
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.00575
    g_Kss : float, optional
        Value of g_Kss, defaults to 0.0595
    g_Kto_f : float, optional
        Value of g_Kto_f, defaults to 0.5347
    g_Kto_s : float, optional
        Value of g_Kto_s, defaults to 0.0
    g_Kur : float, optional
        Value of g_Kur, defaults to 0.25
    g_Na : float, optional
        Value of g_Na, defaults to 16.0
    g_Nab : float, optional
        Value of g_Nab, defaults to 0.0026
    I1_Na_init : float, optional
        Value of I1_Na_init, defaults to 0.000517471697712382
    I2_Na_init : float, optional
        Value of I2_Na_init, defaults to 2.45406116958509e-05
    i_CaL_max : float, optional
        Value of i_CaL_max, defaults to 7.0
    I_init : float, optional
        Value of I_init, defaults to 0.427651445872853
    I_K_init : float, optional
        Value of I_K_init, defaults to 0.00140618453684944
    i_NaK_max : float, optional
        Value of i_NaK_max, defaults to 2.486
    i_pCa_max : float, optional
        Value of i_pCa_max, defaults to 0.0955
    IC_Na2_init : float, optional
        Value of IC_Na2_init, defaults to 0.0174528857380179
    IC_Na3_init : float, optional
        Value of IC_Na3_init, defaults to 0.402980726914811
    IF_Na_init : float, optional
        Value of IF_Na_init, defaults to 0.000306123648969581
    ito_f_init : float, optional
        Value of ito_f_init, defaults to 0.996989882138174
    ito_s_init : float, optional
        Value of ito_s_init, defaults to 0.887568880831388
    iur_init : float, optional
        Value of iur_init, defaults to 0.955684946168062
    K_L : float, optional
        Value of K_L, defaults to 0.3
    K_mAllo : float, optional
        Value of K_mAllo, defaults to 0.0
    K_mCai : float, optional
        Value of K_mCai, defaults to 3.6
    K_mCao : float, optional
        Value of K_mCao, defaults to 1400.0
    k_minus_a : float, optional
        Value of k_minus_a, defaults to 0.07125
    k_minus_b : float, optional
        Value of k_minus_b, defaults to 0.965
    k_minus_c : float, optional
        Value of k_minus_c, defaults to 0.0008
    K_mNai : float, optional
        Value of K_mNai, defaults to 12000.0
    K_mNao : float, optional
        Value of K_mNao, defaults to 88000.0
    k_plus_a : float, optional
        Value of k_plus_a, defaults to 0.006075
    k_plus_b : float, optional
        Value of k_plus_b, defaults to 0.00405
    k_plus_c : float, optional
        Value of k_plus_c, defaults to 0.009
    k_sat : float, optional
        Value of k_sat, defaults to 0.27
    kb : float, optional
        Value of kb, defaults to 0.036778
    Kd : float, optional
        Value of Kd, defaults to 0.6
    kf : float, optional
        Value of kf, defaults to 0.023761
    Ki_init : float, optional
        Value of Ki_init, defaults to 115599.50642567
    Km_Cl : float, optional
        Value of Km_Cl, defaults to 10.0
    Km_CSQN : float, optional
        Value of Km_CSQN, defaults to 630.0
    Km_Ko : float, optional
        Value of Km_Ko, defaults to 1500.0
    Km_Nai : float, optional
        Value of Km_Nai, defaults to 16600.0
    Km_pCa : float, optional
        Value of Km_pCa, defaults to 0.2885
    Km_up : float, optional
        Value of Km_up, defaults to 0.412
    Ko : float, optional
        Value of Ko, defaults to 5400.0
    m : float, optional
        Value of m, defaults to 3.0
    n : float, optional
        Value of n, defaults to 4.0
    Nai_init : float, optional
        Value of Nai_init, defaults to 12364.7482121793
    Nao : float, optional
        Value of Nao, defaults to 134000.0
    nKs_init : float, optional
        Value of nKs_init, defaults to 0.00336735013094628
    O_init : float, optional
        Value of O_init, defaults to 1.23713515513533e-06
    O_K_init : float, optional
        Value of O_K_init, defaults to 0.0131742086125972
    O_Na_init : float, optional
        Value of O_Na_init, defaults to 1.46826771436314e-06
    off_rate : float, optional
        Value of off_rate, defaults to 0.0002
    on_rate : float, optional
        Value of on_rate, defaults to 0.05
    P_C2_init : float, optional
        Value of P_C2_init, defaults to 0.565182571165673
    P_CaL : float, optional
        Value of P_CaL, defaults to 2.5
    P_O1_init : float, optional
        Value of P_O1_init, defaults to 0.00571393383393735
    P_O2_init : float, optional
        Value of P_O2_init, defaults to 2.09864618235341e-08
    P_ryr_const1 : float, optional
        Value of P_ryr_const1, defaults to -0.01
    P_ryr_const2 : float, optional
        Value of P_ryr_const2, defaults to -2.0
    P_RyR_init : float, optional
        Value of P_RyR_init, defaults to 0.000280539508743811
    phi_L : float, optional
        Value of phi_L, defaults to 1.798
    R : float, optional
        Value of R, defaults to 8.314
    T : float, optional
        Value of T, defaults to 308.0
    t_L : float, optional
        Value of t_L, defaults to 1.5
    tau_i_const : float, optional
        Value of tau_i_const, defaults to 643.0
    tau_L : float, optional
        Value of tau_L, defaults to 1150.0
    tau_tr : float, optional
        Value of tau_tr, defaults to 20.0
    tau_xfer : float, optional
        Value of tau_xfer, defaults to 8.0
    v1 : float, optional
        Value of v1, defaults to 4.5
    v1_caff : float, optional
        Value of v1_caff, defaults to 10.0
    v2 : float, optional
        Value of v2, defaults to 3e-05
    v2_caff : float, optional
        Value of v2_caff, defaults to 0.1
    V_init : float, optional
        Value of V_init, defaults to -78.9452115785979
    V_L : float, optional
        Value of V_L, defaults to 0.0
    V_max_NCX : float, optional
        Value of V_max_NCX, defaults to 3.939
    VJSR : float, optional
        Value of VJSR, defaults to 7.7e-08
    vmup_init : float, optional
        Value of vmup_init, defaults to 0.5059
    Vmyo : float, optional
        Value of Vmyo, defaults to 2.2e-05
    VNSR : float, optional
        Value of VNSR, defaults to 2.31e-07
    Vss : float, optional
        Value of Vss, defaults to 2.2e-08
    y_gate_init : float, optional
        Value of y_gate_init, defaults to 0.845044436980163
    y_gate_tau_const1 : float, optional
        Value of y_gate_tau_const1, defaults to 8.0
    y_gate_tau_const2 : float, optional
        Value of y_gate_tau_const2, defaults to 315.0
    """
    SHORTNAME = 'murinemouse'
    MODELID = 'MurineMouse'
    PARAMETERS = ['a', 'Acap', 'aKss_init', 'ato_f_init', 'ato_s_init', 
                  'aur_init', 'b', 'Beta_Na4', 'Beta_Na5', 'Bmax', 'C_K1_init', 
                  'C_K2_init', 'C_Na1_init', 'C_Na2_init', 'Ca_i_init', 
                  'CaJSR_init', 'CaMKt_init', 'CaNSR_init', 'Cao', 'Cass_init', 
                  'Cm', 'const5', 'CSQN_tot', 'delta_V_L', 'E_Cl', 'eta', 'F', 
                  'g_Cab', 'g_ClCa', 'g_K1', 'g_Kr', 'g_Ks', 'g_Kss', 'g_Kto_f', 
                  'g_Kto_s', 'g_Kur', 'g_Na', 'g_Nab', 'I1_Na_init', 
                  'I2_Na_init', 'i_CaL_max', 'I_init', 'I_K_init', 'i_NaK_max', 
                  'i_pCa_max', 'IC_Na2_init', 'IC_Na3_init', 'IF_Na_init', 
                  'ito_f_init', 'ito_s_init', 'iur_init', 'K_L', 'K_mAllo', 
                  'K_mCai', 'K_mCao', 'k_minus_a', 'k_minus_b', 'k_minus_c', 
                  'K_mNai', 'K_mNao', 'k_plus_a', 'k_plus_b', 'k_plus_c', 
                  'k_sat', 'kb', 'Kd', 'kf', 'Ki_init', 'Km_Cl', 'Km_CSQN', 
                  'Km_Ko', 'Km_Nai', 'Km_pCa', 'Km_up', 'Ko', 'm', 'n', 
                  'Nai_init', 'Nao', 'nKs_init', 'O_init', 'O_K_init', 
                  'O_Na_init', 'off_rate', 'on_rate', 'P_C2_init', 'P_CaL', 
                  'P_O1_init', 'P_O2_init', 'P_ryr_const1', 'P_ryr_const2', 
                  'P_RyR_init', 'phi_L', 'R', 'T', 't_L', 'tau_i_const', 
                  'tau_L', 'tau_tr', 'tau_xfer', 'v1', 'v1_caff', 'v2', 
                  'v2_caff', 'V_init', 'V_L', 'V_max_NCX', 'VJSR', 'vmup_init', 
                  'Vmyo', 'VNSR', 'Vss', 'y_gate_init', 'y_gate_tau_const1', 
                  'y_gate_tau_const2']


class NYGRENIonicModel(_AbstractIonicModel):
    """
    Describes the NYGREN ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    extern_Cab : float, optional
        Value of extern_Cab
    G_Ca : float, optional
        Value of G_Ca, defaults to 1.0
    G_Kr : float, optional
        Value of G_Kr, defaults to 1.0
    G_to : float, optional
        Value of G_to, defaults to 1.0
    """
    SHORTNAME = 'nygren'
    MODELID = 'NYGREN'
    PARAMETERS = ['extern_Cab', 'G_Ca', 'G_Kr', 'G_to']


class ORdIonicModel(_AbstractIonicModel):
    """
    Describes the ORd ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    blf_ICab : float, optional
        Value of blf_ICab, defaults to 1.0
    blf_ICaK : float, optional
        Value of blf_ICaK, defaults to 1.0
    blf_ICaL : float, optional
        Value of blf_ICaL, defaults to 1.0
    blf_ICaNa : float, optional
        Value of blf_ICaNa, defaults to 1.0
    blf_INab : float, optional
        Value of blf_INab, defaults to 1.0
    blf_INaCa_i : float, optional
        Value of blf_INaCa_i, defaults to 1.0
    blf_INaCa_ss : float, optional
        Value of blf_INaCa_ss, defaults to 1.0
    blf_INaK : float, optional
        Value of blf_INaK, defaults to 1.0
    cao : float, optional
        Value of cao, defaults to 1.8
    celltype : float, optional
        Value of celltype
    flags : float, optional
        Value of flags
    GK1 : float, optional
        Value of GK1
    GKb : float, optional
        Value of GKb
    GKr : float, optional
        Value of GKr
    GKs : float, optional
        Value of GKs
    GNa : float, optional
        Value of GNa
    GNaL : float, optional
        Value of GNaL
    Gncx : float, optional
        Value of Gncx
    GpCa : float, optional
        Value of GpCa, defaults to 0.0005
    Gto : float, optional
        Value of Gto
    INa_Type : float, optional
        Value of INa_Type
    jrel_stiff_const : float, optional
        Value of jrel_stiff_const, defaults to 0.005
    ko : float, optional
        Value of ko, defaults to 5.4
    nao : float, optional
        Value of nao, defaults to 140.0
    scale_tau_m : float, optional
        Value of scale_tau_m, defaults to 0.3
    shift_m_inf : float, optional
        Value of shift_m_inf, defaults to -8.0
    """
    SHORTNAME = 'ord'
    MODELID = 'ORd'
    PARAMETERS = ['blf_ICab', 'blf_ICaK', 'blf_ICaL', 'blf_ICaNa', 'blf_INab', 
                  'blf_INaCa_i', 'blf_INaCa_ss', 'blf_INaK', 'cao', 'celltype', 
                  'flags', 'GK1', 'GKb', 'GKr', 'GKs', 'GNa', 'GNaL', 'Gncx', 
                  'GpCa', 'Gto', 'INa_Type', 'jrel_stiff_const', 'ko', 'nao', 
                  'scale_tau_m', 'shift_m_inf']


class PanditIonicModel(_AbstractIonicModel):
    """
    Describes the Pandit ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    *flags : float, optional
        Value of *flags
    a : float, optional
        Value of a, defaults to 0.886
    b : float, optional
        Value of b
    Beta : float, optional
        Value of Beta
    C_m : float, optional
        Value of C_m
    cell_type : float, optional
        Value of cell_type, defaults to 0.0
    concFact : float, optional
        Value of concFact, defaults to 1.0
    EGTA : float, optional
        Value of EGTA, defaults to 0.0
    G_CaB : float, optional
        Value of G_CaB, defaults to 3.24e-05
    G_CaL : float, optional
        Value of G_CaL, defaults to 0.031
    G_Na : float, optional
        Value of G_Na, defaults to 0.8
    G_NaB : float, optional
        Value of G_NaB, defaults to 8.015e-05
    G_SS : float, optional
        Value of G_SS, defaults to 0.007
    G_to : float, optional
        Value of G_to, defaults to 0.035
    I_NaK_ss : float, optional
        Value of I_NaK_ss, defaults to 0.08
    K1fact : float, optional
        Value of K1fact, defaults to 1.0
    K_O : float, optional
        Value of K_O, defaults to 5.4
    K_sr : float, optional
        Value of K_sr
    Na_O : float, optional
        Value of Na_O, defaults to 140.0
    Nu_maxf : float, optional
        Value of Nu_maxf, defaults to 0.04
    srMethod : float, optional
        Value of srMethod
    VclampF : float, optional
        Value of VclampF, defaults to 0.0
    VclampV : float, optional
        Value of VclampV, defaults to 0.0
    """
    SHORTNAME = 'pandit'
    MODELID = 'Pandit'
    PARAMETERS = ['*flags', 'a', 'b', 'Beta', 'C_m', 'cell_type', 'concFact', 
                  'EGTA', 'G_CaB', 'G_CaL', 'G_Na', 'G_NaB', 'G_SS', 'G_to', 
                  'I_NaK_ss', 'K1fact', 'K_O', 'K_sr', 'Na_O', 'Nu_maxf', 
                  'srMethod', 'VclampF', 'VclampV']


class PASSIVEIonicModel(_AbstractIonicModel):
    """
    Describes the PASSIVE ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Gm : float, optional
        Value of Gm
    Rm : float, optional
        Value of Rm, defaults to 10.0
    Vrest : float, optional
        Value of Vrest, defaults to 0.0
    """
    SHORTNAME = 'passive'
    MODELID = 'PASSIVE'
    PARAMETERS = ['Gm', 'Rm', 'Vrest']


class PUGIonicModel(_AbstractIonicModel):
    """
    Describes the PUG ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    *capulse_cicr : float, optional
        Value of *capulse_cicr
    *capulse_jsrol : float, optional
        Value of *capulse_jsrol
    *flags : float, optional
        Value of *flags
    Cai_init : float, optional
        Value of Cai_init
    CaNSRbar : float, optional
        Value of CaNSRbar
    Cao : float, optional
        Value of Cao
    cell_type : float, optional
        Value of cell_type, defaults to 0.0
    cicr_pulsecyc : float, optional
        Value of cicr_pulsecyc
    Cm : float, optional
        Value of Cm
    dvdt_thresh : float, optional
        Value of dvdt_thresh, defaults to 20.0
    ECaT : float, optional
        Value of ECaT
    extern_Cab : float, optional
        Value of extern_Cab
    ff_GCaL : float, optional
        Value of ff_GCaL
    ff_GKs : float, optional
        Value of ff_GKs
    ff_INaCa : float, optional
        Value of ff_INaCa
    ff_tauXs : float, optional
        Value of ff_tauXs
    GCab : float, optional
        Value of GCab
    GCaT : float, optional
        Value of GCaT, defaults to 0.05
    GClCa : float, optional
        Value of GClCa
    GK1 : float, optional
        Value of GK1, defaults to 0.54
    GKp : float, optional
        Value of GKp, defaults to 0.008
    GKr : float, optional
        Value of GKr, defaults to 0.035
    GNa : float, optional
        Value of GNa, defaults to 8.0
    GNab : float, optional
        Value of GNab
    Gto : float, optional
        Value of Gto, defaults to 0.06
    Iupbar : float, optional
        Value of Iupbar
    jsrol_pulsecyc : float, optional
        Value of jsrol_pulsecyc
    Ki_const : float, optional
        Value of Ki_const
    Ki_init : float, optional
        Value of Ki_init
    Kmup : float, optional
        Value of Kmup
    Ko : float, optional
        Value of Ko
    Nai_const : float, optional
        Value of Nai_const
    Nai_init : float, optional
        Value of Nai_init
    ratio[0] : float, optional
        Value of ratio[0], defaults to 1.0
    ratio[1] : float, optional
        Value of ratio[1], defaults to 1.0
    ratio[2] : float, optional
        Value of ratio[2], defaults to 1.0
    ratio[3] : float, optional
        Value of ratio[3]
    T : float, optional
        Value of T
    tau_tr : float, optional
        Value of tau_tr
    validate : float, optional
        Value of validate
    Vc : float, optional
        Value of Vc
    Vm_init : float, optional
        Value of Vm_init
    """
    SHORTNAME = 'pug'
    MODELID = 'PUG'
    PARAMETERS = ['*capulse_cicr', '*capulse_jsrol', '*flags', 'Cai_init', 
                  'CaNSRbar', 'Cao', 'cell_type', 'cicr_pulsecyc', 'Cm', 
                  'dvdt_thresh', 'ECaT', 'extern_Cab', 'ff_GCaL', 'ff_GKs', 
                  'ff_INaCa', 'ff_tauXs', 'GCab', 'GCaT', 'GClCa', 'GK1', 'GKp', 
                  'GKr', 'GNa', 'GNab', 'Gto', 'Iupbar', 'jsrol_pulsecyc', 
                  'Ki_const', 'Ki_init', 'Kmup', 'Ko', 'Nai_const', 'Nai_init', 
                  'ratio[0]', 'ratio[1]', 'ratio[2]', 'ratio[3]', 'T', 'tau_tr', 
                  'validate', 'Vc', 'Vm_init']


class Rat_neonIonicModel(_AbstractIonicModel):
    """
    Describes the Rat_neon ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    AcapCm2F : float, optional
        Value of AcapCm2F
    AcapCmFVmyo : float, optional
        Value of AcapCmFVmyo
    CMDNtot : float, optional
        Value of CMDNtot, defaults to 50.0
    CSQNtot : float, optional
        Value of CSQNtot, defaults to 24750.0
    DCa : float, optional
        Value of DCa, defaults to 7.0
    dx : float, optional
        Value of dx, defaults to 0.1
    GCaL : float, optional
        Value of GCaL
    GCaT : float, optional
        Value of GCaT, defaults to 0.2
    Gf : float, optional
        Value of Gf, defaults to 0.021
    GK1 : float, optional
        Value of GK1, defaults to 0.0515
    GKr : float, optional
        Value of GKr, defaults to 0.06
    GKs : float, optional
        Value of GKs, defaults to 0.05
    GNa : float, optional
        Value of GNa, defaults to 35.0
    Gto : float, optional
        Value of Gto, defaults to 0.1
    k_leak : float, optional
        Value of k_leak, defaults to 5e-06
    KmCMDN : float, optional
        Value of KmCMDN, defaults to 2.38
    KmCSQN : float, optional
        Value of KmCSQN, defaults to 800.0
    Kmf : float, optional
        Value of Kmf, defaults to 0.5
    Kmr : float, optional
        Value of Kmr
    KmTRPN : float, optional
        Value of KmTRPN, defaults to 0.5
    kNCX : float, optional
        Value of kNCX, defaults to 2.268e-16
    kRyR : float, optional
        Value of kRyR, defaults to 0.01
    rSL : float, optional
        Value of rSL, defaults to 10.5
    rSR : float, optional
        Value of rSR, defaults to 6.0
    TRPNtot : float, optional
        Value of TRPNtot, defaults to 35.0
    V_sub_SL : float, optional
        Value of V_sub_SL
    V_sub_SR : float, optional
        Value of V_sub_SR
    Vmax : float, optional
        Value of Vmax, defaults to 0.9996
    """
    SHORTNAME = 'rat_neon'
    MODELID = 'Rat_neon'
    PARAMETERS = ['AcapCm2F', 'AcapCmFVmyo', 'CMDNtot', 'CSQNtot', 'DCa', 'dx', 
                  'GCaL', 'GCaT', 'Gf', 'GK1', 'GKr', 'GKs', 'GNa', 'Gto', 
                  'k_leak', 'KmCMDN', 'KmCSQN', 'Kmf', 'Kmr', 'KmTRPN', 'kNCX', 
                  'kRyR', 'rSL', 'rSR', 'TRPNtot', 'V_sub_SL', 'V_sub_SR', 
                  'Vmax']


class RNCIonicModel(_AbstractIonicModel):
    """
    Describes the RNC ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    extern_Cab : float, optional
        Value of extern_Cab
    g_CaL : float, optional
        Value of g_CaL, defaults to 0.24
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.06984
    g_Na : float, optional
        Value of g_Na, defaults to 7.8
    g_to : float, optional
        Value of g_to, defaults to 0.19824
    maxCa_up : float, optional
        Value of maxCa_up, defaults to 15.0
    tau_tr : float, optional
        Value of tau_tr, defaults to 180.0
    tau_u : float, optional
        Value of tau_u, defaults to 8.0
    """
    SHORTNAME = 'rnc'
    MODELID = 'RNC'
    PARAMETERS = ['extern_Cab', 'g_CaL', 'g_Kr', 'g_Na', 'g_to', 'maxCa_up', 
                  'tau_tr', 'tau_u']


class SCIonicModel(_AbstractIonicModel):
    """
    Describes the SC ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    k : float, optional
        Value of k, defaults to 0.0
    wKr : float, optional
        Value of wKr, defaults to 0.0
    wKs : float, optional
        Value of wKs, defaults to 0.0
    """
    SHORTNAME = 'sc'
    MODELID = 'SC'
    PARAMETERS = ['k', 'wKr', 'wKs']


class TT2IonicModel(_AbstractIonicModel):
    """
    Describes the TT2 ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    big_cnt : float, optional
        Value of big_cnt, defaults to 0.0
    Bufc : float, optional
        Value of Bufc, defaults to 0.2
    Bufsr : float, optional
        Value of Bufsr, defaults to 10.0
    Cao : float, optional
        Value of Cao, defaults to 2.0
    CAPACITANCE : float, optional
        Value of CAPACITANCE, defaults to 0.185
    cell_type : float, optional
        Value of cell_type, defaults to 0.0
    F : float, optional
        Value of F, defaults to 96485.3415
    flags : float, optional
        Value of flags
    fmode : float, optional
        Value of fmode, defaults to 0.0
    GbCa : float, optional
        Value of GbCa, defaults to 0.000592
    GbNa : float, optional
        Value of GbNa, defaults to 0.00029
    GCaL : float, optional
        Value of GCaL, defaults to 3.98e-05
    GK1 : float, optional
        Value of GK1, defaults to 5.405
    Gkr : float, optional
        Value of Gkr, defaults to 0.153
    Gks : float, optional
        Value of Gks, defaults to 0.392
    GNa : float, optional
        Value of GNa, defaults to 14.838
    GpCa : float, optional
        Value of GpCa, defaults to 0.1238
    GpK : float, optional
        Value of GpK, defaults to 0.0146
    Gto : float, optional
        Value of Gto, defaults to 0.073
    IpK : float, optional
        Value of IpK
    Kbufc : float, optional
        Value of Kbufc, defaults to 0.001
    Kbufsr : float, optional
        Value of Kbufsr, defaults to 0.3
    KmCa : float, optional
        Value of KmCa, defaults to 1.38
    KmK : float, optional
        Value of KmK, defaults to 1.0
    KmNa : float, optional
        Value of KmNa, defaults to 40.0
    KmNai : float, optional
        Value of KmNai, defaults to 87.5
    knaca : float, optional
        Value of knaca, defaults to 1000.0
    knak : float, optional
        Value of knak, defaults to 2.724
    Ko : float, optional
        Value of Ko, defaults to 5.4
    KpCa : float, optional
        Value of KpCa, defaults to 0.0005
    ksat : float, optional
        Value of ksat, defaults to 0.1
    Kup : float, optional
        Value of Kup, defaults to 0.00025
    n : float, optional
        Value of n, defaults to 0.35
    Nao : float, optional
        Value of Nao, defaults to 140.0
    pKNa : float, optional
        Value of pKNa, defaults to 0.03
    R : float, optional
        Value of R, defaults to 8314.472
    RTONF : float, optional
        Value of RTONF
    scl_tau_f : float, optional
        Value of scl_tau_f, defaults to 1.0
    T : float, optional
        Value of T, defaults to 310.0
    Vc : float, optional
        Value of Vc, defaults to 0.016404
    Vmaxup : float, optional
        Value of Vmaxup, defaults to 0.006375
    Vsr : float, optional
        Value of Vsr, defaults to 0.001094
    """
    SHORTNAME = 'tt2'
    MODELID = 'TT2'
    PARAMETERS = ['big_cnt', 'Bufc', 'Bufsr', 'Cao', 'CAPACITANCE', 'cell_type', 
                  'F', 'flags', 'fmode', 'GbCa', 'GbNa', 'GCaL', 'GK1', 'Gkr', 
                  'Gks', 'GNa', 'GpCa', 'GpK', 'Gto', 'IpK', 'Kbufc', 'Kbufsr', 
                  'KmCa', 'KmK', 'KmNa', 'KmNai', 'knaca', 'knak', 'Ko', 'KpCa', 
                  'ksat', 'Kup', 'n', 'Nao', 'pKNa', 'R', 'RTONF', 'scl_tau_f', 
                  'T', 'Vc', 'Vmaxup', 'Vsr']


class TTIonicModel(_AbstractIonicModel):
    """
    Describes the TT ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Bufc : float, optional
        Value of Bufc, defaults to 0.15
    Bufsr : float, optional
        Value of Bufsr, defaults to 10.0
    Cao : float, optional
        Value of Cao, defaults to 2.0
    CAPACITANCE : float, optional
        Value of CAPACITANCE, defaults to 0.185
    cell_type : float, optional
        Value of cell_type, defaults to 0.0
    F : float, optional
        Value of F, defaults to 96485.3415
    flags : float, optional
        Value of flags
    fmode : float, optional
        Value of fmode
    GbCa : float, optional
        Value of GbCa, defaults to 0.000592
    GbNa : float, optional
        Value of GbNa, defaults to 0.00029
    GCaL : float, optional
        Value of GCaL, defaults to 0.000175
    GK1 : float, optional
        Value of GK1, defaults to 5.405
    Gkr : float, optional
        Value of Gkr, defaults to 0.096
    Gks : float, optional
        Value of Gks, defaults to 0.245
    GNa : float, optional
        Value of GNa, defaults to 14.838
    GpCa : float, optional
        Value of GpCa, defaults to 0.825
    GpK : float, optional
        Value of GpK, defaults to 0.0146
    Gto : float, optional
        Value of Gto, defaults to 0.073
    IpK : float, optional
        Value of IpK
    Kbufc : float, optional
        Value of Kbufc, defaults to 0.001
    Kbufsr : float, optional
        Value of Kbufsr, defaults to 0.3
    KmCa : float, optional
        Value of KmCa, defaults to 1.38
    KmK : float, optional
        Value of KmK, defaults to 1.0
    KmNa : float, optional
        Value of KmNa, defaults to 40.0
    KmNai : float, optional
        Value of KmNai, defaults to 87.5
    knaca : float, optional
        Value of knaca, defaults to 1000.0
    knak : float, optional
        Value of knak, defaults to 1.362
    Ko : float, optional
        Value of Ko, defaults to 5.4
    KpCa : float, optional
        Value of KpCa, defaults to 0.0005
    ksat : float, optional
        Value of ksat, defaults to 0.1
    Kup : float, optional
        Value of Kup, defaults to 0.00025
    n : float, optional
        Value of n, defaults to 0.35
    Nao : float, optional
        Value of Nao, defaults to 140.0
    pKNa : float, optional
        Value of pKNa, defaults to 0.03
    R : float, optional
        Value of R, defaults to 8314.472
    RTONF : float, optional
        Value of RTONF
    T : float, optional
        Value of T, defaults to 310.0
    taufca : float, optional
        Value of taufca, defaults to 2.0
    taug : float, optional
        Value of taug, defaults to 2.0
    Vc : float, optional
        Value of Vc, defaults to 0.016404
    Vmaxup : float, optional
        Value of Vmaxup, defaults to 0.000425
    Vsr : float, optional
        Value of Vsr, defaults to 0.001094
    """
    SHORTNAME = 'tt'
    MODELID = 'TT'
    PARAMETERS = ['Bufc', 'Bufsr', 'Cao', 'CAPACITANCE', 'cell_type', 'F', 
                  'flags', 'fmode', 'GbCa', 'GbNa', 'GCaL', 'GK1', 'Gkr', 'Gks', 
                  'GNa', 'GpCa', 'GpK', 'Gto', 'IpK', 'Kbufc', 'Kbufsr', 'KmCa', 
                  'KmK', 'KmNa', 'KmNai', 'knaca', 'knak', 'Ko', 'KpCa', 'ksat', 
                  'Kup', 'n', 'Nao', 'pKNa', 'R', 'RTONF', 'T', 'taufca', 
                  'taug', 'Vc', 'Vmaxup', 'Vsr']


class TTREDIonicModel(_AbstractIonicModel):
    """
    Describes the TTRED ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Bufc : float, optional
        Value of Bufc
    Bufsr : float, optional
        Value of Bufsr
    Cao : float, optional
        Value of Cao, defaults to 2.0
    CAPACITANCE : float, optional
        Value of CAPACITANCE, defaults to 0.185
    cell_type : float, optional
        Value of cell_type
    fmode : float, optional
        Value of fmode
    GbCa : float, optional
        Value of GbCa, defaults to 0.000592
    GbNa : float, optional
        Value of GbNa, defaults to 0.00029
    GCaL : float, optional
        Value of GCaL, defaults to 0.2786
    GK1 : float, optional
        Value of GK1, defaults to 5.405
    Gkr : float, optional
        Value of Gkr, defaults to 0.101
    Gks : float, optional
        Value of Gks, defaults to 0.392
    GNa : float, optional
        Value of GNa, defaults to 14.838
    GpCa : float, optional
        Value of GpCa, defaults to 0.1238
    GpK : float, optional
        Value of GpK, defaults to 0.0293
    Gto : float, optional
        Value of Gto, defaults to 0.073
    IpK : float, optional
        Value of IpK
    Kbufc : float, optional
        Value of Kbufc
    Kbufsr : float, optional
        Value of Kbufsr
    KmCa : float, optional
        Value of KmCa, defaults to 1.38
    KmK : float, optional
        Value of KmK, defaults to 1.0
    KmNa : float, optional
        Value of KmNa, defaults to 40.0
    KmNai : float, optional
        Value of KmNai, defaults to 87.5
    knaca : float, optional
        Value of knaca, defaults to 1000.0
    knak : float, optional
        Value of knak, defaults to 2.724
    Ko : float, optional
        Value of Ko, defaults to 5.4
    KpCa : float, optional
        Value of KpCa, defaults to 0.0005
    ksat : float, optional
        Value of ksat, defaults to 0.1
    Kup : float, optional
        Value of Kup
    n : float, optional
        Value of n, defaults to 0.35
    Nao : float, optional
        Value of Nao, defaults to 140.0
    pKNa : float, optional
        Value of pKNa, defaults to 0.03
    RTONF : float, optional
        Value of RTONF
    T : float, optional
        Value of T, defaults to 310.0
    Vc : float, optional
        Value of Vc, defaults to 0.016404
    Vmaxup : float, optional
        Value of Vmaxup
    Vsr : float, optional
        Value of Vsr, defaults to 0.001094
    """
    SHORTNAME = 'ttred'
    MODELID = 'TTRED'
    PARAMETERS = ['Bufc', 'Bufsr', 'Cao', 'CAPACITANCE', 'cell_type', 'fmode', 
                  'GbCa', 'GbNa', 'GCaL', 'GK1', 'Gkr', 'Gks', 'GNa', 'GpCa', 
                  'GpK', 'Gto', 'IpK', 'Kbufc', 'Kbufsr', 'KmCa', 'KmK', 'KmNa', 
                  'KmNai', 'knaca', 'knak', 'Ko', 'KpCa', 'ksat', 'Kup', 'n', 
                  'Nao', 'pKNa', 'RTONF', 'T', 'Vc', 'Vmaxup', 'Vsr']


class UCLA_RABIonicModel(_AbstractIonicModel):
    """
    Describes the UCLA_RAB ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    blf_ICaL : float, optional
        Value of blf_ICaL, defaults to 1.0
    blf_IKr : float, optional
        Value of blf_IKr, defaults to 1.0
    blf_IKs : float, optional
        Value of blf_IKs, defaults to 1.0
    blf_INa : float, optional
        Value of blf_INa, defaults to 1.0
    blf_INaCa : float, optional
        Value of blf_INaCa, defaults to 1.0
    blf_recov : float, optional
        Value of blf_recov, defaults to 1.0
    blf_tau_h : float, optional
        Value of blf_tau_h, defaults to 1.0
    Cao : float, optional
        Value of Cao, defaults to 1.8
    d_g : float, optional
        Value of d_g, defaults to 2.58079
    d_taur : float, optional
        Value of d_taur, defaults to 30.0
    gCa : float, optional
        Value of gCa, defaults to 182.0
    gK1 : float, optional
        Value of gK1, defaults to 0.3
    gKr : float, optional
        Value of gKr, defaults to 0.0125
    gKs : float, optional
        Value of gKs, defaults to 0.32
    gNa : float, optional
        Value of gNa, defaults to 12.0
    gNaK : float, optional
        Value of gNaK, defaults to 1.5
    gss : float, optional
        Value of gss
    gtof : float, optional
        Value of gtof, defaults to 0.11
    gtos : float, optional
        Value of gtos, defaults to 0.04
    K_i : float, optional
        Value of K_i, defaults to 140.0
    Ko : float, optional
        Value of Ko, defaults to 5.4
    Na_i : float, optional
        Value of Na_i, defaults to 14.01807252
    Nao : float, optional
        Value of Nao, defaults to 136.0
    T : float, optional
        Value of T, defaults to 308.0
    Vup : float, optional
        Value of Vup, defaults to 0.4
    """
    SHORTNAME = 'ucla_rab'
    MODELID = 'UCLA_RAB'
    PARAMETERS = ['blf_ICaL', 'blf_IKr', 'blf_IKs', 'blf_INa', 'blf_INaCa', 
                  'blf_recov', 'blf_tau_h', 'Cao', 'd_g', 'd_taur', 'gCa', 
                  'gK1', 'gKr', 'gKs', 'gNa', 'gNaK', 'gss', 'gtof', 'gtos', 
                  'K_i', 'Ko', 'Na_i', 'Nao', 'T', 'Vup']


class WANG_NNRIonicModel(_AbstractIonicModel):
    """
    Describes the WANG_NNR ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    Ca_i_init : float, optional
        Value of Ca_i_init, defaults to 0.2049
    i_CaL_slowdown : float, optional
        Value of i_CaL_slowdown, defaults to 1.0
    Ki_init : float, optional
        Value of Ki_init, defaults to 136450.0
    Nai_init : float, optional
        Value of Nai_init, defaults to 21747.0
    """
    SHORTNAME = 'wang_nnr'
    MODELID = 'WANG_NNR'
    PARAMETERS = ['Ca_i_init', 'i_CaL_slowdown', 'Ki_init', 'Nai_init']


class SHANNONIonicModel(_AbstractIonicModel):
    """
    Describes the SHANNON ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    """
    SHORTNAME = 'shannon'
    MODELID = 'SHANNON'
    PARAMETERS = []


class koivumakiIonicModel(_AbstractIonicModel):
    """
    Describes the koivumaki ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    base_phos : float, optional
        Value of base_phos, defaults to 0.1
    cAF_cpumps : float, optional
        Value of cAF_cpumps, defaults to 1.0
    cAF_lcell : float, optional
        Value of cAF_lcell, defaults to 1.0
    cAF_RyR : float, optional
        Value of cAF_RyR, defaults to 1.0
    g_B_Ca : float, optional
        Value of g_B_Ca, defaults to 8.4e-05
    g_B_Na : float, optional
        Value of g_B_Na, defaults to 6.0599e-05
    g_Ca_L : float, optional
        Value of g_Ca_L, defaults to 0.007
    g_K1 : float, optional
        Value of g_K1, defaults to 0.0029
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.0034
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.000175
    g_sus : float, optional
        Value of g_sus, defaults to 0.00225
    g_t : float, optional
        Value of g_t, defaults to 0.0011
    gNa : float, optional
        Value of gNa, defaults to 0.558
    gNaL : float, optional
        Value of gNaL, defaults to 0.000423
    k_NaCa : float, optional
        Value of k_NaCa, defaults to 0.008433945
    PLB_SERCA_ratio : float, optional
        Value of PLB_SERCA_ratio, defaults to 1.0
    SLN_SERCA_ratio : float, optional
        Value of SLN_SERCA_ratio, defaults to 1.0
    """
    SHORTNAME = 'koivumaki'
    MODELID = 'koivumaki'
    PARAMETERS = ['base_phos', 'cAF_cpumps', 'cAF_lcell', 'cAF_RyR', 'g_B_Ca', 
                  'g_B_Na', 'g_Ca_L', 'g_K1', 'g_Kr', 'g_Ks', 'g_sus', 'g_t', 
                  'gNa', 'gNaL', 'k_NaCa', 'PLB_SERCA_ratio', 'SLN_SERCA_ratio']


class koivumaki_newINaIonicModel(_AbstractIonicModel):
    """
    Describes the koivumaki_newINa ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    ACh : float, optional
        Value of ACh, defaults to 0.0
    base_phos : float, optional
        Value of base_phos, defaults to 0.1
    cAF_cpumps : float, optional
        Value of cAF_cpumps, defaults to 1.0
    cAF_lcell : float, optional
        Value of cAF_lcell, defaults to 1.0
    cAF_RyR : float, optional
        Value of cAF_RyR, defaults to 1.0
    EC50_ACh : float, optional
        Value of EC50_ACh, defaults to 0.000125
    g_B_Ca : float, optional
        Value of g_B_Ca
    g_B_Na : float, optional
        Value of g_B_Na
    g_Ca_L : float, optional
        Value of g_Ca_L
    g_K1 : float, optional
        Value of g_K1
    g_KACh : float, optional
        Value of g_KACh
    g_KCa : float, optional
        Value of g_KCa
    g_Kr : float, optional
        Value of g_Kr
    g_Ks : float, optional
        Value of g_Ks
    g_sus : float, optional
        Value of g_sus, defaults to 0.00225
    g_t : float, optional
        Value of g_t
    gNa : float, optional
        Value of gNa
    gNaL : float, optional
        Value of gNaL
    hill_ACh : float, optional
        Value of hill_ACh, defaults to 1.0
    k_NaCa : float, optional
        Value of k_NaCa
    KCa_off : float, optional
        Value of KCa_off
    KCa_on : float, optional
        Value of KCa_on
    PLB_SERCA_ratio : float, optional
        Value of PLB_SERCA_ratio, defaults to 1.0
    SLN_SERCA_ratio : float, optional
        Value of SLN_SERCA_ratio, defaults to 1.0
    """
    SHORTNAME = 'koivumaki_newina'
    MODELID = 'koivumaki_newINa'
    PARAMETERS = ['ACh', 'base_phos', 'cAF_cpumps', 'cAF_lcell', 'cAF_RyR', 
                  'EC50_ACh', 'g_B_Ca', 'g_B_Na', 'g_Ca_L', 'g_K1', 'g_KACh', 
                  'g_KCa', 'g_Kr', 'g_Ks', 'g_sus', 'g_t', 'gNa', 'gNaL', 
                  'hill_ACh', 'k_NaCa', 'KCa_off', 'KCa_on', 'PLB_SERCA_ratio', 
                  'SLN_SERCA_ratio']


class GTT2_fastIonicModel(_AbstractIonicModel):
    """
    Describes the GTT2_fast ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    ENDO : float, optional
        Value of ENDO, defaults to 0.0
    G_bCa : float, optional
        Value of G_bCa, defaults to 0.000592
    G_bNa : float, optional
        Value of G_bNa, defaults to 0.00029
    G_CaL : float, optional
        Value of G_CaL, defaults to 3.98e-05
    G_K1 : float, optional
        Value of G_K1, defaults to 5.405
    G_Kr : float, optional
        Value of G_Kr, defaults to 0.153
    G_Ks : float, optional
        Value of G_Ks, defaults to 0.392
    G_leak : float, optional
        Value of G_leak, defaults to 0.00036
    G_Na : float, optional
        Value of G_Na, defaults to 14.838
    G_NaCa : float, optional
        Value of G_NaCa, defaults to 1000.0
    G_NaK : float, optional
        Value of G_NaK, defaults to 2.724
    G_NaL : float, optional
        Value of G_NaL, defaults to 0.0075
    G_pCa : float, optional
        Value of G_pCa, defaults to 0.1238
    G_pK : float, optional
        Value of G_pK, defaults to 0.0146
    G_rel : float, optional
        Value of G_rel, defaults to 0.102
    G_tf : float, optional
        Value of G_tf, defaults to 1.0
    G_to : float, optional
        Value of G_to, defaults to 0.294
    G_up : float, optional
        Value of G_up, defaults to 0.006375
    G_vxfer : float, optional
        Value of G_vxfer, defaults to 0.0038
    """
    SHORTNAME = 'gtt2_fast'
    MODELID = 'GTT2_fast'
    PARAMETERS = ['ENDO', 'G_bCa', 'G_bNa', 'G_CaL', 'G_K1', 'G_Kr', 'G_Ks', 
                  'G_leak', 'G_Na', 'G_NaCa', 'G_NaK', 'G_NaL', 'G_pCa', 'G_pK', 
                  'G_rel', 'G_tf', 'G_to', 'G_up', 'G_vxfer']


class GTT2_fast_PURKIonicModel(_AbstractIonicModel):
    """
    Describes the GTT2_fast_PURK ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    D_CaL_off : float, optional
        Value of D_CaL_off, defaults to 0.0
    ENDO : float, optional
        Value of ENDO, defaults to 0.0
    G_bCa : float, optional
        Value of G_bCa, defaults to 0.000592
    G_bNa : float, optional
        Value of G_bNa, defaults to 0.00029
    G_CaL : float, optional
        Value of G_CaL, defaults to 3.98e-05
    G_K1 : float, optional
        Value of G_K1, defaults to 5.405
    G_Kr : float, optional
        Value of G_Kr, defaults to 0.153
    G_Ks : float, optional
        Value of G_Ks, defaults to 0.392
    G_leak : float, optional
        Value of G_leak, defaults to 0.00036
    G_Na : float, optional
        Value of G_Na, defaults to 14.838
    G_NaCa : float, optional
        Value of G_NaCa, defaults to 1000.0
    G_NaK : float, optional
        Value of G_NaK, defaults to 2.724
    G_NaL : float, optional
        Value of G_NaL, defaults to 0.0075
    G_pCa : float, optional
        Value of G_pCa, defaults to 0.1238
    G_pK : float, optional
        Value of G_pK, defaults to 0.0146
    G_rel : float, optional
        Value of G_rel, defaults to 0.102
    G_tf : float, optional
        Value of G_tf, defaults to 1.0
    G_to : float, optional
        Value of G_to, defaults to 0.294
    G_up : float, optional
        Value of G_up, defaults to 0.006375
    G_vxfer : float, optional
        Value of G_vxfer, defaults to 0.0038
    xr2_off : float, optional
        Value of xr2_off, defaults to 0.0
    """
    SHORTNAME = 'gtt2_fast_purk'
    MODELID = 'GTT2_fast_PURK'
    PARAMETERS = ['D_CaL_off', 'ENDO', 'G_bCa', 'G_bNa', 'G_CaL', 'G_K1', 
                  'G_Kr', 'G_Ks', 'G_leak', 'G_Na', 'G_NaCa', 'G_NaK', 'G_NaL', 
                  'G_pCa', 'G_pK', 'G_rel', 'G_tf', 'G_to', 'G_up', 'G_vxfer', 
                  'xr2_off']


class JB_COURTEMANCHEIonicModel(_AbstractIonicModel):
    """
    Describes the JB_COURTEMANCHE ionic model

    Parameters
    ----------
    IDs : list of int
        The element IDs to be included in this material region
    name : str
        The name of the model material region
    ACh : float, optional
        Value of ACh, defaults to 1e-06
    factor_dical : float, optional
        Value of factor_dical, defaults to 0.0
    factor_fical : float, optional
        Value of factor_fical, defaults to 0.0
    factor_ical_ft : float, optional
        Value of factor_ical_ft, defaults to 1.0
    factor_ina_c : float, optional
        Value of factor_ina_c, defaults to 0.0
    factor_ina_m : float, optional
        Value of factor_ina_m, defaults to 0.0
    factor_irel : float, optional
        Value of factor_irel, defaults to 1.0
    factor_itr : float, optional
        Value of factor_itr, defaults to 1.0
    factor_iup : float, optional
        Value of factor_iup, defaults to 1.0
    factor_kur : float, optional
        Value of factor_kur, defaults to 1.0
    factor_oa : float, optional
        Value of factor_oa, defaults to 0.0
    factor_tikr : float, optional
        Value of factor_tikr, defaults to 1.0
    g_bCa : float, optional
        Value of g_bCa, defaults to 0.00113
    g_bNa : float, optional
        Value of g_bNa, defaults to 0.000674
    g_CaL : float, optional
        Value of g_CaL, defaults to 0.1238
    g_K1 : float, optional
        Value of g_K1, defaults to 0.09
    g_Kr : float, optional
        Value of g_Kr, defaults to 0.0294
    g_Ks : float, optional
        Value of g_Ks, defaults to 0.129
    g_Na : float, optional
        Value of g_Na, defaults to 7.8
    g_to : float, optional
        Value of g_to, defaults to 0.1652
    K_o : float, optional
        Value of K_o, defaults to 5.4
    maxI_NaCa : float, optional
        Value of maxI_NaCa, defaults to 1600.0
    maxI_NaK : float, optional
        Value of maxI_NaK, defaults to 0.6
    maxI_pCa : float, optional
        Value of maxI_pCa, defaults to 0.275
    maxI_up : float, optional
        Value of maxI_up, defaults to 0.005
    P_ACh : float, optional
        Value of P_ACh, defaults to 1.0
    """
    SHORTNAME = 'jb_courtemanche'
    MODELID = 'JB_COURTEMANCHE'
    PARAMETERS = ['ACh', 'factor_dical', 'factor_fical', 'factor_ical_ft', 
                  'factor_ina_c', 'factor_ina_m', 'factor_irel', 'factor_itr', 
                  'factor_iup', 'factor_kur', 'factor_oa', 'factor_tikr', 
                  'g_bCa', 'g_bNa', 'g_CaL', 'g_K1', 'g_Kr', 'g_Ks', 'g_Na', 
                  'g_to', 'K_o', 'maxI_NaCa', 'maxI_NaK', 'maxI_pCa', 'maxI_up', 
                  'P_ACh']


MAPPING = _OrderedDict()
MAPPING['passive']                = PASSIVEIonicModel
MAPPING['alievpanfilov']          = AlievPanfilovIonicModel
MAPPING['arpf']                   = ARPFIonicModel
MAPPING['steward']                = StewardIonicModel
MAPPING['prd2']                   = PRd2IonicModel
MAPPING['converted_courtemanche'] = converted_COURTEMANCHEIonicModel
MAPPING['converted_difrannoble']  = converted_DiFranNobleIonicModel
MAPPING['converted_lrdii_f']      = converted_LRDII_FIonicModel
MAPPING['converted_mbrdr']        = converted_MBRDRIonicModel
MAPPING['converted_nygren']       = converted_NYGRENIonicModel
MAPPING['converted_rnc']          = converted_RNCIonicModel
MAPPING['converted_tt2']          = converted_TT2IonicModel
MAPPING['converted_ucla_rab']     = converted_UCLA_RABIonicModel
MAPPING['ucla_rab_scr']           = UCLA_RAB_SCRIonicModel
MAPPING['courtemanche']           = COURTEMANCHEIonicModel
MAPPING['difrannoble']            = DiFranNobleIonicModel
MAPPING['ecme']                   = ECMEIonicModel
MAPPING['fhn_rmcc']               = FHN_RMcCIonicModel
MAPPING['fox']                    = FOXIonicModel
MAPPING['gpb']                    = GPBIonicModel
MAPPING['gpvatria']               = GPVatriaIonicModel
MAPPING['gpb_land']               = GPB_LandIonicModel
MAPPING['gw_can']                 = GW_CANIonicModel
MAPPING['hamr']                   = hAMrIonicModel
MAPPING['hh']                     = HHIonicModel
MAPPING['iion_src']               = IION_SRCIonicModel
MAPPING['inada_avn']              = INADA_AVNIonicModel
MAPPING['iribe']                  = IribeIonicModel
MAPPING['kurata']                 = KurataIonicModel
MAPPING['lmcg']                   = LMCGIonicModel
MAPPING['lr1']                    = LR1IonicModel
MAPPING['lrdii']                  = LRDIIIonicModel
MAPPING['lrdii_f']                = LRDII_FIonicModel
MAPPING['maccannell_fb']          = MacCannell_FbIonicModel
MAPPING['mitchellschaeffer']      = MitchellSchaefferIonicModel
MAPPING['mms']                    = mMSIonicModel
MAPPING['mbrdr']                  = MBRDRIonicModel
MAPPING['murinemouse']            = MurineMouseIonicModel
MAPPING['nygren']                 = NYGRENIonicModel
MAPPING['ord']                    = ORdIonicModel
MAPPING['pandit']                 = PanditIonicModel
MAPPING['pug']                    = PUGIonicModel
MAPPING['rat_neon']               = Rat_neonIonicModel
MAPPING['rnc']                    = RNCIonicModel
MAPPING['sc']                     = SCIonicModel
MAPPING['tt2']                    = TT2IonicModel
MAPPING['tt']                     = TTIonicModel
MAPPING['ttred']                  = TTREDIonicModel
MAPPING['ucla_rab']               = UCLA_RABIonicModel
MAPPING['wang_nnr']               = WANG_NNRIonicModel
MAPPING['shannon']                = SHANNONIonicModel
MAPPING['koivumaki']              = koivumakiIonicModel
MAPPING['koivumaki_newina']       = koivumaki_newINaIonicModel
MAPPING['gtt2_fast']              = GTT2_fastIonicModel
MAPPING['gtt2_fast_purk']         = GTT2_fast_PURKIonicModel
MAPPING['jb_courtemanche']        = JB_COURTEMANCHEIonicModel


def keys():
    """
    Return a list of string keys to be used with :func:`get`
    """
    return MAPPING.keys()


def get(*args, **kwargs):
    """
    Get a class from its key string from :func:`keys`
    """
    return MAPPING.get(*args, **kwargs)


def summary(indent=2, ncolumns=4, padding=2):
    """
    Return a string summarizing the models and their parameters
    """
    modelattr = ['SHORTNAME', 'MODELID']
    maxattrlen = max(map(len, modelattr+['PARAMETERS']))
    summarystr = 'IONIC MODELS\n\n'
    indentstr = ' '*indent
    for name, model in MAPPING.items():
        summarystr += indentstr+'{}\n'.format(name)
        for attr in modelattr:
            obj = getattr(model, attr, None)
            if obj is None:
                continue
            summarystr += indentstr*2+'{}: {}\n'.format(attr.lower().ljust(maxattrlen), str(obj))
        attr = 'PARAMETERS'
        obj = getattr(model, attr, None)
        if obj is not None and len(obj) > 0:
            tablestr = stringtable(obj, ncolumns, padding, rowprefix=' '*(maxattrlen+2*indent+2))
            summarystr += indentstr*2+attr.lower().ljust(maxattrlen)+': '+tablestr[len(attr)+2*indent+2:]
        summarystr += '\n'
    return summarystr
