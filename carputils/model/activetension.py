
from .general import AbstractPlugin as _AbstractPlugin, NoPlugin
from collections import OrderedDict as _OrderedDict
from carputils.format import stringtable


class ExcConNHSStress(_AbstractPlugin):
    """
    Describes the ExcConNHS active stress model

    Parameters
    ----------
    alpha_0 : float, optional
        Value of alpha_0
    beta_1 : float, optional
        Value of beta_1
    Ca50ref : float, optional
        Value of Ca50ref
    n : float, optional
        Value of n
    parent_Cab : float, optional
        Value of parent_Cab, defaults to 1.0
    Q1decay : float, optional
        Value of Q1decay
    Q2decay : float, optional
        Value of Q2decay
    Q3decay : float, optional
        Value of Q3decay
    Tscale : float, optional
        Value of Tscale
    unbinding_rate : float, optional
        Value of unbinding_rate
    """
    SHORTNAME = 'excconnhs'
    PLUGIN = 'ExcConNHS'
    PARAMETERS = ['alpha_0', 'beta_1', 'Ca50ref', 'n', 'parent_Cab', 'Q1decay', 
                  'Q2decay', 'Q3decay', 'Tscale', 'unbinding_rate']


class FxStress(_AbstractPlugin):
    """
    Describes the FxStress active stress model

    Parameters
    ----------
    Sdia : float, optional
        Value of Sdia, defaults to 0.0
    Speak : float, optional
        Value of Speak, defaults to 50.0
    tau_c : float, optional
        Value of tau_c, defaults to 10.0
    tau_r : float, optional
        Value of tau_r, defaults to 100.0
    Tdur : float, optional
        Value of Tdur, defaults to 200.0
    Temd : float, optional
        Value of Temd, defaults to 15.0
    VmThresh : float, optional
        Value of VmThresh, defaults to -60.0
    """
    SHORTNAME = 'fx'
    PLUGIN = 'FxStress'
    PARAMETERS = ['Sdia', 'Speak', 'tau_c', 'tau_r', 'Tdur', 'Temd', 'VmThresh']


class LandHumanStress(_AbstractPlugin):
    """
    Describes the LandHumanStress active stress model

    Parameters
    ----------
    beta_0 : float, optional
        Value of beta_0, defaults to 2.3
    beta_1 : float, optional
        Value of beta_1, defaults to -2.4
    dr : float, optional
        Value of dr, defaults to 0.25
    gamma : float, optional
        Value of gamma, defaults to 0.0085
    gamma_wu : float, optional
        Value of gamma_wu, defaults to 0.615
    koff : float, optional
        Value of koff, defaults to 0.1
    ktm_unblock : float, optional
        Value of ktm_unblock, defaults to 1.0
    perm50 : float, optional
        Value of perm50, defaults to 0.35
    phi : float, optional
        Value of phi, defaults to 2.23
    TOT_A : float, optional
        Value of TOT_A, defaults to 25.0
    Tref : float, optional
        Value of Tref, defaults to 120.0
    TRPN_n : float, optional
        Value of TRPN_n, defaults to 2.0
    wfrac : float, optional
        Value of wfrac, defaults to 0.5
    """
    SHORTNAME = 'landhuman'
    PLUGIN = 'LandHumanStress'
    PARAMETERS = ['beta_0', 'beta_1', 'dr', 'gamma', 'gamma_wu', 'koff', 
                  'ktm_unblock', 'perm50', 'phi', 'TOT_A', 'Tref', 'TRPN_n', 
                  'wfrac']


class LandStress(_AbstractPlugin):
    """
    Describes the Land active stress model

    Parameters
    ----------
    a : float, optional
        Value of a, defaults to 0.35
    A_1 : float, optional
        Value of A_1, defaults to -29.0
    A_2 : float, optional
        Value of A_2, defaults to 116.0
    alpha_1 : float, optional
        Value of alpha_1, defaults to 0.1
    alpha_2 : float, optional
        Value of alpha_2, defaults to 0.5
    beta_0 : float, optional
        Value of beta_0, defaults to 1.65
    beta_1 : float, optional
        Value of beta_1, defaults to -1.5
    Ca_50ref : float, optional
        Value of Ca_50ref, defaults to 0.8
    k_TRPN : float, optional
        Value of k_TRPN, defaults to 0.1
    k_xb : float, optional
        Value of k_xb, defaults to 0.1
    n_TRPN : float, optional
        Value of n_TRPN, defaults to 2.0
    n_xb : float, optional
        Value of n_xb, defaults to 5.0
    T_ref : float, optional
        Value of T_ref, defaults to 120.0
    TRPN_50 : float, optional
        Value of TRPN_50, defaults to 0.35
    """
    SHORTNAME = 'land'
    PLUGIN = 'LandStress'
    PARAMETERS = ['a', 'A_1', 'A_2', 'alpha_1', 'alpha_2', 'beta_0', 'beta_1', 
                  'Ca_50ref', 'k_TRPN', 'k_xb', 'n_TRPN', 'n_xb', 'T_ref', 
                  'TRPN_50']


class LumensStress(_AbstractPlugin):
    """
    Describes the Lumens active stress model

    Parameters
    ----------
    VmThresh : float, optional
        Value of VmThresh, defaults to -50.0
    """
    SHORTNAME = 'lumens'
    PLUGIN = 'Lumens'
    PARAMETERS = ['VmThresh']


class MusconStress(_AbstractPlugin):
    """
    Describes the Hunter-McCulloch-ter Keurs active stress model

    Parameters
    ----------
    Alpha0 : float, optional
        Value of Alpha0, defaults to 0.002
    force_factor : float, optional
        Value of force_factor, defaults to 1.0
    parent_Cab : float, optional
        Value of parent_Cab
    Q1decay : float, optional
        Value of Q1decay, defaults to 0.033
    Q2decay : float, optional
        Value of Q2decay, defaults to 2.85
    Rho_0 : float, optional
        Value of Rho_0, defaults to 0.1
    Rho_1 : float, optional
        Value of Rho_1
    """
    SHORTNAME = 'muscon'
    PLUGIN = 'Muscon'
    PARAMETERS = ['Alpha0', 'force_factor', 'parent_Cab', 'Q1decay', 'Q2decay', 
                  'Rho_0', 'Rho_1']


class NHSStress(_AbstractPlugin):
    """
    Describes the Niederer-Hunter-Smith active stress model

    Parameters
    ----------
    a : float, optional
        Value of a, defaults to 0.35
    A_1 : float, optional
        Value of A_1, defaults to -29.0
    A_2 : float, optional
        Value of A_2, defaults to 138.0
    A_3 : float, optional
        Value of A_3, defaults to 129.0
    alpha_0 : float, optional
        Value of alpha_0, defaults to 0.008
    alpha_1 : float, optional
        Value of alpha_1, defaults to 0.03
    alpha_2 : float, optional
        Value of alpha_2, defaults to 0.13
    alpha_3 : float, optional
        Value of alpha_3, defaults to 0.625
    alpha_r1 : float, optional
        Value of alpha_r1, defaults to 0.002
    alpha_r2 : float, optional
        Value of alpha_r2, defaults to 0.00175
    beta_0 : float, optional
        Value of beta_0, defaults to 4.9
    beta_1 : float, optional
        Value of beta_1, defaults to -4.0
    Ca_50ref : float, optional
        Value of Ca_50ref, defaults to 0.00105
    Ca_TRPN_Max : float, optional
        Value of Ca_TRPN_Max, defaults to 0.07
    gamma_trpn : float, optional
        Value of gamma_trpn, defaults to 2.0
    k_on : float, optional
        Value of k_on, defaults to 100.0
    k_Ref_off : float, optional
        Value of k_Ref_off, defaults to 0.2
    K_z : float, optional
        Value of K_z, defaults to 0.15
    n_Hill : float, optional
        Value of n_Hill, defaults to 3.0
    n_Rel : float, optional
        Value of n_Rel, defaults to 3.0
    T_ref : float, optional
        Value of T_ref, defaults to 56.2
    z_p : float, optional
        Value of z_p, defaults to 0.85
    """
    SHORTNAME = 'nhs'
    PLUGIN = 'NHSstress'
    PARAMETERS = ['a', 'A_1', 'A_2', 'A_3', 'alpha_0', 'alpha_1', 'alpha_2', 
                  'alpha_3', 'alpha_r1', 'alpha_r2', 'beta_0', 'beta_1', 
                  'Ca_50ref', 'Ca_TRPN_Max', 'gamma_trpn', 'k_on', 'k_Ref_off', 
                  'K_z', 'n_Hill', 'n_Rel', 'T_ref', 'z_p']


class NPStress(_AbstractPlugin):
    """
    Describes the Nash-Panfilov active stress model

    Parameters
    ----------
    eps0 : float, optional
        Value of eps0, defaults to 1.0
    epsInf : float, optional
        Value of epsInf, defaults to 0.1
    gsi : float, optional
        Value of gsi, defaults to 0.1
    k : float, optional
        Value of k, defaults to 0.5
    noNegStress : float, optional
        Value of noNegStress, defaults to 1.0
    VmRest : float, optional
        Value of VmRest, defaults to -86.0
    VmThresh : float, optional
        Value of VmThresh, defaults to -80.0
    """
    SHORTNAME = 'np'
    PLUGIN = 'NPStress'
    PARAMETERS = ['eps0', 'epsInf', 'gsi', 'k', 'noNegStress', 'VmRest', 
                  'VmThresh']


class RiceStress(_AbstractPlugin):
    """
    Describes the Rice active stress model

    Parameters
    ----------
    failing : float, optional
        Value of failing, defaults to 0.0
    force_coeff : float, optional
        Value of force_coeff
    SLmax : float, optional
        Value of SLmax, defaults to 2.4
    SLmin : float, optional
        Value of SLmin, defaults to 1.4
    """
    SHORTNAME = 'rice'
    PLUGIN = 'Rice'
    PARAMETERS = ['failing', 'force_coeff', 'SLmax', 'SLmin']


class TanhStress(_AbstractPlugin):
    """
    Describes the TanhStress active stress model

    Parameters
    ----------
    lambda_0 : float, optional
        Value of lambda_0, defaults to 0.7
    ld : float, optional
        Value of ld, defaults to 5.0
    ld_up : float, optional
        Value of ld_up, defaults to 500.0
    ldOn : float, optional
        Value of ldOn, defaults to 0.0
    t_dur : float, optional
        Value of t_dur, defaults to 550.0
    t_emd : float, optional
        Value of t_emd, defaults to 15.0
    tau_c0 : float, optional
        Value of tau_c0, defaults to 40.0
    tau_r : float, optional
        Value of tau_r, defaults to 110.0
    Tpeak : float, optional
        Value of Tpeak, defaults to 100.0
    VmThresh : float, optional
        Value of VmThresh, defaults to -60.0
    """
    SHORTNAME = 'tanh'
    PLUGIN = 'TanhStress'
    PARAMETERS = ['lambda_0', 'ld', 'ld_up', 'ldOn', 't_dur', 't_emd', 'tau_c0', 
                  'tau_r', 'Tpeak', 'VmThresh']


MAPPING = _OrderedDict()
MAPPING['excconnhs'] = ExcConNHSStress
MAPPING['fx']        = FxStress
MAPPING['landhuman'] = LandHumanStress
MAPPING['land']      = LandStress
MAPPING['lumens']    = LumensStress
MAPPING['muscon']    = MusconStress
MAPPING['nhs']       = NHSStress
MAPPING['np']        = NPStress
MAPPING['rice']      = RiceStress
MAPPING['tanh']      = TanhStress
MAPPING['none']      = NoPlugin


def keys():
    """
    Return a list of string keys to be used with :func:`get`
    """
    return MAPPING.keys()


def get(*args, **kwargs):
    """
    Get a class from its key string from :func:`keys`
    """
    return MAPPING.get(*args, **kwargs)


def summary(indent=2, ncolumns=4, padding=2):
    """
    Return a string summarizing the models and their parameters
    """
    modelattr = ['SHORTNAME', 'PLUGIN']
    maxattrlen = max(map(len, modelattr+['PARAMETERS']))
    summarystr = 'STRESS MODELS\n\n'
    indentstr = ' '*indent
    for name, model in MAPPING.items():
        summarystr += indentstr+'{}\n'.format(name)
        for attr in modelattr:
            obj = getattr(model, attr, None)
            if obj is None:
                continue
            summarystr += indentstr*2+'{}: {}\n'.format(attr.lower().ljust(maxattrlen), str(obj))
        attr = 'PARAMETERS'
        obj = getattr(model, attr, None)
        if obj is not None and len(obj) > 0:
            tablestr = stringtable(obj, ncolumns, padding, rowprefix=' '*(maxattrlen+2*indent+2))
            summarystr += indentstr*2+attr.lower().ljust(maxattrlen)+': '+tablestr[len(attr)+2*indent+2:]
        summarystr += '\n'
    return summarystr
