
import os
import math
from .general import AbstractModelComponent
from carputils.resources import trace

class Stimulus(AbstractModelComponent):
    """
    Represents a single stimulus argument to CARP.

    Args:
        name : str, optional
            A short descriptive name for this stimulus
        \**kwargs
            The remaining fields for the stimulus
    """

    PRM_ARRAY  = 'stimulus'
    PRM_LENGTH = 'num_stim'

    FIELDS = ['name', 'vtx_file', 'dump_vtx_file', 'x0', 'y0', 'z0', 'xd',
              'yd', 'zd', 'ctr_def', 'geometry', 'start', 'bcl', 'npls',
              'dump_pulse', 'duration', 'd1', 'strength', 's2', 'tau_edge',
              'tau_plateau', 'bias', 'stimtype', 'prescribeFoot', 'balance',
              'total_current', 'pulse_file', 'data_file', 'domain']

    def __init__(self, name=None, **kwargs):
        super(Stimulus, self).__init__(name=name, **kwargs)
    
    @classmethod
    def forced_foot(cls, act_sequence=None, pulse_file=None):
        """
        Create a forced foot stimulus using a pulse file.
    
        Args:
            act_sequence : str, optional
                Path of the activation sequence to use for the stimulus - if not
                given will be computed
            pulse_file : str, optional
                Path of the trace to use to prescribe the foot, without .trc
                extension - a sensible default is used if not given
        """
        
        if pulse_file is None:
            pulse_file = trace.path('standard_ap_foot')
    
        if not os.path.exists(pulse_file + '.trc'):
            msg = ('pulse_file {}.trc does not exist, remember to omit the '
                   '.trc file extension').format(pulse_file)
            raise ValueError(msg)

        kwargs = {'pulse_file': pulse_file}
        if act_sequence is not None:
            kwargs['data_file'] = act_sequence
    
        return cls('forced_foot', stimtype=8, **kwargs)
    
    @classmethod
    def forced_foot_exponential(cls, act_sequence=None):
        """
        Create a forced foot stimulus with an exponential shape.
    
        Args:
            act_sequence : str, optional
                Path of the activation sequence to use for the stimulus - if not
                given will be computed
        """
    
        # Foot settings
        Vrest = -85
        Vth   = -55
        duration = 3
        tau_foot = 3
    
        # Get up needed parameters
        kwargs = {}
        kwargs['strength'] = Vth - Vrest
        kwargs['bias'] = 2 * Vrest - Vth
        kwargs['duration'] = 3
        kwargs['tau_plateau'] = tau_foot / math.log(2)
        kwargs['tau_edge'] = 0.0

        if act_sequence is not None:
            kwargs['data_file'] = act_sequence
    
        # Initialise and return stimulus
        return cls('forced_foot', stimtype=8, **kwargs)
