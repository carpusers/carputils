"""
Some tools for interfacing with the bench executable.
"""

import subprocess

from carputils import settings

_bench_model_cache = None

def _bench_models():

    global _bench_model_cache

    if _bench_model_cache is not None:
        return _bench_model_cache

    _bench_model_cache = {}

    # Get bench to list its models
    out = subprocess.check_output([str(settings.execs.BENCH),
                                   '--list', '--plugin-outputs'])

    for line in out.decode('utf-8').split('\n'):
        
        if 'Bench ' in line:
            # ignoring unnecessary openCARP output
            continue
            
        if 'Ionic models:' in line:
            current = _bench_model_cache['imp'] = []
            continue

        if 'Plug-ins:' in line:
            current = _bench_model_cache['plugin'] = {}
            continue

        if len(line) == 0:
            continue

        if isinstance(current, list):
            current.append(line.strip())
        else:
            parts = line.split()
            current[parts[0]] = parts[1:]

    return _bench_model_cache

def imps():
    return _bench_models()['imp']

def plugins():
    return _bench_models()['plugin']
