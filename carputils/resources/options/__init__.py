
import os

def dir():
    """
    Get the directory path of this package.
    """
    return os.path.dirname(__file__)

def path(name):
    """
    Get the path of an option file. Raises an Exception when the file does not
    exist.
    """
    filename = os.path.join(dir(), name)
    if not os.path.exists(filename):
        tpl = 'No option file "{0}" found in "{1}"'
        raise Exception(tpl.format(name, dir()))
    return filename

def load(name):
    """
    Load the stated option file and return as a list of strings.
    """

    opts = []

    with open(path(name), 'r') as fp:
        for line in fp:
            
            # Ignore leading and trailing whitespace
            line = line.strip()

            # Ignore commented lines
            if line.startswith('#'): continue
            # Ignore blank lines
            if len(line) == 0: continue

            # Build list of items
            opts += line.split()

    return opts
