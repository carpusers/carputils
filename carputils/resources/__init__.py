
import os

def dir():
    return os.path.dirname(__file__)

def path(name):
    filename = os.path.join(dir(), name)
    if not os.path.exists(filename):
        tpl = 'No resource file "{0}" found in "{1}"'
        raise Exception(tpl.format(name, dir()))
    return filename

def load(name):
    fp = open(path(name), 'r')
    data = fp.read()
    fp.close()
    return data
