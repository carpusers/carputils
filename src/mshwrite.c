/*
 * author: Matthias A.F. Gsell
 * email: gsell.matthias@gmail.com
 * date: 03-01-2020
 */ 

// Disable deprecated API versions
#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <Python.h>
#include <numpy/arrayobject.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

PyDoc_STRVAR(doc_write_points, 
    "write_points(filename:string, points:NumpyArray[float,?x3]) -> None\n\n"
    "Function to write CARP points to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the points file (*.pts)\n"
    "  points   : numpy float-array of shape (N x 3)\n"
    "             array holding the point data\n"
);

// write points to a file
static PyObject *write_points(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *pnt_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &pnt_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(pnt_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(pnt_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(pnt_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(pnt_array);
  if (shape[1] != 3) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 3) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *pnt_data = (double*)PyArray_DATA(pnt_array);
  double *pnt = NULL;

  // write number of points
  fprintf(fp, "%ld\n", shape[0]);
  // write data
  for (int i=0; i<shape[0]; i++) {
    pnt = &pnt_data[3*i]; 
    fprintf(fp, "%lf %lf %lf\n", pnt[0], pnt[1], pnt[2]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_UVCs, 
    "write_UVCs(filename:string, UVCs:NumpyArray[float,?x4]) -> None\n\n"
    "Function to write CARP UVCs to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the UVC file (*.uvc)\n"
    "  UVCs     : numpy float-array of shape (N x 4)\n"
    "             array holding the UVC data\n"
);

// write UVCs to a file
static PyObject *write_UVCs(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *uvc_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &uvc_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(uvc_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(uvc_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(uvc_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(uvc_array);
  if (shape[1] != 4) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 4) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *uvc_data = (double*)PyArray_DATA(uvc_array);
  double *uvc = NULL;

  // write number of UVCs
  fprintf(fp, "%ld\n", shape[0]);
  // write data
  for (int i=0; i<shape[0]; i++) {
    uvc = &uvc_data[4*i]; 
    fprintf(fp, "%lf %lf %lf %lf\n", uvc[0], uvc[1], uvc[2], uvc[3]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_fibers, 
    "write_fibers(filename:string, fibers:NumpyArray[float,?x{3,6}]) -> None\n\n"
    "Function to write CARP fibers to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the fibers file (*.lon)\n"
    "  fibers   : numpy float-array of shape (N x 3) or (N x 6)\n"
    "             array holding the fiber data\n"
);

// write fibers to a file
static PyObject *write_fibers(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *lon_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &lon_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(lon_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(lon_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(lon_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(lon_array);
  if ((shape[1] != 3) && (shape[1] != 6)) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 3) or (n x 6) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *lon_data = (double*)PyArray_DATA(lon_array);
  double *lon = NULL;
  
  // write fiber data
  if (shape[1] == 3) { // dimension 1
    // write fiber dimension
    fputs("1\n", fp);
    // write data
    for (int i=0; i<shape[0]; i++) {
      lon = &lon_data[3*i]; 
      fprintf(fp, "%lf %lf %lf\n", lon[0], lon[1], lon[2]);
    }
  }
  else {  // dimension 2
    // write fiber dimension
    fputs("2\n", fp);
    // write data
    for (int i=0; i<shape[0]; i++) {
      lon = &lon_data[6*i]; 
      fprintf(fp, "%lf %lf %lf %lf %lf %lf\n", lon[0], lon[1], lon[2], lon[3], lon[4], lon[5]);
    }
  }
  
  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_scalar_data, 
    "write_scalar_data(filename:string, data:NumpyArray[float,?]) -> None\n\n"
    "Function to write CARP scalar data to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.dat)\n"
    "  data     : numpy float-array\n"
    "             array holding the scalar data\n"
);

// write scalar data to a file
static PyObject *write_scalar_data(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *scl_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &scl_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(scl_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(scl_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(scl_array);
  if (ndim != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(scl_array);

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *scl_data = (double*)PyArray_DATA(scl_array);
  // write scalar data
  for (int i=0; i<shape[0]; i++) {
    fprintf(fp, "%lf\n", scl_data[i]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_scalar_data_int, 
    "write_scalar_data_int(filename:string, data:NumpyArray[int32,?]) -> None\n\n"
    "Function to write CARP scalar integer data to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.dat)\n"
    "  data     : numpy int32-array\n"
    "             array holding the scalar data\n"
);

// write scalar integer data to a file
static PyObject *write_scalar_data_int(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *scl_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &scl_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(scl_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(scl_array);
  if (dtype != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(scl_array);
  if (ndim != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(scl_array);

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  int *scl_data = (int*)PyArray_DATA(scl_array);
  // write scalar data
  for (int i=0; i<shape[0]; i++) {
    fprintf(fp, "%d\n", scl_data[i]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_vector_data, 
    "write_vector_data(filename:string, data:NumpyArray[float,?x3]) -> None\n\n"
    "Function to write CARP vector data to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.vec)\n"
    "  data     : numpy float-array of shape (N x 3)\n"
    "             array holding the vector data\n"
);

// write vector data to a file
static PyObject *write_vector_data(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *vec_array = NULL;
  if (!PyArg_ParseTuple(args, "sO!", &fname, &PyArray_Type, &vec_array))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(vec_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(vec_array);
  if (dtype != NPY_DOUBLE) {
    PyErr_SetString(PyExc_ValueError, "Error, double array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(vec_array);
  if (ndim != 2) {
    PyErr_SetString(PyExc_ValueError, "Error, two dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(vec_array);
  if (shape[1] != 3) {
    PyErr_SetString(PyExc_ValueError, "Error, array of shape (n x 3) expected!");
    return NULL;
  }

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  double *vec_data = (double*)PyArray_DATA(vec_array);
  double *vec = NULL;
  
  // write vector data
  for (int i=0; i<shape[0]; i++) {
    vec = &vec_data[3*i];
    fprintf(fp, "%lf %lf %lf\n", vec[0], vec[1], vec[2]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

PyDoc_STRVAR(doc_write_vertex_data, 
    "write_vertex_data_int(filename:string, data:NumpyArray[int32,?], *, domain:string) -> None\n\n"
    "Function to write CARP vertex data to a file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.vtx)\n"
    "  data     : numpy int32-array\n"
    "             array holding the scalar data\n"
    "  domain   : string, optional, default='intra'\n"
    "             domain specification (usually 'intra' or 'extra')"
);

// write vertex data to a file
static PyObject *write_vertex_data(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  PyArrayObject *vtx_array = NULL;
  char *domain = "intra";
  static char *kwlist[] = {"", "", "domain", NULL};
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "sO!|s", kwlist, &fname, &PyArray_Type, &vtx_array, &domain))
    return NULL;

  // check if array is contiguous
  const int iscont = PyArray_ISCONTIGUOUS(vtx_array);
  if (!iscont) {
    PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
    return NULL;
  }
  // get array data type
  const int dtype = PyArray_TYPE(vtx_array);
  if (dtype != NPY_INT) {
    PyErr_SetString(PyExc_ValueError, "Error, integer array expected!");
    return NULL;
  }
  // get array dimension
  const int ndim = PyArray_NDIM(vtx_array);
  if (ndim != 1) {
    PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
    return NULL;
  }
  // get array shape
  const npy_intp *shape = PyArray_DIMS(vtx_array);

  // open file
  FILE *fp = fopen(fname, "w");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // get array data
  int *vtx_data = (int*)PyArray_DATA(vtx_array);

  // write number of vertices and domain
  fprintf(fp, "%ld\n%s\n", shape[0], domain);  
  
  // write scalar data
  for (int i=0; i<shape[0]; i++) {
    fprintf(fp, "%d\n", vtx_data[i]);
  }

  // close file
  fclose(fp);
  // return None
  Py_RETURN_NONE;
}

// define methods
static PyMethodDef MshWriteMethods[] = {
    {"write_points", (PyCFunction)write_points, METH_VARARGS|METH_KEYWORDS, doc_write_points},
    {"write_UVCs", (PyCFunction)write_UVCs, METH_VARARGS|METH_KEYWORDS, doc_write_UVCs},
    {"write_fibers", (PyCFunction)write_fibers, METH_VARARGS|METH_KEYWORDS, doc_write_fibers},
    {"write_scalar_data", (PyCFunction)write_scalar_data, METH_VARARGS|METH_KEYWORDS, doc_write_scalar_data},
    {"write_scalar_data_int", (PyCFunction)write_scalar_data_int, METH_VARARGS|METH_KEYWORDS, doc_write_scalar_data_int},
    {"write_vector_data", (PyCFunction)write_vector_data, METH_VARARGS|METH_KEYWORDS, doc_write_vector_data},
    {"write_vertex_data", (PyCFunction)write_vertex_data, METH_VARARGS|METH_KEYWORDS, doc_write_vertex_data},
    {NULL, NULL, 0, NULL}
};

PyDoc_STRVAR(doc_mod_mshwrite, "Module providing several functions to write CARP-mesh related data to files.\n");

#if PY_MAJOR_VERSION >= 3

static struct PyModuleDef MshWriteModule = {
    PyModuleDef_HEAD_INIT,
    "mshwrite",       /* name of module */
    doc_mod_mshwrite, /* module documentation, may be NULL */
    -1,               /* size of per-interpreter state of the module,
                         or -1 if the module keeps state in global variables. */
    MshWriteMethods
};

PyMODINIT_FUNC PyInit_mshwrite(void)
{
    // initialize module
    PyObject *mod = PyModule_Create(&MshWriteModule);
    // import numpy module
    import_array();

    return mod;
}

#else

PyMODINIT_FUNC initmshwrite(void)
{
    // initialize module
    PyObject *mod = Py_InitModule3("mshwrite", MshWriteMethods, doc_mod_mshwrite);
    // import numpy module
    import_array();

    (void) mod;
}

#endif
