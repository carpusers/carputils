/*
 * author: Matthias A.F. Gsell
 * email: gsell.matthias@gmail.com
 * date: 03-01-2020
 */ 

// Disable deprecated API versions
#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <Python.h>
#include <numpy/arrayobject.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CAPACITY_INCREASE 64

PyDoc_STRVAR(doc_read_points, 
    "read_points(filename:string) -> NumpyArray[float,?x3]\n\n"
    "Function to read points from a CARP points file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the points file (*.pts)\n"
    "Returns:\n"
    "--------\n"
    "  numpy float-array of shape (N x 3) containing the point data"
);

// read points from a file
static PyObject *read_points(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  if (!PyArg_ParseTuple(args, "s", &fname))
    return NULL;
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // read number of points
  int num;
  fscanf(fp, "%d", &num);
  // allocate memory
  double *pnt_data = (double*)malloc(3*num*sizeof(double));
  double *pnt = NULL;
  // read data
  for (int i=0; i<num; i++) {
    pnt = &pnt_data[3*i]; 
    fscanf(fp, "%lf%lf%lf", &pnt[0], &pnt[1], &pnt[2]);
  }

  // close file
  fclose(fp);

  // convert to numpy array
  npy_intp array_dim[2] = {num,  3};
  PyArrayObject *pnt_array = PyArray_SimpleNewFromData(2, array_dim, NPY_DOUBLE, pnt_data);
  // return numpy array
  return PyArray_Return(pnt_array);
}

PyDoc_STRVAR(doc_read_UVCs, 
    "read_UVCs(filename:string) -> NumpyArray[float,?x4]\n\n"
    "Function to read UVCs from a CARP UVC file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the UVC file (*.uvc)\n"
    "Returns:\n"
    "--------\n"
    "  numpy float-array of shape (N x 4) containing the UVC data"
);

// read UVCs from a file
static PyObject *read_UVCs(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  if (!PyArg_ParseTuple(args, "s", &fname))
    return NULL;
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // read number of UVCs
  int num;
  fscanf(fp, "%d", &num);
  // allocate memory
  double *uvc_data = (double*)malloc(4*num*sizeof(double));
  double *uvc = NULL;
  // read data
  for (int i=0; i<num; i++) {
    uvc = &uvc_data[4*i];
    fscanf(fp, "%lf%lf%lf%lf", &uvc[0], &uvc[1], &uvc[2], &uvc[3]);
  }

  // close file
  fclose(fp);

  // convert to numpy array
  npy_intp array_dim[2] = {num,  4};
  PyArrayObject *uvc_array = PyArray_SimpleNewFromData(2, array_dim, NPY_DOUBLE, uvc_data);
  // return numpy array
  return PyArray_Return(uvc_array);
}

PyDoc_STRVAR(doc_read_elements, 
    "read_elements(filename:string) -> Dict\n\n"
    "Function to read elements from a CARP elements file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the elements file (*.elem)\n"
    "Returns:\n"
    "--------\n"
    "  dictionary containing the following items:\n"
    "    num    : int\n"
    "             number of elements\n"
    "    type   : numpy int32-array of size `num`\n"
    "             array holding the type of each element\n"
    "    tag    : numpy int32-array of size `num`\n"
    "             array holding tag information for each element\n"
    "    offset : numpy int32-array of size `num+1`\n"
    "             array holding offset indices for each element,\n"
    "             the vertex indices of the i-th element are\n"
    "             `index[offset[i]] ... index[offset[i+1]-1]`\n" 
    "    index  : numpy int32-array of size `offset[num]`\n"
    "             array holding the vertex indices for each element\n"            
);

// read elements from a file
static PyObject *read_elements(PyObject *self, PyObject *args, PyObject *keywds) {
  // get filename from the arguments
  char *fname = NULL;
  if (!PyArg_ParseTuple(args, "s", &fname))
    return NULL;
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // read number of elements
  int num;
  fscanf(fp, "%d", &num);
  // allocate memory for type, offset and tag data
  int *elm_type_data = (int*)malloc(num*sizeof(int));
  int *elm_offset_data = (int*)malloc((num+1)*sizeof(int));
  int *elm_tag_data = (int*)malloc(num*sizeof(int));
  // allocate memory for index data
  int elm_index_data_capacity = 4*num;
  int *elm_index_data = (int*)malloc(elm_index_data_capacity*sizeof(int));
  // initial element index data size
  elm_offset_data[0] = 0;
  // element type and data
  char type[4];    
  int elm[11]; // 0..type, 1..num, 2..tag, 3-10..indices

  // read data
  for (int i=0; i<num; i++) {
    // get element type
    fscanf(fp, "%3s", type);
    // read element indices and tag      
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // set element type and number of indices
      elm[0] = 0;
      elm[1] = 4;
      // read indices and tag
      fscanf(fp, "%d%d%d%d%d", &elm[3], &elm[4], &elm[5], &elm[6], &elm[2]);
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // set element type and number of indices
      elm[0] = 1;
      elm[1] = 5;
      // read indices and tag
      fscanf(fp, "%d%d%d%d%d%d", &elm[3], &elm[4], &elm[5], &elm[6], &elm[7], &elm[2]);
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // set element type and number of indices
      elm[0] = 2;
      elm[1] = 6;
      // read indices and tag
      fscanf(fp, "%d%d%d%d%d%d%d", &elm[3], &elm[4], &elm[5], &elm[6], &elm[7], &elm[8], &elm[2]);
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // set element type and number of indices
      elm[0] = 3;
      elm[1] = 6;
      // read indices and tag
      fscanf(fp, "%d%d%d%d%d%d%d", &elm[3], &elm[4], &elm[5], &elm[6], &elm[7], &elm[8], &elm[2]);
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // set element type and number of indices
      elm[0] = 4;
      elm[1] = 8;
      // read indices and tag
      fscanf(fp, "%d%d%d%d%d%d%d%d%d", &elm[3], &elm[4], &elm[5], &elm[6], &elm[7], &elm[8], &elm[9], &elm[10], &elm[2]);
    }
    else { // unknwon element type
      // close file
      fclose(fp);

      // free memory
      free(elm_type_data);
      free(elm_offset_data);
      free(elm_tag_data);
      free(elm_index_data);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }
    // increase index array size of necessary
    if ((elm_offset_data[i]+elm[1]) > elm_index_data_capacity) {
      elm_index_data_capacity += CAPACITY_INCREASE;
      elm_index_data = (int*)realloc(elm_index_data, elm_index_data_capacity*sizeof(int));
    }
    // set element type, offset and tag
    elm_type_data[i] = elm[0];
    elm_offset_data[i+1] = elm_offset_data[i]+elm[1];
    elm_tag_data[i] = elm[2];
    // set element indices
    memcpy(&elm_index_data[elm_offset_data[i]], &elm[3], elm[1]*sizeof(int));
  }

  // close file
  fclose(fp);

  // shrink index array size if necessary
  if (elm_index_data_capacity > elm_offset_data[num]) {
    elm_index_data = (int*)realloc(elm_index_data, elm_offset_data[num]*sizeof(int));
    elm_index_data_capacity = elm_offset_data[num];
  }

  // convert to numpy arrays
  npy_intp array_dim;
  // convert type and tag data
  array_dim = num;
  PyArrayObject *elm_type_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, elm_type_data);
  PyArrayObject *elm_tag_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, elm_tag_data);
  // convert offset data
  array_dim = num+1;
  PyArrayObject *elm_offset_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, elm_offset_data);
  // convert index data
  array_dim = elm_offset_data[num];
  PyArrayObject *elm_index_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, elm_index_data);
  // return element-data dictionary
  return Py_BuildValue("{s:i,s:O,s:O,s:O,s:O}", 
                        "num", num,
                        "type", elm_type_array,
                        "tag", elm_tag_array,
                        "offset", elm_offset_array,
                        "index", elm_index_array);
}

PyDoc_STRVAR(doc_read_element_tags, 
    "read_element_tags(filename:string) -> NumpyArray[int32,?]\n\n"
    "Function to read the element tags from a CARP elements file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the elements file (*.elem)\n"
    "Returns:\n"
    "--------\n"
    "  numpy int32-array containing the tag data"
);

// read element tags from a file
static PyObject *read_element_tags(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename from the arguments
  char *fname = NULL;
  if (!PyArg_ParseTuple(args, "s", &fname))
    return NULL;
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // read number of elements
  int num;
  fscanf(fp, "%d", &num);
  // allocate memory for the tag data
  int *elm_tag_data = (int*)malloc(num*sizeof(int));
  // element type
  char type[4];
  // read data

  for (int i=0; i<num; i++) {
    // get element type
    fscanf(fp, "%3s", type);
    int *tag = &elm_tag_data[i];
    // read element indices and tags
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // read tag
      fscanf(fp, "%*d%*d%*d%*d%d", tag);
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // read indices and tag
      fscanf(fp, "%*d%*d%*d%*d%*d%d", tag);
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // read indices and tag
      fscanf(fp, "%*d%*d%*d%*d%*d%*d%d", tag);
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // read indices and tag
      fscanf(fp, "%*d%*d%*d%*d%*d%*d%d", tag);
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // read indices and tag
      fscanf(fp, "%*d%*d%*d%*d%*d%*d%*d%*d%d", tag);
    }
    else { // unknwon element type
      // close file
      fclose(fp);

      // free memory
      free(elm_tag_data);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }
  }

  // close file
  fclose(fp);

  // convert to numpy arrays
  npy_intp array_dim = num;
  PyArrayObject *elm_tag_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, elm_tag_data);
  // return element tag data
  return PyArray_Return(elm_tag_array);
}

PyDoc_STRVAR(doc_read_surface_elements, 
    "read_surface_elements(filename:string) -> Dict\n"
    "Function to read surface-elements from a CARP surface file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the surface-elements file (*.surf)\n"
    "Returns:\n"
    "--------\n"
    "  dictionary containing the following items:\n"
    "    num    : int\n"
    "             number of surface elements\n"
    "    type   : numpy int32-array of size `num`\n"
    "             array holding the type of each element\n"
    "    offset : numpy int32-array of size `num+1`\n"
    "             array holding offset indices for each surface-element,\n"
    "             the vertex indices of the i-th surface-element are\n"
    "             `index[offset[i]] ... index[offset[i+1]-1]`\n" 
    "    index  : numpy int32-array of size `offset[num]`\n"
    "             array holding the vertex indices for each surface-element\n"            
);

// read surface elements from a file
static PyObject *read_surface_elements(PyObject *self, PyObject *args, PyObject *keywds) {
  // get filename from the arguments
  char *fname = NULL;
  if (!PyArg_ParseTuple(args, "s", &fname))
    return NULL;
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // read number of elements
  int num;
  fscanf(fp, "%d", &num);
  // allocate memory for type, offset and tag data
  int *elm_type_data = (int*)malloc(num*sizeof(int));
  int *elm_offset_data = (int*)malloc((num+1)*sizeof(int));
  // allocate memory for index data
  int elm_index_data_capacity = 3*num;
  int *elm_index_data = (int*)malloc(elm_index_data_capacity*sizeof(int));
  // initial element index data size
  elm_offset_data[0] = 0;
  // element type and data
  char type[4];    
  int elm[7]; // 0..type, 1..num, 2-5..indices
  // read data

  for (int i=0; i<num; i++) {
    // get element type
    fscanf(fp, "%3s", type);
    // read element indices
    if (strcmp(type, "Tr") == 0) { // triangle, 'Tr'-element
      // set element type and number of indices
      elm[0] = 0;
      elm[1] = 3;
      // read indices and tag
      fscanf(fp, "%d%d%d", &elm[2], &elm[3], &elm[4]);
    }
    else if (strcmp(type, "Qd") == 0) { // quad, 'Qd'-element
      // set element type and number of indices
      elm[0] = 1;
      elm[1] = 4;
      // read indices and tag
      fscanf(fp, "%d%d%d%d", &elm[2], &elm[3], &elm[4], &elm[5]);
    }
    else { // unknwon element type
      // close file
      fclose(fp);

      // free memory
      free(elm_type_data);
      free(elm_offset_data);  
      free(elm_index_data);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon surface element type '%s'!", i+1, type);
      return NULL;
    }
    // increase index array size of necessary
    if ((elm_offset_data[i]+elm[1]) > elm_index_data_capacity) {
      elm_index_data_capacity += CAPACITY_INCREASE;
      elm_index_data = (int*)realloc(elm_index_data, elm_index_data_capacity*sizeof(int));
    }
    // set element type, offset and tag
    elm_type_data[i] = elm[0];
    elm_offset_data[i+1] = elm_offset_data[i]+elm[1];
    // set element indices
    memcpy(&elm_index_data[elm_offset_data[i]], &elm[2], elm[1]*sizeof(int));
  }

  // close file
  fclose(fp);

  // shrink index array size if necessary
  if (elm_index_data_capacity > elm_offset_data[num]) {
    elm_index_data = (int*)realloc(elm_index_data, elm_offset_data[num]*sizeof(int));
    elm_index_data_capacity = elm_offset_data[num];
  }

  // convert to numpy arrays
  npy_intp array_dim;
  // convert type data
  array_dim = num;
  PyArrayObject *elm_type_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, elm_type_data);
  // convert offset data
  array_dim = num+1;
  PyArrayObject *elm_offset_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, elm_offset_data);
  // convert index data
  array_dim = elm_offset_data[num];
  PyArrayObject *elm_index_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, elm_index_data);
  // return element-data dictionary
  return Py_BuildValue("{s:i,s:O,s:O,s:O}", 
                        "num", num,
                        "type", elm_type_array,
                        "offset", elm_offset_array,
                        "index", elm_index_array);
}

PyDoc_STRVAR(doc_read_fibers, 
    "read_read_fibers(filename:string, num:int) -> NumpyArray[float,`num`x{3,6}]\n\n"
    "Function to read fibers from a CARP fibers file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the fibers file (*.lon)\n"
    "  num      : int\n"
    "             number of fibers to read\n"
    "Returns:\n"
    "--------\n"
    "  numpy float-array of size (`num` x 3) or (`num` x 6) containing the fiber data"
);

// read fibers from a file
static PyObject *read_fibers(PyObject *self, PyObject *args, PyObject *keywds) {
  // get filename and number of fibers from the arguments
  char *fname = NULL;
  int num = -1;
  if (!PyArg_ParseTuple(args, "si", &fname, &num))
    return NULL;
  // sanity check
  if (num <= 0) {
    PyErr_Format(PyExc_ValueError, "Error, number of fibers %d, positive value expected!", num);
    return NULL;
  }
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // read fiber dimension (1 or 2)
  int dim;
  fscanf(fp, "%d", &dim);
  if (dim == 1) {
    // allocate memory
    double *lon_data = (double*)malloc(3*num*sizeof(double));
    double *lon = NULL;

    // read data
    for (int i=0; i<num; i++) {
      lon = &lon_data[3*i];
      fscanf(fp, "%lf%lf%lf", &lon[0], &lon[1], &lon[2]);
    }

    // close file
    fclose(fp);

    // convert to numpy array
    npy_intp array_dim[2] = {num, 3};
    PyArrayObject *lon_array = PyArray_SimpleNewFromData(2, array_dim, NPY_DOUBLE, lon_data);
    // return numpy array
    return PyArray_Return(lon_array);
  }
  else if (dim == 2) {
    // allocate memory
    double *lon_data = (double*)malloc(6*num*sizeof(double));
    double *lon = NULL;

    // read data
    for (int i=0; i<num; i++) {
      lon = &lon_data[6*i];
      fscanf(fp, "%lf%lf%lf%lf%lf%lf", &lon[0], &lon[1], &lon[2], &lon[3], &lon[4], &lon[5]);
    }

    // close file
    fclose(fp);

    // convert to numpy array
    npy_intp array_dim[2] = {num, 6};
    PyArrayObject *lon_array = PyArray_SimpleNewFromData(2, array_dim, NPY_DOUBLE, lon_data);
    // return numpy array
    return PyArray_Return(lon_array);
  }
  else {
    // close file
    fclose(fp);

    // raise exception if dimension was neither 1 nor 2
    PyErr_Format(PyExc_ValueError, "Error, dimension %d not supported!", dim);
    return NULL;
  }
}

PyDoc_STRVAR(doc_read_scalar_data, 
    "read_scalar_data(filename:string, num:int) -> NumpyArray[float,`num`]\n\n"
    "Function to read scalar data from a CARP data file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.dat)\n"
    "  num      : int\n"
    "             number of values to read\n"
    "Returns:\n"
    "--------\n"
    "  numpy float-array of size `num` containing the data"
);

// read scalar data from a file
static PyObject *read_scalar_data(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename and number of scalars from the arguments
  char *fname = NULL;
  int num = -1;
  if (!PyArg_ParseTuple(args, "si", &fname, &num))
    return NULL;
  // sanity check
  if (num <= 0) {
    PyErr_Format(PyExc_ValueError, "Error, number of vector data %d, positive value expected!", num);
    return NULL;
  }
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // allocate memory
  double *scl_data = (double*)malloc(num*sizeof(double));
  double *scl = NULL;
  // read data
  for (int i=0; i<num; i++) {
    scl = &scl_data[i];
    fscanf(fp, "%lf", &scl[0]);
  }

  // close file
  fclose(fp);

  // convert to numpy array
  npy_intp array_dim = num;
  PyArrayObject *scl_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_DOUBLE, scl_data);
  // return numpy array
  return PyArray_Return(scl_array);
}

PyDoc_STRVAR(doc_read_scalar_data_int, 
    "read_scalar_data_int(filename:string, num:int) -> NumpyArray[int32,`num`]\n\n"
    "Function to read scalar integer data from a CARP data file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.dat)\n"
    "  num      : int\n"
    "             number of values to read\n"
    "Returns:\n"
    "--------\n"
    "  numpy int32-array of size `num` containing the data"
);

// read scalar integer data from a file
static PyObject *read_scalar_data_int(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename and number of scalars from the arguments
  char *fname = NULL;
  int num = -1;
  if (!PyArg_ParseTuple(args, "si", &fname, &num))
    return NULL;
  // sanity check
  if (num <= 0) {
    PyErr_Format(PyExc_ValueError, "Error, number of vector data %d, positive value expected!", num);
    return NULL;
  }
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // allocate memory
  int *scl_data = (int*)malloc(num*sizeof(int));
  int *scl = NULL;
  // read data
  for (int i=0; i<num; i++) {
    scl = &scl_data[i];
    fscanf(fp, "%d", &scl[0]);
  }

  // close file
  fclose(fp);

  // convert to numpy array
  npy_intp array_dim = num;
  PyArrayObject *scl_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, scl_data);
  // return numpy array
  return PyArray_Return(scl_array);
}

PyDoc_STRVAR(doc_read_vector_data, 
    "read_vector_data(filename:string, num:int) -> NumpyArray[float,`num`x3]\n\n"
    "Function to read vector data from a CARP vector data file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the data file (*.vec)\n"
    "  num      : int\n"
    "             number of vectors to read\n"
    "Returns:\n"
    "--------\n"
    "  numpy float-array of size (`num` x  3) containing the vector data"
);

// read vector data from a file
static PyObject *read_vector_data(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename and number of vectors from the arguments
  char *fname = NULL;
  int num = -1;
  if (!PyArg_ParseTuple(args, "si", &fname, &num))
    return NULL;
  // sanity check
  if (num <= 0) {
    PyErr_Format(PyExc_ValueError, "Error, number of vector data %d, positive value expected!", num);
    return NULL;
  }
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // allocate memory
  double *vec_data = (double*)malloc(3*num*sizeof(double));
  double *vec = NULL;

  // read data
  for (int i=0; i<num; i++) {
    vec = &vec_data[3*i];
    fscanf(fp, "%lf%lf%lf", &vec[0], &vec[1], &vec[2]);
  }

  // close file
  fclose(fp);

  // convert to numpy array
  npy_intp array_dim[2] = {num, 3};
  PyArrayObject *vec_array = PyArray_SimpleNewFromData(2, array_dim, NPY_DOUBLE, vec_data);
  // return numpy array
  return PyArray_Return(vec_array);
}

PyDoc_STRVAR(doc_read_vertex_data, 
    "read_vertex_data(filename:string) -> Tuple[NumpyArray[int32,?],string]\n\n"
    "Function to read vertex data from a CARP vertex file\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the vertex file (*.vtx)\n"
    "Returns:\n"
    "--------\n"
    "  tuple of size two containing a numpy int32-array holding the\n"
    "  vertex data and a string specifying the domain ('intra' or 'extra')"
);

// read vertex data from a file
static PyObject *read_vertex_data(PyObject *self, PyObject *args, PyObject *keywds) {

  // get filename and number of vectors from the arguments
  char *fname = NULL;
  if (!PyArg_ParseTuple(args, "s", &fname))
    return NULL;
  // open file
  FILE *fp = fopen(fname, "r");
  if (!fp) {
    // raise exception if file was not opened
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", fname);
    return NULL;
  }

  // read number of elements
  int num;
  fscanf(fp, "%d", &num);
  // allocate memory
  int *vtx_data = (int*)malloc(num*sizeof(int));
  int *vtx = NULL;
  
  // read domain ('intra' or 'extra')
  char dom[16];
  fscanf(fp, "%s", dom);

  // read data
  for (int i=0; i<num; i++) {
    vtx = &vtx_data[i];
    fscanf(fp, "%d", vtx);
  }

  // close file
  fclose(fp);

  // convert to numpy array
  npy_intp array_dim = num;
  PyArrayObject *vtx_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, vtx_data);

  // return Python object
  return Py_BuildValue("Os", vtx_array, dom);
}

// define methods
static PyMethodDef MshReadMethods[] = {
    {"read_points", (PyCFunction)read_points, METH_VARARGS|METH_KEYWORDS, doc_read_points},
    {"read_UVCs", (PyCFunction)read_UVCs, METH_VARARGS|METH_KEYWORDS, doc_read_UVCs},
    {"read_elements", (PyCFunction)read_elements, METH_VARARGS|METH_KEYWORDS, doc_read_elements},
    {"read_element_tags", (PyCFunction)read_element_tags, METH_VARARGS|METH_KEYWORDS, doc_read_element_tags},
    {"read_surface_elements", (PyCFunction)read_surface_elements, METH_VARARGS|METH_KEYWORDS, doc_read_surface_elements},
    {"read_fibers", (PyCFunction)read_fibers, METH_VARARGS|METH_KEYWORDS, doc_read_fibers},
    {"read_scalar_data", (PyCFunction)read_scalar_data, METH_VARARGS|METH_KEYWORDS, doc_read_scalar_data},
    {"read_scalar_data_int", (PyCFunction)read_scalar_data_int, METH_VARARGS|METH_KEYWORDS, doc_read_scalar_data_int},
    {"read_vector_data", (PyCFunction)read_vector_data, METH_VARARGS|METH_KEYWORDS, doc_read_vector_data},
    {"read_vertex_data", (PyCFunction)read_vertex_data, METH_VARARGS|METH_KEYWORDS, doc_read_vertex_data},
    {NULL, NULL, 0, NULL}
};

PyDoc_STRVAR(doc_mod_mshread, "Module providing several functions to read CARP-mesh related data from files.\n");

#if PY_MAJOR_VERSION >= 3

static struct PyModuleDef MshReadModule = {
    PyModuleDef_HEAD_INIT,
    "mshread",       /* name of module */
    doc_mod_mshread, /* module documentation, may be NULL */
    -1,              /* size of per-interpreter state of the module,
                        or -1 if the module keeps state in global variables. */
    MshReadMethods
};

PyMODINIT_FUNC PyInit_mshread(void)
{
    // initialize module
    PyObject *mod = PyModule_Create(&MshReadModule);
    // import numpy module
    import_array();

    // add element type map to module
    PyObject *elm_type_map = Py_BuildValue("(sssss)", "Tt", "Py", "Oc", "Pr", "Hx");
    PyModule_AddObject(mod, "ELEM_TYPE_MAP", elm_type_map);
    // add element size map to module
    PyObject *elm_size_map = Py_BuildValue("(iiiii)", 4, 5, 6, 6, 8);
    PyModule_AddObject(mod, "ELEM_SIZE_MAP", elm_size_map);

    // add surface element type map to module
    PyObject *surf_elm_type_map = Py_BuildValue("(ss)", "Tr", "Qd");
    PyModule_AddObject(mod, "SURF_ELEM_TYPE_MAP", surf_elm_type_map);
    // add element size map to module
    PyObject *surf_elm_size_map = Py_BuildValue("(ii)", 3, 4);
    PyModule_AddObject(mod, "SURF_ELEM_SIZE_MAP", surf_elm_size_map);

    return mod;
}

#else

PyMODINIT_FUNC initmshread(void)
{
    // initialize module
    PyObject *mod = Py_InitModule3("mshread", MshReadMethods, doc_mod_mshread);
    // import numpy module
    import_array();
    
    // add element type map to module
    PyObject *elm_type_map = Py_BuildValue("(sssss)", "Tt", "Py", "Oc", "Pr", "Hx");
    PyModule_AddObject(mod, "ELEM_TYPE_MAP", elm_type_map);
    // add element size map to module
    PyObject *elm_size_map = Py_BuildValue("(iiiii)", 4, 5, 6, 6, 8);
    PyModule_AddObject(mod, "ELEM_SIZE_MAP", elm_size_map);
    
    // add surface element type map to module
    PyObject *surf_elm_type_map = Py_BuildValue("(ss)", "Tr", "Qd");
    PyModule_AddObject(mod, "SURF_ELEM_TYPE_MAP", surf_elm_type_map);
    // add element size map to module
    PyObject *surf_elm_size_map = Py_BuildValue("(ii)", 3, 4);
    PyModule_AddObject(mod, "SURF_ELEM_SIZE_MAP", surf_elm_size_map);

    (void) mod;
}

#endif
