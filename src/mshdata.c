/*
 * author: Matthias A.F. Gsell
 * email: gsell.matthias@gmail.com
 * date: 03-01-2020
 */ 

// Disable deprecated API versions
#define PY_SSIZE_T_CLEAN
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include <Python.h>
#include <numpy/arrayobject.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int cmpfunc_int(const void *a, const void *b) {
   return (*(int*)a - *(int*)b);
}

PyDoc_STRVAR(doc_n2emap_scalar_data, 
    "n2emap_scalar_data(filename:string, data:string, num:int, *, tags=NumpyArray[int32,?], default=float, tag_file=str) -> NumpyArray[float,?]\n\n"    
    "Function to map scalar data from the nodes onto the elements\n"
    "of a CARP mesh by averaging the values. This is done on the\n"    
    "entire mesh or on a tag-based submesh only.\n\n"
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the input elements file (*.elem)\n"
    "  data     : str\n"
    "             path to the input nodal data (*.dat)\n"
    "  num      : int\n"
    "             number of nodes\n"
    "  tags     : numpy int32-array, optional\n"
    "             tags of the elements for which the value is averaged\n"
    "  default  : float, optional, default=0.0\n"
    "             default value for the excluded elements\n"
    "  tag_file : str, optional\n"
    "             path to an alternative tag file (*.tags)\n"
);

// average scalar nodal data to element data
static PyObject *n2emap_scalar_data(PyObject *self, PyObject *args, PyObject *keywds) {
  
  // get filenames from the arguments  
  char *elm_fname = NULL;
  char *dat_fname = NULL;
  char *tag_fname = NULL;
  int num_dat = -1;
  PyArrayObject *tag_array = NULL;
  double def_value = 0.0;
  static char *kwlist[] = {"", "", "", "tags", "default", "tag_file", NULL};  
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "ssi|O!ds", kwlist, &elm_fname, &dat_fname, &num_dat,
                                                                     &PyArray_Type, &tag_array, 
                                                                     &def_value, &tag_fname))
    return NULL;

  // sanity check
  if (num_dat <= 0) {
    PyErr_Format(PyExc_ValueError, "Error, number of scalar data %d, positive value expected!", num_dat);
    return NULL;
  }
  
  int *tag_data = NULL;
  npy_intp num_tags = 0;
  if (tag_array) {
    // check if array is contiguous
    const int iscont = PyArray_ISCONTIGUOUS(tag_array);
    if (!iscont) {
      PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
      return NULL;
    }
    // get array data type
    const int dtype = PyArray_TYPE(tag_array);
    if (dtype != NPY_INT) {
      PyErr_SetString(PyExc_ValueError, "Error, integer array expected!");
      return NULL;
    }
    // get array dimension
    const int ndim = PyArray_NDIM(tag_array);
    if (ndim != 1) {
      PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
      return NULL;
    }
    
    // sort tag data for faster searching
    num_tags = PyArray_DIM(tag_array, 0);
    tag_data = (int*)PyArray_DATA(tag_array);  
    qsort(tag_data, num_tags, sizeof(int), cmpfunc_int);
  }

  // open element file
  FILE *elm_fp = fopen(elm_fname, "r");
  if (!elm_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", elm_fname);
    return NULL;
  }
  // open data file
  FILE *dat_fp = fopen(dat_fname, "r");
  if (!dat_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", dat_fname);
    return NULL;
  }
  // open tags file
  FILE *tag_fp = NULL;
  if (tag_fname) {
    tag_fp = fopen(tag_fname, "r");
    if (!tag_fp) {
      PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", tag_fname);
      return NULL;
    }
  }

  // allocate data memory
  double *scl_data = (double*)malloc(num_dat*sizeof(double));  
  // read scalar data
  for (int i=0; i<num_dat; i++)
    fscanf(dat_fp, "%lf", &scl_data[i]);    
  // close data file
  fclose(dat_fp);
  
  // read number of elements
  int num_elm;
  fscanf(elm_fp, "%d", &num_elm);
  // allocate memory for averaged element data
  double *avg_data = (double*)malloc(num_elm*sizeof(double));  
  double avg_value = def_value;
  // element type, data and tag
  char type[4];
  int idx[8], tag;

  // read elements and tags and average data
  for (int i=0; i<num_elm; i++) {
    avg_value = def_value;

    // get element type
    fscanf(elm_fp, "%3s", type);
    // read element indices
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &tag);      
      // average nodal data
      avg_value = (scl_data[idx[0]] + scl_data[idx[1]] + scl_data[idx[2]] + scl_data[idx[3]])/4.0;
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &tag);
      // average nodal data
      avg_value = (scl_data[idx[0]] + scl_data[idx[1]] + scl_data[idx[2]] + 
                   scl_data[idx[3]] + scl_data[idx[4]])/5.0;
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      // average nodal data
      avg_value = (scl_data[idx[0]] + scl_data[idx[1]] + scl_data[idx[2]] + 
                   scl_data[idx[3]] + scl_data[idx[4]] + scl_data[idx[5]])/6.0;
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      // average nodal data
      avg_value = (scl_data[idx[0]] + scl_data[idx[1]] + scl_data[idx[2]] + 
                   scl_data[idx[3]] + scl_data[idx[4]] + scl_data[idx[5]])/6.0;
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &idx[6], &idx[7], &tag);
      // average nodal data
      avg_value = (scl_data[idx[0]] + scl_data[idx[1]] + scl_data[idx[2]] + 
                   scl_data[idx[3]] + scl_data[idx[4]] + scl_data[idx[5]] + 
                   scl_data[idx[6]] + scl_data[idx[7]])/8.0;      
    }
    else { // unknwon element type
      // close files
      fclose(elm_fp);
      if (tag_fp)
        fclose(tag_fp);

      // free memory
      free(scl_data);
      free(avg_data);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }

    // read alternative element tag
    if (tag_fp)
      fscanf(tag_fp, "%d", &tag);
    
    // fill element data
    avg_data[i] = def_value;
    if ((!tag_data) || ((tag_data) && (bsearch(&tag, tag_data, num_tags, sizeof(int), cmpfunc_int))))
      avg_data[i] = avg_value;

  }
  // close files
  fclose(elm_fp);
  if (tag_fp)
    fclose(tag_fp);

  // free memory
  free(scl_data);

  // convert to numpy array
  npy_intp array_dim = num_elm;
  PyArrayObject *avg_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_DOUBLE, avg_data);
  // return numpy array
  return PyArray_Return(avg_array);
}

PyDoc_STRVAR(doc_n2emap_vector_data, 
    "n2emap_vector_data(filename:string, data:string, num:int, *, tags=NumpyArray[int32,?], default=float, tag_file=str) -> NumpyArray[float,?x3]\n\n"    
    "Function to map vector valued data from the nodes onto the elements\n"
    "of a CARP mesh by averaging the values. This is done on the\n"    
    "entire mesh or on a tag-based submesh only.\n\n"    
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the input elements file (*.elem)\n"
    "  data     : str\n"
    "             path to the input nodal vector data (*.vec)\n"
    "  num      : int\n"
    "             number of nodes\n"
    "  tags     : numpy int32-array, optional\n"
    "             tags of the elements for which the value is averaged\n"
    "  default  : float, optional, default=0.0\n"
    "             default value for the excluded elements\n"
    "  tag_file : str, optional\n"
    "             path to an alternative tag file (*.tags)\n"
);

// average vector valued nodal data to element data
static PyObject *n2emap_vector_data(PyObject *self, PyObject *args, PyObject *keywds) {
  
  // get filenames from the arguments  
  char *elm_fname = NULL;
  char *vec_fname = NULL;
  char *tag_fname = NULL;
  int num_dat = -1;
  PyArrayObject *tag_array = NULL;
  double def_value = 0.0;
  static char *kwlist[] = {"", "", "", "tags", "default", "tag_file", NULL};  
  if (!PyArg_ParseTupleAndKeywords(args, keywds, "ssi|O!ds", kwlist, &elm_fname, &vec_fname, &num_dat,
                                                                     &PyArray_Type, &tag_array, 
                                                                     &def_value, &tag_fname))
    return NULL;

  // sanity check
  if (num_dat <= 0) {
    PyErr_Format(PyExc_ValueError, "Error, number of scalar data %d, positive value expected!", num_dat);
    return NULL;
  }
  
  int *tag_data = NULL;
  npy_intp num_tags = 0;
  if (tag_array) {
    // check if array is contiguous
    const int iscont = PyArray_ISCONTIGUOUS(tag_array);
    if (!iscont) {
      PyErr_SetString(PyExc_ValueError, "Error, contiguous array expected!");
      return NULL;
    }
    // get array data type
    const int dtype = PyArray_TYPE(tag_array);
    if (dtype != NPY_INT) {
      PyErr_SetString(PyExc_ValueError, "Error, integer array expected!");
      return NULL;
    }
    // get array dimension
    const int ndim = PyArray_NDIM(tag_array);
    if (ndim != 1) {
      PyErr_SetString(PyExc_ValueError, "Error, one dimensional array expected!");
      return NULL;
    }
    
    // sort tag data for faster searching
    num_tags = PyArray_DIM(tag_array, 0);
    tag_data = (int*)PyArray_DATA(tag_array);  
    qsort(tag_data, num_tags, sizeof(int), cmpfunc_int);
  }

  // open element file
  FILE *elm_fp = fopen(elm_fname, "r");
  if (!elm_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", elm_fname);
    return NULL;
  }
  // open data file
  FILE *vec_fp = fopen(vec_fname, "r");
  if (!vec_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", vec_fname);
    return NULL;
  }
  // open tags file
  FILE *tag_fp = NULL;
  if (tag_fname) {
    tag_fp = fopen(tag_fname, "r");
    if (!tag_fp) {
      PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", tag_fname);
      return NULL;
    }
  }

  // allocate data memory
  double *vec_data = (double*)malloc(3*num_dat*sizeof(double));  
  double *vec = NULL;
  // read vector data
  for (int i=0; i<num_dat; i++) {
    vec = &vec_data[3*i];
    fscanf(vec_fp, "%lf%lf%lf", &vec[0], &vec[1], &vec[2]);
  }
  // close data file
  fclose(vec_fp);
  
  // read number of elements
  int num_elm;
  fscanf(elm_fp, "%d", &num_elm);
  // allocate memory for averaged element data
  double *avg_data = (double*)malloc(3*num_elm*sizeof(double));  
  double avg_value[3] = {def_value, def_value, def_value};
  // element type, data and tag
  char type[4];
  int idx[8], tag;

  // read elements and tags and average data
  for (int i=0; i<num_elm; i++) {
    avg_value[0] = avg_value[1] = avg_value[2] = def_value;    

    // get element type
    fscanf(elm_fp, "%3s", type);
    // read element indices
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &tag);      
      // average nodal data
      avg_value[0] = (vec_data[3*idx[0]+0] + vec_data[3*idx[1]+0] + 
                      vec_data[3*idx[2]+0] + vec_data[3*idx[3]+0])/4.0;
      avg_value[1] = (vec_data[3*idx[0]+1] + vec_data[3*idx[1]+1] + 
                      vec_data[3*idx[2]+1] + vec_data[3*idx[3]+1])/4.0;
      avg_value[2] = (vec_data[3*idx[0]+2] + vec_data[3*idx[1]+2] + 
                      vec_data[3*idx[2]+2] + vec_data[3*idx[3]+2])/4.0;
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &tag);
      // average nodal data
      avg_value[0] = (vec_data[3*idx[0]+0] + vec_data[3*idx[1]+0] + vec_data[3*idx[2]+0] + 
                      vec_data[3*idx[3]+0] + vec_data[3*idx[4]+0])/5.0;
      avg_value[1] = (vec_data[3*idx[0]+1] + vec_data[3*idx[1]+1] + vec_data[3*idx[2]+1] + 
                      vec_data[3*idx[3]+1] + vec_data[3*idx[4]+1])/5.0;
      avg_value[2] = (vec_data[3*idx[0]+2] + vec_data[3*idx[1]+2] + vec_data[3*idx[2]+2] + 
                      vec_data[3*idx[3]+2] + vec_data[3*idx[4]+2])/5.0;
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      // average nodal data
      avg_value[0] = (vec_data[3*idx[0]+0] + vec_data[3*idx[1]+0] + vec_data[3*idx[2]+0] + 
                      vec_data[3*idx[3]+0] + vec_data[3*idx[4]+0] + vec_data[3*idx[5]+0])/6.0;
      avg_value[1] = (vec_data[3*idx[0]+1] + vec_data[3*idx[1]+1] + vec_data[3*idx[2]+1] + 
                      vec_data[3*idx[3]+1] + vec_data[3*idx[4]+1] + vec_data[3*idx[5]+1])/6.0;
      avg_value[2] = (vec_data[3*idx[0]+2] + vec_data[3*idx[1]+2] + vec_data[3*idx[2]+2] + 
                      vec_data[3*idx[3]+2] + vec_data[3*idx[4]+2] + vec_data[3*idx[5]+2])/6.0;
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      // average nodal data
      avg_value[0] = (vec_data[3*idx[0]+0] + vec_data[3*idx[1]+0] + vec_data[3*idx[2]+0] + 
                      vec_data[3*idx[3]+0] + vec_data[3*idx[4]+0] + vec_data[3*idx[5]+0])/6.0;
      avg_value[1] = (vec_data[3*idx[0]+1] + vec_data[3*idx[1]+1] + vec_data[3*idx[2]+1] + 
                      vec_data[3*idx[3]+1] + vec_data[3*idx[4]+1] + vec_data[3*idx[5]+1])/6.0;
      avg_value[2] = (vec_data[3*idx[0]+2] + vec_data[3*idx[1]+2] + vec_data[3*idx[2]+2] + 
                      vec_data[3*idx[3]+2] + vec_data[3*idx[4]+2] + vec_data[3*idx[5]+2])/6.0;
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &idx[6], &idx[7], &tag);
      // average nodal data
      avg_value[0] = (vec_data[3*idx[0]+0] + vec_data[3*idx[1]+0] + vec_data[3*idx[2]+0] + 
                      vec_data[3*idx[3]+0] + vec_data[3*idx[4]+0] + vec_data[3*idx[5]+0] + 
                      vec_data[3*idx[6]+0] + vec_data[3*idx[7]+0])/8.0;
      avg_value[1] = (vec_data[3*idx[0]+1] + vec_data[3*idx[1]+1] + vec_data[3*idx[2]+1] + 
                      vec_data[3*idx[3]+1] + vec_data[3*idx[4]+1] + vec_data[3*idx[5]+1] + 
                      vec_data[3*idx[6]+1] + vec_data[3*idx[7]+1])/8.0;
      avg_value[2] = (vec_data[3*idx[0]+2] + vec_data[3*idx[1]+2] + vec_data[3*idx[2]+2] + 
                      vec_data[3*idx[3]+2] + vec_data[3*idx[4]+2] + vec_data[3*idx[5]+2] + 
                      vec_data[3*idx[6]+2] + vec_data[3*idx[7]+2])/8.0;
    }
    else { // unknwon element type
      // close files
      fclose(elm_fp);
      if (tag_fp)
        fclose(tag_fp);

      // free memory
      free(vec_data);
      free(avg_data);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }

    // read alternative element tag
    if (tag_fp)
      fscanf(tag_fp, "%d", &tag);

    // fill element data
    avg_data[3*i+0] = avg_data[3*i+1] = avg_data[3*i+2] = def_value;
    if ((!tag_data) || ((tag_data) && (bsearch(&tag, tag_data, num_tags, sizeof(int), cmpfunc_int)))) {
      avg_data[3*i+0] = avg_value[0];
      avg_data[3*i+1] = avg_value[1];
      avg_data[3*i+2] = avg_value[2];
    }

  }
  // close files
  fclose(elm_fp);
  if (tag_fp)
    fclose(tag_fp);

  // free memory      
  free(vec_data);
  
  // convert to numpy array
  npy_intp array_dim[2] = {num_elm, 3};
  PyArrayObject *avg_array = PyArray_SimpleNewFromData(2, array_dim, NPY_DOUBLE, avg_data);
  // return numpy array
  return PyArray_Return(avg_array);
}

static int get_affiliation_flag(const int tag, const int tag_lv, const int tag_rv, const int tag_la, const int tag_ra)
{
    if (tag == tag_lv) return 1;
    else if (tag == tag_rv) return 2;
    else if (tag == tag_la) return 4;
    else if (tag == tag_ra) return 8;
    else return 0;
}

PyDoc_STRVAR(doc_get_node_affiliation, 
    "get_node_affiliation(filename:string, num:int, tag_lv:int, tag_rv:int, tag_la:int, tag_ra:int) -> NumpyArray[int32,num]\n\n"    
    "Function to determine the ventricular affiliation of each node.\n"
    "Nodes belonging to the LV are assigned the value 1, nodes belonging\n"
    "to the RV are assigned the value 2, nodes belonging to the LA are\n"
    "assigned the value 4 and nodes belonging to the RA are assigned the\n"
    "value 8. If a node belongs to more than one tag, the values are combined\n"
    "by a 'bitwise or' operation. If nodse does not belong to any of the tag\n"    
    "regions, the value 0 is assigned.\n\n"    
    "Parameters:\n"
    "-----------\n"
    "  filename : string\n"
    "             path to the input elements file (*.elem)\n"    
    "  num      : int\n"
    "             number of nodes\n"
    "  tag_lv   : int\n"
    "             tag of LV region\n"
    "  tag_rv   : int\n"
    "             tag of RV region\n"
    "  tag_la   : int\n"
    "             tag of LA region\n"
    "  tag_ra   : int\n"
    "             tag of RA region\n"    
);

// average vector valued nodal data to element data
static PyObject *get_node_affiliation(PyObject *self, PyObject *args, PyObject *keywds) {
  
  // get filenames from the arguments  
  char *elm_fname = NULL;  
  int num_pts = -1;  
  int tag_lv = -1, tag_rv = -1, tag_la = -1, tag_ra = -1;
  if (!PyArg_ParseTuple(args, "siiiii", &elm_fname, &num_pts, &tag_lv, &tag_rv, &tag_la, &tag_ra))
    return NULL;

  // sanity check
  if (num_pts <= 0) {
    PyErr_Format(PyExc_ValueError, "Error, number of points %d, positive value expected!", num_pts);
    return NULL;
  }
    
  // open element file
  FILE *elm_fp = fopen(elm_fname, "r");
  if (!elm_fp) {
    PyErr_Format(PyExc_IOError, "Error, failed to open file '%s'!.", elm_fname);
    return NULL;
  }  

  // allocate affiliation data
  int *aff_data = (int*)calloc(num_pts, sizeof(int));    
  int aff_flag = 0;
  
  // read number of elements
  int num_elm;
  fscanf(elm_fp, "%d", &num_elm);  
  // element type, data and tag
  char type[4];
  int idx[8], tag;

  // read elements and tags and determine node affiliation
  for (int i=0; i<num_elm; i++) {    
    aff_flag = 0;

    // get element type
    fscanf(elm_fp, "%3s", type);
    // read element indices
    if (strcmp(type, "Tt") == 0) { // tetrahedron, 'Tt'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &tag);      
      // update affiliation
      aff_flag = get_affiliation_flag(tag, tag_lv, tag_rv, tag_la, tag_ra);      
      aff_data[idx[0]] |= aff_flag;
      aff_data[idx[1]] |= aff_flag;
      aff_data[idx[2]] |= aff_flag;
      aff_data[idx[3]] |= aff_flag;
    }
    else if (strcmp(type, "Py") == 0) { // pyramid, 'Py'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &tag);
      // update affiliation
      aff_flag = get_affiliation_flag(tag, tag_lv, tag_rv, tag_la, tag_ra);      
      aff_data[idx[0]] |= aff_flag;
      aff_data[idx[1]] |= aff_flag;
      aff_data[idx[2]] |= aff_flag;
      aff_data[idx[3]] |= aff_flag;      
      aff_data[idx[4]] |= aff_flag;
    }
    else if (strcmp(type, "Oc") == 0) { // octahedron, 'Oc'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      // update affiliation
      aff_flag = get_affiliation_flag(tag, tag_lv, tag_rv, tag_la, tag_ra);      
      aff_data[idx[0]] |= aff_flag;
      aff_data[idx[1]] |= aff_flag;
      aff_data[idx[2]] |= aff_flag;
      aff_data[idx[3]] |= aff_flag;      
      aff_data[idx[4]] |= aff_flag;
      aff_data[idx[5]] |= aff_flag;
    }
    else if (strcmp(type, "Pr") == 0) { // prism, 'Pr'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &tag);
      // update affiliation
      aff_flag = get_affiliation_flag(tag, tag_lv, tag_rv, tag_la, tag_ra);      
      aff_data[idx[0]] |= aff_flag;
      aff_data[idx[1]] |= aff_flag;
      aff_data[idx[2]] |= aff_flag;
      aff_data[idx[3]] |= aff_flag;      
      aff_data[idx[4]] |= aff_flag;
      aff_data[idx[5]] |= aff_flag;
    }
    else if (strcmp(type, "Hx") == 0) { // hexahedron, 'Hx'-element
      // read indices
      fscanf(elm_fp, "%d%d%d%d%d%d%d%d%d", &idx[0], &idx[1], &idx[2], &idx[3], &idx[4], &idx[5], &idx[6], &idx[7], &tag);
      // update affiliation
      aff_flag = get_affiliation_flag(tag, tag_lv, tag_rv, tag_la, tag_ra);      
      aff_data[idx[0]] |= aff_flag;
      aff_data[idx[1]] |= aff_flag;
      aff_data[idx[2]] |= aff_flag;
      aff_data[idx[3]] |= aff_flag;      
      aff_data[idx[4]] |= aff_flag;
      aff_data[idx[6]] |= aff_flag;
      aff_data[idx[7]] |= aff_flag;
    }
    else { // unknwon element type
      // close files
      fclose(elm_fp);

      // free memory
      free(aff_data);

      // raise exception if type is unknown
      PyErr_Format(PyExc_ValueError, "Error, line %d, unknwon element type '%s'!", i+1, type);
      return NULL;
    }
  }
  // close files
  fclose(elm_fp);
  
  // convert to numpy array
  npy_intp array_dim = num_pts;
  PyArrayObject *aff_array = PyArray_SimpleNewFromData(1, &array_dim, NPY_INT, aff_data);
  // return numpy array
  return PyArray_Return(aff_array);
}

// define methods
static PyMethodDef MshDataMethods[] = {    
    {"n2emap_scalar_data", (PyCFunction)n2emap_scalar_data, METH_VARARGS|METH_KEYWORDS, doc_n2emap_scalar_data},
    {"n2emap_vector_data", (PyCFunction)n2emap_vector_data, METH_VARARGS|METH_KEYWORDS, doc_n2emap_vector_data},
    {"get_node_affiliation", (PyCFunction)get_node_affiliation, METH_VARARGS|METH_KEYWORDS, doc_get_node_affiliation},
    {NULL, NULL, 0, NULL}
};

PyDoc_STRVAR(doc_mod_mshdata, "Module providing several functions to manipulate data defined on CARP-meshes.\n");

#if PY_MAJOR_VERSION >= 3

static struct PyModuleDef MshDataModule = {
    PyModuleDef_HEAD_INIT,
    "mshdata",       /* name of module */
    doc_mod_mshdata, /* module documentation, may be NULL */
    -1,               /* size of per-interpreter state of the module,
                         or -1 if the module keeps state in global variables. */
    MshDataMethods
};

PyMODINIT_FUNC PyInit_mshdata(void)
{
    // initialize module
    PyObject *mod = PyModule_Create(&MshDataModule);
    // import numpy module
    import_array();

    return mod;
}

#else

PyMODINIT_FUNC initmshdata(void)
{
    // initialize module
    PyObject *mod = Py_InitModule3("mshdata", MshDataMethods, doc_mod_mshdata);
    // import numpy module
    import_array();

    (void) mod;
}

#endif

