# carputils

`carputils` is a Python framework for generating and running CARP examples, and for
automatically comparing simulation results against known solutions.

Detailed documentation is available at:
https://carpentry.medunigraz.at/carputils/

To compile the documentation:

```sh
sudo pip install Sphinx
sudo pip install sphinxcontrib-napoleon
cd doc
make clean html
```

The documentation is generated under doc/build.


# Content License

The preferred license for carputils is the Apache Software License, Version 2.0 ("Apache 2.0"),
and the majority of the software is licensed with Apache 2.0. While the project seeks to adhere to the preferred license, 
there may be exceptions that will be handled on a case-by-case basis.