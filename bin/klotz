#!/usr/bin/env python

"""
Determine Klotz curve from (EDV,EDP) pair

"""
import argparse
import sys
from carputils import tools
from carputils.fitting import klotz as kl
from matplotlib import pyplot
import numpy as np

MMHG2KPA = 0.133322387415
KPA2MMHG = 1.0 / MMHG2KPA

EDVDEFAULT = 110.0
EDPDEFAULT = 10.0

# provide Python2 - Python3 compatibility
isPy2 = True
if sys.version_info.major > 2:
    isPy2 = False
    xrange = range

def parser():

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    #parser = tools.standard_parser()

    # mesh settings
    parser.add_argument('--EDP',
                         type=float, default=None,
                         help='Enddiastolic pressure (default is 10. mmHg / kPa or last pressure-trace entry of first pvtrace if provided)')

    parser.add_argument('--EDV',
                         type=float, default=None,
                         help='Enddiastolic volume (default is 110 mL or last volume-trace entry of first pvtrace if provided)')

    parser.add_argument('--plot',
                        action='store_true',
                        help='Plot Klotz curve (true if pvtraces are provided)')

    parser.add_argument('--pvtrace',
                        type=str, nargs='*', default=None, action='append',
                        dest='pvtraces',
                        help='add pressure and volume trace files')

    parser.add_argument('--kPa',
                         action='store_true',
                         help='Use kPa as units of presssure')

    parser.add_argument('--ofile',
                         type=str, default='',
                         help='Write sampled Klotz curve to ofile (default is console only)')

    parser.add_argument('--silent',
                         action='store_true',
                         help='Do not dump Klotz curve to console (true if pvtraces are provided)')
    return parser

def file_dump(vdat,pdat):

   # dump Klotz to file
   if opts.ofile != '':
       f = open(opts.ofile,'wt')

       for i in xrange(0,vdat.size-1):
           f.write('%7.3f %7.3f\n'%(vdat[i],pdat[i]))

       f.close()


if __name__ == '__main__':

   # build parser
   opts = parser().parse_args()

   pvtraces = None
   if opts.pvtraces is not None:
      pvtraces = []
      for i, pvtrace in enumerate(opts.pvtraces):
         if pvtrace[i].endswith('csv'):
             csv_file = np.loadtxt(pvtrace[i], dtype=str, skiprows=1, comments='#',
                                   delimiter=',')
             ttrace = csv_file[:,0].astype('float')
             l_index = np.where(ttrace > 0)[0][0]
             ptrace = csv_file[:,1].astype('float')[0:l_index]
             vtrace = csv_file[:,2].astype('float')[0:l_index]
         else:
             ptrace = np.loadtxt(pvtrace[0], dtype=float).squeeze().T[-1]
             vtrace = np.loadtxt(pvtrace[1], dtype=float).squeeze().T[-1]
         assert ptrace.shape == vtrace.shape
         pvtraces.append(np.stack((ptrace * (1.0 if opts.kPa else MMHG2KPA), vtrace)))
         opts.silent = True
         opts.plot = True
         if i == 0:
            EDV = vtrace[-1] if opts.EDV is None else opts.EDV
            EDP = (ptrace[-1] if opts.EDP is None else opts.EDP) * (1.0 if opts.kPa else MMHG2KPA)
   else:
       EDV = EDVDEFAULT if opts.EDV is None else opts.EDV
       EDP = (EDPDEFAULT if opts.EDP is None else opts.EDP) * (1.0 if opts.kPa else MMHG2KPA)

   print('')
   print('Determing Klotz parameters & curve from (EDV,EDP) pair')
   print('------------------------------------------------------\n\n')


   # Construct the relation
   P = kl.KlotzRelation(EDV, EDP)


   # Sample it
   max_vol = kl.V30(EDV,EDP)*1.1 if pvtraces is None else EDV
   vdat = np.linspace(0, max_vol, 100)
   pdat = P(vdat)

   print('For (EDV, EDP): ', (EDV, EDP))
   print('            V0: ', kl.V0(EDV, EDP))
   print('           V15: ', kl.V15(EDV, EDP))
   print('           V30: ', kl.V30(EDV, EDP))
   print

   if not opts.silent:
       print('%7s %7s'%('V', 'p'))
       for i in xrange(0,vdat.size-1):
           print('%7.3f %7.3f'%(vdat[i],pdat[i]))

   # dump Klotz to file
   if opts.ofile != '':
       file_dump(vdat,pdat)

   if opts.plot:

    # Prepare plot
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)

    # Plot it
    ax.plot(vdat, pdat)
    ax.plot([kl.V0(EDV,EDP), kl.V0(EDV,EDP)], [0, pdat.max()], 'k--')
    ax.axvline(kl.V0(EDV,EDP))
    if pvtraces is not None:
       for i, pvtrace in enumerate(pvtraces):
          if opts.pvtraces[0][0].endswith('csv'):
              print('plot PVR-{0}, size {2}\n\t{1[0]}\n\t{1[1]}'
                    .format(i, opts.pvtraces[0][0], pvtrace.shape[1]))
          else:
              print('plot PVR-{0}, size {2}\n\t{1[0]}\n\t{1[1]}'
                    .format(i, opts.pvtraces[i], pvtrace.shape[1]))
          print('\tEDP : {} kPa ({} mmHg)'.format(pvtrace[0][-1], pvtrace[0][-1]*KPA2MMHG))
          print('\tEDV : {} ml'.format(pvtrace[1][-1]))
          print('\tV_0 : {} ml'.format(pvtrace[1][0]))
          print
          ax.plot(pvtrace[1], pvtrace[0], label='PVR-{}'.format(i))


    # Mark som data points
    if pvtraces is None:
        ax.scatter([EDV], [EDP], 40, c='r', label='Measured')
        ax.scatter([kl.V0(EDV, EDP)],  [0.], 40, marker='x', c='b', label='V0')
        ax.scatter([kl.V15(EDV, EDP)], [15. * MMHG2KPA], 40,marker='x', c='g',
                   label='V15')
        ax.scatter([kl.V30(EDV, EDP)], [30. * MMHG2KPA], 40,marker='x', c='r',
                   label='V30')

    # Plot presentation
    ax.set_xlabel('Volume (ml)')
    ax.set_ylabel('Pressure (kPa)')
    pyplot.legend(loc='upper left')

    # Display plot
    pyplot.show()

