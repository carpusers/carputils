default: cextensions clean

cextensions:
	@echo "* building carputils C extensions"
	@(if [ -e carputils/cext ]; then rm -f carputils/cext/*.so; fi)
	@echo "... for PYTHON2"
	@(python2 setup.py build_ext --inplace)
	@echo "... for PYTHON3"
	@(python3 setup.py build_ext --inplace)

doxygen:
	doxygen doxydoc/doc.doxygen

clean:
	@echo "* cleaning up .."
	@(rm -rf build)

status st:
	git status

update up:
	git pull

